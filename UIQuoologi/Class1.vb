﻿Public Class UserInterface
    Public Shared ikon As System.Drawing.Icon
    Public Shared Function IkonAnimasi(ByVal index As Integer) As System.Drawing.Icon
        ikon = My.Resources.ResourceManager.GetObject("_" & index)
        Return ikon
    End Function
End Class

Public Class Gambar
    Public Shared Gambar As System.Drawing.Bitmap
    Public Shared Function AmbilGambar(ByVal nama As String) As System.Drawing.Bitmap
        Gambar = My.Resources.ResourceManager.GetObject("g" + nama.ToLower)
        Return Gambar
    End Function
End Class
