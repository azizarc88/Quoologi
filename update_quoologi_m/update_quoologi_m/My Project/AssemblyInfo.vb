﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Quoologi Updater")> 
<Assembly: AssemblyDescription("Quoologi Updater (base)")> 
<Assembly: AssemblyCompany("Izarc Software")> 
<Assembly: AssemblyProduct("Quoologi Updater")> 
<Assembly: AssemblyCopyright("Copyright © 2013 - 2014 Izarc Software")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("9b2b5eba-6f70-4983-a774-2537a7f3fee5")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.2.45")>
<Assembly: AssemblyFileVersion("1.0.2.45")>
