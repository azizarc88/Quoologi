﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updater_main
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(updater_main))
        Me.PB = New DevComponents.DotNetBar.Controls.ProgressBarX()
        Me.LProses = New DevComponents.DotNetBar.LabelX()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX2 = New DevComponents.DotNetBar.ButtonX()
        Me.LNamaFile = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.BInfo = New DevComponents.DotNetBar.ButtonX()
        Me.SuspendLayout()
        '
        'PB
        '
        Me.PB.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.PB.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.PB.ForeColor = System.Drawing.Color.Black
        Me.PB.Location = New System.Drawing.Point(4, 40)
        Me.PB.Name = "PB"
        Me.PB.Size = New System.Drawing.Size(508, 23)
        Me.PB.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.PB.TabIndex = 0
        '
        'LProses
        '
        Me.LProses.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LProses.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LProses.ForeColor = System.Drawing.Color.Black
        Me.LProses.Location = New System.Drawing.Point(7, 68)
        Me.LProses.Name = "LProses"
        Me.LProses.Size = New System.Drawing.Size(219, 23)
        Me.LProses.TabIndex = 1
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.AntiAlias = True
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground
        Me.ButtonX1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonX1.Location = New System.Drawing.Point(245, 68)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(75, 23)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 15.0!
        Me.ButtonX1.TabIndex = 2
        Me.ButtonX1.Text = "Tutup"
        '
        'ButtonX2
        '
        Me.ButtonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX2.AntiAlias = True
        Me.ButtonX2.Location = New System.Drawing.Point(438, 68)
        Me.ButtonX2.Name = "ButtonX2"
        Me.ButtonX2.Size = New System.Drawing.Size(75, 23)
        Me.ButtonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX2.Symbol = ""
        Me.ButtonX2.SymbolSize = 13.0!
        Me.ButtonX2.TabIndex = 3
        Me.ButtonX2.Text = "Update"
        '
        'LNamaFile
        '
        Me.LNamaFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LNamaFile.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LNamaFile.ForeColor = System.Drawing.Color.Black
        Me.LNamaFile.Location = New System.Drawing.Point(6, 12)
        Me.LNamaFile.Name = "LNamaFile"
        Me.LNamaFile.Size = New System.Drawing.Size(219, 23)
        Me.LNamaFile.TabIndex = 4
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(239, 12)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LabelX1.Size = New System.Drawing.Size(271, 23)
        Me.LabelX1.Symbol = ""
        Me.LabelX1.SymbolSize = 13.0!
        Me.LabelX1.TabIndex = 5
        '
        'BInfo
        '
        Me.BInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BInfo.AntiAlias = True
        Me.BInfo.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BInfo.Location = New System.Drawing.Point(325, 68)
        Me.BInfo.Name = "BInfo"
        Me.BInfo.Size = New System.Drawing.Size(108, 23)
        Me.BInfo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BInfo.Symbol = ""
        Me.BInfo.SymbolSize = 15.0!
        Me.BInfo.TabIndex = 6
        Me.BInfo.Text = "Apa Yang Baru ?"
        '
        'updater_main
        '
        Me.AcceptButton = Me.ButtonX2
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonX1
        Me.ClientSize = New System.Drawing.Size(516, 95)
        Me.Controls.Add(Me.BInfo)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.LNamaFile)
        Me.Controls.Add(Me.ButtonX2)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.LProses)
        Me.Controls.Add(Me.PB)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "updater_main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Quoologi Updater"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PB As DevComponents.DotNetBar.Controls.ProgressBarX
    Friend WithEvents LProses As DevComponents.DotNetBar.LabelX
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonX2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LNamaFile As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BInfo As DevComponents.DotNetBar.ButtonX

End Class
