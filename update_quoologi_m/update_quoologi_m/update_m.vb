﻿Imports DevComponents.DotNetBar
Imports System.Net
Imports System.IO
Imports Microsoft.Win32
Imports System.Environment

Public Class updater_main
    Inherits Metro.MetroForm
    Private WithEvents Downloader As WebFileDownloader
    Dim sem_upd_a As List(Of String) = New List(Of String)
    Dim link As List(Of String) = New List(Of String)
    Dim nama As List(Of String) = New List(Of String)
    Dim alamat As List(Of String) = New List(Of String)
    Dim alamatfix As List(Of String) = New List(Of String)
    Dim temppath As String = My.Computer.FileSystem.SpecialDirectories.Temp.ToString
    Dim alamatrepair As String = GetFolderPath(SpecialFolder.ApplicationData) + "\Izarc Software\Quoologi\Repair"
    Dim banyak As Byte
    Dim terunduh As List(Of String) = New List(Of String)
    Dim verbaru As String
    Public installdir As String
    Dim info As String

    Public Shared Sub Main()
        Application.DoEvents()
    End Sub

    Public Sub TentukanAlamat()
        Dim a As String
        For Each item As String In alamat
            If item = "AppDataQuoologi" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Quoologi"
                alamatfix.Add(a)
            ElseIf item = "ProgramFilesQuoologi" Then
                a = installdir
                alamatfix.Add(a)
            ElseIf item = "Desktop" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                alamatfix.Add(a)
            ElseIf item = "MyComputer" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer)
                alamatfix.Add(a)
            ElseIf item = "DocQuotes" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Quoologi\Quotes"
                alamatfix.Add(a)
            ElseIf item = "DocSounds" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Quoologi\Sounds"
                alamatfix.Add(a)
            ElseIf item = "Recent" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.Recent)
                alamatfix.Add(a)
            ElseIf item = "Startup" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.Startup)
                alamatfix.Add(a)
            ElseIf item = "System" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.System)
                alamatfix.Add(a)
            Else
                MessageBoxEx.Show("String alamat tidak dikenal: " + item, "Update Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Next
    End Sub

    Sub done()
        Dim a As Byte = 0
        For Each Data As String In terunduh
            If FileIO.FileSystem.FileExists(alamatfix(a) + "\" + nama.Item(a)) Then
                Try
                    FileIO.FileSystem.DeleteFile(alamatfix(a) + "\" + nama.Item(a), FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                Catch ex As Exception
                    MsgBox("Quoologi harus ditutup saat melakukan update, silakan tutup Quoologi terlebih dahulu, jika sudah, tekan OK", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Pembaruan Quoologi")
                    FileIO.FileSystem.DeleteFile(alamatfix(a) + "\" + nama.Item(a), FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                End Try
            End If
            If FileIO.FileSystem.FileExists(alamatrepair + nama.Item(a)) Then
                FileIO.FileSystem.DeleteFile(alamatrepair + nama.Item(a))
            End If
            FileIO.FileSystem.CopyFile(temppath + "\" + Data, alamatrepair + "\" + nama.Item(a), True)
            FileIO.FileSystem.MoveFile(temppath + "\" + Data, alamatfix(a) + "\" + nama.Item(a), True)
            a += 1
        Next
        a = 0
        For Each berkas As String In nama
            Using sw As New StreamWriter(alamatrepair + "\repair.ini", True)
                sw.WriteLine("<nama>" & berkas & "</nama><alamat>" & alamat(a) & "</alamat>")
            End Using
            a += 1
        Next

        Using SW As New IO.StreamWriter(installdir & "\info_update.txt", True)
            SW.WriteLine()
            SW.WriteLine("Quoologi v. " + verbaru.ToString)
            SW.WriteLine("#==================================================================#")
            SW.WriteLine(info)
            SW.Close()
        End Using

        MsgBox("Update selesai", MsgBoxStyle.Information, "Update Selesai")
        FileIO.FileSystem.DeleteFile(temppath & "\main_v.txt", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        FileIO.FileSystem.DeleteFile(temppath & "\info_update.txt", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        Process.Start(installdir + "\Quoologi.exe")
        Me.Close()
    End Sub

    Sub baca_file()
        Try
            Dim objReader As New System.IO.StreamReader(temppath & "\main_v.txt")
            Do While Not objReader.EndOfStream
                sem_upd_a.Add(objReader.ReadLine())
            Loop
            verbaru = sem_upd_a.Item(1).ToString
            banyak = sem_upd_a.Item(4).ToString

            If banyak = 0 Then
                MsgBox("Supaya tidak terjadi kesalahan, Silakan update program melalui pengaturan di Quoologi, kesalahan: (1001001)", MsgBoxStyle.Exclamation, "Pembaruan Quoologi")
                Me.Close()
            End If

            objReader.Close()

            Dim objReader2 As New System.IO.StreamReader(temppath & "\info_update.txt")
            Do While Not objReader2.EndOfStream
                info = objReader2.ReadToEnd
            Loop
            objReader2.Close()

            For Each baris As String In sem_upd_a
                If baris.Contains("link: #") Then
                    link.Add(baris.Replace("link: #", ""))
                ElseIf baris.Contains("nama: #") Then
                    nama.Add(baris.Replace("nama: #", ""))
                ElseIf baris.Contains("alamat: #") Then
                    alamat.Add(baris.Replace("alamat: #", ""))
                End If
            Next
            TentukanAlamat()
        Catch ex As IO.FileNotFoundException
            MsgBox("Supaya tidak terjadi kesalahan, Silakan update program melalui pengaturan di Quoologi, kesalahan: (1001002)", MsgBoxStyle.Exclamation, "Pembaruan Quoologi")
            Me.Close()
        End Try
    End Sub

    Private Sub updater_main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dispose()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        baca_file()

        installdir = Registry.GetValue("HKEY_CURRENT_USER\Software\Quoologi", "InstallDir", "gagal")
        Show()
        Try
            LabelX1.Text = "Versi Sekarang: " + System.Diagnostics.FileVersionInfo.GetVersionInfo(installdir + "\Quoologi.exe").ProductVersion.ToString + ", Versi Terbaru: " + verbaru.ToString
        Catch ex As Exception
            ToastNotification.Show(Me, "Quoologi lama tidak ditemukan")
        End Try
    End Sub

    Private Sub _Downloader_FileDownloadSizeObtained(ByVal iFileSize As Long) Handles Downloader.FileDownloadSizeObtained
        PB.Value = 0
        PB.Maximum = Convert.ToInt32(iFileSize)
    End Sub

    Private Sub _Downloader_FileDownloadComplete() Handles Downloader.FileDownloadComplete
        PB.Value = PB.Maximum
    End Sub

    Private Sub _Downloader_FileDownloadFailed(ByVal ex As System.Exception) Handles Downloader.FileDownloadFailed
        MessageBox.Show("Galat ketika sedang mendownload update, " & ex.Message, "Pembaruan Quoologi")
        Me.Close()
    End Sub

    Private Sub _Downloader_AmountDownloadedChanged(ByVal iNewProgress As Long) Handles Downloader.AmountDownloadedChanged
        PB.Value = Convert.ToInt32(iNewProgress)
        LProses.Text = WebFileDownloader.FormatFileSize(iNewProgress) & " dari " & WebFileDownloader.FormatFileSize(PB.Maximum) & " terunduh"
        Application.DoEvents()
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

    Private Function GetFileNameFromURL(ByVal URL As String) As String
        If URL.IndexOf("/"c) = -1 Then Return String.Empty

        Return "\" & URL.Substring(URL.LastIndexOf("/"c) + 1)
    End Function

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles ButtonX2.Click
        ButtonX2.Enabled = False
        Dim h As String

        For a As Byte = 0 To banyak - 1
            h = a + 1
            LNamaFile.Text = "Berkas: " + nama(a) + " (" + h + "/" + banyak.ToString + ")"
            Try
                Downloader = New WebFileDownloader
                Downloader.DownloadFileWithProgress(link.Item(a).ToString, temppath + GetFileNameFromURL(link.Item(a).ToString))
                terunduh.Add(link.Item(a).ToString.Substring(link.Item(a).ToString.LastIndexOf("/"c) + 1))
            Catch ex As Exception
                MsgBox("Update gagal!" + Chr(13) + "Silakan cek terlebih dahulu apakah Anda terhubung dengan internet dan Quoologi_updater sudah diijinkan di firewall atau tidak", MsgBoxStyle.Critical, "Galat")
                Exit Sub
                Exit For
            End Try
        Next
        done()
    End Sub

    Private Sub BInfo_Click(sender As Object, e As EventArgs) Handles BInfo.Click
        MsgBox("Quoologi v. " + verbaru.ToString + Chr(13) + Chr(13) + info)
    End Sub
End Class
