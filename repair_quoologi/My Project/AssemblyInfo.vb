﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Repair Quoologi")> 
<Assembly: AssemblyDescription("Aplikasi untuk memperbaiki Quoologi")> 
<Assembly: AssemblyCompany("Aziz Nur Ariffianto")> 
<Assembly: AssemblyProduct("repair_quoologi")> 
<Assembly: AssemblyCopyright("Copyright © 2015 Aziz Nur Ariffianto")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("32654995-c3e9-472c-aaf8-4e678a40f59d")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.0")> 
<Assembly: AssemblyFileVersion("2.0.0.0")> 
