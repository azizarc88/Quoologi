﻿Imports System.Environment
Imports Microsoft.Win32

Public Class Form1
    Dim installdir As String
    Dim alamatrepair As String = GetFolderPath(SpecialFolder.ApplicationData) + "\Izarc Software\Quoologi\Repair"
    Dim nama As List(Of String) = New List(Of String)
    Dim alamat As List(Of String) = New List(Of String)
    Dim alamatfix As List(Of String) = New List(Of String)

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dispose()
    End Sub

    Public Sub TentukanAlamat()
        Dim a As String
        For Each item As String In alamat
            If item = "AppDataQuoologi" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Quoologi"
                alamatfix.Add(a)
            ElseIf item = "ProgramFilesQuoologi" Then
                a = installdir
                alamatfix.Add(a)
            ElseIf item = "Desktop" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
                alamatfix.Add(a)
            ElseIf item = "MyComputer" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer)
                alamatfix.Add(a)
            ElseIf item = "DocQuotes" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Quoologi\Quotes"
                alamatfix.Add(a)
            ElseIf item = "DocSounds" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Quoologi\Sounds"
                alamatfix.Add(a)
            ElseIf item = "Recent" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.Recent)
                alamatfix.Add(a)
            ElseIf item = "Startup" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.Startup)
                alamatfix.Add(a)
            ElseIf item = "System" Then
                a = Environment.GetFolderPath(Environment.SpecialFolder.System)
                alamatfix.Add(a)
            Else
                MessageBox.Show("String alamat tidak dikenal: " + item, "Update Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Next
    End Sub

    Private Sub BacaPengaturan()
        Dim temp As String
        Try
            Using berkas As New IO.StreamReader(alamatrepair + "\repair.ini")
                While Not berkas.EndOfStream
                    temp = berkas.ReadLine
                    nama.Add(temp.Substring(6, temp.IndexOf("</nama>") - 6))
                    alamat.Add(temp.Substring(temp.IndexOf("<alamat>") + 8, temp.IndexOf("</alamat>") - temp.LastIndexOf("<alamat>") - 8))
                End While
            End Using
        Catch ex As Exception
            MessageBox.Show("Terjadi galat" + ex.Message, "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        TentukanAlamat()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        installdir = Registry.GetValue("HKEY_CURRENT_USER\Software\Quoologi", "InstallDir", "gagal")

        If installdir = Nothing Then
            MsgBox("Silakan install Quoologi terlebih dahulu", MsgBoxStyle.Critical, "Gagal")
            Me.Close()
            Exit Sub
        End If

        BacaPengaturan()
        Dim a As Integer = 0
        For Each berkas As String In nama
            Try
                If FileIO.FileSystem.FileExists(alamatfix(a) + "\" + berkas) = False Then
                    FileCopy(alamatrepair + "\" + berkas, alamatfix(a) + "\" + berkas)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
            a += 1
        Next
        Threading.Thread.Sleep(500)
        MessageBox.Show("Quoologi berhasil diperbaiki, jika galat masih terjadi, silakan instal ulang Quoologi, atau hubungi kami", "Repair Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
    End Sub
End Class
