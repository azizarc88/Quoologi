﻿Imports DevComponents.DotNetBar

Module Quoologi
    Public tinggi_c, lebar_c As Integer
    Public postinggi As Integer = 138
    Public lebar As Integer = 285
    Public lebarq As Integer = 266
    Public WaktuAutoHide As Integer = 0

    Public Sub TentukanBaris()
        Dim teks As String = Form_Quote.TBQuotes.Text
        Dim batas As Integer = Form_Quote.TBQuotes.GetLineFromCharIndex(teks.Length)
        Dim h As Integer = Form_Quote.TBQuotes.Font.Height
        Dim tinggi As Integer = 138
        Dim tinggiq As Integer = 96
        tinggi = 53
        tinggiq = 21
        postinggi = 62

        For a As Integer = 1 To batas
            tinggi += h
            tinggiq += h
            postinggi += h
        Next

        tinggi += OffTinggiJendela + (UkuranHuruf - 12) - batas
        tinggiq += OffTinggiQuote + (UkuranHuruf - 12) - batas
        postinggi -= batas

        Form_Quote.Height = tinggi
        Form_Quote.TBQuotes.Height = tinggiq
    End Sub

    Sub GantiSymbol()
        Dim animasi_base As String
        animasi_base = AnimasiTutup
        If animasi_base = "Geser Horizontal Kiri" Then
            Form_Quote.BHide.SymbolSize = "18"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Geser Horizontal Kanan" Then
            Form_Quote.BHide.SymbolSize = "18"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Geser Vertikal Atas" Then
            Form_Quote.BHide.SymbolSize = "15"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Geser Vertikal Bawah" Then
            Form_Quote.BHide.SymbolSize = "15"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Persempit Horizontal Kiri" Then
            Form_Quote.BHide.SymbolSize = "15"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Persempit Horizontal Kanan" Then
            Form_Quote.BHide.SymbolSize = "15"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Persempit Vertikal Atas" Then
            Form_Quote.BHide.SymbolSize = "15"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Persempit Vertikal Bawah" Then
            Form_Quote.BHide.SymbolSize = "15"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Tengah" Then
            Form_Quote.BHide.SymbolSize = "15"
            Form_Quote.BHide.Symbol = ""
        ElseIf animasi_base = "Menyatu" Then
            Form_Quote.BHide.SymbolSize = "14"
            Form_Quote.BHide.Symbol = ""
        End If
    End Sub

    Public Sub ShowQuoologi()
        Form_Quote.TopMost = True
        tinggi_c = postinggi + OffsetVer
        lebar_c = lebar + OffsetHor
        Form_Quote.Width = lebar + OffLebarJendela
        Form_Quote.TBQuotes.Width = lebarq + OffLebarQuote
        Form_Quote.y = Screen.PrimaryScreen.WorkingArea.Height - tinggi_c
        Form_Quote.x = Screen.PrimaryScreen.WorkingArea.Width - lebar_c
        Form_Quote.Location = New Point(Form_Quote.x, Form_Quote.y)
        Try
            Form_Quote.Opacity = Transparasi / 100
        Catch ex As System.ComponentModel.Win32Exception
            MessageBoxEx.Show(ex.ToString)
            Form_Quote.Opacity = 1
        End Try
        Form_Quote.TBQuotes.SelectionLength = 0
        AnimasiOpen(Form_Quote.Handle)
        If MainkanSuara = True Then
            Try
                My.Computer.Audio.Play(AlamatDokumen + "\Sounds\" + BerkasSuara)
            Catch ex As Exception
                MessageBoxEx.Show("Tidak dapat menemukan berkas suara yang dipilih, silakan memperbarui data. (kesalahan: 110203)", "Berkas Tidak Ditemukan", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Form_PengaturanUtama.Show()
                Form_PengaturanUtama.CBFileSuara.Focus()
                Exit Sub
            End Try
        End If
        Form_Handle.TSelama.Enabled = True
        Form_Handle.TSetiap.Enabled = False
        Form_PengaturanUtama.TPreview.Enabled = False
        QuoteMuncul = True
        Form_Quote.TAutoHide.Enabled = True
        WaktuAutoHide = 0
    End Sub

    Public Sub HideQuoologi(Optional ByVal astransparan As Boolean = False)
        Form_Quote.HilangTombol()

        Dim a As Integer = Transparasi
        If astransparan = True Then
            AnimateWindow(Form_Quote.Handle, 400, AnimateWindowFlags.MENYATU + AnimateWindowFlags.HILANG)
            Form_Handle.TPosisiMouse.Enabled = True
        Else
            AnimasiCLose(Form_Quote.Handle)
            Form_Handle.TSelama.Enabled = False
            If Form_Handle.MIHentikan.Checked = False Or Form_Handle.MIHentikan.Text.Contains(".exe") Then
                Form_Handle.TSetiap.Enabled = True
            ElseIf Form_Handle.MIHentikan.Checked = True Then
                Form_Handle.TSetiap.Enabled = False
            End If
            QuoteMuncul = False
            Form_Quote.TopMost = False
            Form_Quote.BInfo.Visible = False
        End If
    End Sub

    Public Sub TampilDariOpa()
        AnimateWindow(Form_Quote.Handle, 400, AnimateWindowFlags.MENYATU + AnimateWindowFlags.AKTIF)
        Form_Handle.TPosisiMouse.Enabled = False
    End Sub
End Module
