﻿Imports Microsoft.Win32
Imports DevComponents.DotNetBar

Module Pengaturan
    'System
    Public AlamatInstall As String = Registry.GetValue("HKEY_CURRENT_USER\Software\Quoologi", "InstallDir", "gagal")
    Public BerkasSuara, AlamatTampungan, UserName, TeksMulai, TerinstallPada As String
    Public AlamatAppData As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Quoologi"
    Public AlamatDokumen As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Quoologi"
    Public AlamatPengaturan As String = AlamatAppData + "\Quoologi.ini"
    Public TampilSelama, TampilSetiap, OffsetVer, OffsetHor As Integer
    Public SatuanSetiap As SatuanWaktu
    Public ConfigText As String
    Public Posisi As PosisiQuote
    Public ModeUpdate As Update
    Public QuoteTerpakai As List(Of String) = New List(Of String)
    Public ProgramTerkecuali As List(Of String) = New List(Of String)
    Public Pencegah As List(Of String) = New List(Of String)
    Public MainkanSuara, OtoSembunyi, StartupQuoologi, FirstRun, Kunci, JanganUlangi, OtomatisHentikan, OtoTutupJendela As Boolean
    Public AdaUpProgram, AdaUpQuote As Byte
    Public server As String = "https://api.aziz-nur.id/quoologi/php/"

    'Facebook
    Public KarakterTag, NamaAkunFB As String
    Public UbahSpasi, HapusSpasi, HapusKarNon, IzinkanKomentar, IzinkanEdit, KomenSebelumQuote, TambahPetik, OtoPetik As Boolean

    'Animasi
    Public AnimasiBuka, AnimasiTutup As String
    Public KecAnimasi As Integer

    'Gaya Quoologi
    Public Transparasi, UkuranHuruf, OffTinggiJendela, OffLebarJendela, OffTinggiQuote, OffLebarQuote As Integer
    Public GayaHuruf As String
    Public WarnaHurufJendela, WarnaBackJendela, WarnaHurufQuote, WarnaBackQuote As System.Drawing.Color
    Public SelaluTampilTombol As Boolean

    Public Data As List(Of String) = New List(Of String)

    <Flags()> _
    Public Enum BagianPenyimpanan
        Umum = 1
        QuoteTerpakai = 2
        ProgramTerkecuali = 3
        Pencegah = 4
        TeksWelcome = 5
    End Enum

    <Flags()> _
    Public Enum SatuanWaktu
        Detik = 0
        Menit = 1
        Jam = 2
    End Enum

    <Flags()> _
    Public Enum PosisiQuote
        AtasKiri = 0
        AtasTengah = 1
        AtasKanan = 2
        BawahKiri = 3
        BawahTengah = 4
        BawahKanan = 5
    End Enum

    <Flags()> _
    Public Enum Update
        BeritahuUpdate = 0
        UpdateManual = 1
    End Enum

    <Flags()> _
    Public Enum AnimateWindowFlags
        HORIZONTAL_KANAN = &H1
        HORIZONTAL_KIRI = &H2
        VERTIKAL_BAWAH = &H4
        VERTIKAL_ATAS = &H8
        TENGAH = &H10
        HILANG = &H10000
        AKTIF = &H20000
        GESER = &H40000
        MENYATU = &H80000
    End Enum

    Private Sub SetPengaturanAsali(Optional ByVal Bagian As BagianPenyimpanan = BagianPenyimpanan.Umum)
        If Bagian = BagianPenyimpanan.Umum Then
            TerinstallPada = Now.Date.ToShortDateString
            TampilSelama = 30
            TampilSetiap = 5
            SatuanSetiap = SatuanWaktu.Menit
            OffsetVer = 0
            OffsetHor = 0
            AdaUpProgram = 0
            AdaUpQuote = 0
            BerkasSuara = "water-drip2-med.wav"
            MainkanSuara = False
            OtoSembunyi = True
            AlamatTampungan = AlamatAppData
            StartupQuoologi = True
            FirstRun = True
            Kunci = True
            UserName = Nothing
            JanganUlangi = True
            ModeUpdate = Update.UpdateManual
            OtomatisHentikan = True
            KarakterTag = "#"
            UbahSpasi = False
            HapusSpasi = True
            HapusKarNon = True
            IzinkanKomentar = True
            IzinkanEdit = False
            KomenSebelumQuote = True
            TambahPetik = True
            OtoPetik = True
            NamaAkunFB = "{Belum Login}"
            AnimasiBuka = "Menyatu"
            AnimasiTutup = "Menyatu"
            KecAnimasi = 45
            Transparasi = 100
            GayaHuruf = "Bailey Sans ITC TT"
            UkuranHuruf = 12
            OffTinggiJendela = 0
            OffLebarJendela = 0
            OffTinggiQuote = 0
            OffLebarQuote = 0
            WarnaHurufJendela = Color.FromArgb(-11235884)
            WarnaBackJendela = Color.FromArgb(-15784898)
            WarnaHurufQuote = Color.FromArgb(-7490590)
            WarnaBackQuote = Color.FromArgb(-15255971)
            SelaluTampilTombol = False
            Posisi = PosisiQuote.BawahKanan
            OtoTutupJendela = True
        ElseIf Bagian = BagianPenyimpanan.QuoteTerpakai Then
            QuoteTerpakai.Add("Info Langka.qot")
            QuoteTerpakai.Add("Kata - Kata Mutiara.qot")
            QuoteTerpakai.Add("Romantis Sweet.qot")
            QuoteTerpakai.Add("Tips Tips.qot")
            QuoteTerpakai.Add("WOW Fakta.qot")
            QuoteTerpakai.Add("Yuk Pintar!.qot")
        ElseIf Bagian = BagianPenyimpanan.TeksWelcome Then
            TeksMulai = "Quoologi siap memberikan semua informasi, motivasi & hiburan sesuai keinginan Anda." + Environment.NewLine + Environment.NewLine + "Jadilah yang lebih baik. :)"
        End If
    End Sub

    Private Sub TulisDaftarKeBerkas(ByVal NamaBerkas As String, ByVal Jenis As String, ByVal Koleksi As List(Of String), ByVal Versi As String, ByVal SyaratVersi As String)
        Using Berkas As New IO.StreamWriter(AlamatAppData + "\" + NamaBerkas)
            Berkas.WriteLine("#Daftar " + NamaBerkas.Remove(NamaBerkas.IndexOf(".")))
            Berkas.WriteLine("#v " + Versi + " untuk Quoologi versi " + SyaratVersi + " keatas")
            Berkas.WriteLine("!Silakan edit secara manual di sini jika ingin menambah / mengubah / menghapus daftar " + Jenis)
            Berkas.WriteLine("")
            Berkas.WriteLine("[Mulai]")
            Berkas.WriteLine(" [" + Jenis + "]")
            Try
                For Each Data As String In Koleksi
                    Berkas.WriteLine("  " + Data)
                Next
            Catch ex As NullReferenceException
                'skip
            End Try
            Berkas.WriteLine("[Akhir]")
        End Using
    End Sub

    Private Function BacaDaftarDariBerkas(ByVal NamaBerkas As String) As List(Of String)
        Dim sem As String
        Dim daftar As List(Of String) = New List(Of String)
        Using Berkas As New IO.StreamReader(AlamatAppData + "\" + NamaBerkas)
            While Not Berkas.EndOfStream
                sem = Berkas.ReadLine
                If sem.Contains("  ") Then
                    daftar.Add(sem.Replace("  ", ""))
                End If
            End While
        End Using
        Return daftar
    End Function

    Public Sub SimpanPengaturan(Optional ByVal Bagian As BagianPenyimpanan = BagianPenyimpanan.Umum)
        If Not IO.Directory.Exists(AlamatPengaturan) Then
            System.IO.Directory.CreateDirectory(AlamatAppData)
        End If

        If Bagian = BagianPenyimpanan.Umum Then
            Using Berkas As New IO.StreamWriter(AlamatPengaturan)
                Berkas.WriteLine("#Pengaturan Quoologi")
                Berkas.WriteLine("#v 2.0 untuk Quoologi versi 2.5.8.97 keatas")
                Berkas.WriteLine("!Supaya menghindari adanya galat, disarankan untuk tidak sembarangan dalam mengedit berkas ini.")
                Berkas.WriteLine("TerinstallPada=" + TerinstallPada)
                Berkas.WriteLine("")
                Berkas.WriteLine("[Mulai]")
                Berkas.WriteLine(" [System]")
                Berkas.WriteLine("  AlamatInstall=" + AlamatInstall)
                Berkas.WriteLine("  TampilSelama=" + TampilSelama.ToString)
                Berkas.WriteLine("  TampilSetiap=" + TampilSetiap.ToString)
                Berkas.WriteLine("  SatuanSetiap=" & SatuanSetiap)
                Berkas.WriteLine("  OffsetVer=" + OffsetVer.ToString)
                Berkas.WriteLine("  OffsetHor=" + OffsetHor.ToString)
                Berkas.WriteLine("  BerkasSuara=" + BerkasSuara)
                Berkas.WriteLine("  MainkanSuara=" + MainkanSuara.ToString)
                Berkas.WriteLine("  OtoSembunyi=" + OtoSembunyi.ToString)
                Berkas.WriteLine("  AlamatTampungan=" + AlamatTampungan)
                Berkas.WriteLine("  StartupQuoologi=" + StartupQuoologi.ToString)
                Berkas.WriteLine("  FirstRun=" + FirstRun.ToString)
                Berkas.WriteLine("  Kunci=" + Kunci.ToString)
                Berkas.WriteLine("  UserName=" + UserName)
                Berkas.WriteLine("  JanganUlangi=" + JanganUlangi.ToString)
                Berkas.WriteLine("  ModeUpdate=" & ModeUpdate)
                Berkas.WriteLine("  OtomatisHentikan=" + OtomatisHentikan.ToString)
                Berkas.WriteLine("  Posisi=" & Posisi)
                Berkas.WriteLine("  AdaUpProgram=" & AdaUpProgram)
                Berkas.WriteLine("  AdaUpQuote=" & AdaUpQuote)
                Berkas.WriteLine("  OtoTutupJendela=" & OtoTutupJendela)
                Berkas.WriteLine("")
                Berkas.WriteLine(" [Facebook]")
                Berkas.WriteLine("  KarakterTag=" + KarakterTag)
                Berkas.WriteLine("  UbahSpasi=" + UbahSpasi.ToString)
                Berkas.WriteLine("  HapusSpasi=" + HapusSpasi.ToString)
                Berkas.WriteLine("  HapusKarNon=" + HapusKarNon.ToString)
                Berkas.WriteLine("  IzinkanKomentar=" + IzinkanKomentar.ToString)
                Berkas.WriteLine("  IzinkanEdit=" + IzinkanEdit.ToString)
                Berkas.WriteLine("  KomenSebelumQuote=" + KomenSebelumQuote.ToString)
                Berkas.WriteLine("  TambahPetik=" + TambahPetik.ToString)
                Berkas.WriteLine("  OtoPetik=" + OtoPetik.ToString)
                Berkas.WriteLine("  NamaAkunFB=" + NamaAkunFB)
                Berkas.WriteLine("")
                Berkas.WriteLine(" [Animasi]")
                Berkas.WriteLine("  AnimasiBuka=" + AnimasiBuka)
                Berkas.WriteLine("  AnimasiTutup=" + AnimasiTutup)
                Berkas.WriteLine("  KecAnimasi=" + KecAnimasi.ToString)
                Berkas.WriteLine("")
                Berkas.WriteLine(" [Gaya Quoologi]")
                Berkas.WriteLine("  Transparasi=" + Transparasi.ToString)
                Berkas.WriteLine("  GayaHuruf=" + GayaHuruf)
                Berkas.WriteLine("  UkuranHuruf=" + UkuranHuruf.ToString)
                Berkas.WriteLine("  OffTinggiJendela=" + OffTinggiJendela.ToString)
                Berkas.WriteLine("  OffLebarJendela=" + OffLebarJendela.ToString)
                Berkas.WriteLine("  OffTinggiQuote=" + OffTinggiQuote.ToString)
                Berkas.WriteLine("  OffLebarQuote=" + OffLebarQuote.ToString)
                Berkas.WriteLine("  WarnaHurufJendela=" + WarnaHurufJendela.ToArgb.ToString)
                Berkas.WriteLine("  WarnaBackJendela=" + WarnaBackJendela.ToArgb.ToString)
                Berkas.WriteLine("  WarnaHurufQuote=" + WarnaHurufQuote.ToArgb.ToString)
                Berkas.WriteLine("  WarnaBackQuote=" + WarnaBackQuote.ToArgb.ToString)
                Berkas.WriteLine("  SelaluTampilTombol=" + SelaluTampilTombol.ToString)
                Berkas.WriteLine("[Akhir]")
            End Using
        ElseIf Bagian = BagianPenyimpanan.ProgramTerkecuali Then
            TulisDaftarKeBerkas("Program Terkecuali.ini", "Program", ProgramTerkecuali, "1.0", "2.5.8.97")
            Application.DoEvents()
        ElseIf Bagian = BagianPenyimpanan.QuoteTerpakai Then
            TulisDaftarKeBerkas("Quote Terpakai.ini", "Quote", QuoteTerpakai, "1.0", "2.5.8.97")
            Application.DoEvents()
        ElseIf Bagian = BagianPenyimpanan.Pencegah Then
            TulisDaftarKeBerkas("Teks Pencegah.ini", "Teks", Pencegah, "1.0", "2.5.8.97")
            Application.DoEvents()
        ElseIf Bagian = BagianPenyimpanan.TeksWelcome Then
            Using Berkas As New IO.StreamWriter(AlamatAppData + "\Teks Mulai.txt")
                Berkas.Write(TeksMulai)
            End Using
            Application.DoEvents()
        End If
    End Sub

    Public Sub BacaPengaturan(Optional ByVal Bagian As BagianPenyimpanan = BagianPenyimpanan.Umum)
        Dim sem As String
        Data.Clear()
        If Bagian = BagianPenyimpanan.Umum Then
            If Not IO.File.Exists(AlamatPengaturan) Then
                SetPengaturanAsali()
                SimpanPengaturan(BagianPenyimpanan.Umum)
            End If

            Using Berkas As New IO.StreamReader(AlamatPengaturan)
                ConfigText = Berkas.ReadToEnd
            End Using

            Using Berkas As New IO.StreamReader(AlamatPengaturan)
                While Not Berkas.EndOfStream
                    sem = Berkas.ReadLine
                    If sem.Contains("=") And Not sem.Contains("TerinstallPada=") Then
                        Data.Add(sem.Substring(sem.LastIndexOf("=") + 1))
                    ElseIf sem.Contains("TerinstallPada=") Then
                        TerinstallPada = sem.Replace("TerinstallPada=", "")
                    End If
                End While
            End Using

            AlamatInstall = Data(0)
            TampilSelama = Data(1)
            TampilSetiap = Data(2)
            SatuanSetiap = Data(3)
            OffsetVer = Data(4)
            OffsetHor = Data(5)
            BerkasSuara = Data(6)
            MainkanSuara = Data(7)
            OtoSembunyi = Data(8)
            AlamatTampungan = Data(9)
            StartupQuoologi = Data(10)
            FirstRun = Data(11)
            Kunci = Data(12)
            UserName = Data(13)
            JanganUlangi = Data(14)
            ModeUpdate = Data(15)
            OtomatisHentikan = Data(16)
            Posisi = Data(17)
            AdaUpProgram = Data(18)
            AdaUpQuote = Data(19)
            OtoTutupJendela = Data(20)
            KarakterTag = Data(21)
            UbahSpasi = Data(22)
            HapusSpasi = Data(23)
            HapusKarNon = Data(24)
            IzinkanKomentar = Data(25)
            IzinkanEdit = Data(26)
            KomenSebelumQuote = Data(27)
            TambahPetik = Data(28)
            OtoPetik = Data(29)
            NamaAkunFB = Data(30)
            AnimasiBuka = Data(31)
            AnimasiTutup = Data(32)
            KecAnimasi = Data(33)
            Transparasi = Data(34)
            GayaHuruf = Data(35)
            UkuranHuruf = Data(36)
            OffTinggiJendela = Data(37)
            OffLebarJendela = Data(38)
            OffTinggiQuote = Data(39)
            OffLebarQuote = Data(40)
            WarnaHurufJendela = Color.FromArgb(Integer.Parse(Data(41)))
            WarnaBackJendela = Color.FromArgb(Integer.Parse(Data(42)))
            WarnaHurufQuote = Color.FromArgb(Integer.Parse(Data(43)))
            WarnaBackQuote = Color.FromArgb(Integer.Parse(Data(44)))
            SelaluTampilTombol = Data(45)
            Form_Handle.kartag = KarakterTag
            Form_Handle.hapuskarnon = HapusKarNon
            Form_Handle.hapusspasi = HapusSpasi
            Form_Handle.izinkanedit = IzinkanEdit
            Form_Handle.izinkankomentar = IzinkanKomentar
            Form_Handle.komensebelumquote = KomenSebelumQuote
            Form_Handle.tambahpetik = TambahPetik
            Form_Handle.ubahspasi = UbahSpasi
        ElseIf Bagian = BagianPenyimpanan.ProgramTerkecuali Then
            If Not IO.File.Exists(AlamatAppData + "\Program Terkecuali.ini") Then
                MessageBoxEx.Show("Berkas ""Program Terkecuali.ini"" tidak ditemukan (kesalahan: 010205)" + Environment.NewLine + "Namun, kami dapat memperbaikinya, harap tunggu sebentar.", "Berkas Hilang", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                PerbaikiQuoologi()
                CekPerbaikan()
                MessageBoxEx.Show("Tunggu Sebentar selagi kami memperbaiki Quoologi" + Environment.NewLine + Environment.NewLine + "Klik tombol OK di sini ketika perbaikan sudah selesai!", "Perbaiki", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
            ProgramTerkecuali = BacaDaftarDariBerkas("Program Terkecuali.ini")
        ElseIf Bagian = BagianPenyimpanan.QuoteTerpakai Then
            If Not IO.File.Exists(AlamatAppData + "\Quote Terpakai.ini") Then
                SetPengaturanAsali(BagianPenyimpanan.QuoteTerpakai)
                SimpanPengaturan(BagianPenyimpanan.QuoteTerpakai)
            End If
            QuoteTerpakai = BacaDaftarDariBerkas("Quote Terpakai.ini")
            If QuoteTerpakai.Count = 0 Then
                SetPengaturanAsali(BagianPenyimpanan.QuoteTerpakai)
                SimpanPengaturan(BagianPenyimpanan.QuoteTerpakai)
            End If
        ElseIf Bagian = BagianPenyimpanan.Pencegah Then
            If Not IO.File.Exists(AlamatAppData + "\Teks Pencegah.ini") Then
                MessageBoxEx.Show("Berkas ""Teks Pencegah.ini"" tidak ditemukan (kesalahan: 010206)" + Environment.NewLine + "Namun, kami dapat memperbaikinya, harap tunggu sebentar.", "Berkas Hilang", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                PerbaikiQuoologi()
                CekPerbaikan()
                MessageBoxEx.Show("Tunggu Sebentar selagi kami memperbaiki Quoologi" + Environment.NewLine + Environment.NewLine + "Klik tombol OK di sini ketika perbaikan sudah selesai!", "Perbaiki", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
            Pencegah = BacaDaftarDariBerkas("Teks Pencegah.ini")
        ElseIf Bagian = BagianPenyimpanan.TeksWelcome Then
            If Not IO.File.Exists(AlamatAppData + "\Teks Mulai.txt") Then
                SetPengaturanAsali(BagianPenyimpanan.TeksWelcome)
                SimpanPengaturan(BagianPenyimpanan.TeksWelcome)
            End If
            Using Berkas As New IO.StreamReader(AlamatAppData + "\Teks Mulai.txt")
                TeksMulai = Berkas.ReadToEnd
            End Using
        End If
    End Sub

    Private Sub CekPerbaikan()
        Dim ada As Boolean
2:
        ada = False
        For Each p As Process In Process.GetProcesses()
            If p.ProcessName = "repair_quoologi" Then
                ada = True
            End If
        Next

        If ada = False Then
            MessageBoxEx.Show("Sebaiknya Anda melakukan perbaikan dahulu", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            PerbaikiQuoologi()
            GoTo 2
        End If
    End Sub
End Module