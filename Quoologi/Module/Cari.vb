﻿Module Cari
    Public Function CariDataPT(ByVal teks As List(Of String)) As List(Of String)
        Dim data As List(Of String) = New List(Of String)
        Dim banyak As Integer = teks.Count
        Dim alamat As Integer = 0
        If banyak = 1 Then
            For Each baris As String In DaftarQuote
                baris = baris.ToLower
                If baris.Contains(teks.Item(0) + " ") Then
                    data.Add(alamat)
                End If
                alamat += 1
            Next
        ElseIf banyak = 2 Then
            For Each baris As String In DaftarQuote
                baris = baris.ToLower
                If baris.Contains(teks.Item(0) + " ") And baris.Contains(teks.Item(1) + " ") Then
                    data.Add(alamat)
                End If
                alamat += 1
            Next
        ElseIf banyak = 3 Then
            For Each baris As String In DaftarQuote
                baris = baris.ToLower
                If baris.Contains(teks.Item(0) + " ") And baris.Contains(teks.Item(1) + " ") And baris.Contains(teks.Item(2) + " ") Then
                    data.Add(alamat)
                End If
                alamat += 1
            Next
        ElseIf banyak = 4 Then
            For Each baris As String In DaftarQuote
                baris = baris.ToLower
                If baris.Contains(teks.Item(0) + " ") And baris.Contains(teks.Item(1) + " ") And baris.Contains(teks.Item(2) + " ") And baris.Contains(teks.Item(3) + " ") Then
                    data.Add(alamat)
                End If
                alamat += 1
            Next
        ElseIf banyak = 5 Then
            For Each baris As String In DaftarQuote
                baris = baris.ToLower
                If baris.Contains(teks.Item(0) + " ") And baris.Contains(teks.Item(1) + " ") And baris.Contains(teks.Item(2) + " ") And baris.Contains(teks.Item(3) + " ") And baris.Contains(teks.Item(4) + " ") Then
                    data.Add(alamat)
                End If
                alamat += 1
            Next
        End If

        If data.Count = 0 Then
            alamat = 0
            If banyak = 1 Then
                For Each baris As String In DaftarQuote
                    baris = baris.ToLower
                    If baris.Contains(teks.Item(0) + " ") Then
                        data.Add(alamat)
                    End If
                    alamat += 1
                Next
            ElseIf banyak = 2 Then
                For Each baris As String In DaftarQuote
                    baris = baris.ToLower
                    If baris.Contains(teks.Item(0) + " ") Or baris.Contains(teks.Item(1) + " ") Then
                        data.Add(alamat)
                    End If
                    alamat += 1
                Next
            ElseIf banyak = 3 Then
                For Each baris As String In DaftarQuote
                    baris = baris.ToLower
                    If baris.Contains(teks.Item(0) + " ") Or baris.Contains(teks.Item(1) + " ") Or baris.Contains(teks.Item(2) + " ") Then
                        data.Add(alamat)
                    End If
                    alamat += 1
                Next
            ElseIf banyak = 4 Then
                For Each baris As String In DaftarQuote
                    baris = baris.ToLower
                    If baris.Contains(teks.Item(0) + " ") Or baris.Contains(teks.Item(1) + " ") Or baris.Contains(teks.Item(2) + " ") Or baris.Contains(teks.Item(3) + " ") Then
                        data.Add(alamat)
                    End If
                    alamat += 1
                Next
            ElseIf banyak = 5 Then
                For Each baris As String In DaftarQuote
                    baris = baris.ToLower
                    If baris.Contains(teks.Item(0) + " ") Or baris.Contains(teks.Item(1) + " ") Or baris.Contains(teks.Item(2) + " ") Or baris.Contains(teks.Item(3) + " ") Or baris.Contains(teks.Item(4) + " ") Then
                        data.Add(alamat)
                    End If
                    alamat += 1
                Next
            End If
        End If
        Return data
    End Function
End Module
