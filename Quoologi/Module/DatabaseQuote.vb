﻿Imports DevComponents.DotNetBar

Module DatabaseQuote
    Public AlamatQuote As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Quoologi\Quotes"
    Public DaftarQuote As List(Of String) = New List(Of String)
    Public DaftarAlamatQuote As List(Of String) = New List(Of String)
    Public QuoteSudahTampil As List(Of String) = New List(Of String)

    Public Sub TulisSudahTampil(ByVal nilai As String)
        If My.Computer.FileSystem.FileExists(AlamatTampungan + "\quote.dft") = False Then
            Try
                Using Berkas As New IO.StreamWriter(AlamatTampungan + "\quote.dft", True)
                    Berkas.WriteLine("#Daftar alamat quote yang sudah tampil")
                    Berkas.WriteLine("#v 2.0 untuk Quoologi versi 2.5.8.97 keatas")
                    Berkas.WriteLine("")
                    Berkas.WriteLine("[Opsi]")
                    Dim bulan As String = Now.Month.ToString
                    Dim tanggal As String = Now.Day.ToString
                    If bulan.Length = 1 Then
                        bulan = "0" + Now.Month.ToString
                    End If
                    If tanggal.Length = 1 Then
                        tanggal = "0" + Now.Day.ToString
                    End If
                    Berkas.WriteLine("*Dibuat pada: " + Now.Year.ToString + " - " + bulan + " - " + tanggal)
                    Berkas.WriteLine("BerlakuSampai=" + GenerateTanggalAkhir(DaftarQuote.Count))
                    Berkas.WriteLine("SudahSemua=false")
                    Berkas.WriteLine("")
                    Berkas.WriteLine("[Daftar Quote Terpakai]")
                    For Each item As String In QuoteTerpakai
                        Berkas.WriteLine("  " + item)
                    Next
                    Berkas.WriteLine("")
                    Berkas.WriteLine("[Alamat]")
                End Using
            Catch ex As Exception
                MessageBoxEx.Show("Galat saat membuat berkas tampungan quote yang sudah tampil. Pesan eror: " + ex.Message + ". (kesalahan: 111002)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
        Try
            Using SW As New IO.StreamWriter(AlamatTampungan + "\quote.dft", True)
                SW.WriteLine(nilai)
                SW.Close()
            End Using
        Catch ex As Exception
            MessageBoxEx.Show("Galat saat menambahkan data ke berkas tampungan quote yang sudah tampil. Pesan eror: " + ex.Message + ". (kesalahan: 111003)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
    End Sub

    Public Sub MuatQuoteTampil()
        Dim bulan As String = Now.Month.ToString
        Dim tanggal As String = Now.Day.ToString
        If bulan.Length = 1 Then
            bulan = "0" + Now.Month.ToString
        End If
        If tanggal.Length = 1 Then
            tanggal = "0" + Now.Day.ToString
        End If
        Dim tanggalsekarang As String = Now.Year.ToString + bulan + tanggal
        Dim tanggalberakhir As String
        Dim sudahsemua As String
        Try
            Dim objReader As New System.IO.StreamReader(AlamatAppData + "\quote.dft")
            Dim temp As String
            Do While Not objReader.EndOfStream
                temp = objReader.ReadLine()
                If temp.Contains("*") Or temp.Contains("[") Or temp.Contains("#") Or temp.Contains("  ") Or temp = Nothing Then
                    'skip
                ElseIf temp.Contains("BerlakuSampai=") Then
                    tanggalberakhir = temp.Replace("BerlakuSampai=", "")
                    tanggalberakhir = tanggalberakhir.Replace(" - ", "")
                    If tanggalsekarang >= tanggalberakhir Then
                        objReader.Close()
                        My.Computer.FileSystem.DeleteFile(AlamatAppData + "\quote.dft", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                        Exit Sub
                    End If
                ElseIf temp.Contains("SudahSemua=") Then
                    sudahsemua = temp.Replace("SudahSemua=", "")
                    If sudahsemua = "true" Then
                        objReader.Close()
                        My.Computer.FileSystem.DeleteFile(AlamatAppData + "\quote.dft", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                    End If
                Else
                    QuoteSudahTampil.Add(temp)
                    Application.DoEvents()
                End If
            Loop
            objReader.Close()
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub



    Public Sub MuatDataQuotePengaturan()
        Dim temp, fix As String
        Dim f, l As Integer
        Dim semula As Integer = 0
        Dim ada As Boolean = True
        For Each Data As String In QuoteTerpakai
            If Data = "[kosongkan]" Then
                Exit Sub
            End If
            ada = True
            Try
                Using Berkas As New IO.StreamReader(AlamatDokumen + "\Quotes\" + Data)
                    Do While Not Berkas.EndOfStream
                        temp = Berkas.ReadLine
                        If temp.Contains("  Q[") Then
                            f = temp.IndexOf("]") + 1
                            l = temp.LastIndexOf("[") - f
                            fix = temp.Substring(f, l)
                            DaftarQuote.Add(fix)
                            DaftarAlamatQuote.Add(Data.Replace(".qot", ""))
                            semula += 1
                        ElseIf temp.Contains("%") Then
                            f = 0
                            l = temp.LastIndexOf(" %") - f
                            fix = temp.Substring(f, l)
                            DaftarQuote.Add(fix)
                            DaftarAlamatQuote.Add(Data.Replace(".qot", ""))
                            semula += 1
                        End If
                    Loop
                End Using
            Catch ex As IO.FileNotFoundException
                semula += 1
                MessageBoxEx.Show("Tidak dapat menemukan berkas Quote yang sedang digunakan, silakan memperbarui data. (kesalahan: 110201)" + Environment.NewLine + "Berkas bermasalah: " + Data, "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Form_PengaturanUtama.Show()
            End Try
            If semula = 0 Then
                MessageBoxEx.Show("Kami tidak dapat menemukan adanya data quote pada salah satu berkas quote terpakai, hal ini terjadi mungkin karena Anda menggunakan versi quote yang diperuntukan untuk versi Quoologi di atas " & Application.ProductVersion & Environment.NewLine & "Berkas bermasalah: " & Data, "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next
        If ada = False Then
            MessageBoxEx.Show("Quote terpilih kosong, silakan pilih minimal satu berkas Quote terlebih dahulu di menu pengaturan.", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Form_PengaturanUtama.Show()
        End If
    End Sub

    Public Sub MuatDataQuote()
        Dim temp, fix As String
        Dim f, l As Integer
        Dim semula As Integer = 0
        Dim ada As Boolean = True
        For Each Data As String In QuoteTerpakai
            If Data = "[kosongkan]" Then
                Exit Sub
            End If
            ada = True
            Try
                Using Berkas As New IO.StreamReader(AlamatDokumen + "\Quotes\" + Data)
                    Do While Not Berkas.EndOfStream
                        temp = Berkas.ReadLine
                        If temp.Contains("  Q[") Then
                            f = temp.IndexOf("]") + 1
                            l = temp.LastIndexOf("[") - f
                            fix = temp.Substring(f, l)
                            DaftarQuote.Add(fix)
                            DaftarAlamatQuote.Add(Data.Replace(".qot", ""))
                            semula += 1
                        ElseIf temp.Contains("%") Then
                            f = 0
                            l = temp.LastIndexOf(" %") - f
                            fix = temp.Substring(f, l)
                            DaftarQuote.Add(fix)
                            DaftarAlamatQuote.Add(Data.Replace(".qot", ""))
                            semula += 1
                        End If
                    Loop
                End Using
            Catch ex As IO.FileNotFoundException
                semula += 1
            End Try
            If semula = 0 Then
                MessageBoxEx.Show("Kami tidak dapat menemukan adanya data quote pada salah satu berkas quote terpakai, hal ini terjadi mungkin karena Anda menggunakan versi quote yang diperuntukan untuk versi Quoologi di atas " & Application.ProductVersion & Environment.NewLine & "Berkas bermasalah: " & Data, "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next
        If ada = False Then
            MessageBoxEx.Show("Quote terpilih kosong, silakan pilih minimal satu berkas Quote terlebih dahulu di menu pengaturan.", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Form_PengaturanUtama.Show()
        End If
    End Sub

    Public Function MuatDataQuote(ByVal koleksi As CheckedListBox.CheckedItemCollection) As List(Of List(Of String))
        Dim temp, fix As String
        Dim f, l As Integer
        Dim ada As Boolean = True
        Dim q As List(Of String) = New List(Of String)
        Dim a As List(Of String) = New List(Of String)
        Dim d As List(Of List(Of String)) = New List(Of List(Of String))
        For Each Data As String In koleksi
            ada = True
            Try
                Using Berkas As New IO.StreamReader(AlamatDokumen + "\Quotes\" + Data)
                    Do While Not Berkas.EndOfStream
                        temp = Berkas.ReadLine
                        If temp.Contains("  Q[") Then
                            f = temp.IndexOf("]") + 1
                            l = temp.LastIndexOf("[") - f
                            fix = temp.Substring(f, l)
                            q.Add(fix)
                            a.Add(Data.Replace(".qot", ""))
                        End If
                    Loop
                End Using
            Catch ex As IO.FileNotFoundException
                MessageBoxEx.Show("Tidak dapat menemukan berkas Quote yang sedang digunakan, silakan memperbarui data. (kesalahan: 110201)" + Environment.NewLine + "Berkas bermasalah: " + Data, "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Form_PengaturanUtama.Show()
            End Try
        Next
        d.Add(q)
        d.Add(a)
        Return d
    End Function
End Module
