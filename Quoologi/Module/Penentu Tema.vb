﻿Imports DevComponents.DotNetBar
Imports System.Runtime.InteropServices
Imports System.Text

Module Penentu_Tema
    Public DaftarProgramPT As List(Of String) = New List(Of String)
    Public DaftarKataPT As List(Of String) = New List(Of String)
    Public JudulKata As List(Of String) = New List(Of String)
    Public JudulProgram, judulsem As String
    Public judulberubah, SudahTampilSemua As Boolean

    Private hShellWindow As IntPtr = GetShellWindow()
    Private dictWindows As New Dictionary(Of IntPtr, String)
    Private currentProcessID As Integer

    <DllImport("USER32.DLL")> _
    Private Function GetShellWindow() As IntPtr
    End Function

    <DllImport("USER32.DLL")> _
    Private Function GetWindowText(ByVal hWnd As IntPtr, ByVal lpString As StringBuilder, ByVal nMaxCount As Integer) As Integer
    End Function

    <DllImport("USER32.DLL")> _
    Private Function GetWindowTextLength(ByVal hWnd As IntPtr) As Integer
    End Function

    <DllImport("user32.dll", SetLastError:=True)> _
    Private Function GetWindowThreadProcessId(ByVal hWnd As IntPtr, <Out()> ByRef lpdwProcessId As UInt32) As UInt32
    End Function

    <DllImport("USER32.DLL")> _
    Private Function IsWindowVisible(ByVal hWnd As IntPtr) As Boolean
    End Function

    Private Delegate Function EnumWindowsProc(ByVal hWnd As IntPtr, ByVal lParam As Integer) As Boolean

    <DllImport("USER32.DLL")> _
    Private Function EnumWindows(ByVal enumFunc As EnumWindowsProc, ByVal lParam As Integer) As Boolean
    End Function

    Public Sub MuatDataPT()
        Try
            Dim obj As New System.IO.StreamReader(AlamatInstall + "\penentu_tema.ini")
            Dim temp As String
            Do While Not obj.EndOfStream
                temp = obj.ReadLine()
                temp = temp.ToLower
                If temp = "" Or temp.Contains("//") Or temp.Contains("[") Then
                    'skip
                ElseIf temp.Contains("*") Then
                    DaftarProgramPT.Add(temp.Replace("*", ""))
                Else
                    DaftarKataPT.Add(temp)
                End If
            Loop
        Catch ex As Exception
            If MessageBoxEx.Show("Kami tidak dapat menemukan berkas ""penentu_tema.ini"". (kesalahan: 010203)" + Environment.NewLine + "Namun, kami dapat memperbaikinya, perbaiki ?", "Galat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                PerbaikiQuoologi()
            End If
        End Try
    End Sub

    Private Function enumWindowsInternal(ByVal hWnd As IntPtr, ByVal lParam As Integer) As Boolean
        If (hWnd <> hShellWindow) Then
            Dim windowPid As UInt32
            If Not IsWindowVisible(hWnd) Then
                Return True
            End If
            Dim length As Integer = GetWindowTextLength(hWnd)
            If (length = 0) Then
                Return True
            End If
            GetWindowThreadProcessId(hWnd, windowPid)
            If (windowPid <> currentProcessID) Then
                Return True
            End If
            Dim stringBuilder As New StringBuilder(length)
            GetWindowText(hWnd, stringBuilder, (length + 1))
            dictWindows.Add(hWnd, stringBuilder.ToString)
        End If
        Return True
    End Function

    Public Function GetOpenWindowsFromPID(ByVal processID As Integer) As IDictionary(Of IntPtr, String)
        dictWindows.Clear()
        currentProcessID = processID
        EnumWindows(AddressOf enumWindowsInternal, 0)
        Return dictWindows
    End Function

    Public Sub CekJudulSama(ByVal proses As String)
        Dim processes As Process() = Process.GetProcessesByName(proses)
        JudulProgram = ""
        Dim a As Boolean = False
        For Each process As Process In processes
            Dim windows As IDictionary(Of IntPtr, String) = GetOpenWindowsFromPID(process.Id)
            For Each kvp As KeyValuePair(Of IntPtr, String) In windows
                Try
                    JudulProgram = kvp.ToString.Remove(0, kvp.ToString.IndexOf(", ") + 2)
                    If JudulProgram.Contains("|") Then
                        JudulProgram = JudulProgram.Remove(JudulProgram.IndexOf("|"))
                        a = True
                    End If
                    If JudulProgram.Contains(":") Then
                        JudulProgram = JudulProgram.Remove(JudulProgram.IndexOf(":"))
                        a = True
                    End If
                    If JudulProgram.Contains("-") Then
                        JudulProgram = JudulProgram.Remove(JudulProgram.IndexOf("-"))
                        a = True
                    End If
                    If JudulProgram.Contains("""") Then
                        JudulProgram = JudulProgram.Replace("""", "")
                        a = True
                    End If
                    If a = True Then
                        JudulProgram = JudulProgram.Remove(JudulProgram.Length - 1, 1)
                    End If
                Catch ex As Exception
                End Try
            Next
        Next

        If judulsem <> "" Then
            If JudulProgram <> judulsem Then
                judulberubah = True
                SudahTampilSemua = False
            End If
        End If
    End Sub

    Public Sub AmbilJudul(ByVal proses As String)
        CekJudulSama(proses)

        judulsem = JudulProgram
        If JudulProgram = "" Then
            Exit Sub
        End If

        Form_Handle.TBKataJudul.Clear()

        For Each huruf As Char In JudulProgram
            If huruf <> " " Then
                Form_Handle.TBKataJudul.Text += huruf
            Else
                Form_Handle.TBKataJudul.Text += Environment.NewLine
            End If
        Next

        JudulKata.Clear()

        For Each baris As String In Form_Handle.TBKataJudul.Lines
            baris = baris.ToLower()
            If Not (DaftarKataPT.Contains(baris) = True Or baris.Contains("(") = True Or IsNumeric(baris) = True) Then
                Dim temp As String = baris
                If baris.Contains(".") Then
                    temp = baris.Remove(baris.LastIndexOf("."))
                End If
                JudulKata.Add(temp)
            End If
        Next
    End Sub

    Public Function CekAdaAppPT() As String
        Dim ada As String = "salah"
        For Each proses As Process In Process.GetProcesses
            If DaftarProgramPT.Contains(proses.ProcessName.ToString.ToLower) Then
                ada = proses.ProcessName.ToString.ToLower
                Form_Handle.NamaProgramPT = proses.MainModule.FileVersionInfo.FileDescription
                Exit For
            End If
        Next
        Return ada
    End Function
End Module
