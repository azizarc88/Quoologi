﻿Imports DevComponents.DotNetBar

Module PengaturanUI
    Public Sub setstatus()
        If adakoneksi = False Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru, Validator.eHighlightColor.Red)
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.Red)
        Else
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.None)
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru, Validator.eHighlightColor.None)
        End If

        If AdaUpProgram = 1 Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.Orange)
        ElseIf AdaUpProgram = 2 Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.None)
        End If

        If AdaUpQuote = 1 Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru, Validator.eHighlightColor.Orange)
        ElseIf AdaUpQuote = 2 Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru, Validator.eHighlightColor.None)
        End If
    End Sub
End Module
