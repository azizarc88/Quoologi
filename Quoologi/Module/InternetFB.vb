﻿Imports System.Net
Imports DevComponents.DotNetBar

Module InternetFB
    Public ready As Boolean
    Public adakoneksi As Boolean = True
    Public updating As update = New update

    Public Function CekKoneksi(Optional tampilkanpsan As Boolean = True) As Boolean
        Form_LoginFacebook.WebBrowser1.ScriptErrorsSuppressed = False
        Try
            Using client = New WebClient()
                Application.DoEvents()
                Form_Pesan.LabelX1.Text = "buka koneksi..."
                Using stream = client.OpenRead("https://graph.facebook.com/")
                    Application.DoEvents()
                    adakoneksi = True
                    Return True
                End Using
                Application.DoEvents()
            End Using
            Form_Pesan.LabelX1.Text = "sukses"
            Application.DoEvents()
            setstatus()
        Catch EX As Exception
            If EX.Message.Contains("(400)") Then
                adakoneksi = True
                setstatus()
                Return True
            Else
                If tampilkanpsan = True Then
                    If Form_Handle.psanmuncul = True Then
                        Form_Pesan.Close()
                    End If
                    MessageBoxEx.Show("Tidak dapat melakukan koneksi internet" + Environment.NewLine + "Silakan cek apakah Anda terhubung dengan internet dan Quoologi diizinkan dari firewall atau tidak. (kesalahan: 040401)" + vbNewLine + EX.Message.ToString, "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
                adakoneksi = False
                setstatus()
                Return False
            End If
        End Try
    End Function

    Public Sub pagewaiter(ByVal sender As Object, ByVal e As WebBrowserDocumentCompletedEventArgs)
        If Form_LoginFacebook.WebBrowser1.ReadyState = WebBrowserReadyState.Complete Then
            ready = True
            RemoveHandler Form_LoginFacebook.WebBrowser1.DocumentCompleted, New WebBrowserDocumentCompletedEventHandler(AddressOf pagewaiter)
        End If
    End Sub

    Public Sub waitforpageload()
        AddHandler Form_LoginFacebook.WebBrowser1.DocumentCompleted, New WebBrowserDocumentCompletedEventHandler(AddressOf pagewaiter)
        While Not ready
            Application.DoEvents()
        End While
        ready = False
    End Sub

    Private Function hapusnonhuruf(ByVal teks As String, Optional ByVal all As Boolean = False) As String
        Dim sem As String
        sem = teks
        If all = True Then
            For Each huruf As Char In sem
                If Asc(huruf) > 31 And Asc(huruf) < 65 Then
                    sem = sem.Replace(huruf, "")
                End If
            Next
            Return sem
            Exit Function
        End If
        For Each huruf As Char In sem
            If Asc(huruf) > 32 And Asc(huruf) < 65 Then
                sem = sem.Replace(huruf, "")
            End If
        Next
        Return sem
    End Function

    Public Sub UpdateStatus(status As String)
        Dim teks As HtmlElement = Form_LoginFacebook.WebBrowser1.Document.GetElementById("xhpc_message")
        teks.InnerText = status
        Dim post As HtmlElement = Form_LoginFacebook.WebBrowser1.Document.GetElementById("__CONFIRM__")
        post.InvokeMember("click")
    End Sub
End Module
