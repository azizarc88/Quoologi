﻿Imports DevComponents.DotNetBar

Module PembuatQuote
    Dim angka As Integer = 0

    Public Function Kapitalkan(ByVal Teks As String, judul As Boolean) As String
        Dim hasil As String = Nothing
        Dim awal As Boolean = True
        Dim c As Char
        Dim a As Char
        If judul = True Then
            For Each kar As Char In Teks
                Dim b As Integer = AscW(kar)
                Dim d As Integer = AscW(c)
                If Not kar = " " Then
                    If (b > 64 And b < 91) And (d > 96 And d < 123) Then
                        awal = True
                    End If

                    If awal = True Then
                        hasil += kar.ToString.ToUpper
                        awal = False
                    ElseIf awal = False Then
                        hasil += kar.ToString.ToLower
                    End If
                Else
                    hasil += kar
                    awal = True
                End If
                c = kar
            Next
        Else
            Try
                Dim temp As String
                a = Teks.Chars(0)
                temp = Teks.Remove(0, 1)
                hasil = a + temp
            Catch ex As Exception
                'skip
            End Try
        End If
        Return hasil
    End Function

    Public Function PenentuAlasan(ByVal Teks As String, ByVal Min As Integer, ByVal Maks As Integer) As String
        Dim a As String = Nothing
        Dim ada As Boolean

        If Teks.Length >= Min And Teks.Length <= Maks Then
            For Each baris As String In Pencegah
                If Teks.Contains(baris) Then
                    ada = True
                    a += """" + baris + """ "
                End If
            Next
        End If

        If Teks.Length < Min And Teks.Length > 0 And ada = False Then
            Return "{Panjang < " + Min.ToString + "}"
            Exit Function
        ElseIf Teks.Length = 0 Then
            Return "{Baris kosong}"
            Exit Function
        ElseIf Teks.Length > Maks And ada = False Then
            Return "{Panjang > " + Min.ToString + "}"
            Exit Function
        ElseIf Teks.Length >= Min And Teks.Length <= Maks And ada = False Then
            Return "{Terdapat data yang sama}"
            Exit Function
        End If
        Return a
    End Function

    Public Sub TambahTerbuang(ByVal panjang As Integer, ByVal alamat As Integer, ByVal data As String)
        Dim banyak As Integer = 0
        Form_PembuatQuote.panjangterbuang.Add(panjang)
        Form_PembuatQuote.alamatterbuang.Add(alamat)
        Form_PembuatQuote.terbuang.Add(data)
        For Each baris As String In Pencegah
            If data.Contains(baris) Then
                banyak += 1
            End If
        Next
        If data.Length >= 70 And banyak = 1 Then
            Form_PembuatQuote.ragu += 1
        End If
    End Sub

    Public Sub TambahAda(ByVal panjang As Integer, ByVal alamat As Integer)
        Form_PembuatQuote.panjangada.Add(panjang)
        Form_PembuatQuote.alamatada.Add(alamat)
    End Sub

    Public Sub SimpanKeBerkas(ByVal NamaBerkas As String, ByVal Koleksi As ListBox.ObjectCollection, Optional ByVal Tambahkan As Boolean = False)
        Dim a As Integer = 0
        Dim r As Random = New Random()
        If Tambahkan = False Then
            Using Berkas As New IO.StreamWriter(AlamatQuote + "\" + NamaBerkas + ".qot")
                Berkas.WriteLine("#Daftar Quote Quoologi dengan tema " + NamaBerkas)
                Berkas.WriteLine("#v 2.0 untuk Quoologi versi 2.5.8.97 keatas")
                Berkas.WriteLine("!Silakan edit secara manual di sini jika ingin menambah / mengubah / menghapus daftar Quote")
                Berkas.WriteLine("")
                Berkas.WriteLine("[Mulai]")
                Berkas.WriteLine(" [Quote]")
                Try
                    For Each Data As String In Koleksi
                        Berkas.WriteLine("  Q[" & a & "]" & Data & "[" & r.Next(100, 999) & "]")
                        a += 1
                    Next
                Catch ex As NullReferenceException
                    'skip
                End Try
                Berkas.WriteLine("[Akhir]")
            End Using
        Else
            Dim temp As String = Nothing
            Dim fix As String = Nothing
            Using Berkas As New IO.StreamReader(AlamatQuote + "\" + NamaBerkas + ".qot")
                While Not Berkas.EndOfStream
                    temp = Berkas.ReadLine
                    If temp <> "[Akhir]" Then
                        fix += temp + Environment.NewLine
                    End If
                End While
            End Using
            Using Berkas As New IO.StreamWriter(AlamatQuote + "\" + NamaBerkas + ".qot")
                Berkas.Write(fix)
                Berkas.WriteLine(" [Ditambahkan Pada: " & Now.Day & "-" & Now.Month & "-" & Now.Year & "]")
                Try
                    For Each Data As String In Koleksi
                        Berkas.WriteLine("  Q[" & a & "]" & Data & "[" & r.Next(100, 999) & "]")
                        a += 1
                    Next
                    Berkas.WriteLine("[Akhir]")
                Catch ex As NullReferenceException
                    'skip
                End Try
            End Using
        End If
    End Sub

    Public Sub Kerjakan()
        Dim sem As List(Of String) = New List(Of String)
        Dim except As Boolean
        angka = 0
        Form_PembuatQuote.data = New List(Of String)
        Form_PembuatQuote.terbuang = New List(Of String)
        Form_PembuatQuote.alamatterbuang = New List(Of String)
        Form_PembuatQuote.panjangterbuang = New List(Of String)
        Form_PembuatQuote.alamatada = New List(Of String)
        Form_PembuatQuote.panjangada = New List(Of String)
        Form_PembuatQuote.ragu = 0
        ToastNotification.Show(Form_PembuatQuote, "Memproses....", 100000, eToastPosition.MiddleCenter)
        Application.DoEvents()
        If sem.Count = 0 Then
            For Each baris_data As String In Form_PembuatQuote.TBData.Lines
                If Form_PembuatQuote.TBSampah.Lines.Length > 0 Then
                    For Each baris_sampah As String In Form_PembuatQuote.TBSampah.Lines
                        If baris_sampah.Length > 0 Then
                            If baris_data.Contains(baris_sampah) Then
                                sem.Add(baris_data.Replace(baris_sampah, ""))
                                GoTo here
                            End If
                        End If
                        Application.DoEvents()
                    Next
                    sem.Add(baris_data)
here:
                ElseIf Form_PembuatQuote.TBSampah.Lines.Length = 0 Then
                    sem.Add(baris_data)
                End If
                Application.DoEvents()
            Next
        End If
        Pencegah.Clear()
        For Each baris As String In Form_PembuatQuote.TBPencegah.Lines
            Pencegah.Add(baris)
        Next

        For Each baris As String In sem
            except = False
            For Each i As String In Pencegah
                If baris.IndexOf(i) <> -1 Then
                    except = True
                    Exit For
                End If
                Application.DoEvents()
            Next

            If except = False Then
                If baris.Length < Form_PembuatQuote.TBMin.Text Or baris.Length > Form_PembuatQuote.TBMaks.Text Or Form_PembuatQuote.data.Contains(baris) Then
                    TambahTerbuang(baris.Length, angka, baris)
                Else
                    Form_PembuatQuote.data.Add(Kapitalkan(baris, False))
                    TambahAda(baris.Length, angka)
                End If
            ElseIf except = True Then
                TambahTerbuang(baris.Length, angka, baris)
            End If
            angka += 1
            Application.DoEvents()
        Next

        Form_PembuatQuote.LBData.DataSource = Form_PembuatQuote.data
        Form_PembuatQuote.BSimpan.Enabled = True
        Form_PembuatQuote.BBersihkan.Enabled = True
        ToastNotification.Close(Form_PembuatQuote)
        If Form_PembuatQuote.LBData.Items.Count >= 1 Then
            Form_PembuatQuote.BBukan.Enabled = True
        Else
            Form_PembuatQuote.BBukan.Enabled = False
        End If
    End Sub
End Module
