﻿Imports DevComponents.DotNetBar

Module Utama
    Public statusqot As Byte
    Public welcomenote, QuoteMuncul As Boolean
    Public listcari As List(Of String) = New List(Of String)
    Public defoffsethor, asaltinggiquotes, asallebarquotes, asaltinggijendela, asallebarjendela As Integer
    Public sem_upd_a As List(Of String) = New List(Of String)
    Public sem_upd_q As List(Of String) = New List(Of String)

    Public Sub PerbaikiQuoologi()
        Try
            Process.Start(AlamatInstall + "\repair_quoologi.exe")
        Catch ex As IO.FileNotFoundException
            MessageBoxEx.Show("Berkas ""repair_quoologi.exe"" tidak ditemukan, Untuk memperbaiki program, silakan install ulang Quoologi. (kesalahan: 010201)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Catch ex As Exception
            Form_Handle.perbaikidicancel = True
        End Try
        Form_Handle.perbaikidicancel = False
    End Sub

    Public Function MaksBulan(ByVal bulan As Integer) As Integer
        Dim maks As Integer
        Select Case bulan
            Case 1
                maks = 31
            Case 2
                maks = 28
            Case 3
                maks = 31
            Case 4
                maks = 30
            Case 5
                maks = 31
            Case 6
                maks = 30
            Case 7
                maks = 31
            Case 8
                maks = 31
            Case 9
                maks = 30
            Case 10
                maks = 31
            Case 11
                maks = 30
            Case 12
                maks = 31
        End Select
        Return maks
    End Function

    Public Function GenerateTanggalAkhir(ByVal Banyak As Integer) As String
        Dim tanggal As Integer = Now.Day
        Dim bulan As Integer = Now.Month
        Dim tahun As Integer = Now.Year
        Dim perubah As Integer
        Dim d As Integer = Banyak

        If d > 200 Then
            perubah = (d - (d Mod 200)) / 200
        Else
            perubah = 1
        End If

        tanggal = tanggal + perubah

        While tanggal > MaksBulan(bulan)
            tanggal = tanggal - MaksBulan(bulan)
            bulan += 1
            If bulan > 12 Then
                bulan = 1
                tahun += 1
            End If
        End While
        Dim bulanteks As String = bulan
        Dim tanggalteks As String = tanggal
        If bulan.ToString.Length = 1 Then
            bulanteks = "0" + bulan.ToString
        End If
        If tanggal.ToString.Length = 1 Then
            tanggalteks = "0" + tanggal.ToString
        End If
        Return tahun.ToString + " - " + bulanteks + " - " + tanggalteks
    End Function

    Private Function GetFooterImage(status As Boolean) As Image
        If status = True Then
            Return My.Resources.nnx
        Else
            Return Nothing
        End If
    End Function

    Private Function GetPerintah(status As Boolean, teksyes1 As String, teksyes2 As String, teksno1 As String, teksno2 As String) As Command()
        If status = True Then
            Form_Handle.CBYes.Text = "<font size=""+2"">" + teksyes1 + "</font><br/>" + teksyes2
            Form_Handle.CBNo.Text = "<font size=""+2"">" + teksno1 + "</font><br/>" + teksno2
            Return New Command() {Form_Handle.CBYes, Form_Handle.CBNo}
        Else
            Return Nothing
        End If
    End Function

    <Flags()> _
    Public Enum Orientasi
        Vertikal = 0
        Horizontal = 1
    End Enum

    <Flags()> _
    Public Enum Pengubah
        Dikurangi = 0
        Ditambah = 1
    End Enum

    Public Sub GeserKontrol(ByVal control As System.Windows.Forms.Control, ByVal NilaiAkhir As Integer, ByVal Pengubah As Pengubah, ByVal Perubah As Orientasi, Optional ByVal NilaiPerubah As Integer = 1, Optional ByVal Jeda As Integer = 2)
        Dim x, y, delay, batasdelay, NilaiAwal As Integer

        Try
            If Perubah = Orientasi.Horizontal Then
                NilaiAwal = control.Location.X
                x = NilaiAwal
                y = control.Location.Y
                If Pengubah = Utama.Pengubah.Ditambah Then
                    batasdelay = ((80 / 100) * (NilaiAkhir - NilaiAwal)) + NilaiAwal
                    Do Until x = NilaiAkhir
                        x = x + NilaiPerubah
                        control.Location = New Point(x, y)
                        Application.DoEvents()
                        If y >= batasdelay Then
                            delay = 10
                        Else
                            delay = Jeda
                        End If
                        System.Threading.Thread.Sleep(delay)
                    Loop
                ElseIf Pengubah = Utama.Pengubah.Dikurangi Then
                    batasdelay = ((20 / 100) * (NilaiAwal - NilaiAkhir)) + NilaiAkhir
                    Do Until x = NilaiAkhir
                        x = x - NilaiPerubah
                        control.Location = New Point(x, y)
                        Application.DoEvents()
                        If y <= batasdelay Then
                            delay = 10
                        Else
                            delay = Jeda
                        End If
                        System.Threading.Thread.Sleep(delay)
                    Loop
                End If
            ElseIf Perubah = Orientasi.Vertikal Then
                NilaiAwal = control.Location.Y
                y = NilaiAwal
                x = control.Location.X
                If Pengubah = Utama.Pengubah.Ditambah Then
                    batasdelay = ((80 / 100) * (NilaiAkhir - NilaiAwal)) + NilaiAwal
                    Do Until y = NilaiAkhir
                        y = y + NilaiPerubah
                        control.Location = New Point(x, y)
                        Application.DoEvents()
                        If y >= batasdelay Then
                            delay = 10
                        Else
                            delay = Jeda
                        End If
                        System.Threading.Thread.Sleep(delay)
                    Loop
                ElseIf Pengubah = Utama.Pengubah.Dikurangi Then
                    batasdelay = ((20 / 100) * (NilaiAwal - NilaiAkhir)) + NilaiAkhir
                    Do Until y = NilaiAkhir
                        y = y - NilaiPerubah
                        control.Location = New Point(x, y)
                        Application.DoEvents()
                        If y <= batasdelay Then
                            delay = 10
                        Else
                            delay = Jeda
                        End If
                        System.Threading.Thread.Sleep(delay)
                    Loop
                End If
            End If
        Catch ex As Exception
            Debug.WriteLine("SKIP")
        End Try
    End Sub

    Public Function NilaiString(ByVal teks As String) As Integer
        Dim nilai As Integer = 0
        Dim pengkondisi As Integer = 0
        teks = teks.ToLower
        For Each huruf As Char In teks
            pengkondisi += 1
            Select Case huruf
                Case "a"
                    nilai += 1
                Case "b"
                    nilai += 2
                Case "c"
                    nilai += 3
                Case "d"
                    nilai += 4
                Case "e"
                    nilai += 5
                Case "f"
                    nilai += 6
                Case "g"
                    nilai += 7
                Case "h"
                    nilai += 8
                Case "i"
                    nilai += 9
                Case "j"
                    nilai += 10
                Case "k"
                    nilai += 11
                Case "l"
                    nilai += 12
                Case "m"
                    nilai += 13
                Case "n"
                    nilai += 14
                Case "o"
                    nilai += 15
                Case "p"
                    nilai += 16
                Case "q"
                    nilai += 17
                Case "r"
                    nilai += 18
                Case "s"
                    nilai += 19
                Case "t"
                    nilai += 20
                Case "u"
                    nilai += 21
                Case "v"
                    nilai += 22
                Case "w"
                    nilai += 23
                Case "x"
                    nilai += 24
                Case "y"
                    nilai += 25
                Case "z"
                    nilai += 26
                Case " "
                    nilai += 27
                Case "."
                    nilai += 28
                Case ","
                    nilai += 29
                Case "%"
                    nilai += 30
                Case ":"
                    nilai += 31
                Case "/"
                    nilai += 32
                Case "#"
                    nilai += 33
                Case """"
                    nilai += 34
                Case "&"
                    nilai += 35
                Case "-"
                    nilai += 36
            End Select
        Next
        Return nilai
    End Function

    Public Function psan(judul As String, icon As eTaskDialogIcon, header As String, isi As String, tombol As eTaskDialogButton, Optional warna_dialog As eTaskDialogBackgroundColor = eTaskDialogBackgroundColor.Blue, Optional statusperintah As Boolean = False, Optional teksyes1 As String = Nothing, Optional teksyes2 As String = Nothing, Optional teksno1 As String = Nothing, Optional teksno2 As String = Nothing, Optional footer As String = Nothing, Optional footerimage As Boolean = False) As eTaskDialogResult
        Dim info As New TaskDialogInfo(judul, icon, header, isi, tombol, warna_dialog, Nothing, GetPerintah(statusperintah, teksyes1, teksyes2, teksno1, teksno2), Nothing, footer, GetFooterImage(footerimage))
        Dim result As eTaskDialogResult = TaskDialog.Show(info)
        TaskDialog.EnableGlass = True
        Return result
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Function AnimateWindow(ByVal hwnd As IntPtr, ByVal time As Integer, ByVal flags As AnimateWindowFlags) As Boolean
    End Function

    Function AnimasiOpen(ByVal kontrol As IntPtr)
        Dim animasi_base As String
        Dim waktu As String
        animasi_base = AnimasiBuka
        Dim anim As AnimateWindowFlags = Nothing
        If animasi_base = "Geser Horizontal Kiri" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KIRI
        ElseIf animasi_base = "Geser Horizontal Kanan" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KANAN
        ElseIf animasi_base = "Geser Vertikal Atas" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_ATAS
        ElseIf animasi_base = "Geser Vertikal Bawah" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_BAWAH
        ElseIf animasi_base = "Perluas Horizontal Kiri" Then
            anim = AnimateWindowFlags.HORIZONTAL_KIRI
        ElseIf animasi_base = "Perluas Horizontal Kanan" Then
            anim = AnimateWindowFlags.HORIZONTAL_KANAN
        ElseIf animasi_base = "Perluas Vertikal Atas" Then
            anim = AnimateWindowFlags.VERTIKAL_ATAS
        ElseIf animasi_base = "Perluas Vertikal Bawah" Then
            anim = AnimateWindowFlags.VERTIKAL_BAWAH
        ElseIf animasi_base = "Tengah" Then
            anim = AnimateWindowFlags.TENGAH
        ElseIf animasi_base = "Menyatu" Then
            anim = AnimateWindowFlags.MENYATU
            AnimasiTutup = "Menyatu"
            Transparasi = 100
        Else
            anim = AnimateWindowFlags.MENYATU
            AnimasiTutup = "Menyatu"
            Transparasi = 100
        End If
        waktu = KecAnimasi.ToString + "0"
        Return AnimateWindow(kontrol, waktu, anim)
    End Function

    Function AnimasiCLose(ByVal kontrol As IntPtr)
        Dim animasi_base As String
        Dim waktu As String
        animasi_base = AnimasiTutup
        Dim anim As AnimateWindowFlags = Nothing
        If animasi_base = "Geser Horizontal Kiri" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KIRI Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Geser Horizontal Kanan" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KANAN Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Geser Vertikal Atas" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_ATAS Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Geser Vertikal Bawah" Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_BAWAH Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Persempit Horizontal Kiri" Then
            anim = AnimateWindowFlags.HORIZONTAL_KIRI Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Persempit Horizontal Kanan" Then
            anim = AnimateWindowFlags.HORIZONTAL_KANAN Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Persempit Vertikal Atas" Then
            anim = AnimateWindowFlags.VERTIKAL_ATAS Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Persempit Vertikal Bawah" Then
            anim = AnimateWindowFlags.VERTIKAL_BAWAH Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Tengah" Then
            anim = AnimateWindowFlags.TENGAH Or AnimateWindowFlags.HILANG
        ElseIf animasi_base = "Menyatu" Then
            anim = AnimateWindowFlags.MENYATU Or AnimateWindowFlags.HILANG
            Transparasi = 100
        Else
            anim = AnimateWindowFlags.MENYATU Or AnimateWindowFlags.HILANG
            Transparasi = 100
        End If
        waktu = KecAnimasi.ToString + "0"
        Return AnimateWindow(kontrol, waktu, anim)
    End Function

    Sub set_timer()
        Dim n As String
        Dim a As Byte = SatuanSetiap
        Dim satuan As Int32
        If a = 0 Then
            satuan = 1000
        ElseIf a = 1 Then
            satuan = 60000
        ElseIf a = 2 Then
            satuan = 3600000
        End If
        n = TampilSelama
        Form_Handle.TSelama.Interval = n + "000"
        Form_Handle.TSetiap.Interval = TampilSetiap * satuan
    End Sub

    Public Sub QOT()
        Form_PembuatQuote.Close()
        Form_PembuatQuote.Show()
        If statusqot = 1 Then
            Form_PembuatQuote.status = 1
        End If
    End Sub

    Public Sub QOT2()
        Form_PembuatQuote.Close()
        Form_PembuatQuote.Show()
        If statusqot = 1 Then
            Form_PembuatQuote.status = 1
        End If
        Form_PembuatQuote.TBDari.Enabled = False
        Form_PembuatQuote.TBData.Enabled = False
        Form_PembuatQuote.TBFileName.Enabled = False
        Form_PembuatQuote.LBData.Enabled = False
        Form_PembuatQuote.BPanduan.Text = "Hentikan"
        Form_PembuatQuote.BKerjakan.Enabled = False
        Form_PembuatQuote.BBersihkan.Enabled = False
        Form_PembuatQuote.BSimpan.Enabled = False
        Form_PembuatQuote.Timer1.Enabled = True
    End Sub

    Public Function acak(ByVal maks As Integer) As Integer
        Dim angka As Int16
        Dim ganti As Integer = 1
        Dim r = New Random
        Dim sem As Integer
ulang:
        If maks < 9 Then
            angka = r.Next(0, maks)
        ElseIf maks >= 9 And maks < 1000 Then
            sem = r.Next(0, maks)
            If ganti = 1 Then
                angka = sem - r.Next(Now.DayOfWeek)
                ganti = 0
            Else
                angka = sem + r.Next(Now.DayOfWeek)
                ganti = 1
            End If
        ElseIf maks >= 1000 And maks < 5000 Then
            sem = r.Next(0, maks)
            If ganti = 1 Then
                angka = sem - r.Next(Now.Hour)
                ganti = 0
            Else
                angka = sem + r.Next(Now.Hour)
                ganti = 1
            End If
        ElseIf maks >= 5000 And maks < 10000 Then
            sem = r.Next(0, maks)
            If ganti = 1 Then
                angka = sem - r.Next(Now.Minute)
                ganti = 0
            Else
                angka = sem + r.Next(Now.Minute)
                ganti = 1
            End If
        ElseIf maks >= 10000 Then
            sem = r.Next(0, maks)
            If ganti = 1 Then
                angka = sem - r.Next(Now.Millisecond)
                ganti = 0
            Else
                angka = sem + r.Next(Now.Millisecond)
                ganti = 1
            End If
        End If
        If angka < 0 Or angka > maks Then
            GoTo ulang
        End If
        Return angka
    End Function

    Public Sub TampilkanHasilTema()
        Dim d As Int16
        Dim r As New Random
        Dim aw As Integer
        Dim banyak As Integer
        Dim n As Integer = 0

        d = DaftarQuote.Count.ToString
        banyak = listcari.Count
        aw = r.Next(0, (listcari.Count))
        Try
            If JanganUlangi = True Then
                If QuoteSudahTampil.Count <> d Then
                    Do While QuoteSudahTampil.Contains(listcari.Item(aw))
                        aw = r.Next(0, (listcari.Count))
                        n += 1
                        If n > banyak Then
                            aw = acak(d)
                            SudahTampilSemua = True
                            Exit Do
                        End If
                    Loop
                Else
                    Try
                        Using SW As New IO.StreamWriter(AlamatTampungan + "\quote.dft", True)
                            SW.WriteLine("SudahSemua=true")
                            SW.Close()
                        End Using
                        My.Computer.FileSystem.DeleteFile(AlamatTampungan + "\quote.dft", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                    Catch ex As Exception
                        MessageBoxEx.Show("Pesan ini muncul mungkin karena tak disengaja berkas ""quote.dft"" telah terhapus, kami akan memperbaikinya. (kesalahan: 110202)", "Berkas Hilang", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End Try
                    QuoteSudahTampil = New List(Of String)
                    QuoteSudahTampil.Clear()
                    aw = acak(d)
                    SudahTampilSemua = True
                End If
                If SudahTampilSemua = False Then
                    QuoteSudahTampil.Add(listcari.Item(aw))
                    TulisSudahTampil(listcari.Item(aw))
                ElseIf SudahTampilSemua = True Then
                    QuoteSudahTampil.Add(aw)
                    TulisSudahTampil(aw)
                End If
            End If
        Catch ex As System.IO.DirectoryNotFoundException
            MessageBoxEx.Show("Galat Saat Menampilkan Quote, hal ini terjadi mungkin karena alamat tampungan quote terpakai tidak ditemukan" + ". (kesalahan: 110901)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex2 As Exception
            MessageBoxEx.Show("Galat Saat Menampilkan Quote, Pesan eror: " + ex2.Message + ". (kesalahan: 110901)" + Environment.NewLine + "Pesan developer: banyak = " + banyak.ToString + ", aw = " + aw.ToString, "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        If SudahTampilSemua = False Then
            read_q_from(listcari.Item(aw))
        ElseIf SudahTampilSemua = True Then
            read_q_from(aw, False)
        End If
        Form_Quote.Label2.Visible = False
        showquoologi()
    End Sub

    Public Sub read_q()
        Try
            Dim r = New Random()
            Dim last As Int16
            Dim d As Int16
            d = DaftarQuote.Count
            Form_PengaturanUtama.LTotalQot.Text = d
            last = acak(d)
            Try
                If JanganUlangi = True Then
                    If QuoteSudahTampil.Count <> d Then
                        Do While QuoteSudahTampil.Contains(last)
                            last = acak(d)
                        Loop
                    Else
                        Try
                            My.Computer.FileSystem.DeleteFile(AlamatTampungan + "\quote.dft", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                        Catch ex As Exception
                            MessageBoxEx.Show("Berkas tidak ditemukan. Pesan ini muncul mungkin karena tak disengaja berkas ""quote.dft"" telah terhapus, kami akan memperbaikinya. (kesalahan: 110202)", "Berkas Hilang", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End Try
                        QuoteSudahTampil = New List(Of String)
                        QuoteSudahTampil.Clear()
                        last = acak(d)
                    End If
                    QuoteSudahTampil.Add(last)
                    TulisSudahTampil(last)
                End If
            Catch ex As System.IO.DirectoryNotFoundException
                MessageBoxEx.Show("Galat Saat Menampilkan Quote, hal ini terjadi mungkin karena alamat tampungan quote terpakai tidak ditemukan" + ". (kesalahan: 110901)", "Berkas Hilang", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Catch ex2 As Exception
                MessageBoxEx.Show("Galat saat menampilka Quote. Pesan eror: " + ex2.Message + ". (kesalahan: 110901)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
            Form_Quote.TBQuotes.Text = DaftarQuote(last)
            Form_Quote.Label1.Text = DaftarAlamatQuote(last) + " - Quoologi"
            Form_Handle.asal = DaftarAlamatQuote(last)
            TentukanBaris()
            Form_Handle.last = Form_Quote.TBQuotes.Text
        Catch ex As Exception

        End Try
    End Sub

    Public Sub read_q_from(ByVal alamat As Integer, Optional ByVal atur As Boolean = True)
        Dim r As New Random
        Try
            Form_Quote.TBQuotes.Text = DaftarQuote(alamat)
        Catch ex As Exception
            Form_Quote.TBQuotes.Text = DaftarQuote(r.Next(DaftarQuote.Count - 1))
        End Try
        If atur = True Then
            Form_Quote.Label1.Text = DaftarAlamatQuote(alamat) + " - Quoologi"
        Else
            Form_Quote.Label1.Text = DaftarAlamatQuote(alamat) + " - Quoologi"
        End If
        Form_Handle.asal = DaftarAlamatQuote(alamat)
        TentukanBaris()
        Form_Handle.last = Form_Quote.TBQuotes.Text
    End Sub
End Module
