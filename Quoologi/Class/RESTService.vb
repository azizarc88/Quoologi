﻿Imports System.Net
Imports System.Collections.Specialized
Imports System.Text

Public Class RESTService
    Private response As String
    Public Delegate Sub RequestCompleteHandler(ByVal sender As Object, ByVal e As RequestCompleteEventArgs)
    Public Event RequestComplete As RequestCompleteHandler

    Public Sub MakeRequest(ByVal requestUrl As String, ByVal formData As NameValueCollection, ByVal bagian As String)
        Dim webClient As WebClient = New WebClient()

        If formData Is Nothing Then
            formData = New NameValueCollection()
            formData("1") = "1"
        End If

        Dim responseBytes As Byte() = Nothing

        Try
            responseBytes = webClient.UploadValues(Pengaturan.server & requestUrl, "POST", formData)
            response = Encoding.UTF8.GetString(responseBytes)
        Catch e As Exception
            response = "{""hasil"":""GAGAL""}"
        End Try

        webClient.Dispose()
        Dim args As RequestCompleteEventArgs = New RequestCompleteEventArgs(response, bagian)
        RaiseEvent RequestComplete(Me, args)
    End Sub

    Private webClientAsync As WebClient

    Public Sub MakeRequestAsync(ByVal requestUrl As String, ByVal formData As NameValueCollection, ByVal bagian As String)
        webClientAsync = New WebClient()
        Dim uri As Uri = New Uri(Pengaturan.server & requestUrl)
        AddHandler webClientAsync.UploadValuesCompleted, AddressOf WebClient_UploadValuesCompleted

        If formData Is Nothing Then
            formData = New NameValueCollection()
            formData("1") = "1"
        End If

        Try
            webClientAsync.UploadValuesAsync(uri, "POST", formData, bagian)
        Catch e As Exception
            response = "{""hasil"":""GAGAL""}"
        End Try
    End Sub

    Private Sub WebClient_UploadValuesCompleted(ByVal sender As Object, ByVal e As UploadValuesCompletedEventArgs)
        If e.[Error] Is Nothing Then
            Dim args As RequestCompleteEventArgs = New RequestCompleteEventArgs(Encoding.UTF8.GetString(e.Result), e.UserState.ToString())
            RaiseEvent RequestComplete(Me, args)
        Else
            Dim args As RequestCompleteEventArgs = New RequestCompleteEventArgs("{""hasil"":""GAGAL""}", e.UserState.ToString())
            RaiseEvent RequestComplete(Me, args)
        End If

        webClientAsync.Dispose()
    End Sub

    Public Function GetString(ByVal namavalue As String) As String
        Dim hasil As String = ""
        Dim temp As String = response.Replace("{", "")
        temp = temp.Replace("}", "")
        Dim sem As String() = temp.Split(","c)

        For Each item In sem

            If item.Contains("""") Then

                If item.Contains("""" & namavalue & """") Then
                    Dim v As String = """" & namavalue & """:"
                    hasil = item.Substring(item.LastIndexOf(v) + v.Length)
                    hasil = hasil.Replace("\/", "/")
                    hasil = hasil.Replace("""", "")
                End If
            End If
        Next

        Return hasil
    End Function

    Public Function GetString(ByVal response As String, ByVal namavalue As String) As String
        Dim hasil As String = ""
        Dim temp As String = response.Replace("{", "")
        temp = temp.Replace("}", "")
        temp = temp.Replace("]", "")
        Dim sem As String() = temp.Split(","c)

        For Each item In sem

            If item.Contains("""") Then

                If item.Contains("""" & namavalue & """") Then
                    Dim v As String = """" & namavalue & """:"
                    hasil = item.Substring(item.LastIndexOf(v) + v.Length)
                    hasil = hasil.Replace("\/", "/")
                    hasil = hasil.Replace("""", "")
                End If
            End If
        Next

        Return hasil
    End Function

    Public Function GetArray(ByVal namavalue As String) As String()
        Dim v As String = """" & namavalue & """:[{"
        Dim sem As String = response.Substring(response.LastIndexOf(v) + v.Length)
        sem = sem.Replace("\/", "/")
        sem = sem.Replace("]}", "")
        sem = sem.Replace("},", "")
        Dim temp As String() = sem.Split("{"c)
        Return temp
    End Function

    Public Function GetArray(ByVal response As String, ByVal namavalue As String) As String()
        Dim v As String = """" & namavalue & """:[{"
        Dim sem As String = response.Substring(response.LastIndexOf(v) + v.Length)
        sem = sem.Replace("\/", "/")
        sem = sem.Replace("]}", "")
        sem = sem.Replace("},", "")
        Dim temp As String() = sem.Split("{"c)
        Return temp
    End Function
End Class
