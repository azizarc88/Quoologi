﻿Public Class RequestCompleteEventArgs
    Inherits System.EventArgs

    Private response, bagian As String

    Public Sub New(ByVal response As String, ByVal bagian As String)
        Me.response = response
        Me.bagian = bagian
    End Sub

    Public ReadOnly Property dBagian As String
        Get
            Return Bagian
        End Get
    End Property

    Public ReadOnly Property dResponse As String
        Get
            Return Response
        End Get
    End Property
End Class

