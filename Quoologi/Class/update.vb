﻿Imports DevComponents.DotNetBar
Imports System.Net

Public Class update
    Private WithEvents Downloader As WebFileDownloader

    Public Function CekKoneksi2(Optional ByVal tampilkanpsan As Boolean = True) As Boolean
        Try
            Using client = New WebClient()
                Application.DoEvents()
                If My.Computer.Network.IsAvailable = True Then
                    adakoneksi = True
                    setstatus()
                    Return True
                Else
                    If tampilkanpsan = True Then
                        MessageBoxEx.Show("Tidak dapat melakukan koneksi internet" + Environment.NewLine + "Silakan cek apakah Anda terhubung dengan internet dan Quoologi diizinkan dari firewall atau tidak. (kesalahan: 040401)", "Koneksi Gagal", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                    adakoneksi = False
                    setstatus()
                    Return False
                End If
                Application.DoEvents()
            End Using
            Application.DoEvents()
            setstatus()
        Catch EX As Exception
            If tampilkanpsan = True Then
                MessageBoxEx.Show("Tidak dapat melakukan koneksi internet" + Environment.NewLine + "Silakan cek apakah Anda terhubung dengan internet dan Quoologi diizinkan dari firewall atau tidak. (kesalahan: 040401)", "Koneksi Gagal", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            adakoneksi = False
            setstatus()
            Return False
        End Try
    End Function

    Public Sub cek_pengaturan_a_t()
        If Form_Handle.timer = 10 And sem_upd_a.Count = 0 Or Form_PengaturanUtama.Highlighter1.GetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi) = Validator.eHighlightColor.Red Then
            Try
                Application.DoEvents()
                Form_PengaturanUtama.Client.DownloadFile("https://sites.google.com/site/izarcsoftware/main_v.txt", My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\main_v.txt")
                Application.DoEvents()
                Form_PengaturanUtama.Client.DownloadFile("https://sites.google.com/site/izarcsoftware/info_update.txt", My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\info_update.txt")
                Application.DoEvents()
            Catch ex As Exception
                Form_Handle.TCekUpdate.Enabled = False
                Form_Handle.timer = 0
                Exit Sub
            End Try

            Dim objReader As New System.IO.StreamReader(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\main_v.txt")
            Form_Handle.semver_a = My.Application.Info.Version.ToString
            Form_Handle.versek = Form_Handle.semver_a.ToString.Replace(".", "")
            Do While Not objReader.EndOfStream
                sem_upd_a.Add(objReader.ReadLine())
            Loop
            objReader.Close()
            Form_Handle.verbaru = sem_upd_a.Item(1).ToString.Replace(".", "")
        ElseIf Form_Handle.timer < 10 Then
            Exit Sub
        End If

        If Form_Handle.verbaru > Form_Handle.versek Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.Orange)
            AdaUpProgram = 1
            SimpanPengaturan()
        ElseIf Form_Handle.verbaru = Form_Handle.versek Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.None)
            AdaUpProgram = 2
            SimpanPengaturan()
        End If
    End Sub

    Public Sub cek_pengaturan_q()
        Dim folder As IO.FileInfo() = Form_PengaturanUtama.pathquote.GetFiles("*.qot")
        Dim semcek As List(Of String) = New List(Of String)
        Form_Handle.cekq = 1

        If Form_PengaturanUtama.Highlighter1.GetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru) = Validator.eHighlightColor.Orange Then
            Form_Handle.cekq = 0
            Form_Handle.qotlama.Clear()
            Form_Handle.qotbaru.Clear()
            Form_Handle.linkqotbaru.Clear()
            sem_upd_q.Clear()
            semcek.Clear()
            Try
                Dim objReader As New System.IO.StreamReader(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\quotes_v.txt")
                Do While Not objReader.EndOfStream
                    sem_upd_q.Add(objReader.ReadLine())
                Loop
                objReader.Close()
            Catch ex As Exception
            End Try
            Form_Handle.ada = True
            Form_Handle.gagalq = False
            GoTo here
        End If

        If CekKoneksi2() = False Then
            Exit Sub
        End If
        If FileIO.FileSystem.FileExists(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\daftar_file.txt") Then
            Try
                Process.Start(AlamatInstall + "\update_quoologi_q.exe")
                sem_upd_q.Clear()
                Form_PengaturanUtama.Close()
            Catch ex As System.ComponentModel.Win32Exception
                If ex.Message = "The system cannot find the file specified" Then
                    If MessageBoxEx.Show("Kami tidak dapat menemukan berkas ""update_quoologi_q.exe"", Apakah anda ingin memperbaiki ?. (kesalahan: 140201)", "Galat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        PerbaikiQuoologi()
                    Else
                        Form_PengaturanUtama.Show()
                    End If
                End If
            End Try
            Exit Sub
        End If

        sem_upd_q.Clear()
        Form_Handle.qotlama.Clear()
        Form_Handle.qotbaru.Clear()
        Form_Handle.linkqotbaru.Clear()
        semcek.Clear()
        Form_Handle.ada = True

        If sem_upd_q.Count = 0 Or Form_PengaturanUtama.Highlighter1.GetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru) = Validator.eHighlightColor.Red Then
            Try
                Downloader = New WebFileDownloader
                Downloader.DownloadFile("https://sites.google.com/site/izarcsoftware/quotes_v.txt", My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\quotes_v.txt")
                Form_Handle.gagalq = False
            Catch ex As Exception
                MessageBoxEx.Show("Pengecekan gagal, silakan cek koneksi internet Anda dan apakah Quoologi diijinkan dari firewall atau tidak. (kesalahan: 140301)", "Gagal Mengecek Update", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Form_Handle.gagalq = True
                Exit Sub
            End Try

            Try
                Dim objReader As New System.IO.StreamReader(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\quotes_v.txt")
                Do While Not objReader.EndOfStream
                    sem_upd_q.Add(objReader.ReadLine())
                Loop
                objReader.Close()
            Catch ex As Exception
                MessageBoxEx.Show("Pengecekan gagal, silakan cek koneksi internet Anda dan apakah Quoologi diijinkan dari firewall atau tidak. (kesalahan: 140402)", "Gagal Mengecek Update", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End Try
        End If
        Form_Handle.cekq = 0
here:
        Form_PengaturanUtama.BCekQuotesBaru.Enabled = True
        For Each Form_PengaturanUtama.berkas In folder
            Form_Handle.qotlama.Add(Form_PengaturanUtama.berkas.Name)
        Next

        For Each baris As String In sem_upd_q
            If baris.Contains("quotes: #") Then
                Form_Handle.qotbaru.Add(baris)
                If Form_Handle.qotlama.Contains(baris.Replace("quotes: #", "")) = False Then
                    semcek.Add(baris)
                End If
            ElseIf baris.Contains("link: #") Then
                Form_Handle.linkqotbaru.Add(baris)
            End If
        Next

        For Each Data As String In semcek
            If Form_Handle.qotlama.Contains(Data) = False Then
                Form_Handle.ada = False
            End If
        Next

        If Form_Handle.ada = False And Form_Handle.qotbaru.Count > 0 Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru, Validator.eHighlightColor.Orange)
            Form_PengaturanUtama.Close()
            Form_UpdateQuote.Show()
            Form_UpdateQuote.status = 1
        ElseIf Form_Handle.ada = True Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCekQuotesBaru, Validator.eHighlightColor.None)
            MessageBoxEx.Show("Semua Quote² yang ada di server kami sudah Anda miliki", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            AdaUpQuote = 2
            SimpanPengaturan()
        End If
        setstatus()
    End Sub

    Public Sub cek_pengaturan_a()
        If CekKoneksi() = False Then
            Exit Sub
        End If
        If sem_upd_a.Count = 0 Or Form_PengaturanUtama.Highlighter1.GetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi) = Validator.eHighlightColor.Red Then
            Try
                Form_PengaturanUtama.Client.DownloadFile("https://sites.google.com/site/izarcsoftware/main_v.txt", My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\main_v.txt")
                Form_PengaturanUtama.Client.DownloadFile("https://sites.google.com/site/izarcsoftware/info_update.txt", My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\info_update.txt")
                Form_Handle.gagala = False
            Catch ex As Exception
                MessageBoxEx.Show("Pengecekan gagal, silakan cek koneksi internet Anda dan apakah Quoologi diijinkan dari firewall atau tidak. (kesalahan: 140302)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Form_Handle.gagala = True
                Exit Sub
            End Try

            Try
                Dim objReader As New System.IO.StreamReader(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\main_v.txt")
                Form_Handle.semver_a = My.Application.Info.Version.ToString
                Form_Handle.versek = Form_Handle.semver_a.ToString.Replace(".", "")
                sem_upd_a.Clear()
                Do While Not objReader.EndOfStream
                    sem_upd_a.Add(objReader.ReadLine())
                Loop
                objReader.Close()
            Catch ex As Exception
                MessageBoxEx.Show("Pengecekan gagal, silakan cek koneksi internet Anda dan apakah Quoologi diijinkan dari firewall atau tidak. (kesalahan: 140402)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End Try
            Form_Handle.verbaru = sem_upd_a.Item(1).ToString.Replace(".", "")
        End If

        Form_PengaturanUtama.TThreadA.Enabled = False
        Form_Handle.ceka = 0
        Form_Handle.gagala = False
        Form_PengaturanUtama.BCPembaruanQuoologi.Enabled = True

        If Form_Handle.verbaru > Form_Handle.versek Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.Orange)
            AdaUpProgram = 1
            SimpanPengaturan()
            If MessageBoxEx.Show("Versi terbaru ditemukan:" + Environment.NewLine + "Versi sekarang: " + My.Application.Info.Version.ToString + Environment.NewLine + "Versi terbaru: " + sem_upd_a.Item(1).ToString + Environment.NewLine + "Apakah Anda ingin memperbarui Quoologi ?", "Update Ditemukan", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Try
                    Process.Start(AlamatInstall + "\update_quoologi_m.exe")
                    Form_Handle.Close()
                Catch ex As System.ComponentModel.Win32Exception
                    If ex.Message = "The system cannot find the file specified" Then
                        If MessageBoxEx.Show("Kami tidak dapat menemukan berkas ""update_quoologi_m.exe"", Apakah anda ingin memperbaiki ?. (kesalahan: 140303)", "Galat", MessageBoxButtons.YesNo, MessageBoxIcon.Error) = DialogResult.Yes Then
                            PerbaikiQuoologi()
                        Else
                            Form_PengaturanUtama.Show()
                        End If
                    End If
                End Try
            End If
        ElseIf Form_Handle.verbaru = Form_Handle.versek Then
            Form_PengaturanUtama.Highlighter1.SetHighlightColor(Form_PengaturanUtama.BCPembaruanQuoologi, Validator.eHighlightColor.None)
            AdaUpProgram = 2
            SimpanPengaturan()
            MessageBoxEx.Show("Program sekarang: " + My.Application.Info.Version.ToString + ", adalah program terbaru.", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBoxEx.Show("Program Anda lebih update dari yang ada di server kami, WOW >.<", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        setstatus()
    End Sub
End Class
