﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_ProgramTerkecuali
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LBProgram = New System.Windows.Forms.ListBox()
        Me.BHapus = New DevComponents.DotNetBar.ButtonX()
        Me.BTambah = New DevComponents.DotNetBar.ButtonX()
        Me.BSimpan = New DevComponents.DotNetBar.ButtonX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuspendLayout()
        '
        'LBProgram
        '
        Me.LBProgram.BackColor = System.Drawing.Color.White
        Me.LBProgram.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBProgram.ForeColor = System.Drawing.Color.Black
        Me.LBProgram.FormattingEnabled = True
        Me.LBProgram.Location = New System.Drawing.Point(12, 12)
        Me.LBProgram.Name = "LBProgram"
        Me.LBProgram.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBProgram.Size = New System.Drawing.Size(264, 210)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBProgram, New DevComponents.DotNetBar.SuperTooltipInfo("Program Terkecuali", "", "Berisi daftar nama berkas dari program terkecuali.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.LBProgram.TabIndex = 36
        '
        'BHapus
        '
        Me.BHapus.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BHapus.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BHapus.Enabled = False
        Me.BHapus.Location = New System.Drawing.Point(12, 269)
        Me.BHapus.Name = "BHapus"
        Me.BHapus.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BHapus.Size = New System.Drawing.Size(32, 23)
        Me.BHapus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BHapus, New DevComponents.DotNetBar.SuperTooltipInfo("Hapus", "", "Klik untuk menghapus program terpilih dari daftar program terkecuali.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BHapus.Symbol = ""
        Me.BHapus.SymbolSize = 16.0!
        Me.BHapus.TabIndex = 38
        '
        'BTambah
        '
        Me.BTambah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTambah.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BTambah.Location = New System.Drawing.Point(244, 269)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BTambah.Size = New System.Drawing.Size(32, 23)
        Me.BTambah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BTambah, New DevComponents.DotNetBar.SuperTooltipInfo("Tambah", "", "Klik untuk menambah nama berkas ke daftar program terkecuali.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BTambah.Symbol = ""
        Me.BTambah.SymbolSize = 16.0!
        Me.BTambah.TabIndex = 37
        '
        'BSimpan
        '
        Me.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSimpan.AntiAlias = True
        Me.BSimpan.Location = New System.Drawing.Point(152, 269)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.BSimpan.Size = New System.Drawing.Size(75, 23)
        Me.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BSimpan, New DevComponents.DotNetBar.SuperTooltipInfo("Simpan", "", "Klik untuk menyimpan daftar program terkecuali.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BSimpan.TabIndex = 39
        Me.BSimpan.Text = "Simpan | F1"
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.AntiAlias = True
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(61, 269)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(75, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, New DevComponents.DotNetBar.SuperTooltipInfo("Tutup", "", "Klik untuk menutup jendela dan mengabaikan perubahan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BBatal.TabIndex = 40
        Me.BBatal.Text = "Tutup | Esc"
        '
        'Program_Hentikan
        '
        Me.AcceptButton = Me.BSimpan
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(288, 264)
        Me.ControlBox = False
        Me.Controls.Add(Me.BSimpan)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BHapus)
        Me.Controls.Add(Me.BTambah)
        Me.Controls.Add(Me.LBProgram)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Program_Hentikan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Program"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LBProgram As System.Windows.Forms.ListBox
    Friend WithEvents BHapus As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BTambah As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BSimpan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
End Class
