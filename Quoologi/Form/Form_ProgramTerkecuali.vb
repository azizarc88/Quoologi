﻿Imports DevComponents.DotNetBar

Public Class Form_ProgramTerkecuali
    Inherits DevComponents.DotNetBar.Metro.MetroForm
    Public listsementara As List(Of String) = New List(Of String)

    Private Sub baca_data()
        For Each item As Object In ProgramTerkecuali
            listsementara.Add(item)
        Next

        For Each item As String In listsementara
            LBProgram.Items.Add(item)
        Next
    End Sub

    Public Sub segarkan()
        LBProgram.Items.Clear()
        For Each item As String In listsementara
            LBProgram.Items.Add(item)
        Next
        LBProgram.Sorted = True
    End Sub

    Private Sub Program_Hentikan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        GeserKontrol(BTambah, 269, Pengubah.Ditambah, Orientasi.Vertikal)
        GeserKontrol(BSimpan, 269, Pengubah.Ditambah, Orientasi.Vertikal)
        GeserKontrol(BBatal, 269, Pengubah.Ditambah, Orientasi.Vertikal)
        GeserKontrol(BHapus, 269, Pengubah.Ditambah, Orientasi.Vertikal)
    End Sub

    Private Sub Program_Hentikan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        listsementara.Clear()
        LBProgram.Items.Clear()
        baca_data()
        LBProgram.Sorted = True
    End Sub

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        Form_TambahProgramTerkecuali.ShowDialog()
    End Sub

    Private Sub BHapus_Click(sender As Object, e As EventArgs) Handles BHapus.Click
        For Each terpilih As Object In LBProgram.SelectedItems
            listsementara.Remove(terpilih)
        Next
        segarkan()
        BHapus.Enabled = False
    End Sub

    Private Sub LBProgram_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBProgram.SelectedIndexChanged
        BHapus.Enabled = True
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        ProgramTerkecuali.Clear()
        For Each item As Object In LBProgram.Items
            ProgramTerkecuali.Add(item)
        Next
        SimpanPengaturan(BagianPenyimpanan.ProgramTerkecuali)
        ToastNotification.Show(Me, "Pengaturan telah tersimpan", eToastPosition.BottomCenter)
    End Sub

    Private Sub Program_Hentikan_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        GeserKontrol(BHapus, 231, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BBatal, 231, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BSimpan, 231, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BTambah, 231, Pengubah.Dikurangi, Orientasi.Vertikal)

        If FirstRun = True And Form_Handle.programterkecualisudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berisi daftar program terkecuali" + Environment.NewLine + "Anda bisa menghapus / menambah" + Environment.NewLine + "program terkecuali disini", 7000, eToastPosition.MiddleCenter)
            Form_Handle.programterkecualisudah = True
        End If
    End Sub
End Class