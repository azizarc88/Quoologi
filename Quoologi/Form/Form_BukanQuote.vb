﻿Imports DevComponents.DotNetBar

Public Class Form_BukanQuote
    Inherits Metro.MetroForm
    Private Sub BukanQuote_Load(sender As Object, e As EventArgs) Handles Me.Load
        LBaris.Text = Form_PembuatQuote.terbuang.Count
        TBBukan.DataSource = Form_PembuatQuote.terbuang
        If Form_PembuatQuote.terbuang.Count > 5 Then
            ToastNotification.Show(Me, "Bukan hasil yang diinginkan ?, silakan coba ubah bagian teks pencegah dan pengaturan lainya.", 5000, eToastPosition.BottomCenter)
        End If

        If FirstRun = True And Form_Handle.BukanQuoteSudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian Bukan Quote" + Environment.NewLine + Environment.NewLine + "Jendela ini berfungsi untuk melihat baris dari bagian data yang dianggap sebagai bukan quote", 7000, eToastPosition.MiddleCenter)
            Form_Handle.BukanQuoteSudah = True
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

    Private Sub TBBukan_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TBBukan.SelectedIndexChanged
        Dim indexchar As Integer
        Dim panjang As Integer
        Dim temp As String
        indexchar = Form_PembuatQuote.TBData.GetFirstCharIndexFromLine(Form_PembuatQuote.alamatterbuang.Item(TBBukan.SelectedIndex))
        indexchar = indexchar + Form_PembuatQuote.panjangterbuang.Item(TBBukan.SelectedIndex)
        temp = "-" + Form_PembuatQuote.panjangterbuang.Item(TBBukan.SelectedIndex)
        LPencegah.Text = PenentuAlasan(TBBukan.SelectedItem.ToString, Form_PembuatQuote.TBMin.Text, Form_PembuatQuote.TBMaks.Text)
        LNilai.Text = (Form_PembuatQuote.alamatterbuang.Item(TBBukan.SelectedIndex) + 1).ToString + ", " + Form_PembuatQuote.panjangterbuang.Item(TBBukan.SelectedIndex)
        panjang = temp
        Form_PembuatQuote.TBData.Select(indexchar, panjang)
        Form_PembuatQuote.TBData.ScrollToCaret()
    End Sub
End Class