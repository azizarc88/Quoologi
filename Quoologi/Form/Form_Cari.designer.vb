﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Cari
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Cari))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LRiwayat = New DevComponents.DotNetBar.LabelX()
        Me.BDaftar = New DevComponents.DotNetBar.ButtonX()
        Me.BSBelum = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.CBQuoteTerpakai = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TBTeks = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TBDataKe = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.BLanjut = New DevComponents.DotNetBar.ButtonX()
        Me.BCari = New DevComponents.DotNetBar.ButtonX()
        Me.CBCaseSenv = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CLBQuote = New System.Windows.Forms.CheckedListBox()
        Me.Highlighter1 = New DevComponents.DotNetBar.Validator.Highlighter()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel1.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.LRiwayat)
        Me.GroupPanel1.Controls.Add(Me.BDaftar)
        Me.GroupPanel1.Controls.Add(Me.BSBelum)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Controls.Add(Me.CBQuoteTerpakai)
        Me.GroupPanel1.Controls.Add(Me.TBTeks)
        Me.GroupPanel1.Controls.Add(Me.TBDataKe)
        Me.GroupPanel1.Controls.Add(Me.BLanjut)
        Me.GroupPanel1.Controls.Add(Me.BCari)
        Me.GroupPanel1.Controls.Add(Me.CBCaseSenv)
        Me.GroupPanel1.Controls.Add(Me.CLBQuote)
        Me.GroupPanel1.DrawTitleBox = False
        Me.GroupPanel1.Location = New System.Drawing.Point(4, 2)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(382, 102)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.CustomizeBackground2
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 5
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(251, 40)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(2, 23)
        Me.LabelX2.TabIndex = 11
        Me.LabelX2.Text = "|"
        '
        'LRiwayat
        '
        Me.LRiwayat.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LRiwayat.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LRiwayat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LRiwayat.ForeColor = System.Drawing.Color.Black
        Me.LRiwayat.Location = New System.Drawing.Point(5, 69)
        Me.LRiwayat.Name = "LRiwayat"
        Me.LRiwayat.Size = New System.Drawing.Size(140, 23)
        Me.LRiwayat.TabIndex = 10
        Me.LRiwayat.Text = "Riwayat Pencarian"
        Me.ToolTip1.SetToolTip(Me.LRiwayat, "Riwayat Pencarian")
        '
        'BDaftar
        '
        Me.BDaftar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BDaftar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BDaftar.Enabled = False
        Me.BDaftar.Location = New System.Drawing.Point(274, 69)
        Me.BDaftar.Name = "BDaftar"
        Me.BDaftar.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor()
        Me.BDaftar.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3)
        Me.BDaftar.Size = New System.Drawing.Size(69, 23)
        Me.BDaftar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BDaftar, New DevComponents.DotNetBar.SuperTooltipInfo("Daftar / Tampil", "Pintas: F3", resources.GetString("BDaftar.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(214, 204)))
        Me.BDaftar.Symbol = ""
        Me.BDaftar.SymbolSize = 14.0!
        Me.BDaftar.TabIndex = 9
        Me.BDaftar.Text = " Daftar"
        '
        'BSBelum
        '
        Me.BSBelum.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSBelum.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BSBelum.Enabled = False
        Me.BSBelum.Location = New System.Drawing.Point(247, 69)
        Me.BSBelum.Name = "BSBelum"
        Me.BSBelum.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(4, 0, 4, 0)
        Me.BSBelum.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F2)
        Me.BSBelum.Size = New System.Drawing.Size(26, 23)
        Me.BSBelum.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BSBelum, New DevComponents.DotNetBar.SuperTooltipInfo("Tampilkan", "Pintas: F2", "Tombol ini berfungsi untuk menampilkan Quotes sebelumnya dari hasil pencarian.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BSBelum.Symbol = ""
        Me.BSBelum.SymbolSize = 14.0!
        Me.BSBelum.TabIndex = 8
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(5, 40)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(140, 23)
        Me.LabelX1.TabIndex = 7
        Me.LabelX1.Text = "Cari teks dari berkas quote:"
        '
        'CBQuoteTerpakai
        '
        Me.CBQuoteTerpakai.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.CBQuoteTerpakai.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBQuoteTerpakai.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBQuoteTerpakai.Checked = True
        Me.CBQuoteTerpakai.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBQuoteTerpakai.CheckValue = "Y"
        Me.CBQuoteTerpakai.ForeColor = System.Drawing.Color.Black
        Me.CBQuoteTerpakai.Location = New System.Drawing.Point(151, 40)
        Me.CBQuoteTerpakai.Name = "CBQuoteTerpakai"
        Me.CBQuoteTerpakai.Size = New System.Drawing.Size(97, 23)
        Me.CBQuoteTerpakai.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBQuoteTerpakai, New DevComponents.DotNetBar.SuperTooltipInfo("Cari Dalam Qote Terpakai", "", "Pilih pilihan ini untuk mencari teks dari berkas² yang digunakan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CBQuoteTerpakai.TabIndex = 6
        Me.CBQuoteTerpakai.Text = "Yang digunakan"
        '
        'TBTeks
        '
        Me.TBTeks.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBTeks.Border.Class = "TextBoxBorder"
        Me.TBTeks.Border.CornerDiameter = 4
        Me.TBTeks.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.TBTeks.ForeColor = System.Drawing.Color.Black
        Me.Highlighter1.SetHighlightOnFocus(Me.TBTeks, True)
        Me.TBTeks.Location = New System.Drawing.Point(4, 10)
        Me.TBTeks.Name = "TBTeks"
        Me.TBTeks.PreventEnterBeep = True
        Me.TBTeks.Size = New System.Drawing.Size(296, 24)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBTeks, New DevComponents.DotNetBar.SuperTooltipInfo("Cari teks", "", "Pada bagian ini berfungsi untuk mengisi kata yang ingin dicari." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Untuk mencari " & _
            "Quote dengan lebih dari satu kata, silakan pisahkan antar kata dengan koma" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "co" & _
            "ntoh: " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "kata1,kata2,kata3", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(258, 136)))
        Me.TBTeks.TabIndex = 0
        Me.TBTeks.WatermarkText = "Cari teks ...."
        '
        'TBDataKe
        '
        Me.TBDataKe.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBDataKe.Border.Class = "TextBoxBorder"
        Me.TBDataKe.Border.CornerDiameter = 4
        Me.TBDataKe.Border.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.TBDataKe.Enabled = False
        Me.TBDataKe.ForeColor = System.Drawing.Color.Black
        Me.Highlighter1.SetHighlightOnFocus(Me.TBDataKe, True)
        Me.TBDataKe.Location = New System.Drawing.Point(305, 10)
        Me.TBDataKe.Name = "TBDataKe"
        Me.TBDataKe.PreventEnterBeep = True
        Me.TBDataKe.Size = New System.Drawing.Size(67, 24)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBDataKe, New DevComponents.DotNetBar.SuperTooltipInfo("Data ke", "", "Pada bagian ini berfungsi untuk mengisi urutan data ke berapa yang ingin ditampil" & _
            "kan pada data hasil pencarian.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBDataKe.TabIndex = 1
        Me.TBDataKe.WatermarkText = "Data ke ...."
        '
        'BLanjut
        '
        Me.BLanjut.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BLanjut.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BLanjut.Enabled = False
        Me.BLanjut.Location = New System.Drawing.Point(344, 69)
        Me.BLanjut.Name = "BLanjut"
        Me.BLanjut.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 4, 0, 4)
        Me.BLanjut.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F4)
        Me.BLanjut.Size = New System.Drawing.Size(26, 23)
        Me.BLanjut.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BLanjut, New DevComponents.DotNetBar.SuperTooltipInfo("Tampilkan", "Pintas: F4", "Tombol ini berfungsi untuk menampilkan Quotes setelahnya dari hasil pencarian.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BLanjut.Symbol = ""
        Me.BLanjut.SymbolSize = 14.0!
        Me.BLanjut.TabIndex = 3
        '
        'BCari
        '
        Me.BCari.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BCari.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BCari.Enabled = False
        Me.BCari.Location = New System.Drawing.Point(154, 69)
        Me.BCari.Name = "BCari"
        Me.BCari.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(4)
        Me.BCari.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.BCari.Size = New System.Drawing.Size(88, 23)
        Me.BCari.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BCari, New DevComponents.DotNetBar.SuperTooltipInfo("Cari", "Pintas: F1", "Tombol ini berfungsi untuk mencari teks yang terdapat di bagian ""Cari teks"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BCari.Symbol = ""
        Me.BCari.SymbolSize = 14.0!
        Me.BCari.TabIndex = 2
        Me.BCari.Text = " Cari | F3"
        '
        'CBCaseSenv
        '
        Me.CBCaseSenv.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.CBCaseSenv.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBCaseSenv.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBCaseSenv.ForeColor = System.Drawing.Color.Black
        Me.CBCaseSenv.Location = New System.Drawing.Point(259, 40)
        Me.CBCaseSenv.Name = "CBCaseSenv"
        Me.CBCaseSenv.Size = New System.Drawing.Size(113, 23)
        Me.CBCaseSenv.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.CBCaseSenv.TabIndex = 4
        Me.CBCaseSenv.Text = "Case sensitive"
        '
        'CLBQuote
        '
        Me.CLBQuote.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.CLBQuote.BackColor = System.Drawing.Color.White
        Me.CLBQuote.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CLBQuote.CheckOnClick = True
        Me.CLBQuote.ColumnWidth = 156
        Me.CLBQuote.ForeColor = System.Drawing.Color.Black
        Me.CLBQuote.FormattingEnabled = True
        Me.CLBQuote.Location = New System.Drawing.Point(4, 69)
        Me.CLBQuote.MultiColumn = True
        Me.CLBQuote.Name = "CLBQuote"
        Me.CLBQuote.Size = New System.Drawing.Size(366, 122)
        Me.CLBQuote.Sorted = True
        Me.SuperTooltip1.SetSuperTooltip(Me.CLBQuote, New DevComponents.DotNetBar.SuperTooltipInfo("Pilihan Pencarian", "", "Pilih pilihan di dalam sini untuk mencari teks dari berkas pilihan yang terpilih." & _
            "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Dobel klik untuk mencentang semua.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CLBQuote.TabIndex = 5
        Me.CLBQuote.UseCompatibleTextRendering = True
        Me.CLBQuote.Visible = False
        '
        'Highlighter1
        '
        Me.Highlighter1.ContainerControl = Me
        '
        'Timer1
        '
        Me.Timer1.Interval = 500
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.ForeColor = System.Drawing.Color.Black
        Me.TextBox1.Location = New System.Drawing.Point(391, 13)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(382, 98)
        Me.TextBox1.TabIndex = 2
        '
        'Form_Cari
        '
        Me.AcceptButton = Me.BCari
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(391, 109)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.GroupPanel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "Form_Cari"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cari Quotes"
        Me.TitleText = "Cari Quotes"
        Me.GroupPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents CBCaseSenv As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents BCari As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BLanjut As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBDataKe As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TBTeks As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Highlighter1 As DevComponents.DotNetBar.Validator.Highlighter
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents CBQuoteTerpakai As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CLBQuote As System.Windows.Forms.CheckedListBox
    Friend WithEvents BDaftar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BSBelum As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LRiwayat As DevComponents.DotNetBar.LabelX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
End Class
