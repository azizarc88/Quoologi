﻿Imports DevComponents.DotNetBar.Metro.MetroForm
Imports DevComponents.DotNetBar

Public Class Form_Kesukaan
    Inherits Metro.MetroForm
    Dim quote As List(Of String) = New List(Of String)
    Dim asalquote As List(Of String) = New List(Of String)
    Dim quote2 As List(Of String) = New List(Of String)
    Dim asalquote2 As List(Of String) = New List(Of String)
    Dim tersimpan As Boolean = False
    Dim teredit As Boolean = False
    Dim a As String = Nothing

    Private Sub Kesukaan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Me.Dispose()
    End Sub

    Public Sub BacaData()
        quote.Clear()
        asalquote.Clear()
        TBDaftarQuote.Items.Clear()
        quote2.Clear()
        asalquote2.Clear()
        Dim path As String = AlamatAppData
        Dim temp, fix, fix1 As String
        Dim l, a, b As Integer
        Try
            Dim filepath As String = AlamatAppData + "\quote_fav.dft"
            Using Berkas As New IO.StreamReader(filepath)
                While Not Berkas.EndOfStream
                    temp = Berkas.ReadLine()
                    If temp.Contains("  Q[") Then
                        l = temp.LastIndexOf(">") - 9
                        a = temp.LastIndexOf("AQ[") + 3
                        b = temp.Length - (a + 1)
                        fix = temp.Substring(9, l)
                        fix1 = temp.Substring(a, b)
                        TBDaftarQuote.Items.Add(fix)
                        quote.Add(fix)
                        asalquote.Add(fix1)
                    End If
                End While
                Application.DoEvents()
            End Using
        Catch ex As Exception
        End Try
        LabelX1.Text = TBDaftarQuote.Items.Count.ToString
        For Each item As String In quote
            quote2.Add(item)
        Next

        For Each item As String In asalquote
            asalquote2.Add(item)
        Next
    End Sub

    Private Sub Kesukaan_Load(sender As Object, e As EventArgs) Handles Me.Load
        BacaData()
        TBDaftarQuote.ContextMenu = ContextMenu1
    End Sub

    Private Sub TampilkankeQuoologi()
        Application.DoEvents()
        Quoologi.hidequoologi()
        Try
            a = TBDaftarQuote.Items.Item(TBDaftarQuote.SelectedIndex).ToString
        Catch ex As Exception
            ToastNotification.Show(Me, "Tidak ada item terpilih yang akan ditampilkan", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        End Try
        Try
            Form_Quote.TBQuotes.Text = a
            Form_Quote.Label1.Text = asalquote.Item(TBDaftarQuote.SelectedIndex) + " - Quoologi"
            Application.DoEvents()
        Catch ex As ArgumentOutOfRangeException
            ToastNotification.Show(Me, "Galat", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        End Try
        Application.DoEvents()
        TentukanBaris()
        Quoologi.showquoologi()
    End Sub

    Private Sub TBDaftarQuote_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles TBDaftarQuote.MouseDoubleClick
        TampilkankeQuoologi()
    End Sub

    Private Sub TBDaftarQuote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TBDaftarQuote.SelectedIndexChanged
        If teredit = True Then
            Try
                LAsal.Text = "@" + asalquote2.Item(TBDaftarQuote.SelectedIndex)
            Catch ex As Exception

            End Try
        Else
            Try
                LAsal.Text = "@" + asalquote.Item(TBDaftarQuote.SelectedIndex)
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub BuangDariDaftarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIUbah.Click
        teredit = True
        quote2.Clear()
        asalquote2.Clear()
        For Each item As String In quote
            quote2.Add(item)
        Next

        For Each item As String In asalquote
            asalquote2.Add(item)
        Next
        TBDaftarQuote.SelectionMode = SelectionMode.MultiExtended
        TBDaftarQuote.BackColor = Color.AliceBlue
        GeserKontrol(BTutup, BTutup.Location.Y + 34, Pengubah.Ditambah, Orientasi.Vertikal)
        BTutup.Text = "Batal"
        GeserKontrol(BHapus, BTutup.Location.Y - 34, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BSimpan, BTutup.Location.Y - 34, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BTutup, BTutup.Location.Y - 34, Pengubah.Dikurangi, Orientasi.Vertikal)
    End Sub

    Private Sub BTutup_Click(sender As Object, e As EventArgs) Handles BTutup.Click
        If BTutup.Text = "Tutup" Then
            Me.Close()
        Else
            TBDaftarQuote.SelectionMode = SelectionMode.One
            GeserKontrol(BHapus, BTutup.Location.Y + 34, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(BSimpan, BTutup.Location.Y + 34, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(BTutup, BTutup.Location.Y + 34, Pengubah.Ditambah, Orientasi.Vertikal)
            BTutup.Text = "Tutup"
            GeserKontrol(BTutup, BTutup.Location.Y - 34, Pengubah.Dikurangi, Orientasi.Vertikal)
            TBDaftarQuote.BackColor = Color.White
            If tersimpan = False Then
                segarkan2()
            End If
            tersimpan = False
            teredit = False
        End If
    End Sub

    Private Sub TampilkanKeQuoologiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MITampil.Click
        TampilkankeQuoologi()
    End Sub

    Private Sub BHapus_Click(sender As Object, e As EventArgs) Handles BHapus.Click
        Dim dihapusasal As List(Of String) = New List(Of String)
        Dim dihapusquote As List(Of String) = New List(Of String)
        For Each item As Integer In TBDaftarQuote.SelectedIndices
            dihapusasal.Add(asalquote2.Item(item))
            dihapusquote.Add(quote2.Item(item))
        Next

        For Each item As String In dihapusasal
            asalquote2.Remove(item)
        Next
        For Each item As String In dihapusquote
            quote2.Remove(item)
        Next
        segarkan()
    End Sub

    Public Sub segarkan()
        TBDaftarQuote.Items.Clear()
        For Each item As String In quote2
            TBDaftarQuote.Items.Add(item)
            Application.DoEvents()
        Next
    End Sub

    Private Sub segarkan2()
        TBDaftarQuote.Items.Clear()
        For Each item As String In quote
            TBDaftarQuote.Items.Add(item)
            Application.DoEvents()
        Next
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        tersimpan = True
        Dim a As Integer = 0
        System.IO.File.Delete(AlamatTampungan + "\quote_fav.dft")
        For Each item As String In TBDaftarQuote.Items
            Form_Quote.TulisQuoteFav(item, asalquote.Item(a))
            a += 1
        Next
        ToastNotification.Show(Me, "Quote Favorit telah diperbarui", eToastPosition.BottomCenter)
        Timer1.Enabled = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        TBDaftarQuote.SelectionMode = SelectionMode.One
        GeserKontrol(BHapus, BTutup.Location.Y + 34, Pengubah.Ditambah, Orientasi.Vertikal)
        Application.DoEvents()
        GeserKontrol(BSimpan, BTutup.Location.Y + 34, Pengubah.Ditambah, Orientasi.Vertikal)
        Application.DoEvents()
        GeserKontrol(BTutup, BTutup.Location.Y + 34, Pengubah.Ditambah, Orientasi.Vertikal)
        Application.DoEvents()
        BTutup.Text = "Tutup"
        GeserKontrol(BTutup, BTutup.Location.Y - 34, Pengubah.Dikurangi, Orientasi.Vertikal)
        Application.DoEvents()
        TBDaftarQuote.BackColor = Color.White
        tersimpan = False
        teredit = False
        Timer1.Enabled = False
    End Sub

    Private Sub Form_Kesukaan_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If FirstRun = True And Form_Handle.kesukaansudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berfungsi untuk melihat daftar quote yang difavoritkan oleh pengguna", 7000, eToastPosition.MiddleCenter)
            Timer2.Interval = 5000
            Form_Handle.kesukaansudah = True
        End If
        Timer2.Enabled = True
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        ToastNotification.Show(Me, "Klik kanan daftar quote favorit dan pilih Ubah untuk pergi ke mode Ubah", 5000, eToastPosition.BottomCenter)
        Timer2.Enabled = False
    End Sub
End Class