﻿Imports DevComponents.DotNetBar

Public Class Form_Cari
    Inherits Metro.MetroForm
    Dim indeks As Integer = 0
    Dim ke2 As Integer = 0
    Dim banyak As Integer = 0
    Public datacari As List(Of String) = New List(Of String)
    Public data2cari As List(Of String) = New List(Of String)
    Dim a As Integer = 1
    Public tekscari As List(Of String) = New List(Of String)
    Dim banyakcari As Byte
    Dim katapertama As String
    Dim spesial As Boolean = False
    Public dataquote As List(Of String) = New List(Of String)
    Public dataquoteasal As List(Of String) = New List(Of String)
    Public fileReader As System.IO.StreamReader

    Sub tampil(ByVal naik As Boolean)
        If naik = True Then
            indeks += 1
            If indeks > datacari.Count Then
                indeks = 1
            End If
            TBDataKe.Text = indeks
        Else
            indeks -= 1
            If indeks = 0 Then
                indeks = datacari.Count
            End If
            TBDataKe.Text = indeks
        End If
        Application.DoEvents()
        Quoologi.hidequoologi()
        Try
            Form_Quote.TBQuotes.Text = datacari.Item(indeks - 1)
            Form_Quote.Label1.Text = dataquoteasal(data2cari.Item(indeks - 1)) + " - Quoologi"
            Application.DoEvents()
        Catch ex As ArgumentOutOfRangeException
            ToastNotification.Show(Me, "Data ke " + TBDataKe.Text.ToString + " tidak ada", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        End Try
        Application.DoEvents()
        TentukanBaris()
        Form_Quote.Label2.Text = TBDataKe.Text.ToString + " - " + datacari.Count.ToString
        Quoologi.showquoologi()
    End Sub

    Sub ManageDataCari()
        dataquote.Clear()
        dataquoteasal.Clear()
        If CBQuoteTerpakai.Checked = True Then
            For Each Data As String In DaftarQuote
                dataquote.Add(Data)
            Next
            For Each Data As String In DaftarAlamatQuote
                dataquoteasal.Add(Data)
            Next
        Else
            dataquote = MuatDataQuote(CLBQuote.CheckedItems)(0)
            dataquoteasal = MuatDataQuote(CLBQuote.CheckedItems)(1)
        End If
    End Sub

    Public Sub cari()
        datacari.Clear()
        tekscari.Clear()
        banyak = 0
        banyakcari = 0
        ke2 = 0
        a = 0
        data2cari.Clear()
        ManageDataCari()

        For Each baris As String In TextBox1.Lines
            If CBCaseSenv.Checked = False Then
                tekscari.Add(baris.ToLower)
            Else
                tekscari.Add(baris)
            End If
        Next

        banyakcari = tekscari.Count

        If banyakcari = 1 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And datacari.Contains(dataquote(ke2)) = False Then
                    banyak += 1
                    datacari.Add(dataquote(ke2))
                    data2cari.Add(ke2)
                    TBDataKe.Enabled = True
                    TBDataKe.Text = 1
                End If
                ke2 += 1
            Next
        ElseIf banyakcari = 2 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) And datacari.Contains(dataquote(ke2)) = False Then
                    banyak += 1
                    datacari.Add(dataquote(ke2))
                    data2cari.Add(ke2)
                    TBDataKe.Enabled = True
                    TBDataKe.Text = 1
                End If
                ke2 += 1
            Next
        ElseIf banyakcari = 3 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) And baris.Contains(tekscari.Item(2)) And datacari.Contains(dataquote(ke2)) = False Then
                    banyak += 1
                    datacari.Add(dataquote(ke2))
                    data2cari.Add(ke2)
                    TBDataKe.Enabled = True
                    TBDataKe.Text = 1
                End If
                ke2 += 1
            Next
        ElseIf banyakcari = 4 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) And baris.Contains(tekscari.Item(2)) And baris.Contains(tekscari.Item(3)) And datacari.Contains(dataquote(ke2)) = False Then
                    banyak += 1
                    datacari.Add(dataquote(ke2))
                    data2cari.Add(ke2)
                    TBDataKe.Enabled = True
                    TBDataKe.Text = 1
                End If
                ke2 += 1
            Next
        ElseIf banyakcari = 5 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) And baris.Contains(tekscari.Item(2)) And baris.Contains(tekscari.Item(3)) And baris.Contains(tekscari.Item(4)) And datacari.Contains(dataquote(ke2)) = False Then
                    banyak += 1
                    datacari.Add(dataquote(ke2))
                    data2cari.Add(ke2)
                    TBDataKe.Enabled = True
                    TBDataKe.Text = 1
                End If
                ke2 += 1
            Next
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles BCari.Click
        cari()
        Dim kk As String = """" + TBTeks.Text + """"
        If LRiwayat.Text = "Riwayat Pencarian" Then
            LRiwayat.Text = """" + TBTeks.Text + """"
        ElseIf LRiwayat.Text.Contains(kk) = False Then
            LRiwayat.Text = LRiwayat.Text + " | " + """" + TBTeks.Text + """"
        End If
        ToastNotification.Show(Me, "Ditemukan " + banyak.ToString, eToastPosition.BottomCenter)
        If banyak >= 1 Then
            BLanjut.Enabled = True
            BSBelum.Enabled = True
            BDaftar.Enabled = True
        Else
            BLanjut.Enabled = False
            BSBelum.Enabled = False
            BDaftar.Enabled = False
        End If
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles BLanjut.Click
        If spesial = True Then
            Form_Quote.TBQuotes.Text = "4A696BC991206BC991CF8520C68565D0BFC991C9BEC2B22074CF856CCF8573206D65D0BF6369D0BF74C991692064C991D0BF2067C991207065D0BF6765D0BF206B6568696CC991D0BF67C991D0BFC2B8206369D0BF74C991696CC9916820C9916BCF852070C99164C9912077C9916B74CF8520D2AFC991D0BF67207465CF81C99174C2B82073C991C9917420C9916BCF85207369C991702064C991D0BF206BC991CF85206ACF8567C991207369C991702E2E2E2E2EC2B820C9916BCF85206AC9916D69D0BFC2B8206974CF8520C9916BC991D0BF20C991C685C99164692E2E2E2E20"
            Form_Quote.Label1.Text = "Aziz Nur Ariffianto - Quoologi"
            TentukanBaris()
            Form_Quote.TBQuotes.ScrollBars = ScrollBars.Vertical
            Quoologi.ShowQuoologi()
            Exit Sub
        End If
        If IsNumeric(TBDataKe.Text) = True Then
        ElseIf TBDataKe.Text <> "" Then
            ToastNotification.Show(Me, """" + TBDataKe.Text + """ bukan angka", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        ElseIf TBDataKe.Text = "" Then
            ToastNotification.Show(Me, TBDataKe.Text + "Data ke tidak boleh kosong", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        End If
        Form_Quote.Label2.Visible = True
        tampil(True)
    End Sub

    Private Sub TBDataKe_TextChanged(sender As Object, e As EventArgs) Handles TBDataKe.TextChanged
        Try
            If TBDataKe.Text > datacari.Count Then
                Highlighter1.FocusHighlightColor = Validator.eHighlightColor.Red
            Else
                Highlighter1.FocusHighlightColor = Validator.eHighlightColor.Blue
                If TBDataKe.Text <> indeks Then
                    BDaftar.Text = " Tampil"
                    BDaftar.Symbol = ""
                Else
                    BDaftar.Text = " Daftar"
                    BDaftar.Symbol = ""
                End If
            End If
        Catch ex As InvalidCastException
            Highlighter1.FocusHighlightColor = Validator.eHighlightColor.Red
        End Try
    End Sub

    Private Sub TBTeks_TextChanged(sender As Object, e As EventArgs) Handles TBTeks.TextChanged
        TextBox1.Clear()
        For Each huruf As Char In TBTeks.Text
            If huruf <> "," Then
                TextBox1.Text += huruf
            Else
                TextBox1.Text += Environment.NewLine
            End If
        Next
        If TBTeks.Text = "" Then
            BCari.Enabled = False
        Else
            BCari.Enabled = True
        End If
        Timer1.Dispose()
        Timer1.Enabled = True
        Timer1.Interval = 500
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim ada As Boolean
        tekscari.Clear()
        ManageDataCari()
        For Each baris As String In TextBox1.Lines
            If CBCaseSenv.Checked = False Then
                tekscari.Add(baris.ToLower)
            Else
                tekscari.Add(baris)
            End If
        Next

        banyakcari = tekscari.Count

        If banyakcari = 1 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) Then
                    ada = True
                    Exit For
                Else
                    ada = False
                End If
            Next
        ElseIf banyakcari = 2 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) Then
                    ada = True
                    Exit For
                Else
                    ada = False
                End If
            Next
        ElseIf banyakcari = 3 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) And baris.Contains(tekscari.Item(2)) Then
                    ada = True
                    Exit For
                Else
                    ada = False
                End If
            Next
        ElseIf banyakcari = 4 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) And baris.Contains(tekscari.Item(2)) And baris.Contains(tekscari.Item(3)) Then
                    ada = True
                    Exit For
                Else
                    ada = False
                End If
            Next
        ElseIf banyakcari = 5 Then
            For Each baris As String In dataquote
                If CBCaseSenv.Checked = False Then
                    baris = baris.ToLower
                End If
                If baris.Contains(tekscari.Item(0)) And baris.Contains(tekscari.Item(1)) And baris.Contains(tekscari.Item(2)) And baris.Contains(tekscari.Item(3)) And baris.Contains(tekscari.Item(4)) Then
                    ada = True
                    Exit For
                Else
                    ada = False
                End If
            Next
        End If

        If ada = False And TBTeks.Text <> "" And (TBTeks.Text.Contains("aziz") Or TBTeks.Text.Contains("Aziz") Or TBTeks.Text.Contains("AZIZ")) = False Then
            Highlighter1.FocusHighlightColor = Validator.eHighlightColor.Red
        ElseIf ada = True Or TBTeks.Text = "" And (TBTeks.Text.Contains("aziz") Or TBTeks.Text.Contains("Aziz") Or TBTeks.Text.Contains("AZIZ")) = False Then
            Highlighter1.FocusHighlightColor = Validator.eHighlightColor.Blue
        ElseIf TBTeks.Text.Contains("aziz") Or TBTeks.Text.Contains("Aziz") Or TBTeks.Text.Contains("AZIZ") And TBTeks.Text <> "" Then
            Highlighter1.FocusHighlightColor = Validator.eHighlightColor.Orange
            spesial = True
            BLanjut.Enabled = True
        End If
        Timer1.Dispose()
        Timer1.Enabled = False
    End Sub

    Private Sub TBDataKe_Keydown(sender As Object, e As KeyEventArgs) Handles TBDataKe.KeyDown
        If e.KeyCode.ToString = "Return" Then
            BLanjut.PerformClick()
        End If
    End Sub

    Private Sub TBCari_Keydown(sender As Object, e As KeyEventArgs) Handles TBTeks.KeyDown
        If e.KeyCode.ToString = "Return" Then
            BCari.PerformClick()
        ElseIf e.KeyCode.ToString = "Escape" Then
            Me.Close()
        End If
    End Sub

    Private Sub Cari_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Form_Quote.TBQuotes.ScrollBars = ScrollBars.None
        Dispose()
    End Sub

    Private Sub Cari_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim folder As IO.FileInfo() = Form_PengaturanUtama.pathquote.GetFiles("*.qot")
        For Each Form_PengaturanUtama.berkas In folder
            CLBQuote.Items.Add(Form_PengaturanUtama.berkas.Name)
        Next
    End Sub

    Private Sub Cari_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        TBTeks.Focus()
        If FirstRun = True And Form_Handle.carisudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian Pencarian Quote" + Environment.NewLine + Environment.NewLine + "Jendela ini berfungsi untuk mencari quote" + Environment.NewLine + "dengan teks yang dimasukan oleh pengguna", 7000, eToastPosition.MiddleCenter)
            Form_Handle.carisudah = True
        End If
    End Sub

    Private Sub CBCaseSenv_CheckedChanged(sender As Object, e As EventArgs) Handles CBCaseSenv.CheckedChanged
        If TBTeks.Text.Length > 1 Then
            Timer1.Dispose()
            Timer1.Enabled = True
            Timer1.Interval = 500
        End If
    End Sub

    Private Sub CheckBoxX1_CheckedChanged(sender As Object, e As EventArgs) Handles CBQuoteTerpakai.CheckedChanged
        If CBQuoteTerpakai.Checked = False Then
            Me.Height = 269
            AnimateWindow(CLBQuote.Handle, 500, AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_BAWAH Or AnimateWindowFlags.AKTIF)
        Else
            AnimateWindow(CLBQuote.Handle, 500, AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_ATAS Or AnimateWindowFlags.HILANG)
            Me.Height = 141
        End If
    End Sub

    Private Sub CLBQuote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CLBQuote.MouseDoubleClick
        Dim diubah As Boolean
        If CLBQuote.CheckedItems.Count = CLBQuote.Items.Count Then
            diubah = False
        Else
            diubah = True
        End If
        For n As Integer = 0 To CLBQuote.Items.Count - 1
            CLBQuote.SetItemChecked(n, diubah)
        Next
    End Sub

    Private Sub BSBelum_Click(sender As Object, e As EventArgs) Handles BSBelum.Click
        If spesial = True Then
            Form_Quote.TBQuotes.Text = "4A696BC991206BC991CF8520C68565D0BFC991C9BEC2B22074CF856CCF8573206D65D0BF6369D0BF74C991692064C991D0BF2067C991207065D0BF6765D0BF206B6568696CC991D0BF67C991D0BFC2B8206369D0BF74C991696CC9916820C9916BCF852070C99164C9912077C9916B74CF8520D2AFC991D0BF67207465CF81C99174C2B82073C991C9917420C9916BCF85207369C991702064C991D0BF206BC991CF85206ACF8567C991207369C991702E2E2E2E2EC2B820C9916BCF85206AC9916D69D0BFC2B8206974CF8520C9916BC991D0BF20C991C685C99164692E2E2E2E20"
            Form_Quote.Label1.Text = "Aziz Nur Ariffianto - Quoologi"
            TentukanBaris()
            Form_Quote.TBQuotes.ScrollBars = ScrollBars.Vertical
            Quoologi.showquoologi()
            Exit Sub
        End If
        If IsNumeric(TBDataKe.Text) = True Then
        ElseIf TBDataKe.Text <> "" Then
            ToastNotification.Show(Me, """" + TBDataKe.Text + """ bukan angka", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        ElseIf TBDataKe.Text = "" Then
            ToastNotification.Show(Me, TBDataKe.Text + "Data ke tidak boleh kosong", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        End If
        Form_Quote.Label2.Visible = True
        tampil(False)
    End Sub

    Private Sub BDaftar_Click(sender As Object, e As EventArgs) Handles BDaftar.Click
        If BDaftar.Text = " Daftar" Then
            Form_HasilPencarian.Show()
            Application.DoEvents()
            ToastNotification.Show(Form_HasilPencarian.TBDaftarQuote, "Memuat...", 50000, eToastPosition.MiddleCenter)
            For Each Data As String In datacari
                Application.DoEvents()
                Form_HasilPencarian.TBDaftarQuote.Items.Add(Data)
                Application.DoEvents()
            Next
            ToastNotification.Close(Form_HasilPencarian.TBDaftarQuote)
            Form_HasilPencarian.LBaris.Text = Form_HasilPencarian.TBDaftarQuote.Items.Count
        Else
            Application.DoEvents()
            Quoologi.hidequoologi()
            Try
                Form_Quote.TBQuotes.Text = datacari.Item(TBDataKe.Text - 1)
                Form_Quote.Label1.Text = dataquoteasal(data2cari.Item(TBDataKe.Text - 1)) + " - Quoologi"
                Application.DoEvents()
            Catch ex As ArgumentOutOfRangeException
                ToastNotification.Show(Me, "Data ke " + TBDataKe.Text.ToString + " tidak ada", Nothing, 4000, eToastGlowColor.Red)
                Exit Sub
            End Try
            Application.DoEvents()
            TentukanBaris()
            Form_Quote.Label2.Text = TBDataKe.Text.ToString + " - " + datacari.Count.ToString
            Quoologi.ShowQuoologi()
            BDaftar.Text = " Daftar"
            BDaftar.Symbol = ""
        End If
    End Sub

    Private Sub LRiwayat_Click(sender As Object, e As EventArgs) Handles LRiwayat.MouseHover
        ToolTip1.SetToolTip(LRiwayat, LRiwayat.Text)
    End Sub
End Class