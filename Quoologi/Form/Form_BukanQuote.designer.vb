﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_BukanQuote
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.TBBukan = New System.Windows.Forms.ListBox()
        Me.LNilai = New DevComponents.DotNetBar.LabelX()
        Me.LBaris = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.LPencegah = New DevComponents.DotNetBar.LabelX()
        Me.SuspendLayout()
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonX1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonX1.Location = New System.Drawing.Point(628, 510)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(75, 23)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX1, New DevComponents.DotNetBar.SuperTooltipInfo("Tutup", "", "Klik untuk menutup jendela ini.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.ButtonX1.TabIndex = 7
        Me.ButtonX1.Text = "Tutup"
        '
        'TBBukan
        '
        Me.TBBukan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBBukan.BackColor = System.Drawing.Color.White
        Me.TBBukan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBBukan.ForeColor = System.Drawing.Color.Black
        Me.TBBukan.FormattingEnabled = True
        Me.TBBukan.HorizontalScrollbar = True
        Me.TBBukan.Location = New System.Drawing.Point(5, 5)
        Me.TBBukan.Name = "TBBukan"
        Me.TBBukan.Size = New System.Drawing.Size(698, 496)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBBukan, New DevComponents.DotNetBar.SuperTooltipInfo("Bukan Quote", "", "Berisi teks yang tidak terpilih menjadi quote.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(148, 58)))
        Me.TBBukan.TabIndex = 8
        '
        'LNilai
        '
        Me.LNilai.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LNilai.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LNilai.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LNilai.ForeColor = System.Drawing.Color.Black
        Me.LNilai.Location = New System.Drawing.Point(5, 510)
        Me.LNilai.Name = "LNilai"
        Me.LNilai.Size = New System.Drawing.Size(75, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LNilai, New DevComponents.DotNetBar.SuperTooltipInfo("Indikator", "", "A, B" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "A berisi data ""baris ke"" dari data terbuang terpilih yang berasal dari ba" & _
            "gian data di QOT Maker." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "B berisi banyaknya karakter dari data terbuang yang t" & _
            "erpilih.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(184, 137)))
        Me.LNilai.TabIndex = 9
        '
        'LBaris
        '
        Me.LBaris.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBaris.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LBaris.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LBaris.ForeColor = System.Drawing.Color.Black
        Me.LBaris.Location = New System.Drawing.Point(517, 509)
        Me.LBaris.Name = "LBaris"
        Me.LBaris.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LBaris.Size = New System.Drawing.Size(75, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBaris, New DevComponents.DotNetBar.SuperTooltipInfo("Banyak Baris", "", "Menunjukan banyaknya baris di dalam data terbuang.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.LBaris.TabIndex = 10
        '
        'LabelX1
        '
        Me.LabelX1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(598, 509)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LabelX1.Size = New System.Drawing.Size(24, 23)
        Me.LabelX1.TabIndex = 11
        Me.LabelX1.Text = "baris"
        '
        'LPencegah
        '
        Me.LPencegah.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LPencegah.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LPencegah.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LPencegah.Font = New System.Drawing.Font("Consolas", 8.25!)
        Me.LPencegah.ForeColor = System.Drawing.Color.Black
        Me.LPencegah.Location = New System.Drawing.Point(86, 510)
        Me.LPencegah.Name = "LPencegah"
        Me.LPencegah.Size = New System.Drawing.Size(425, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LPencegah, New DevComponents.DotNetBar.SuperTooltipInfo("Alasan", "", "Teks yang ditampilkan disini menunjukan alasan kenapa teks yang terpilih tidak di" & _
            "anggap sebagai quote.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(187, 84)))
        Me.LPencegah.TabIndex = 12
        Me.LPencegah.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'DaftarQuote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BottomLeftCornerSize = 3
        Me.BottomRightCornerSize = 3
        Me.CancelButton = Me.ButtonX1
        Me.ClientSize = New System.Drawing.Size(708, 538)
        Me.Controls.Add(Me.LPencegah)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.LBaris)
        Me.Controls.Add(Me.LNilai)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.TBBukan)
        Me.MinimumSize = New System.Drawing.Size(500, 500)
        Me.Name = "DaftarQuote"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bukan Quote"
        Me.TopLeftCornerSize = 3
        Me.TopMost = True
        Me.TopRightCornerSize = 3
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBBukan As System.Windows.Forms.ListBox
    Friend WithEvents LNilai As DevComponents.DotNetBar.LabelX
    Friend WithEvents LBaris As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents LPencegah As DevComponents.DotNetBar.LabelX
End Class
