﻿Imports DevComponents.DotNetBar
Imports UIQuoologi.Gambar

Public Class Form_Partner
    Inherits Metro.MetroForm
    Public status As Byte

    Private Sub partner_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        AnimateWindow(Me.Handle, 500, AnimateWindowFlags.MENYATU Or AnimateWindowFlags.HILANG)
        If status = 1 Then
            Form_Tentang.Show()
            Form_Tentang.statuspartner = 0
        End If
        Dispose()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Process.Start("www.amikompurwokerto.ac.id")
        status = 0
        Me.Close()
    End Sub

    Private Sub ReflectionLabel3_Click(sender As Object, e As EventArgs) Handles ReflectionLabel3.MouseEnter
        ReflectionLabel3.ForeColor = Color.SteelBlue
        ReflectionLabel3.Font = New Font("Century Gothic", 10, FontStyle.Underline)
    End Sub

    Private Sub ReflectionLabel3_MouseLeave(sender As Object, e As EventArgs) Handles ReflectionLabel3.MouseLeave
        ReflectionLabel3.ForeColor = Color.Black
        ReflectionLabel3.Font = New Font("Century Gothic", 10, FontStyle.Regular)
    End Sub

    Private Sub ReflectionLabel2_MouseEnter(sender As Object, e As EventArgs) Handles ReflectionLabel1.MouseEnter
        ReflectionLabel1.ForeColor = Color.SteelBlue
        ReflectionLabel1.Font = New Font("Century Gothic", 12, FontStyle.Underline)
    End Sub

    Private Sub ReflectionLabel2_MouseLeave(sender As Object, e As EventArgs) Handles ReflectionLabel1.MouseLeave
        ReflectionLabel1.ForeColor = Color.Black
        ReflectionLabel1.Font = New Font("Century Gothic", 12, FontStyle.Regular)
    End Sub

    Private Sub ReflectionLabel1_Click(sender As Object, e As EventArgs)
        Process.Start("www.amikompurwokerto.ac.id")
        status = 0
        Me.Close()
    End Sub

    Private Sub ReflectionLabel3_Click_1(sender As Object, e As EventArgs) Handles ReflectionLabel3.Click
        Process.Start("www.facebook.com/lppm.amikompurwokerto")
        status = 0
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Form_Partner_Load(sender As Object, e As EventArgs) Handles Me.Load
        PictureBox1.Image = AmbilGambar("logo_amikom")
        AnimateWindow(Me.Handle, 500, AnimateWindowFlags.MENYATU Or AnimateWindowFlags.AKTIF)
    End Sub

    Private Sub Form_Partner_MouseClick(sender As Object, e As MouseEventArgs) Handles Me.MouseClick
        Me.Close()
    End Sub
End Class