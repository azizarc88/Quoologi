﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_UpdateQuote
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LBQuotesBaru = New System.Windows.Forms.ListBox()
        Me.LBQuotesLama = New System.Windows.Forms.ListBox()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.BPerbarui = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuspendLayout()
        '
        'LBQuotesBaru
        '
        Me.LBQuotesBaru.BackColor = System.Drawing.Color.White
        Me.LBQuotesBaru.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBQuotesBaru.ForeColor = System.Drawing.Color.Black
        Me.LBQuotesBaru.FormattingEnabled = True
        Me.LBQuotesBaru.Location = New System.Drawing.Point(178, 42)
        Me.LBQuotesBaru.Name = "LBQuotesBaru"
        Me.LBQuotesBaru.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBQuotesBaru.Size = New System.Drawing.Size(156, 119)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBQuotesBaru, New DevComponents.DotNetBar.SuperTooltipInfo("Daftar Quote Baru", "", "Berisi daftar quote yang tidak tersedia di komputer Pengguna.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(183, 61)))
        Me.LBQuotesBaru.TabIndex = 39
        '
        'LBQuotesLama
        '
        Me.LBQuotesLama.BackColor = System.Drawing.Color.White
        Me.LBQuotesLama.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBQuotesLama.ForeColor = System.Drawing.Color.Black
        Me.LBQuotesLama.FormattingEnabled = True
        Me.LBQuotesLama.Location = New System.Drawing.Point(7, 42)
        Me.LBQuotesLama.Name = "LBQuotesLama"
        Me.LBQuotesLama.Size = New System.Drawing.Size(156, 119)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBQuotesLama, New DevComponents.DotNetBar.SuperTooltipInfo("Berkas Quote Lama", "", "Berisi daftar quote yang tersedia di komputer Pengguna.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(180, 61)))
        Me.LBQuotesLama.TabIndex = 38
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(177, 14)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(132, 23)
        Me.LabelX8.TabIndex = 37
        Me.LabelX8.Text = "Berkas Quotes baru:"
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(7, 14)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(132, 23)
        Me.LabelX7.TabIndex = 36
        Me.LabelX7.Text = "Berkas Quotes lama:"
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(176, 172)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(75, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, New DevComponents.DotNetBar.SuperTooltipInfo("Batal", "", "Klik untuk membatalkan pengupdatean quote.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BBatal.TabIndex = 41
        Me.BBatal.Text = "Batal"
        '
        'BPerbarui
        '
        Me.BPerbarui.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BPerbarui.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BPerbarui.Location = New System.Drawing.Point(260, 172)
        Me.BPerbarui.Name = "BPerbarui"
        Me.BPerbarui.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.BPerbarui.Size = New System.Drawing.Size(75, 23)
        Me.BPerbarui.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BPerbarui, New DevComponents.DotNetBar.SuperTooltipInfo("Perbarui", "", "Klik untuk mengunduh quote baru yang terpilih.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BPerbarui.Symbol = ""
        Me.BPerbarui.SymbolSize = 13.0!
        Me.BPerbarui.TabIndex = 40
        Me.BPerbarui.Text = "Perbarui"
        '
        'Form_UpdateQuote
        '
        Me.AcceptButton = Me.BBatal
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BPerbarui
        Me.ClientSize = New System.Drawing.Size(341, 201)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BPerbarui)
        Me.Controls.Add(Me.LBQuotesBaru)
        Me.Controls.Add(Me.LBQuotesLama)
        Me.Controls.Add(Me.LabelX8)
        Me.Controls.Add(Me.LabelX7)
        Me.DoubleBuffered = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form_UpdateQuote"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update Quotes"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LBQuotesBaru As System.Windows.Forms.ListBox
    Friend WithEvents LBQuotesLama As System.Windows.Forms.ListBox
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BPerbarui As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
End Class
