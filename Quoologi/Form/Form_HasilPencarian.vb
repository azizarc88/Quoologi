﻿Imports DevComponents.DotNetBar

Public Class Form_HasilPencarian
    Inherits Metro.MetroForm
    Dim a As String = Nothing
    Dim b As String = Nothing

    Private Sub TBDaftarQuote_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles TBDaftarQuote.MouseDoubleClick
        Application.DoEvents()
        Quoologi.hidequoologi()
        Try
            Form_Quote.TBQuotes.Text = a
            Form_Quote.Label1.Text = Form_Cari.dataquoteasal(Form_Cari.data2cari.Item(TBDaftarQuote.SelectedIndex)) + " - Quoologi"
            Application.DoEvents()
        Catch ex As ArgumentOutOfRangeException
            ToastNotification.Show(Me, "Galat", Nothing, 4000, eToastGlowColor.Red)
            Exit Sub
        End Try
        Application.DoEvents()
        TentukanBaris()
        Form_Quote.Label2.Visible = True
        Form_Quote.Label2.Text = (TBDaftarQuote.SelectedIndex + 1).ToString + " - " + TBDaftarQuote.Items.Count.ToString
        Quoologi.showquoologi()
    End Sub

    Private Sub TBDaftarQuote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TBDaftarQuote.SelectedIndexChanged
        LAlasan.Text = "Alasan"
        a = TBDaftarQuote.SelectedItem.ToString
        Try
            If Form_Cari.CBCaseSenv.Checked = False Then
                b = a.ToLower
            Else
                b = a
            End If
        Catch ex As Exception
            LAlasan.Text = "Alasan"
            Exit Sub
        End Try
        If Form_Cari.CBCaseSenv.Checked = False Then
            For Each Data As String In Form_Cari.tekscari
                If b.Contains(Data.ToLower) Then
                    If LAlasan.Text = "Alasan" Then
                        LAlasan.Text = Data
                    Else
                        LAlasan.Text = LAlasan.Text + " | " + Data
                    End If
                End If
            Next
        Else
            For Each Data As String In Form_Cari.tekscari
                If b.Contains(Data) Then
                    If LAlasan.Text = "Alasan" Then
                        LAlasan.Text = Data
                    Else
                        LAlasan.Text = LAlasan.Text + " | " + Data
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub BTutup_Click(sender As Object, e As EventArgs) Handles BTutup.Click
        Me.Close()
        Me.Dispose()
    End Sub

    Private Sub Form_HasilPencarian_Load(sender As Object, e As EventArgs) Handles Me.Load
        If FirstRun = True And Form_Handle.hasilpsudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian Hasil Pencarian" + Environment.NewLine + Environment.NewLine + "Jendela ini berfungsi untuk melihat daftar quote dari hasil pencarian", 7000, eToastPosition.BottomCenter)
            Form_Handle.hasilpsudah = True
        End If
    End Sub
End Class