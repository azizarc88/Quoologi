﻿Imports DevComponents.DotNetBar.Metro

Public Class Form_Pesan
    Inherits MetroForm
    Private Sub Pesan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Application.DoEvents()
        Form_Handle.psanmuncul = False
        Application.DoEvents()
        CircularProgress1.IsRunning = False
        Application.DoEvents()
    End Sub

    Private Sub Pesan_Load(sender As Object, e As EventArgs) Handles Me.Load
        CircularProgress1.Value = 1
        Application.DoEvents()
        CircularProgress1.IsRunning = True
        Application.DoEvents()

        If FirstRun = True Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
        End If
    End Sub

    Private Sub Pesan_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Application.DoEvents()
        Form_Handle.psanmuncul = True
        Application.DoEvents()
    End Sub
End Class