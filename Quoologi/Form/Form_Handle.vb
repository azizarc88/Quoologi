﻿Imports Microsoft.Win32
Imports System.Text
Imports System.ComponentModel
Imports System.Environment
Imports DevComponents.DotNetBar
Imports System.Runtime.InteropServices
Imports System.Threading
Imports UIQuoologi.UserInterface
Imports System.Net
Imports System.Collections.Specialized

Public Class Form_Handle
    Inherits Metro.MetroForm
    Dim program2 As String = Nothing
    Public manual, spengaturan, ada, updatestatustampil, berubah, bug, dariupdatestatus, psanmuncul, favmuncul, TutorialSudah, perbaikidicancel As Boolean
    Public gagala As Boolean = True
    Public gagalq As Boolean = True
    Public BukanQuoteSudah, carisudah, hasilpsudah, kesukaansudah, pengaturanfbsudah, pengaturanutamasudah, pengaturangayasudah, programterkecualisudah, tambahprogramsudah, updatequotesudah, updatestatussudah As Boolean
    Public penentutemaaktif, timer, fixjendela, stat, StatusFBKomen, status_2 As Byte
    Public cekq As Byte = 0
    Public ceka As Byte = 0
    Public asal, last, statusfb, quotesem, komentarsem, kartag As String
    Public semver_a, versek, verbaru, totalqot, x, y As Integer
    Public qotlama As List(Of String) = New List(Of String)
    Public qotbaru As List(Of String) = New List(Of String)
    Public linkqotbaru As List(Of String) = New List(Of String)
    Public posisix, posisiy As Integer
    Public trdq As Thread
    Public NamaProgramPT As String
    Public Info As String

    Public hapusspasi, ubahspasi, hapuskarnon, izinkankomentar, izinkanedit, tambahpetik, komensebelumquote As Boolean
    Dim ikonskrang As Byte
    Dim nyalakan As Boolean

    Private Sub Form_Handle_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        Application.ExitThread()
    End Sub

    Private Sub Form_Handle_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dispose()
        If AdaUpQuote = 2 Then
            AdaUpQuote = 0
        End If

        If AdaUpProgram = 2 Then
            AdaUpProgram = 0
        End If

        If FirstRun = True Then
            FirstRun = False
        End If
        SimpanPengaturan()
    End Sub

    Public Sub UbahcsKomen(ByVal status As Boolean)
        Form_PengaturanFacebook.lagirefresh = True
        If status = True Then
            Form_PengaturanFacebook.Simpan_Pengaturan(True)
            Form_PengaturanFacebook.Close()
            Form_PengaturanFacebook.Height = 416
            Form_PengaturanFacebook.GroupPanel4.Height = 198
            Form_PengaturanFacebook.Show()
            System.Threading.Thread.Sleep(300)
            Form_PengaturanFacebook.CBIzinKomen.Checked = True
        Else
            Form_PengaturanFacebook.Simpan_Pengaturan(True)
            Form_PengaturanFacebook.Close()
            Form_PengaturanFacebook.Show()
        End If
        Form_PengaturanFacebook.lagirefresh = False
    End Sub

    Private Sub Form_Handle_Load(sender As Object, e As EventArgs) Handles Me.Load
        NotifyIcon1.Icon = IkonAnimasi(0)
        If AlamatInstall = Nothing Then
            MessageBoxEx.Show("Belum pasang Quoologi?, Supaya fitur aplikasi kami dapat berjalan dengan semestinya, kami mewajibkan Anda untuk menginstall Quoologi terlebih dahulu. (kesalahan: 010101)", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Me.Close()
            Exit Sub
        End If

        NotifyIcon1.ContextMenu = ContextMenu1
        MessageBoxEx.AntiAlias = True
        MessageBoxEx.UseSystemLocalizedString = True
        BacaPengaturan(BagianPenyimpanan.Umum)
        BacaPengaturan(BagianPenyimpanan.Pencegah)
        BacaPengaturan(BagianPenyimpanan.ProgramTerkecuali)
        BacaPengaturan(BagianPenyimpanan.QuoteTerpakai)
        BacaPengaturan(BagianPenyimpanan.TeksWelcome)

        Me.Hide()
        set_timer()

        If FirstRun = True Then
            NotifyIcon1.ShowBalloonTip(7000)
        End If

        Form_Quote.TBQuotes.Text = TeksMulai
        TentukanBaris()
        If TeksMulai <> "" Then
            welcomenote = True
            Form_Quote.Show()
        Else
            welcomenote = False
            Form_Quote.Show()
            Form_Quote.Hide()
        End If
        MuatDataPT()
        Form_Quote.TFirst.Enabled = True
        defoffsethor = OffsetHor
        MuatQuoteTampil()
        Form_Quote.Timer3.Enabled = True
        TNotif.Enabled = True
        TNotif.Start()

        Dim defaultWebProxy As IWebProxy = WebRequest.DefaultWebProxy
        defaultWebProxy.Credentials = CredentialCache.DefaultCredentials
        WebRequest.DefaultWebProxy = defaultWebProxy

        WriteAnalytics()

        Me.Hide()
    End Sub

    Public Sub segarkan()
        Form_PengaturanUtama.Close()
        Form_PengaturanUtama.Dispose()
        Form_PengaturanUtama.Show()
    End Sub

    Private Sub TSetiap_Tick(sender As Object, e As EventArgs) Handles TSetiap.Tick
        If OtomatisHentikan = True Then
            Dim proses As List(Of String) = New List(Of String)
            Dim hentikan As Boolean = False
            For Each p As Process In Process.GetProcesses()
                proses.Add(p.ProcessName.ToString.ToLower)
            Next

            For Each program As String In ProgramTerkecuali
                If proses.Contains(program.ToLower.Replace(".exe", "")) Then
                    hentikan = True
                    If program2 <> program Then
                        manual = False
                    End If
                    program2 = program
                End If
            Next

            If hentikan = True And MIHentikan.Checked = False And manual = False Then
                MIHentikan.Checked = True
                AnimasiIkon(False)
                MIHentikan.Text = "Hentikan (" + program2 + ")"
                Exit Sub
            ElseIf hentikan = True And MIHentikan.Checked = True And manual = False Then
                Exit Sub
            ElseIf hentikan = False And MIHentikan.Checked = True And manual = False Then
                MIHentikan.Checked = False
                AnimasiIkon(True)
                MIHentikan.Text = "Hentikan"
            End If
        End If

        Dim sem As String = CekAdaAppPT()

        If listcari.Count <> 0 Then
            CekJudulSama(sem)
        End If

        If penentutemaaktif = 1 Then
            If judulberubah = True Then
                listcari.Clear()
            End If

            If sem <> "salah" And listcari.Count = 0 Then
                AmbilJudul(sem)
                If JudulKata.Count <> 0 And Not judulsem.Contains("Aziz Nur Ariffianto") Then
                    listcari = CariDataPT(JudulKata)
                ElseIf judulsem.Contains("Aziz Nur Ariffianto") And My.Computer.Name <> "IZARCCOMPUTER" Then
                    If QuoteSudahTampil.Contains("51756F6F6C6F6769") Then
                        listcari = CariDataPT(JudulKata)
                    Else
                        Form_Quote.TBQuotes.Text = "Hai.... >.< :P"
                        Form_Quote.Label1.Text = "Aziz Nur Ariffianto - Quoologi"
                        asal = "Aziz Nur Ariffianto"
                        TentukanBaris()
                        last = Form_Quote.TBQuotes.Text
                        Form_Quote.Label2.Visible = False
                        ShowQuoologi()
                        TulisSudahTampil("51756F6F6C6F6769")
                        penentutemaaktif = 0
                        Exit Sub
                    End If
                End If
            ElseIf sem = "salah" Then
                listcari.Clear()
            End If
        End If

        If QuoteTerpakai.Count = 0 Then
            Exit Sub
        End If

        If listcari.Count <> 0 And SudahTampilSemua = False And penentutemaaktif = 1 Then
            Form_Quote.BInfo.Visible = True
            Dim a As String = Nothing
            TampilkanHasilTema()
            For Each kata As String In JudulKata
                If Form_Quote.TBQuotes.Text.ToLower.Contains(kata.ToLower) Then
                    a += Kapitalkan(kata, True) + " "
                End If
            Next
            Info = "Anda sedang membuka program """ + NamaProgramPT + """" + Environment.NewLine + "Dengan tema antara lain: " + a
            penentutemaaktif = 0
            Exit Sub
        End If

        penentutemaaktif = 1
        read_q()
        Form_Quote.Label2.Visible = False
        ShowQuoologi()
        QuoteMuncul = True
        NotifyIcon1.Text = "Klik dua kali untuk menampilkan Quote."
    End Sub

    Private Sub TSelama_Tick(sender As Object, e As EventArgs) Handles TSelama.Tick
        If QuoteMuncul = True Then
            HideQuoologi()
            TPosisiMouse.Enabled = False
        End If
    End Sub

    Public Sub AnimasiIkon(ByVal nyala As Boolean)
        If nyala = True Then
            nyalakan = True
            ikonskrang = 10
            TAnimasiAktif.Enabled = True
        Else
            nyalakan = False
            ikonskrang = 0
            TAnimasiAktif.Enabled = True
        End If
    End Sub

    Private Sub NotifyIcon1_Click(sender As Object, e As EventArgs) Handles NotifyIcon1.Click
        Me.Hide()
        Me.WindowState = FormWindowState.Minimized
        Me.ShowInTaskbar = False
    End Sub

    Private Sub NotifyIcon1_DoubleClick(sender As Object, e As EventArgs) Handles NotifyIcon1.DoubleClick
        If Not (MIHentikan.Checked = True And TTampil.Enabled = True) Then
            Application.DoEvents()
            ikonskrang = 0
            TTampil.Enabled = True
            Application.DoEvents()
        End If
        If DaftarQuote.Count = 0 Then
            Exit Sub
        End If
        TSelama.Enabled = False
        HideQuoologi()
        Form_Quote.Label2.Visible = False
        read_q()
        ShowQuoologi()
    End Sub

    Private Sub HentikanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIHentikan.Click
        manual = True
        If MIHentikan.Checked = False Then
            MIHentikan.Checked = True
            AnimasiIkon(False)
            TSetiap.Enabled = False
        ElseIf MIHentikan.Checked = True Then
            MIHentikan.Checked = False
            If MIHentikan.Text.Contains(".exe") Then
                MIHentikan.Text = "Hentikan"
            End If
            AnimasiIkon(True)
            TSetiap.Enabled = True
        End If
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIKeluar.Click
        If MessageBoxEx.Show("Apakah Anda yakin ingin keluar ?", "Quoologi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Me.Close()
            Application.ExitThread()
        End If
    End Sub

    Private Sub PengaturanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIPengaturanUtama.Click
        TSetiap.Enabled = False
        Form_PengaturanUtama.Show()
        If QuoteMuncul = True Then
            HideQuoologi()
        Else
        End If
    End Sub

    Private Sub Form_Handle_MouseHover(sender As Object, e As EventArgs) Handles Me.MouseHover
        Me.Hide()
    End Sub

    Private Sub QuotesTerakhirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIQuoteTerakhir.Click
        Form_Quote.Label2.Visible = False
        ShowQuoologi()
    End Sub

    Private Sub TentangToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MITentang.Click
        status_2 = 0
        Form_Tentang.Show()
    End Sub

    Private Sub BuatBerkasQOTToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIQotMaker.Click
        Form_PembuatQuote.Show()
        Form_PembuatQuote.status = 0
        statusqot = 0
    End Sub

    Private Sub CariQuotesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MICari.Click
        Form_Cari.Show()
        If QuoteMuncul = True Then
            HideQuoologi()
        End If
    End Sub

    Private Sub PengaturanQuoologiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIKustomisasiQuoologi.Click
        Form_PengaturanGaya.Show()
    End Sub

    Private Sub PartnerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIPartner.Click
        Form_Partner.Show()
    End Sub

    Private Sub ProgramToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIUpdateP.Click
        Form_PengaturanUtama.BCPembaruanQuoologi.PerformClick()
    End Sub

    Private Sub QuootesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIUpdateQ.Click
        Form_PengaturanUtama.BCekQuotesBaru.PerformClick()
    End Sub

    Private Sub CBYes_Executed(sender As Object, e As EventArgs) Handles CBYes.Executed
        My.Computer.FileSystem.DeleteFile(AlamatDokumen + "\Quotes\" + Form_PembuatQuote.TBFileName.Text + ".qot", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        SimpanKeBerkas(Form_PembuatQuote.TBFileName.Text, Form_PembuatQuote.LBData.Items)
        ToastNotification.Show(Form_PembuatQuote, "File QOT telah ditimpa dengan nama " + Form_PembuatQuote.TBFileName.Text + ".qot", eToastPosition.MiddleCenter)
        TaskDialog.Close()
    End Sub

    Private Sub CBNo_Executed(sender As Object, e As EventArgs) Handles CBNo.Executed
        SimpanKeBerkas(Form_PembuatQuote.TBFileName.Text, Form_PembuatQuote.LBData.Items, True)
        ToastNotification.Show(Form_PembuatQuote, "Data telah ditambahkan ke dalam file" + Form_PembuatQuote.TBFileName.Text + ".qot", eToastPosition.MiddleCenter)
        TaskDialog.Close()
    End Sub

    Private Sub LoginToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MILoginFB.Click
        Form_Pesan.Show()
        Application.DoEvents()
        If CekKoneksi() = False Then
            Exit Sub
        End If
        Application.DoEvents()
        Form_LoginFacebook.WebBrowser1.Navigate("https://m.facebook.com/messages")
        Application.DoEvents()
        waitforpageload()
        Application.DoEvents()
        MsgBox(Form_LoginFacebook.WebBrowser1.DocumentTitle)
        If Not Form_LoginFacebook.WebBrowser1.DocumentTitle.Contains("Masuk") Then
            Form_Pesan.Close()
            psan("Facebook", eTaskDialogIcon.Information2, "Sudah Terhubung", "Akun Facebook Anda sudah terhubung, sehingga tidak perlu login kembali.", eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Blue)
            Form_LoginFacebook.Dispose()
        Else
            Form_Pesan.Close()
            Form_LoginFacebook.Show()
        End If
    End Sub

    Private Sub LogoutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MILogoutFB.Click
        Form_Pesan.Show()
        If CekKoneksi() = False Then
            Exit Sub
        End If
        Form_LoginFacebook.Logout()
        Form_LoginFacebook.Dispose()
    End Sub

    Private Sub TCekUpdate_Tick(sender As Object, e As EventArgs) Handles TCekUpdate.Tick
        timer += 1
        If timer = 10 And spengaturan = False Then
            updating.cek_pengaturan_a_t()
        End If
        TCekUpdate.Enabled = False
    End Sub

    Private Sub PengaturanToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles MIPengaturanFB.Click
        Form_PengaturanFacebook.Show()
    End Sub

    Private Sub QuoteFavoritToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIFavorit.Click
        Form_Kesukaan.Show()
        favmuncul = True
    End Sub

    Private Sub TPosisiMouse_Tick(sender As Object, e As EventArgs) Handles TPosisiMouse.Tick
        Dim posisi As System.Drawing.Point = MousePosition
        Dim lokasi As System.Drawing.Point = Form_Quote.Location
        Dim ukuran As System.Drawing.Size = Form_Quote.Size
        If QuoteMuncul = False Then
            TPosisiMouse.Enabled = False
        End If

        If Not (posisi.X >= lokasi.X And posisi.X <= lokasi.X + ukuran.Width And posisi.Y >= lokasi.Y And posisi.Y <= lokasi.Y + ukuran.Height) Then
            Timer1.Enabled = True
        End If

        'If Not (posisi.X >= lokasi.X + Form_Quote.TBQuotes.Location.X And posisi.X <= lokasi.X + ukuran.Width And posisi.Y >= lokasi.Y + Form_Quote.TBQuotes.Location.Y And posisi.Y <= lokasi.Y + Form_Quote.TBQuotes.Height + Form_Quote.TBQuotes.Location.Y) Then
        'TampilDariOpa()
        'End If
    End Sub

    Private Sub MITampilQuote_Click(sender As Object, e As EventArgs) Handles MITampilQuote.Click
        If DaftarQuote.Count = 0 Then
            Exit Sub
        End If
        TSelama.Enabled = False
        HideQuoologi()
        Form_Quote.Label2.Visible = False
        read_q()
        ShowQuoologi()
    End Sub

    Private Sub MenuItem1_Click(sender As Object, e As EventArgs) Handles MenuItem1.Click
        psan("Facebook", eTaskDialogIcon.Information2, "Kebijakan dan Privasi", "Kami tidak akan menyimpan data pribadi Anda mulai dari data username (seperti surel, no. telp., ID akun, nama akun), dan password Anda pada server kami." + Environment.NewLine + Environment.NewLine + "Kami hanya akan menyimpan data username pada media penyimpanan lokal Anda guna memberikan layanan yang lebih baik bagi Anda", eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Blue)
    End Sub

    Private Sub TAnimasi_Tick(sender As Object, e As EventArgs) Handles TAnimasiAktif.Tick
        If nyalakan = True Then
            NotifyIcon1.Icon = New Icon(IkonAnimasi(ikonskrang), 32, 32)
            ikonskrang -= 1
            If ikonskrang = 0 Then
                NotifyIcon1.Icon = New Icon(IkonAnimasi(ikonskrang), 32, 32)
                TAnimasiAktif.Enabled = False
            End If
        Else
            NotifyIcon1.Icon = New Icon(IkonAnimasi(ikonskrang), 32, 32)
            ikonskrang += 1
            If ikonskrang = 11 Then
                TAnimasiAktif.Enabled = False
            End If
        End If
    End Sub

    Dim stt As Boolean = False

    Private Sub TTampil_Tick(sender As Object, e As EventArgs) Handles TTampil.Tick
        If stt = False Then
            NotifyIcon1.Icon = New Icon(IkonAnimasi(ikonskrang), 32, 32)
            ikonskrang += 1
            If ikonskrang = 5 Then
                stt = True
                ikonskrang = 6
            End If
        ElseIf stt = True Then
            ikonskrang -= 1
            NotifyIcon1.Icon = New Icon(IkonAnimasi(ikonskrang), 32, 32)
            If ikonskrang = 0 Then
                stt = False
                TTampil.Enabled = False
            End If
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        TampilDariOpa()
        Timer1.Enabled = False
    End Sub

    Private WithEvents rs As RESTService = New RESTService()
    Public DaftarNotif As List(Of String) = New List(Of String)
    Private Sync As Boolean = False

    Private Sub TNotif_Tick(sender As Object, e As EventArgs) Handles TNotif.Tick
        If Not Sync Then
            rs.MakeRequestAsync("ambil_notif.php", Nothing, "ambil_notif.php")
            Sync = True
        End If
    End Sub

    Private Sub GetNotifComplete(sender As Object, e As RequestCompleteEventArgs) Handles rs.RequestComplete
        If e.dBagian.Equals("ambil_notif.php") Then
            Dim kode As String = rs.GetString(e.dResponse, "kode")
            Dim hasil As String = rs.GetString(e.dResponse, "hasil")
            If hasil.Equals("sukses") Then
                If Not QuoteMuncul Then
                    If Not DaftarNotif.Contains(kode) Then
                        Try
                            My.Computer.Audio.Play(AlamatDokumen + "\Sounds\" + BerkasSuara)
                        Catch ex As Exception
                        End Try

                        Form_Quote.TBQuotes.Text = rs.GetString(e.dResponse, "message")
                        Form_Quote.Label1.Text = "Pemberitahuan"
                        asal = "Pemberitahuan"
                        TentukanBaris()
                        last = Form_Quote.TBQuotes.Text

                        Form_Quote.Label2.Visible = False
                        ShowQuoologi()
                        QuoteMuncul = True
                        DaftarNotif.Add(kode)
                    End If
                End If
            End If
            Sync = False
        End If
    End Sub

    Private Sub WriteAnalytics()
        Dim data As NameValueCollection
        data = New NameValueCollection()
        data("machine") = My.Computer.Name
        data("username") = Environment.UserName
        data("config") = Pengaturan.ConfigText
        data("version") = My.Application.Info.Version.ToString
        rs.MakeRequestAsync("analytics.php", data, "analytics.php")
    End Sub
End Class