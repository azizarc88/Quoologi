﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pesan
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LInfo = New DevComponents.DotNetBar.Controls.ReflectionLabel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.CircularProgress1 = New DevComponents.DotNetBar.Controls.CircularProgress()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuspendLayout()
        '
        'LInfo
        '
        '
        '
        '
        Me.LInfo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LInfo.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.LInfo.Dock = System.Windows.Forms.DockStyle.Left
        Me.LInfo.Font = New System.Drawing.Font("Century Gothic", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LInfo.Location = New System.Drawing.Point(0, 0)
        Me.LInfo.Name = "LInfo"
        Me.LInfo.Size = New System.Drawing.Size(324, 142)
        Me.LInfo.TabIndex = 0
        Me.LInfo.Text = "HARAP TUNGGU...."
        '
        'CircularProgress1
        '
        Me.CircularProgress1.AnimationSpeed = 50
        '
        '
        '
        Me.CircularProgress1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CircularProgress1.Location = New System.Drawing.Point(286, 53)
        Me.CircularProgress1.Name = "CircularProgress1"
        Me.CircularProgress1.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Donut
        Me.CircularProgress1.ProgressColor = System.Drawing.Color.Black
        Me.CircularProgress1.ProgressTextFormat = ""
        Me.CircularProgress1.Size = New System.Drawing.Size(39, 30)
        Me.CircularProgress1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP
        Me.CircularProgress1.TabIndex = 1
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!)
        Me.LabelX1.Location = New System.Drawing.Point(2, 126)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(362, 15)
        Me.SuperTooltip1.SetSuperTooltip(Me.LabelX1, New DevComponents.DotNetBar.SuperTooltipInfo("Informasi", "", "Berisi indikator hal yang dilakukan oleh Quoologi untuk proses tertentu.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.LabelX1.TabIndex = 2
        Me.LabelX1.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'Form_Pesan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(366, 142)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.CircularProgress1)
        Me.Controls.Add(Me.LInfo)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Form_Pesan"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pesan"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LInfo As DevComponents.DotNetBar.Controls.ReflectionLabel
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents CircularProgress1 As DevComponents.DotNetBar.Controls.CircularProgress
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
End Class
