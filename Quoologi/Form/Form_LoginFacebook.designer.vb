﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_LoginFacebook
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.TBUser = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TBPass = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX2 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX3 = New DevComponents.DotNetBar.ButtonX()
        Me.Highlighter1 = New DevComponents.DotNetBar.Validator.Highlighter()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuspendLayout()
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Location = New System.Drawing.Point(20, 168)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(250, 192)
        Me.WebBrowser1.TabIndex = 5
        Me.WebBrowser1.Visible = False
        '
        'TBUser
        '
        Me.TBUser.AutoSelectAll = True
        Me.TBUser.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBUser.Border.Class = "TextBoxBorder"
        Me.TBUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBUser.ForeColor = System.Drawing.Color.Black
        Me.Highlighter1.SetHighlightOnFocus(Me.TBUser, True)
        Me.TBUser.Location = New System.Drawing.Point(106, 10)
        Me.TBUser.Name = "TBUser"
        Me.TBUser.PreventEnterBeep = True
        Me.TBUser.Size = New System.Drawing.Size(172, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBUser, New DevComponents.DotNetBar.SuperTooltipInfo("E-mail / No. Telp", "", "Tempat untuk mengisikan username / e-mail / no. telp pengguna ketika ingin login " & _
            "ke Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(250, 71)))
        Me.TBUser.TabIndex = 0
        '
        'TBPass
        '
        Me.TBPass.AutoSelectAll = True
        Me.TBPass.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBPass.Border.Class = "TextBoxBorder"
        Me.TBPass.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBPass.ButtonCustom.Shortcut = DevComponents.DotNetBar.eShortcut.CtrlShiftT
        Me.TBPass.ButtonCustom.Text = "A"
        Me.TBPass.ButtonCustom.Visible = True
        Me.TBPass.ForeColor = System.Drawing.Color.Black
        Me.Highlighter1.SetHighlightOnFocus(Me.TBPass, True)
        Me.TBPass.Location = New System.Drawing.Point(106, 38)
        Me.TBPass.Name = "TBPass"
        Me.TBPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TBPass.PreventEnterBeep = True
        Me.TBPass.Size = New System.Drawing.Size(172, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBPass, New DevComponents.DotNetBar.SuperTooltipInfo("Password", "", "Tempat untuk mengisikan password untuk login Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(217, 61)))
        Me.TBPass.TabIndex = 1
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(4, 9)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(92, 23)
        Me.LabelX1.TabIndex = 6
        Me.LabelX1.Text = "E-mail / No. Telp:"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(4, 37)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 7
        Me.LabelX2.Text = "Password:"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.Location = New System.Drawing.Point(204, 70)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(73, 23)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX1, New DevComponents.DotNetBar.SuperTooltipInfo("Login", "", "Klik untuk me-login-kan akun Facebook Anda.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 13.0!
        Me.ButtonX1.TabIndex = 3
        Me.ButtonX1.Text = "Login"
        '
        'ButtonX2
        '
        Me.ButtonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX2.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonX2.Location = New System.Drawing.Point(126, 70)
        Me.ButtonX2.Name = "ButtonX2"
        Me.ButtonX2.Size = New System.Drawing.Size(72, 23)
        Me.ButtonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX2, New DevComponents.DotNetBar.SuperTooltipInfo("Batal", "", "Klik untuk keluar jendela Login Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(169, 58)))
        Me.ButtonX2.Symbol = ""
        Me.ButtonX2.SymbolSize = 13.0!
        Me.ButtonX2.TabIndex = 2
        Me.ButtonX2.Text = "Batal"
        '
        'ButtonX3
        '
        Me.ButtonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX3.Location = New System.Drawing.Point(2, 70)
        Me.ButtonX3.Name = "ButtonX3"
        Me.ButtonX3.Size = New System.Drawing.Size(118, 23)
        Me.ButtonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX3, New DevComponents.DotNetBar.SuperTooltipInfo("Kebijakan dan Privasi", "", "Klik untuk melihat kebijakan kami dalam menjaga privasi Anda tentang semua hal ya" & _
            "ng berkaitan dengan penggunaan fitur Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(224, 84)))
        Me.ButtonX3.TabIndex = 4
        Me.ButtonX3.Text = "Kebijakan dan Privasi"
        '
        'Highlighter1
        '
        Me.Highlighter1.ContainerControl = Me
        '
        'Form_LoginFacebook
        '
        Me.AcceptButton = Me.ButtonX1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonX2
        Me.ClientSize = New System.Drawing.Size(282, 97)
        Me.Controls.Add(Me.ButtonX3)
        Me.Controls.Add(Me.ButtonX2)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.LabelX2)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.TBPass)
        Me.Controls.Add(Me.TBUser)
        Me.Controls.Add(Me.WebBrowser1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "Form_LoginFacebook"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login Facebook"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents TBUser As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TBPass As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonX2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonX3 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Highlighter1 As DevComponents.DotNetBar.Validator.Highlighter
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
End Class
