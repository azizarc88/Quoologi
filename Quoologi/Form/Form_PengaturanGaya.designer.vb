﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PengaturanGaya
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_PengaturanGaya))
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.GroupPanel3 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.ButtonX3 = New DevComponents.DotNetBar.ButtonX()
        Me.TBSelamatDatang = New System.Windows.Forms.TextBox()
        Me.ButtonX2 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.BKunci = New DevComponents.DotNetBar.ButtonX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.BSimpan = New DevComponents.DotNetBar.ButtonX()
        Me.GroupPanel2 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.CBTTombol = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.LTransparasi = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.STransparasi = New DevComponents.DotNetBar.Controls.Slider()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.CPBBJendela = New DevComponents.DotNetBar.ColorPickerButton()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.CPBHJendela = New DevComponents.DotNetBar.ColorPickerButton()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.IILebarJendela = New DevComponents.Editors.IntegerInput()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.IITinggiJendela = New DevComponents.Editors.IntegerInput()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel5 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LabelX14 = New DevComponents.DotNetBar.LabelX()
        Me.IILebarQuotes = New DevComponents.Editors.IntegerInput()
        Me.LabelX15 = New DevComponents.DotNetBar.LabelX()
        Me.IITinggiQuotes = New DevComponents.Editors.IntegerInput()
        Me.LabelX16 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.CPBBQuotes = New DevComponents.DotNetBar.ColorPickerButton()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.CPBHQuotes = New DevComponents.DotNetBar.ColorPickerButton()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.IIUkuranHQuotes = New DevComponents.Editors.IntegerInput()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.CBHQuotes = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.GroupPanel1.SuspendLayout()
        Me.GroupPanel3.SuspendLayout()
        Me.GroupPanel2.SuspendLayout()
        CType(Me.IILebarJendela, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IITinggiJendela, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPanel5.SuspendLayout()
        CType(Me.IILebarQuotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IITinggiQuotes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IIUkuranHQuotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel1.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.GroupPanel3)
        Me.GroupPanel1.Controls.Add(Me.ButtonX2)
        Me.GroupPanel1.Controls.Add(Me.ButtonX1)
        Me.GroupPanel1.Controls.Add(Me.BKunci)
        Me.GroupPanel1.Controls.Add(Me.BBatal)
        Me.GroupPanel1.Controls.Add(Me.BSimpan)
        Me.GroupPanel1.Controls.Add(Me.GroupPanel2)
        Me.GroupPanel1.Controls.Add(Me.GroupPanel5)
        Me.GroupPanel1.Location = New System.Drawing.Point(6, 0)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(340, 387)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 0
        Me.GroupPanel1.Text = "Kustomisasi Quoologi"
        '
        'GroupPanel3
        '
        Me.GroupPanel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel3.BackColor = System.Drawing.Color.White
        Me.GroupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel3.Controls.Add(Me.ButtonX3)
        Me.GroupPanel3.Controls.Add(Me.TBSelamatDatang)
        Me.GroupPanel3.Location = New System.Drawing.Point(8, 249)
        Me.GroupPanel3.Name = "GroupPanel3"
        Me.GroupPanel3.Size = New System.Drawing.Size(318, 74)
        '
        '
        '
        Me.GroupPanel3.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel3.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel3.Style.BackColorGradientAngle = 90
        Me.GroupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderBottomWidth = 1
        Me.GroupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderLeftWidth = 1
        Me.GroupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderRightWidth = 1
        Me.GroupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderTopWidth = 1
        Me.GroupPanel3.Style.CornerDiameter = 4
        Me.GroupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel3.Style.CornerTypeBottomLeft = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel3.Style.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel3.TabIndex = 15
        Me.GroupPanel3.Text = "Teks Selamat Datang"
        Me.GroupPanel3.TitleImagePosition = DevComponents.DotNetBar.eTitleImagePosition.Center
        '
        'ButtonX3
        '
        Me.ButtonX3.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX3.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX3.Location = New System.Drawing.Point(294, 0)
        Me.ButtonX3.Name = "ButtonX3"
        Me.ButtonX3.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(4, 0, 4, 0)
        Me.ButtonX3.Size = New System.Drawing.Size(20, 50)
        Me.ButtonX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX3, New DevComponents.DotNetBar.SuperTooltipInfo("Teks Asali", "", "Klik untuk mengatur ulang teks selamat datang.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.ButtonX3.Symbol = ""
        Me.ButtonX3.SymbolSize = 13.0!
        Me.ButtonX3.TabIndex = 16
        '
        'TBSelamatDatang
        '
        Me.TBSelamatDatang.AcceptsReturn = True
        Me.TBSelamatDatang.BackColor = System.Drawing.Color.White
        Me.TBSelamatDatang.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBSelamatDatang.ForeColor = System.Drawing.Color.Black
        Me.TBSelamatDatang.Location = New System.Drawing.Point(3, 4)
        Me.TBSelamatDatang.Multiline = True
        Me.TBSelamatDatang.Name = "TBSelamatDatang"
        Me.TBSelamatDatang.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBSelamatDatang.Size = New System.Drawing.Size(285, 46)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBSelamatDatang, New DevComponents.DotNetBar.SuperTooltipInfo("Teks Selamat Datang", "", "Teks disini merupakan teks yang akan ditampilkan tiap kali Quoologi dijalankan." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & _
            "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Kosongkan jika tidak ingin menampilkan Quoologi saat pertama kali dijalankan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBSelamatDatang.TabIndex = 0
        '
        'ButtonX2
        '
        Me.ButtonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonX2.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX2.FocusCuesEnabled = False
        Me.ButtonX2.Location = New System.Drawing.Point(182, 337)
        Me.ButtonX2.Name = "ButtonX2"
        Me.ButtonX2.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.ButtonX2.Size = New System.Drawing.Size(20, 20)
        Me.ButtonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX2, New DevComponents.DotNetBar.SuperTooltipInfo("Tema Selanjutnya", "", "Klik untuk melihat gaya tema selanjutnya.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.ButtonX2.Symbol = ""
        Me.ButtonX2.SymbolSize = 13.0!
        Me.ButtonX2.TabIndex = 14
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Enabled = False
        Me.ButtonX1.FocusCuesEnabled = False
        Me.ButtonX1.Location = New System.Drawing.Point(131, 337)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.ButtonX1.Size = New System.Drawing.Size(20, 20)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX1, New DevComponents.DotNetBar.SuperTooltipInfo("Tema Sebelumnya", "", "Klik untuk melihat gaya tema sebelumnya.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 13.0!
        Me.ButtonX1.TabIndex = 13
        '
        'BKunci
        '
        Me.BKunci.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BKunci.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BKunci.Checked = True
        Me.BKunci.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BKunci.FocusCuesEnabled = False
        Me.BKunci.Location = New System.Drawing.Point(157, 337)
        Me.BKunci.Name = "BKunci"
        Me.BKunci.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BKunci.Size = New System.Drawing.Size(20, 20)
        Me.BKunci.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BKunci, New DevComponents.DotNetBar.SuperTooltipInfo("Kunci", "", "Tombol ini berfungsi untuk menyamakan nilai ukuran pada kedua bagian, Quotes dan " & _
            "Jendela.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BKunci.Symbol = ""
        Me.BKunci.SymbolSize = 13.0!
        Me.BKunci.TabIndex = 11
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BBatal.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(8, 334)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(84, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, New DevComponents.DotNetBar.SuperTooltipInfo("Batal", "Jalan Pintas: Esc", "Tombol ini berfungsi untuk membatalkan perubahan terhadap Quoologi.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BBatal.Symbol = ""
        Me.BBatal.SymbolSize = 13.0!
        Me.BBatal.TabIndex = 0
        Me.BBatal.Text = " Batal | Esc"
        '
        'BSimpan
        '
        Me.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSimpan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BSimpan.Location = New System.Drawing.Point(236, 334)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5)
        Me.BSimpan.Size = New System.Drawing.Size(90, 23)
        Me.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BSimpan, New DevComponents.DotNetBar.SuperTooltipInfo("Simpan", "Jalan Pintas: F5", "Tombol ini berfugsi untuk menerapkan perubahan dan menyimpan pengaturan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BSimpan.Symbol = ""
        Me.BSimpan.SymbolSize = 13.0!
        Me.BSimpan.TabIndex = 12
        Me.BSimpan.Text = " Simpan | F5"
        '
        'GroupPanel2
        '
        Me.GroupPanel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel2.BackColor = System.Drawing.Color.White
        Me.GroupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel2.Controls.Add(Me.CBTTombol)
        Me.GroupPanel2.Controls.Add(Me.LTransparasi)
        Me.GroupPanel2.Controls.Add(Me.LabelX2)
        Me.GroupPanel2.Controls.Add(Me.LabelX12)
        Me.GroupPanel2.Controls.Add(Me.STransparasi)
        Me.GroupPanel2.Controls.Add(Me.LabelX10)
        Me.GroupPanel2.Controls.Add(Me.CPBBJendela)
        Me.GroupPanel2.Controls.Add(Me.LabelX11)
        Me.GroupPanel2.Controls.Add(Me.CPBHJendela)
        Me.GroupPanel2.Controls.Add(Me.LabelX7)
        Me.GroupPanel2.Controls.Add(Me.IILebarJendela)
        Me.GroupPanel2.Controls.Add(Me.LabelX6)
        Me.GroupPanel2.Controls.Add(Me.IITinggiJendela)
        Me.GroupPanel2.Controls.Add(Me.LabelX4)
        Me.GroupPanel2.Controls.Add(Me.LabelX5)
        Me.GroupPanel2.Location = New System.Drawing.Point(8, 108)
        Me.GroupPanel2.Name = "GroupPanel2"
        Me.GroupPanel2.Size = New System.Drawing.Size(318, 136)
        '
        '
        '
        Me.GroupPanel2.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel2.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel2.Style.BackColorGradientAngle = 90
        Me.GroupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderBottomWidth = 1
        Me.GroupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderLeftWidth = 1
        Me.GroupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderRightWidth = 1
        Me.GroupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderTopWidth = 1
        Me.GroupPanel2.Style.CornerDiameter = 4
        Me.GroupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel2.Style.CornerTypeBottomLeft = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel2.Style.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel2.TabIndex = 2
        Me.GroupPanel2.Text = "Jendela"
        Me.GroupPanel2.TitleImagePosition = DevComponents.DotNetBar.eTitleImagePosition.Center
        '
        'CBTTombol
        '
        '
        '
        '
        Me.CBTTombol.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBTTombol.Location = New System.Drawing.Point(6, 86)
        Me.CBTTombol.Name = "CBTTombol"
        Me.CBTTombol.Size = New System.Drawing.Size(301, 23)
        Me.CBTTombol.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBTTombol, New DevComponents.DotNetBar.SuperTooltipInfo("Selalu Tampilkan Ikon", "", "Ketika opsi ini terpilih, maka tombol favorit, salin, cari, dan facebook akan sel" & _
            "alu ditampilkan pada saat penampilan Quote." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Jika tidak, tombol tersebut akan " & _
            "otomatis muncul dan otomatis hilang.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(249, 109)))
        Me.CBTTombol.TabIndex = 74
        Me.CBTTombol.Text = "Selalu tampilkan tombol (Salin, Cari, Facebook, dll)"
        '
        'LTransparasi
        '
        Me.LTransparasi.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LTransparasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LTransparasi.ForeColor = System.Drawing.Color.Black
        Me.LTransparasi.Location = New System.Drawing.Point(91, 57)
        Me.LTransparasi.Name = "LTransparasi"
        Me.LTransparasi.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LTransparasi.Size = New System.Drawing.Size(20, 23)
        Me.LTransparasi.TabIndex = 73
        Me.LTransparasi.Text = "###"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(113, 57)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(13, 23)
        Me.LabelX2.TabIndex = 72
        Me.LabelX2.Text = "%"
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(4, 57)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(90, 23)
        Me.LabelX12.Symbol = ""
        Me.LabelX12.SymbolSize = 14.0!
        Me.LabelX12.TabIndex = 70
        Me.LabelX12.Text = " Transparasi:"
        '
        'STransparasi
        '
        Me.STransparasi.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.STransparasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.STransparasi.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.STransparasi.ForeColor = System.Drawing.Color.Black
        Me.STransparasi.LabelVisible = False
        Me.STransparasi.Location = New System.Drawing.Point(128, 60)
        Me.STransparasi.Minimum = 5
        Me.STransparasi.Name = "STransparasi"
        Me.STransparasi.Size = New System.Drawing.Size(180, 20)
        Me.STransparasi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.STransparasi, New DevComponents.DotNetBar.SuperTooltipInfo("Transparasi quotes", "", "Slider ini berfungsi untuk menetapkan besar kepadatan pada jendela Quotes." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Semak" & _
            "in sedikit nilainya maka jendela Quotes akan semakin pudar, jika semakin banyak " & _
            "maka akan semakin padat.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.STransparasi.TabIndex = 71
        Me.STransparasi.Value = 100
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(196, 28)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(74, 23)
        Me.LabelX10.Symbol = ""
        Me.LabelX10.SymbolSize = 1.0!
        Me.LabelX10.TabIndex = 69
        Me.LabelX10.Text = "W. Backgrnd:"
        '
        'CPBBJendela
        '
        Me.CPBBJendela.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.CPBBJendela.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.CPBBJendela.Image = CType(resources.GetObject("CPBBJendela.Image"), System.Drawing.Image)
        Me.CPBBJendela.Location = New System.Drawing.Point(271, 28)
        Me.CPBBJendela.Name = "CPBBJendela"
        Me.CPBBJendela.SelectedColorImageRectangle = New System.Drawing.Rectangle(2, 2, 12, 12)
        Me.CPBBJendela.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.CPBBJendela.Size = New System.Drawing.Size(36, 23)
        Me.CPBBJendela.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CPBBJendela, New DevComponents.DotNetBar.SuperTooltipInfo("Warna Background Jendela", "", "Pada bagian ini berfungsi untuk mengatur warna background jendela Quoologi.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CPBBJendela.TabIndex = 9
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(103, 28)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(54, 23)
        Me.LabelX11.Symbol = ""
        Me.LabelX11.SymbolSize = 1.0!
        Me.LabelX11.TabIndex = 67
        Me.LabelX11.Text = "W. Huruf:"
        '
        'CPBHJendela
        '
        Me.CPBHJendela.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.CPBHJendela.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.CPBHJendela.Image = CType(resources.GetObject("CPBHJendela.Image"), System.Drawing.Image)
        Me.CPBHJendela.Location = New System.Drawing.Point(159, 28)
        Me.CPBHJendela.Name = "CPBHJendela"
        Me.CPBHJendela.SelectedColorImageRectangle = New System.Drawing.Rectangle(2, 2, 12, 12)
        Me.CPBHJendela.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.CPBHJendela.Size = New System.Drawing.Size(36, 23)
        Me.CPBHJendela.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CPBHJendela, New DevComponents.DotNetBar.SuperTooltipInfo("Warna Huruf Jendela", "", "Pada bagian ini berfungsi untuk mengatur warna huruf teks yang ada pada jendela Q" & _
            "uoologi.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CPBHJendela.TabIndex = 8
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(209, 3)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(18, 23)
        Me.LabelX7.Symbol = ""
        Me.LabelX7.SymbolSize = 13.0!
        Me.LabelX7.TabIndex = 65
        '
        'IILebarJendela
        '
        Me.IILebarJendela.AntiAlias = True
        Me.IILebarJendela.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IILebarJendela.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IILebarJendela.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IILebarJendela.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IILebarJendela.ForeColor = System.Drawing.Color.Black
        Me.IILebarJendela.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center
        Me.IILebarJendela.Location = New System.Drawing.Point(229, 4)
        Me.IILebarJendela.MaxValue = 999999
        Me.IILebarJendela.MinValue = -100
        Me.IILebarJendela.Name = "IILebarJendela"
        Me.IILebarJendela.ShowUpDown = True
        Me.IILebarJendela.Size = New System.Drawing.Size(78, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IILebarJendela, New DevComponents.DotNetBar.SuperTooltipInfo("Ukuran Jendela Horizontal", "", "Bagian ini berfungsi untuk mengatur ukuran horizontal (lebar) dari jendela Quoolo" & _
            "gi.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.IILebarJendela.TabIndex = 7
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(112, 3)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(12, 23)
        Me.LabelX6.Symbol = ""
        Me.LabelX6.SymbolSize = 13.0!
        Me.LabelX6.TabIndex = 63
        '
        'IITinggiJendela
        '
        Me.IITinggiJendela.AntiAlias = True
        Me.IITinggiJendela.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IITinggiJendela.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IITinggiJendela.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IITinggiJendela.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IITinggiJendela.ForeColor = System.Drawing.Color.Black
        Me.IITinggiJendela.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center
        Me.IITinggiJendela.Location = New System.Drawing.Point(126, 4)
        Me.IITinggiJendela.Name = "IITinggiJendela"
        Me.IITinggiJendela.ShowUpDown = True
        Me.IITinggiJendela.Size = New System.Drawing.Size(78, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IITinggiJendela, New DevComponents.DotNetBar.SuperTooltipInfo("Ukuran Jendela Vertikal", "", "Bagian ini berfungsi untuk mengatur ukuran vertikal (tinggi) dari jendela Quoolog" & _
            "i.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.IITinggiJendela.TabIndex = 6
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(4, 28)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(82, 23)
        Me.LabelX4.Symbol = ""
        Me.LabelX4.SymbolSize = 13.0!
        Me.LabelX4.TabIndex = 60
        Me.LabelX4.Text = " Warna:"
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(4, 2)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(110, 23)
        Me.LabelX5.Symbol = ""
        Me.LabelX5.SymbolSize = 13.0!
        Me.LabelX5.TabIndex = 2
        Me.LabelX5.Text = " Ukuran Jendela:"
        '
        'GroupPanel5
        '
        Me.GroupPanel5.BackColor = System.Drawing.Color.White
        Me.GroupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel5.Controls.Add(Me.LabelX14)
        Me.GroupPanel5.Controls.Add(Me.IILebarQuotes)
        Me.GroupPanel5.Controls.Add(Me.LabelX15)
        Me.GroupPanel5.Controls.Add(Me.IITinggiQuotes)
        Me.GroupPanel5.Controls.Add(Me.LabelX16)
        Me.GroupPanel5.Controls.Add(Me.LabelX9)
        Me.GroupPanel5.Controls.Add(Me.CPBBQuotes)
        Me.GroupPanel5.Controls.Add(Me.LabelX8)
        Me.GroupPanel5.Controls.Add(Me.CPBHQuotes)
        Me.GroupPanel5.Controls.Add(Me.LabelX3)
        Me.GroupPanel5.Controls.Add(Me.IIUkuranHQuotes)
        Me.GroupPanel5.Controls.Add(Me.LabelX1)
        Me.GroupPanel5.Controls.Add(Me.CBHQuotes)
        Me.GroupPanel5.Location = New System.Drawing.Point(8, 4)
        Me.GroupPanel5.Name = "GroupPanel5"
        Me.GroupPanel5.Size = New System.Drawing.Size(318, 100)
        '
        '
        '
        Me.GroupPanel5.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel5.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel5.Style.BackColorGradientAngle = 90
        Me.GroupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderBottomWidth = 1
        Me.GroupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderLeftWidth = 1
        Me.GroupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderRightWidth = 1
        Me.GroupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderTopWidth = 1
        Me.GroupPanel5.Style.CornerDiameter = 4
        Me.GroupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel5.Style.CornerTypeBottomLeft = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.Style.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.TabIndex = 1
        Me.GroupPanel5.Text = "Quotes"
        Me.GroupPanel5.TitleImagePosition = DevComponents.DotNetBar.eTitleImagePosition.Center
        '
        'LabelX14
        '
        Me.LabelX14.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX14.ForeColor = System.Drawing.Color.Black
        Me.LabelX14.Location = New System.Drawing.Point(210, 29)
        Me.LabelX14.Name = "LabelX14"
        Me.LabelX14.Size = New System.Drawing.Size(18, 23)
        Me.LabelX14.Symbol = ""
        Me.LabelX14.SymbolSize = 13.0!
        Me.LabelX14.TabIndex = 70
        '
        'IILebarQuotes
        '
        Me.IILebarQuotes.AntiAlias = True
        Me.IILebarQuotes.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IILebarQuotes.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IILebarQuotes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IILebarQuotes.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IILebarQuotes.ForeColor = System.Drawing.Color.Black
        Me.IILebarQuotes.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center
        Me.IILebarQuotes.Location = New System.Drawing.Point(230, 30)
        Me.IILebarQuotes.MaxValue = 999999
        Me.IILebarQuotes.MinValue = -100
        Me.IILebarQuotes.Name = "IILebarQuotes"
        Me.IILebarQuotes.ShowUpDown = True
        Me.IILebarQuotes.Size = New System.Drawing.Size(78, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IILebarQuotes, New DevComponents.DotNetBar.SuperTooltipInfo("Ukuran Quotes Horizontal", "", "Bagian ini berfungsi untuk mengatur ukuran horizontal (lebar) dari Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.IILebarQuotes.TabIndex = 3
        '
        'LabelX15
        '
        Me.LabelX15.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX15.ForeColor = System.Drawing.Color.Black
        Me.LabelX15.Location = New System.Drawing.Point(113, 29)
        Me.LabelX15.Name = "LabelX15"
        Me.LabelX15.Size = New System.Drawing.Size(12, 23)
        Me.LabelX15.Symbol = ""
        Me.LabelX15.SymbolSize = 13.0!
        Me.LabelX15.TabIndex = 68
        '
        'IITinggiQuotes
        '
        Me.IITinggiQuotes.AntiAlias = True
        Me.IITinggiQuotes.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IITinggiQuotes.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IITinggiQuotes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IITinggiQuotes.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IITinggiQuotes.ForeColor = System.Drawing.Color.Black
        Me.IITinggiQuotes.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center
        Me.IITinggiQuotes.Location = New System.Drawing.Point(127, 30)
        Me.IITinggiQuotes.Name = "IITinggiQuotes"
        Me.IITinggiQuotes.ShowUpDown = True
        Me.IITinggiQuotes.Size = New System.Drawing.Size(78, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IITinggiQuotes, New DevComponents.DotNetBar.SuperTooltipInfo("Ukuran Quotes Vertikal", "", "Bagian ini berfungsi untuk mengatur ukuran vertikal (tinggi) dari Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.IITinggiQuotes.TabIndex = 2
        '
        'LabelX16
        '
        Me.LabelX16.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX16.ForeColor = System.Drawing.Color.Black
        Me.LabelX16.Location = New System.Drawing.Point(4, 28)
        Me.LabelX16.Name = "LabelX16"
        Me.LabelX16.Size = New System.Drawing.Size(110, 23)
        Me.LabelX16.Symbol = ""
        Me.LabelX16.SymbolSize = 13.0!
        Me.LabelX16.TabIndex = 66
        Me.LabelX16.Text = " Ukuran Jendela:"
        '
        'LabelX9
        '
        Me.LabelX9.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.ForeColor = System.Drawing.Color.Black
        Me.LabelX9.Location = New System.Drawing.Point(197, 54)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(74, 23)
        Me.LabelX9.Symbol = ""
        Me.LabelX9.SymbolSize = 1.0!
        Me.LabelX9.TabIndex = 64
        Me.LabelX9.Text = "W. Backgrnd:"
        '
        'CPBBQuotes
        '
        Me.CPBBQuotes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.CPBBQuotes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.CPBBQuotes.Image = CType(resources.GetObject("CPBBQuotes.Image"), System.Drawing.Image)
        Me.CPBBQuotes.Location = New System.Drawing.Point(272, 54)
        Me.CPBBQuotes.Name = "CPBBQuotes"
        Me.CPBBQuotes.SelectedColorImageRectangle = New System.Drawing.Rectangle(2, 2, 12, 12)
        Me.CPBBQuotes.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.CPBBQuotes.Size = New System.Drawing.Size(36, 23)
        Me.CPBBQuotes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CPBBQuotes, New DevComponents.DotNetBar.SuperTooltipInfo("Warna Background Quotes", "", "Bagian ini berfungsi untuk mengatur warna background dari Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CPBBQuotes.TabIndex = 5
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(102, 54)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(54, 23)
        Me.LabelX8.Symbol = ""
        Me.LabelX8.SymbolSize = 1.0!
        Me.LabelX8.TabIndex = 62
        Me.LabelX8.Text = "W. Huruf:"
        '
        'CPBHQuotes
        '
        Me.CPBHQuotes.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.CPBHQuotes.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.CPBHQuotes.Image = CType(resources.GetObject("CPBHQuotes.Image"), System.Drawing.Image)
        Me.CPBHQuotes.Location = New System.Drawing.Point(158, 54)
        Me.CPBHQuotes.Name = "CPBHQuotes"
        Me.CPBHQuotes.SelectedColorImageRectangle = New System.Drawing.Rectangle(2, 2, 12, 12)
        Me.CPBHQuotes.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.CPBHQuotes.Size = New System.Drawing.Size(36, 23)
        Me.CPBHQuotes.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CPBHQuotes, New DevComponents.DotNetBar.SuperTooltipInfo("Warna Huruf Quotes", "", "Bagian ini berfungsi untuk mengatur warna pada huruf Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CPBHQuotes.TabIndex = 4
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(4, 54)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(82, 23)
        Me.LabelX3.Symbol = ""
        Me.LabelX3.SymbolSize = 13.0!
        Me.LabelX3.TabIndex = 60
        Me.LabelX3.Text = " Warna:"
        '
        'IIUkuranHQuotes
        '
        Me.IIUkuranHQuotes.AntiAlias = True
        Me.IIUkuranHQuotes.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IIUkuranHQuotes.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IIUkuranHQuotes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IIUkuranHQuotes.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IIUkuranHQuotes.ForeColor = System.Drawing.Color.Black
        Me.IIUkuranHQuotes.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center
        Me.IIUkuranHQuotes.Location = New System.Drawing.Point(261, 4)
        Me.IIUkuranHQuotes.Name = "IIUkuranHQuotes"
        Me.IIUkuranHQuotes.ShowUpDown = True
        Me.IIUkuranHQuotes.Size = New System.Drawing.Size(46, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IIUkuranHQuotes, New DevComponents.DotNetBar.SuperTooltipInfo("Ukuran Huruf", "", "Bagian ini berfungsi untuk mengatur ukuran dari teks Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.IIUkuranHQuotes.TabIndex = 1
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(4, 2)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(82, 23)
        Me.LabelX1.Symbol = ""
        Me.LabelX1.SymbolSize = 13.0!
        Me.LabelX1.TabIndex = 1
        Me.LabelX1.Text = " Gaya Huruf:"
        '
        'CBHQuotes
        '
        Me.CBHQuotes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.CBHQuotes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.CBHQuotes.DisplayMember = "Text"
        Me.CBHQuotes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CBHQuotes.FocusCuesEnabled = False
        Me.CBHQuotes.ForeColor = System.Drawing.Color.Black
        Me.CBHQuotes.FormattingEnabled = True
        Me.CBHQuotes.ItemHeight = 14
        Me.CBHQuotes.Location = New System.Drawing.Point(112, 4)
        Me.CBHQuotes.Name = "CBHQuotes"
        Me.CBHQuotes.Size = New System.Drawing.Size(144, 20)
        Me.CBHQuotes.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.CBHQuotes, New DevComponents.DotNetBar.SuperTooltipInfo("Gaya Huruf", "", "Bagian ini berfungsi untuk mengubah gaya huruf pada teks Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CBHQuotes.TabIndex = 99
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.HoverDelayMultiplier = 5
        '
        'Form_PengaturanGaya
        '
        Me.AcceptButton = Me.BSimpan
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(351, 392)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupPanel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "Form_PengaturanGaya"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Kustomisasi Quoologi"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel3.ResumeLayout(False)
        Me.GroupPanel3.PerformLayout()
        Me.GroupPanel2.ResumeLayout(False)
        CType(Me.IILebarJendela, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IITinggiJendela, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPanel5.ResumeLayout(False)
        CType(Me.IILebarQuotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IITinggiQuotes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IIUkuranHQuotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents GroupPanel5 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents CBHQuotes As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents IIUkuranHQuotes As DevComponents.Editors.IntegerInput
    Friend WithEvents CPBHQuotes As DevComponents.DotNetBar.ColorPickerButton
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupPanel2 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents IILebarJendela As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents IITinggiJendela As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents CPBBQuotes As DevComponents.DotNetBar.ColorPickerButton
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents CPBBJendela As DevComponents.DotNetBar.ColorPickerButton
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents CPBHJendela As DevComponents.DotNetBar.ColorPickerButton
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BSimpan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BKunci As DevComponents.DotNetBar.ButtonX
    Friend WithEvents IILebarQuotes As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents IITinggiQuotes As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents ButtonX2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupPanel3 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents TBSelamatDatang As System.Windows.Forms.TextBox
    Friend WithEvents ButtonX3 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LTransparasi As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents STransparasi As DevComponents.DotNetBar.Controls.Slider
    Friend WithEvents CBTTombol As DevComponents.DotNetBar.Controls.CheckBoxX
End Class
