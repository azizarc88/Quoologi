﻿Imports DevComponents.DotNetBar
Imports System.Threading

Public Class Form_Quote
    Public TombolTerlihat, sedanghilang, sedangmuncul As Boolean
    Dim kehiden As Byte
    Public x As Integer
    Public y As Integer

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_NOACTIVATE As Integer = &H8000000
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Const WS_EX_WINDOWEDGE As Integer = &H100
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_NOACTIVATE Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST Or WS_EX_WINDOWEDGE
            Return Result
        End Get
    End Property

    Public Sub setgaya()
        'Begin Jendela
        Me.BackColor = WarnaBackJendela
        Me.BHide.BackColor = WarnaBackJendela
        Me.BCari.BackColor = WarnaBackJendela
        Me.BSalin.BackColor = WarnaBackJendela
        Me.FB.BackColor = WarnaBackJendela
        Me.BInfo.BackColor = WarnaBackJendela
        Me.BFav.BackColor = WarnaBackJendela
        Me.Label1.ForeColor = WarnaHurufJendela
        Me.Label2.ForeColor = WarnaHurufJendela
        Me.BHide.TextColor = WarnaHurufJendela
        Me.BCari.TextColor = WarnaHurufJendela
        Me.BSalin.TextColor = WarnaHurufJendela
        Me.FB.TextColor = WarnaHurufJendela
        Me.BInfo.TextColor = WarnaHurufJendela
        Me.BFav.TextColor = WarnaHurufJendela
        Me.Opacity = Transparasi / 100
        'End Jendela'

        'Begin Quotes
        Me.TBQuotes.Font = New Font(GayaHuruf, UkuranHuruf)
        Me.TBQuotes.ForeColor = WarnaHurufQuote
        Me.TBQuotes.BackColor = WarnaBackQuote
        'End Quotes

        Dim semulay As Integer = Me.Size.Height
        Dim semulax As Integer = Me.Size.Width
        If SelaluTampilTombol Then
            BFav.Location = New Point(semulax - 269, 0)
            BSalin.Location = New Point(semulax - 267, semulay - 17)
            BCari.Location = New Point(semulax - 251, semulay - 17)
            FB.Location = New Point(semulax - 236, semulay - 17)
            Label2.Location = New Point(semulax - 218, semulay - 16)
        Else
            BFav.Location = New Point(semulax - 269, -16)
            BSalin.Location = New Point(semulax - 267, semulay)
            BCari.Location = New Point(semulax - 251, semulay)
            FB.Location = New Point(semulax - 236, semulay)
            Label2.Location = New Point(semulax - 265, semulay - 16)
        End If
    End Sub

    Public Sub HilangTombol()
        If TombolTerlihat Then
            sedanghilang = True
            Dim semulah As Integer = Me.Size.Height
            GeserKontrol(FB, semulah, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(BCari, semulah, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(BSalin, semulah, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(BFav, -16, Pengubah.Dikurangi, Orientasi.Vertikal)
            If Label2.Visible = True Then
                GeserKontrol(Label2, 21, Pengubah.Dikurangi, Orientasi.Horizontal)
            End If
            TombolTerlihat = False
            sedanghilang = False
        End If
    End Sub

    Public Sub TampilTombol()
        If Not TombolTerlihat Then
            sedangmuncul = True
            Dim semulat As Integer = Me.Size.Height - 17
            If Label2.Visible = True Then
                GeserKontrol(Label2, 68, Pengubah.Ditambah, Orientasi.Horizontal)
            End If
            GeserKontrol(BFav, 0, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(BSalin, semulat, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(BCari, semulat, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(FB, semulat, Pengubah.Dikurangi, Orientasi.Vertikal)
            TombolTerlihat = True
            sedangmuncul = False
        End If
    End Sub

    Private Sub Form_Quote_Click(sender As Object, e As EventArgs) Handles Me.Click
        Dim a As Thread
        TTombol.Enabled = False
        a = New Thread(AddressOf TampilTombol)
        a.IsBackground = True
        a.Start()
    End Sub

    Public Sub Quoologi_Load(sender As Object, e As EventArgs) Handles Me.Load
        setgaya()
        tinggi_c = postinggi + OffsetVer
        lebar_c = lebar + OffsetHor
        Me.Width = Me.Width + OffLebarJendela
        Me.TBQuotes.Width = TBQuotes.Width + OffLebarQuote
        Form_Handle.y = Screen.PrimaryScreen.WorkingArea.Height - tinggi_c
        Form_Handle.x = Screen.PrimaryScreen.WorkingArea.Width - lebar_c
        Me.Location = New Point(Form_Handle.x, Form_Handle.y)
        GantiSymbol()
        If welcomenote = True Then
            AnimasiOpen(Me.Handle)
            If SelaluTampilTombol = False Then
                TampilTombol()
                Timer2.Enabled = True
            End If
            QuoteMuncul = True
        End If
        Form_Handle.TSelama.Enabled = True
        Form_Handle.TSetiap.Enabled = True
        Timer3.Enabled = True
        If FirstRun = True Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles BHide.Click
        BHide.FocusOnLeftMouseButtonDown = False
        HideQuoologi()
    End Sub

    Private Sub ButtonX1_Click_1(sender As Object, e As EventArgs) Handles BSalin.Click
        Dim temps As String = TBQuotes.Text
        BHide.FocusOnLeftMouseButtonDown = False
        Try
            Do While temps.Chars(temps.Length - 1) = "."
                temps = temps.Remove(temps.Length - 1)
            Loop
        Catch ex As Exception
        End Try
        Try
            My.Computer.Clipboard.SetText(temps + " - " + Form_PengaturanFacebook.setTag(Label1.Text.Remove(Label1.Text.Length - 11), True))
        Catch ex As ArgumentOutOfRangeException
            MessageBoxEx.Show("Aw, saya bahagia banget anda berniat mengcopas tulisan ini :3" + Chr(13) + "  Tapi maaf, kami tidak bisa :v", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End Try
        ToastNotification.Show(Me, "Tersalin", 1000, eToastPosition.BottomCenter)
        Timer1.Enabled = True
    End Sub

    Private Sub Quoologi_MouseEnter(sender As Object, e As EventArgs) Handles Me.MouseEnter
        Form_Handle.TSelama.Enabled = False
        If SelaluTampilTombol = False Then
            TDelayT.Enabled = True
        End If
    End Sub

    Private Sub TBQuotes_MouseEnter(sender As Object, e As EventArgs) Handles TBQuotes.MouseEnter
        Form_Handle.TSelama.Enabled = False
        TTombol.Enabled = False
    End Sub

    Private Sub ButtonX1_MouseEnter(sender As Object, e As EventArgs) Handles BSalin.MouseEnter
        Form_Handle.TSelama.Enabled = False
        TTombol.Enabled = False
    End Sub

    Private Sub BHide_MouseEnter(sender As Object, e As EventArgs) Handles BHide.MouseEnter
        Form_Handle.TSelama.Enabled = False
        TTombol.Enabled = False
    End Sub

    Private Sub ButtonX1_MouseLeave(sender As Object, e As EventArgs) Handles BSalin.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False Then
            TTombol.Enabled = True
        End If
    End Sub

    Private Sub Quoologi_MouseLeave(sender As Object, e As EventArgs) Handles Me.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False And TDelayT.Enabled = False Then
            TTombol.Enabled = True
        ElseIf TDelayT.Enabled = True Then
            TDelayT.Enabled = False
        End If
    End Sub

    Private Sub FB_MouseEnter(sender As Object, e As EventArgs) Handles FB.MouseEnter
        Form_Handle.TSelama.Enabled = False
        TTombol.Enabled = False
    End Sub

    Private Sub FB_MouseLeave(sender As Object, e As EventArgs) Handles FB.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False Then
            TTombol.Enabled = True
        End If
    End Sub

    Private Sub BCari_MouseEnter(sender As Object, e As EventArgs) Handles BCari.MouseEnter
        Form_Handle.TSelama.Enabled = False
        TTombol.Enabled = False
    End Sub

    Private Sub BCari_MouseLeave(sender As Object, e As EventArgs) Handles BCari.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False Then
            TTombol.Enabled = True
        End If
    End Sub

    Private Sub BFav_MouseEnter(sender As Object, e As EventArgs) Handles BFav.MouseEnter
        Form_Handle.TSelama.Enabled = False
        TTombol.Enabled = False
    End Sub

    Private Sub BFav_MouseLeave(sender As Object, e As EventArgs) Handles BFav.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False Then
            TTombol.Enabled = True
        End If
    End Sub

    Private Sub Label2_MouseEnter(sender As Object, e As EventArgs) Handles Label2.MouseEnter
        Form_Handle.TSelama.Enabled = False
        TTombol.Enabled = False
    End Sub

    Private Sub Label2_MouseLeave(sender As Object, e As EventArgs) Handles Label2.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False Then
            TTombol.Enabled = True
        End If
    End Sub

    Private Sub TBQuotes_MouseHover(sender As Object, e As EventArgs) Handles TBQuotes.MouseEnter
        TFormHilang.Enabled = True
    End Sub

    Private Sub TBQuotes_MouseLeave(sender As Object, e As EventArgs) Handles TBQuotes.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False And TFormHilang.Enabled = False Then
            TTombol.Enabled = True
        ElseIf TFormHilang.Enabled = True Then
            TFormHilang.Enabled = False
        End If
    End Sub

    Private Sub BHide_MouseLeave(sender As Object, e As EventArgs) Handles BHide.MouseLeave
        Form_Handle.TSelama.Enabled = True
        If SelaluTampilTombol = False Then
            TTombol.Enabled = True
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        HideQuoologi()
        Timer1.Enabled = False
    End Sub

    Private Sub FileOpen(p1 As Integer, p2 As String)
        Throw New NotImplementedException
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles BCari.Click
        Form_Cari.Show()
        If QuoteMuncul = True Then
            HideQuoologi()
        End If
    End Sub

    Private Sub TFirst_Tick(sender As Object, e As EventArgs) Handles TFirst.Tick
        HideQuoologi()
        TFirst.Enabled = False
    End Sub

    Private Sub FB_Click(sender As Object, e As EventArgs) Handles FB.Click
        Form_Handle.statusfb = TBQuotes.Text
        If Form_Handle.updatestatustampil = True Then
            Form_UpdateStatus.TBTag.Text = Form_PengaturanFacebook.setTag(Label1.Text.Remove(Label1.Text.Length - 11), True)
            Form_UpdateStatus.TBQuote.Text = TBQuotes.Text
            Form_UpdateStatus.nilaiawal = NilaiString(Form_UpdateStatus.TBQuote.Text)
            Exit Sub
        End If

        If IzinkanKomentar = True Or IzinkanEdit = True Then
            Form_UpdateStatus.Show()
        Else
            Dim tempsfb As String = TBQuotes.Text
            Try
                Do While tempsfb.Chars(tempsfb.Length - 1) = "."
                    tempsfb = tempsfb.Remove(tempsfb.Length - 1)
                Loop
            Catch ex As Exception

            End Try
            Try
                If TambahPetik = True Then
                    tempsfb = """" + TBQuotes.Text + """" + " - " + Form_PengaturanFacebook.setTag(Label1.Text.Remove(Label1.Text.Length - 11))
                Else
                    tempsfb = TBQuotes.Text + " - " + Form_PengaturanFacebook.setTag(Label1.Text.Remove(Label1.Text.Length - 11))
                End If
            Catch ex As Exception
                ToastNotification.Show(Me, "Tidak bisa update quote ini", Nothing, 1000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            End Try

            Form_Handle.TSelama.Enabled = False

            If CekKoneksi() = True Then
                Try
                    ToastNotification.Show(Me, "Menghubungkan ke Facebook", Nothing, 5000, eToastGlowColor.Blue, eToastPosition.BottomCenter)
                    Application.DoEvents()
                    Form_LoginFacebook.WebBrowser1.Navigate("https://m.facebook.com/dialog/feed?app_id=638861432861662&redirect_uri=https%3A%2F%2Fm.facebook.com")
                    Application.DoEvents()
                    waitforpageload()
                Catch ex As Exception
                    ToastNotification.Show(Me, "Menghubungkan gagal, cek koneksi", Nothing, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter)
                    Form_Handle.TSelama.Enabled = True
                    Form_LoginFacebook.Dispose()
                    Exit Sub
                End Try
            Else
                Form_LoginFacebook.Dispose()
                Exit Sub
            End If

            ToastNotification.Show(Me, "Cek Akun", Nothing, 5000, eToastGlowColor.Blue, eToastPosition.BottomCenter)
            Application.DoEvents()

            If Form_LoginFacebook.WebBrowser1.DocumentTitle = "Selamat datang di Facebook" Then
                ToastNotification.Show(Me, "Belum Login", Nothing, 5000, eToastGlowColor.Orange, eToastPosition.BottomCenter)
                Application.DoEvents()
                If MessageBoxEx.Show("Belum login Facebook?, Silakan login terlebih dahulu dengan akun Facebook Anda" + Environment.NewLine + "Ingin login ?", "Facebook", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Form_LoginFacebook.Show()
                    Form_LoginFacebook.fromstat = 1
                    Exit Sub
                Else
                    Form_Handle.TSelama.Enabled = True
                    Form_LoginFacebook.Dispose()
                    Exit Sub
                End If
            End If

            Try
                ToastNotification.Show(Me, "Mengirim ke Facebook", Nothing, 1000, eToastGlowColor.Blue, eToastPosition.BottomCenter)
                Application.DoEvents()
                UpdateStatus(tempsfb)
                waitforpageload()
            Catch ex As Exception
                ToastNotification.Show(Me, "Update status gagal", Nothing, 5000, eToastGlowColor.Red, eToastPosition.BottomCenter)
                Form_Handle.TSelama.Enabled = True
                Form_LoginFacebook.Dispose()
                Exit Sub
            End Try

            Form_LoginFacebook.Dispose()
            ToastNotification.Show(Me, "Status terupdate", 1000, eToastPosition.BottomCenter)
            Form_Handle.TSelama.Enabled = True
        End If
    End Sub

    Public Sub TulisQuoteFav(ByVal Quote As String, ByVal asal As String)
        Dim r As Random = New Random()
        If My.Computer.FileSystem.FileExists(AlamatAppData + "\quote_fav.dft") = False Then
            Try
                Using Berkas As New IO.StreamWriter(AlamatAppData + "\quote_fav.dft", True)
                    Berkas.WriteLine("#Daftar Alamat Quote Favorit")
                    Berkas.WriteLine("#v 2.0 untuk Quoologi versi 2.5.8.97 keatas")
                    Berkas.WriteLine("!Silakan edit secara manual di sini jika ingin menambah / mengubah / menghapus daftar quote favorit")
                    Berkas.WriteLine("")
                    Berkas.WriteLine("[Mulai]")
                    Berkas.WriteLine(" [Quote Favorit]")
                    Berkas.WriteLine("  Q[" + r.Next(100, 999).ToString + "]<" + Quote + ">[" + r.Next(100, 999).ToString + "] AQ[" + asal + "]")
                End Using
            Catch ex As Exception
                MessageBoxEx.Show("Galat saat membuat berkas quote favorit, pesan eror: " + ex.Message + ". (kesalahan: 111003)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        Else
            Dim temp As String
            Try
                Dim filepath As String = AlamatAppData + "\quote_fav.dft"
                Dim BacaFav As New System.IO.StreamReader(filepath)
                Do While Not BacaFav.EndOfStream
                    temp = BacaFav.ReadToEnd
                    If temp.Contains(Quote) Then
                        ToastNotification.Show(Me, "Quote sudah terfavorit", Nothing, 3000, eToastGlowColor.Orange, eToastPosition.BottomCenter)
                        Exit Sub
                    End If
                    Application.DoEvents()
                Loop
                BacaFav.Close()
            Catch ex As Exception
            End Try

            Try
                Using Berkas As New IO.StreamWriter(AlamatAppData + "\quote_fav.dft", True)
                    Berkas.WriteLine("  Q[" + r.Next(100, 999).ToString + "]<" + Quote + ">[" + r.Next(100, 999).ToString + "] AQ[" + asal + "]")
                End Using
            Catch ex As Exception
                MessageBoxEx.Show("Galat saat membuat berkas quote favorit, pesan eror: " + ex.Message + ". (kesalahan: 111004)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
        ToastNotification.Show(Me, "Terfavorit", 1000, eToastPosition.BottomCenter)
    End Sub

    Private Sub ButtonX1_Click_2(sender As Object, e As EventArgs) Handles BFav.Click
        Try
            TulisQuoteFav(TBQuotes.Text, Label1.Text.Remove(Label1.Text.Length - 11))
            If Form_Handle.favmuncul = True Then
                Form_Kesukaan.BacaData()
            End If
        Catch ex As Exception
            ToastNotification.Show(Me, "Tidak bisa ditambahkan ke favorit", Nothing, 3000, eToastGlowColor.Red, eToastPosition.BottomCenter)
        End Try
    End Sub

    Private Sub TAutoHide_Tick(sender As Object, e As EventArgs) Handles TAutoHide.Tick
        WaktuAutoHide += 1
    End Sub

    Dim a As Thread
    Private Sub TTombol_Tick(sender As Object, e As EventArgs) Handles TTombol.Tick
        HilangTombol()
        TTombol.Enabled = False
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        HilangTombol()
        Timer2.Enabled = False
    End Sub

    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        MuatDataQuote()
        Form_Handle.totalqot = DaftarQuote.Count
        Form_PengaturanUtama.LTotalQot.Text = DaftarQuote.Count
        If Form_Handle.totalqot > 0 Then
            Form_PengaturanUtama.ButtonX1.Enabled = True
        End If
        Timer3.Enabled = False
    End Sub

    Private Sub TDelayT_Tick(sender As Object, e As EventArgs) Handles TDelayT.Tick
        TTombol.Enabled = False
        TampilTombol()
        TDelayT.Enabled = False
    End Sub

    Private Sub TFormHilang_Tick(sender As Object, e As EventArgs) Handles TFormHilang.Tick
        If OtoSembunyi = True Then
            HideQuoologi(True)
            TAutoHide.Enabled = False
            If WaktuAutoHide <= 5 Then
                kehiden += 1
            End If

            If kehiden = 5 Then
                Form_Handle.NotifyIcon1.ShowBalloonTip(10000, "Info", "Tidak nyaman dengan Otomatis Sembunyikan Quote ?, silakan matikan fitur tersebut di jendela Pengaturan" + Environment.NewLine + Environment.NewLine + "Klik kanan icon ini untuk membuka menu", ToolTipIcon.Info)
            End If
        End If
        TFormHilang.Enabled = False
    End Sub

    Private Sub BInfo_Click(sender As Object, e As EventArgs) Handles BInfo.Click
        MessageBoxEx.Show(Nothing, Form_Handle.Info, "Penentu Tema", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, True)
        Application.DoEvents()
    End Sub
End Class