﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_TambahProgramTerkecuali
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TBNamaP = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.BTambah = New DevComponents.DotNetBar.ButtonX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.LBProgramAda = New System.Windows.Forms.ListBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.CheckBoxX1 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.SuspendLayout()
        '
        'TBNamaP
        '
        Me.TBNamaP.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBNamaP.Border.Class = "TextBoxBorder"
        Me.TBNamaP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBNamaP.ForeColor = System.Drawing.Color.Black
        Me.TBNamaP.Location = New System.Drawing.Point(12, 207)
        Me.TBNamaP.Name = "TBNamaP"
        Me.TBNamaP.Size = New System.Drawing.Size(264, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBNamaP, New DevComponents.DotNetBar.SuperTooltipInfo("Nama Program", "", "Tempat untuk mengisi nama program yang ingin ditambahkan sebagai program terkecua" & _
            "li." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Jika ingin menambahkan banyak program sekaligus, silakan pisahkan antar p" & _
            "rogram dengan tanda "";"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(216, 124)))
        Me.TBNamaP.TabIndex = 0
        Me.TBNamaP.WatermarkText = "Masukan nama berkas program (program.exe)"
        '
        'BTambah
        '
        Me.BTambah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTambah.AntiAlias = True
        Me.BTambah.Location = New System.Drawing.Point(201, 269)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.BTambah.Size = New System.Drawing.Size(75, 23)
        Me.BTambah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BTambah, New DevComponents.DotNetBar.SuperTooltipInfo("Tambah", "", "Klik untuk menambah program terpilih / program tertulis.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BTambah.TabIndex = 41
        Me.BTambah.Text = "Tambah | F1"
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.AntiAlias = True
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(12, 269)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(75, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, New DevComponents.DotNetBar.SuperTooltipInfo("Batal", "", "Menutup jendela.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BBatal.TabIndex = 42
        Me.BBatal.Text = "Batal | Esc"
        '
        'LBProgramAda
        '
        Me.LBProgramAda.BackColor = System.Drawing.Color.White
        Me.LBProgramAda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBProgramAda.ForeColor = System.Drawing.Color.Black
        Me.LBProgramAda.FormattingEnabled = True
        Me.LBProgramAda.HorizontalScrollbar = True
        Me.LBProgramAda.Location = New System.Drawing.Point(12, 4)
        Me.LBProgramAda.Name = "LBProgramAda"
        Me.LBProgramAda.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBProgramAda.Size = New System.Drawing.Size(264, 197)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBProgramAda, New DevComponents.DotNetBar.SuperTooltipInfo("Daftar Program Berjalan", "", "Berisi daftar program yang berjalan di komputer Anda.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(157, 57)))
        Me.LBProgramAda.TabIndex = 43
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.ForeColor = System.Drawing.Color.Black
        Me.TextBox1.Location = New System.Drawing.Point(95, 233)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 22)
        Me.TextBox1.TabIndex = 44
        Me.TextBox1.Text = "sementara"
        Me.TextBox1.Visible = False
        Me.TextBox1.WordWrap = False
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Location = New System.Drawing.Point(231, 4)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 0, 4, 4)
        Me.ButtonX1.Size = New System.Drawing.Size(23, 23)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX1, New DevComponents.DotNetBar.SuperTooltipInfo("Segarkan", "", "Klik tombol ini untuk memperbarui daftar proses yang berjalan di komputer Anda.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 14.0!
        Me.ButtonX1.TabIndex = 45
        Me.ButtonX1.Visible = False
        '
        'CheckBoxX1
        '
        Me.CheckBoxX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CheckBoxX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBoxX1.CheckSignSize = New System.Drawing.Size(10, 10)
        Me.CheckBoxX1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        Me.CheckBoxX1.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxX1.Location = New System.Drawing.Point(100, 270)
        Me.CheckBoxX1.Name = "CheckBoxX1"
        Me.CheckBoxX1.Size = New System.Drawing.Size(88, 23)
        Me.CheckBoxX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CheckBoxX1, New DevComponents.DotNetBar.SuperTooltipInfo("Tampilkan Semua", "", "Ketika opsi ini dicentang, maka daftar program akan memuat program dari system.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CheckBoxX1.TabIndex = 46
        Me.CheckBoxX1.Text = "Tampilkan semua"
        '
        'Form_TambahProgramTerkecuali
        '
        Me.AcceptButton = Me.BTambah
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(288, 264)
        Me.ControlBox = False
        Me.Controls.Add(Me.CheckBoxX1)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.LBProgramAda)
        Me.Controls.Add(Me.BTambah)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.TBNamaP)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "Form_TambahProgramTerkecuali"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Tambah Program"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TBNamaP As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents BTambah As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LBProgramAda As System.Windows.Forms.ListBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents CheckBoxX1 As DevComponents.DotNetBar.Controls.CheckBoxX
End Class
