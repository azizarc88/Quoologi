﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_GenerateNumbering
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.IIMaks = New DevComponents.Editors.IntegerInput()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.CBFormat = New DevComponents.DotNetBar.Controls.ComboBoxEx()
        Me.ComboItem2 = New DevComponents.Editors.ComboItem()
        Me.ComboItem3 = New DevComponents.Editors.ComboItem()
        Me.ComboItem5 = New DevComponents.Editors.ComboItem()
        Me.ComboItem4 = New DevComponents.Editors.ComboItem()
        Me.ComboItem1 = New DevComponents.Editors.ComboItem()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.BOk = New DevComponents.DotNetBar.ButtonX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        CType(Me.IIMaks, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'IIMaks
        '
        Me.IIMaks.AllowEmptyState = False
        Me.IIMaks.AntiAlias = True
        Me.IIMaks.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IIMaks.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IIMaks.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IIMaks.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IIMaks.ForeColor = System.Drawing.Color.Black
        Me.IIMaks.Location = New System.Drawing.Point(138, 3)
        Me.IIMaks.MaxValue = 999
        Me.IIMaks.MinValue = 1
        Me.IIMaks.Name = "IIMaks"
        Me.IIMaks.ShowUpDown = True
        Me.IIMaks.Size = New System.Drawing.Size(139, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IIMaks, New DevComponents.DotNetBar.SuperTooltipInfo("Jumlah Numbering", "", "Berisi jumlah numbering yang ingin dimasukan ke dalam teks sampah, contoh 5:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "5" & _
            "." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "4." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "3." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "2." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "1.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(206, 145)))
        Me.IIMaks.TabIndex = 0
        Me.IIMaks.Value = 10
        Me.IIMaks.WatermarkEnabled = False
        '
        'GroupPanel1
        '
        Me.GroupPanel1.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.CBFormat)
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Controls.Add(Me.IIMaks)
        Me.GroupPanel1.Location = New System.Drawing.Point(3, 3)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(286, 76)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 1
        Me.GroupPanel1.Text = "Kondisi"
        '
        'CBFormat
        '
        Me.CBFormat.DisplayMember = "Text"
        Me.CBFormat.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.CBFormat.ForeColor = System.Drawing.Color.Black
        Me.CBFormat.FormattingEnabled = True
        Me.CBFormat.ItemHeight = 14
        Me.CBFormat.Items.AddRange(New Object() {Me.ComboItem2, Me.ComboItem3, Me.ComboItem5, Me.ComboItem4, Me.ComboItem1})
        Me.CBFormat.Location = New System.Drawing.Point(138, 31)
        Me.CBFormat.Name = "CBFormat"
        Me.CBFormat.Size = New System.Drawing.Size(139, 20)
        Me.CBFormat.Sorted = True
        Me.CBFormat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBFormat, New DevComponents.DotNetBar.SuperTooltipInfo("Format Penomoran", "", "Isikan format penomoran yang ingin dimasukkan ke dalam teks sampah dengan menamba" & _
            "hkan syntax ""{angka}"" untuk penempatan angkanya. Contoh (tanpa petik): " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """{ang" & _
            "ka}. "" = ""1. """ & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """({angka}). "" = ""(1). """, Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(248, 122)))
        Me.CBFormat.TabIndex = 5
        Me.CBFormat.WatermarkText = "Format Penomoran..."
        '
        'ComboItem2
        '
        Me.ComboItem2.Text = "({angka}). "
        '
        'ComboItem3
        '
        Me.ComboItem3.Text = "[{angka}]. "
        '
        'ComboItem5
        '
        Me.ComboItem5.Text = "{angka} "
        '
        'ComboItem4
        '
        Me.ComboItem4.Text = "{angka}) "
        '
        'ComboItem1
        '
        Me.ComboItem1.Text = "{angka}. "
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(3, 28)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(113, 23)
        Me.LabelX2.TabIndex = 3
        Me.LabelX2.Text = "Format Penomoran:"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(3, 0)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(129, 23)
        Me.LabelX1.TabIndex = 2
        Me.LabelX1.Text = "Nilai Numbering Tertinggi:"
        '
        'BOk
        '
        Me.BOk.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BOk.Location = New System.Drawing.Point(215, 86)
        Me.BOk.Name = "BOk"
        Me.BOk.Size = New System.Drawing.Size(75, 23)
        Me.BOk.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BOk, New DevComponents.DotNetBar.SuperTooltipInfo("OK", "", "Klik untuk menambahkan numbering dengan kondisi yang sudah ditentukan ke dalam te" & _
            "ks sampah di QOT Maker.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(184, 84)))
        Me.BOk.TabIndex = 2
        Me.BOk.Text = "OK"
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(134, 86)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(75, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Tutup jendela ini.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(91, 0)))
        Me.BBatal.TabIndex = 3
        Me.BBatal.Text = "Batal"
        '
        'Form_GenerateNumbering
        '
        Me.AcceptButton = Me.BOk
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(293, 112)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BOk)
        Me.Controls.Add(Me.GroupPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form_GenerateNumbering"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Numbering"
        Me.TopMost = True
        CType(Me.IIMaks, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents IIMaks As DevComponents.Editors.IntegerInput
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents BOk As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents CBFormat As DevComponents.DotNetBar.Controls.ComboBoxEx
    Friend WithEvents ComboItem1 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem2 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem3 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem4 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem5 As DevComponents.Editors.ComboItem
End Class
