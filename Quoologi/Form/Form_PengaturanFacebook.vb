﻿Imports DevComponents.DotNetBar
Imports Microsoft.Win32
Imports UIQuoologi.Gambar

Public Class Form_PengaturanFacebook
    Inherits Metro.MetroForm
    Dim temp As String
    Public lagirefresh As Boolean

    Public Sub baca_pengaturan()
        If Form_Handle.kartag = "@" Then
            RBAt.Checked = True
        ElseIf Form_Handle.kartag = "#" Then
            RBCross.Checked = True
        Else
            RBCustom.Checked = True
            TBCustom.Text = Pengaturan.KarakterTag
        End If

        CBHapusSpasi.Checked = Form_Handle.hapusspasi
        CBUbahSpasi.Checked = Form_Handle.ubahspasi
        CBHapusKNon.Checked = Form_Handle.hapuskarnon
        CBIzinKomen.Checked = Form_Handle.izinkankomentar
        CBIzinEdit.Checked = Pengaturan.IzinkanEdit
        CBTambahPetik.Checked = Form_Handle.tambahpetik

        If Pengaturan.KomenSebelumQuote = True Then
            RBSebelumKomen.Checked = True
        Else
            RBSetelahKomen.Checked = True
        End If
    End Sub

    Public Sub Simpan_Pengaturan(Optional ByVal Sementara As Boolean = False)
        If Sementara = False Then
            If RBAt.Checked = True Then
                Pengaturan.KarakterTag = "@"
            ElseIf RBCross.Checked = True Then
                Pengaturan.KarakterTag = "#"
            Else
                Pengaturan.KarakterTag = TBCustom.Text
            End If

            Pengaturan.HapusSpasi = CBHapusSpasi.Checked
            Pengaturan.UbahSpasi = CBUbahSpasi.Checked
            Pengaturan.HapusKarNon = CBHapusKNon.Checked
            Pengaturan.IzinkanKomentar = CBIzinKomen.Checked
            Pengaturan.IzinkanEdit = CBIzinEdit.Checked
            Pengaturan.TambahPetik = CBTambahPetik.Checked

            If RBSebelumKomen.Checked = True Then
                Pengaturan.KomenSebelumQuote = True
            Else
                Pengaturan.KomenSebelumQuote = False
            End If

            Pengaturan.SimpanPengaturan()
            BacaPengaturan()
        Else
            If RBAt.Checked = True Then
                Form_Handle.kartag = "@"
            ElseIf RBCross.Checked = True Then
                Form_Handle.kartag = "#"
            Else
                Form_Handle.kartag = TBCustom.Text
            End If

            Form_Handle.hapusspasi = CBHapusSpasi.Checked
            Form_Handle.ubahspasi = CBUbahSpasi.Checked
            Form_Handle.hapuskarnon = CBHapusKNon.Checked
            Form_Handle.izinkankomentar = CBIzinKomen.Checked
            Form_Handle.izinkanedit = CBIzinEdit.Checked
            Form_Handle.tambahpetik = CBTambahPetik.Checked

            If RBSebelumKomen.Checked = True Then
                Form_Handle.komensebelumquote = True
            Else
                Form_Handle.komensebelumquote = False
            End If
        End If
    End Sub

    Public Sub csKomentar(ByVal status As Boolean)
        If status = True Then
            Me.Height = 416
            GroupPanel4.Height = 198
        Else
            Me.Height = 388
            GroupPanel4.Height = 169
        End If
    End Sub

    Private Sub AturStatus()
        If RBSebelumKomen.Checked = True And CBIzinKomen.Checked = True Then
            GeserKontrol(TBPratinjau, 86, Pengubah.Ditambah, Orientasi.Vertikal)
            TextBoxX1.Visible = True
            Application.DoEvents()
            GeserKontrol(TextBoxX1, 57, Pengubah.Dikurangi, Orientasi.Vertikal, 1, 1)
        ElseIf RBSebelumKomen.Checked = True And CBIzinKomen.Checked = False Then
            'TextBoxX1.Location = New Point(1, 57)
            'TBPratinjau.Location = New Point(1, 84)
            TextBoxX1.Visible = True
            GeserKontrol(TBPratinjau, 55, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(TextBoxX1, 134, Pengubah.Ditambah, Orientasi.Vertikal, 1, 1)
        Else
            'TextBoxX1.Location = New Point(1, 57)
            'TBPratinjau.Location = New Point(1, 84)
            TextBoxX1.Visible = True
            GeserKontrol(TBPratinjau, 55, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(TextBoxX1, 134, Pengubah.Ditambah, Orientasi.Vertikal, 1, 1)
        End If
    End Sub

    Private Sub setStyleFB()
        LNamaAkun.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LNamaAkun.Text = Pengaturan.NamaAkunFB
        Label2.ForeColor = Color.FromArgb(255, 145, 151, 163)
        LSuka.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LSuka.BackColor = Color.FromArgb(255, 250, 251, 251)

        LKomentar.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LKomentar.BackColor = Color.FromArgb(255, 250, 251, 251)

        LPromosikan.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LPromosikan.BackColor = Color.FromArgb(255, 250, 251, 251)

        LBagikan.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LBagikan.BackColor = Color.FromArgb(255, 250, 251, 251)

        LTitik.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LTitik.BackColor = Color.FromArgb(255, 250, 251, 251)
    End Sub

    Private Sub Pengaturan_FB_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Form_Handle.dariupdatestatus = True And lagirefresh = False Then
            Form_UpdateStatus.daripengaturan = True
            Form_UpdateStatus.Show()
            Form_Handle.dariupdatestatus = False
        End If
        If lagirefresh = False Then
            Pengaturan.BacaPengaturan()
        End If
    End Sub

    Private Sub Pengaturan_FB_Load(sender As Object, e As EventArgs) Handles Me.Load
        PictureBox1.Image = AmbilGambar("fp_fb")
        PictureBox2.Image = AmbilGambar("privasi")
        temp = "Quoologi adalah aplikasi edukatif yang berisi kumpulan dari berbagai macam informasi, motivasi, nasehat, hadist, dll yang kami kemas dalam sebuah aplikasi yang menarik dengan GUI yang user friendly untuk komputer dengan OS Windows."
        baca_pengaturan()
        If Pengaturan.IzinkanKomentar = True And Form_Handle.StatusFBKomen = 1 Then
            Me.Height = 416
            GroupPanel4.Height = 198
        End If
        setStyleFB()
        If CBHapusSpasi.Checked = True Then
            CBHapusKNon.Location = New Point(3, 27)
        End If
        If RBSebelumKomen.Checked = True And CBIzinKomen.Checked = False Then
            TextBoxX1.Location = New Point(1, 57)
            TBPratinjau.Location = New Point(1, 84)
        ElseIf RBSebelumKomen.Checked = True And CBIzinKomen.Checked = True Then
            TextBoxX1.Location = New Point(1, 134)
            TBPratinjau.Location = New Point(1, 55)
        ElseIf RBSetelahKomen.Checked = True And CBIzinKomen.Checked = True Then
            TextBoxX1.Location = New Point(1, 134)
            TBPratinjau.Location = New Point(1, 55)
            TextBoxX1.Visible = True
        ElseIf RBSetelahKomen.Checked = True And CBIzinKomen.Checked = False Then
            TextBoxX1.Location = New Point(1, 134)
            TBPratinjau.Location = New Point(1, 55)
            TextBoxX1.Visible = True
        End If
    End Sub

    Private Function hapusnonhuruf(ByVal teks As String, Optional ByVal all As Boolean = False) As String
        Dim sem As String
        sem = teks
        If all = True Then
            For Each huruf As Char In sem
                If Asc(huruf) > 31 And Asc(huruf) < 65 Then
                    sem = sem.Replace(huruf, "")
                End If
            Next
            Return sem
            Exit Function
        End If
        For Each huruf As Char In sem
            If Asc(huruf) > 32 And Asc(huruf) < 65 Then
                sem = sem.Replace(huruf, "")
            End If
        Next
        Return sem
    End Function

    Public Function setTag(ByVal asaltag As String, Optional ByVal base As Boolean = False) As String
        Dim tag As String = "#"
        Dim asal As String
        Dim stringtag As String = " - "

        If base = False Then
            If CBHapusSpasi.Checked = True And CBHapusKNon.Checked = False And CBUbahSpasi.Checked = False Then
                asal = asaltag.Replace(" ", "")
            ElseIf CBHapusSpasi.Checked = True And CBHapusKNon.Checked = False And CBUbahSpasi.Checked = True Then
                asal = asaltag.Replace(" ", "_")
            ElseIf CBHapusKNon.Checked = True And CBHapusSpasi.Checked = False And CBUbahSpasi.Checked = False Then
                asal = hapusnonhuruf(asaltag)
                asal = asal.Replace("  ", " ")
                asal = asal.Replace("   ", " ")
                asal = asal.Replace("    ", " ")
            ElseIf CBHapusSpasi.Checked = True And CBHapusKNon.Checked = True And CBUbahSpasi.Checked = False Then
                asal = hapusnonhuruf(asaltag, True)
            ElseIf CBHapusSpasi.Checked = True And CBHapusKNon.Checked = True And CBUbahSpasi.Checked = True Then
                asal = hapusnonhuruf(asaltag)
                asal = asal.Replace(" ", "_")
                asal = asal.Replace("__", "_")
                asal = asal.Replace("___", "_")
                asal = asal.Replace("____", "_")
            Else
                asal = asaltag
            End If

            If RBAt.Checked = True Then
                tag = "@"
            ElseIf RBCross.Checked = True Then
                tag = "#"
            ElseIf RBCustom.Checked = True Then
                tag = TBCustom.Text
            End If
        Else
            tag = Pengaturan.KarakterTag
            If Pengaturan.HapusSpasi = True And Pengaturan.HapusKarNon = False And Pengaturan.UbahSpasi = False Then
                asal = asaltag.Replace(" ", "")
            ElseIf Pengaturan.HapusSpasi = True And Pengaturan.HapusKarNon = False And Pengaturan.UbahSpasi = True Then
                asal = asaltag.Replace(" ", "_")
            ElseIf Pengaturan.HapusKarNon = True And Pengaturan.HapusSpasi = False And Pengaturan.UbahSpasi = False Then
                asal = hapusnonhuruf(asaltag)
                asal = asal.Replace("  ", " ")
                asal = asal.Replace("   ", " ")
                asal = asal.Replace("    ", " ")
            ElseIf Pengaturan.HapusSpasi = True And Pengaturan.HapusKarNon = True And Pengaturan.UbahSpasi = False Then
                asal = hapusnonhuruf(asaltag, True)
            ElseIf Pengaturan.HapusSpasi = True And Pengaturan.HapusKarNon = True And Pengaturan.UbahSpasi = True Then
                asal = hapusnonhuruf(asaltag)
                asal = asal.Replace(" ", "_")
                asal = asal.Replace("__", "_")
                asal = asal.Replace("___", "_")
                asal = asal.Replace("____", "_")
            Else
                asal = asaltag
            End If
        End If

        stringtag = tag + asal

        Return stringtag
    End Function

    Private Sub RBCustom_CheckedChanged(sender As Object, e As EventArgs) Handles RBCustom.CheckedChanged
        If RBCustom.Checked = True Then
            TBCustom.Enabled = True
        Else
            TBCustom.Enabled = False
        End If
    End Sub

    Private Sub Pengaturan_FB_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
        AturStatus()
        If CBHapusSpasi.Checked = True Then
            GeserKontrol(CBHapusKNon, 50, Pengubah.Ditambah, Orientasi.Vertikal)
        End If
        If CBIzinKomen.Checked = True Then
            GeserKontrol(CBTambahPetik, 87, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(CBIzinEdit, 64, Pengubah.Ditambah, Orientasi.Vertikal)
        End If

        If FirstRun = True And Form_Handle.pengaturanfbsudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berisi properti² pengaturan untuk update status ke Facebook" + Environment.NewLine + "Anda bisa merubah pengaturan update status disini", 8000, eToastPosition.MiddleCenter)
            Form_Handle.pengaturanfbsudah = True
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        Simpan_Pengaturan()
        ToastNotification.Show(Me, "Pengaturan telah tersimpan", eToastPosition.BottomCenter)
    End Sub

    Private Sub LSuka_Click(sender As Object, e As EventArgs) Handles LSuka.MouseEnter
        LSuka.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LSuka_Leave(sender As Object, e As EventArgs) Handles LSuka.MouseLeave
        LSuka.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub LKomentar_Click(sender As Object, e As EventArgs) Handles LKomentar.MouseEnter
        LKomentar.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LKomentar_Leave(sender As Object, e As EventArgs) Handles LKomentar.MouseLeave
        LKomentar.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub LPromosikan_Click(sender As Object, e As EventArgs) Handles LPromosikan.MouseEnter
        LPromosikan.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LPromosikan_Leave(sender As Object, e As EventArgs) Handles LPromosikan.MouseLeave
        LPromosikan.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub LBagikan_Click(sender As Object, e As EventArgs) Handles LBagikan.MouseEnter
        LBagikan.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LBagikan_Leave(sender As Object, e As EventArgs) Handles LBagikan.MouseLeave
        LBagikan.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub CBSpasi_CheckedChanged(sender As Object, e As EventArgs) Handles CBHapusSpasi.CheckedChanged
        If CBHapusSpasi.Checked = True Then
            GeserKontrol(CBHapusKNon, 50, Pengubah.Ditambah, Orientasi.Vertikal)
        Else
            CBUbahSpasi.Checked = False
            GeserKontrol(CBHapusKNon, 27, Pengubah.Dikurangi, Orientasi.Vertikal)
        End If
        TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
    End Sub

    Private Sub CBKomen_CheckedChanged(sender As Object, e As EventArgs) Handles CBIzinKomen.CheckedChanged
        If CBIzinKomen.Checked = True Then
            If Form_Handle.StatusFBKomen = 0 Then
                Form_Handle.StatusFBKomen = 1
                Form_Handle.UbahcsKomen(True)
            End If

            If Form_Handle.tambahpetik = False Then
                Form_Handle.tambahpetik = True
                Pengaturan.OtoPetik = True
                Pengaturan.SimpanPengaturan()
            End If
        Else
            If Pengaturan.OtoPetik = True Then
                CBTambahPetik.Checked = False
                Pengaturan.OtoPetik = False
                SimpanPengaturan()
            End If
            Form_Handle.StatusFBKomen = 0
            GeserKontrol(CBIzinEdit, 24, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(CBTambahPetik, 47, Pengubah.Dikurangi, Orientasi.Vertikal)
            System.Threading.Thread.Sleep(300)
            Form_Handle.izinkankomentar = False
            Form_Handle.UbahcsKomen(False)
            Simpan_Pengaturan(True)
        End If
    End Sub

    Private Sub RBAt_CheckedChanged(sender As Object, e As EventArgs) Handles RBAt.CheckedChanged
        TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
    End Sub

    Private Sub RBCross_CheckedChanged(sender As Object, e As EventArgs) Handles RBCross.CheckedChanged
        TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
    End Sub

    Private Sub TBCustom_TextChanged(sender As Object, e As EventArgs) Handles TBCustom.TextChanged
        TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
    End Sub

    Private Sub CBNon_CheckedChanged(sender As Object, e As EventArgs) Handles CBHapusKNon.CheckedChanged
        TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
    End Sub

    Private Sub CBUbahSpasi_CheckedChanged(sender As Object, e As EventArgs) Handles CBUbahSpasi.CheckedChanged
        TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
    End Sub

    Private Sub RBSebelumKomen_CheckedChanged(sender As Object, e As EventArgs) Handles RBSebelumKomen.CheckedChanged
        AturStatus()
    End Sub

    Private Sub CBPetik_CheckedChanged(sender As Object, e As EventArgs) Handles CBTambahPetik.CheckedChanged
        If CBTambahPetik.Checked = True Then
            temp = temp.Insert(0, """")
            temp = temp.Insert(temp.Length, """")
            TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
        Else
            temp = temp.Replace("""", "")
            TBPratinjau.Text = temp + " - " + setTag("Quoologi - IS")
        End If
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub
End Class