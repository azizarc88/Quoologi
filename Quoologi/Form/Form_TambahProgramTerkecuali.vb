﻿Imports DevComponents.DotNetBar
Imports System.ComponentModel

Public Class Form_TambahProgramTerkecuali
    Inherits DevComponents.DotNetBar.Metro.MetroForm
    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        Dim pesan As String
        Dim a As Boolean
        TextBox1.Clear()
        For Each huruf As Char In TBNamaP.Text
            If a = True Then
                a = False
                If huruf = " " Then
                    GoTo 100
                End If
            End If
            If huruf <> ";" Then
                TextBox1.Text += huruf
            Else
                a = True
                TextBox1.Text += Environment.NewLine
            End If
100:
        Next

        If TextBox1.Lines.Length = 1 Then
            If Form_ProgramTerkecuali.listsementara.Contains(TextBox1.Text) Then
                ToastNotification.Show(Me, "Data sudah ada", Nothing, 2000, eToastGlowColor.Red, eToastPosition.BottomCenter)
                Exit Sub
            End If
            Form_ProgramTerkecuali.listsementara.Add(TextBox1.Text)
            pesan = TextBox1.Text + " telah ditambahkan"
            Form_ProgramTerkecuali.segarkan()
        ElseIf TextBox1.Lines.Length > 1 Then
            For Each baris As String In TextBox1.Lines
                If Form_ProgramTerkecuali.listsementara.Contains(baris) = False Then
                    Form_ProgramTerkecuali.listsementara.Add(baris)
                End If
            Next
            pesan = "beberapa program telah ditambahkan"
            Form_ProgramTerkecuali.segarkan()
        Else
            ToastNotification.Show(Me, "Isi nama program / pilih dahulu", Nothing, 2000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            Exit Sub
        End If
        Me.Close()
        ToastNotification.Show(Form_ProgramTerkecuali, pesan, eToastPosition.BottomCenter)
    End Sub

    Private Sub TambahProgram_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        ButtonX1.Visible = False
        LBProgramAda.Items.Clear()
        GeserKontrol(BTambah, 270, Pengubah.Ditambah, Orientasi.Vertikal)
        GeserKontrol(CheckBoxX1, 270, Pengubah.Ditambah, Orientasi.Vertikal)
        GeserKontrol(BBatal, 270, Pengubah.Ditambah, Orientasi.Vertikal)
    End Sub

    Private Sub TambahProgram_Load(sender As Object, e As EventArgs) Handles MyBase.Shown
        ToastNotification.Show(LBProgramAda, "Memuat", 50000, eToastPosition.MiddleCenter)
        TBNamaP.Clear()
        Dim des As String
        ButtonX1.Visible = True
        For Each p As Process In Process.GetProcesses()
            Try
                Application.DoEvents()
                des = p.MainModule.FileVersionInfo.FileDescription
                If des.Length > 30 Then
                    des = des.Remove(27) + "..."
                End If
                Application.DoEvents()
                If LBProgramAda.Items.Contains(des + " (" + p.ProcessName + ".exe" + ")") = False And p.ProcessName <> "Quoologi" Then
                    LBProgramAda.Items.Add(des + " (" + p.ProcessName + ".exe" + ")")
                End If
                Application.DoEvents()
            Catch
                Application.DoEvents()
                If CheckBoxX1.Checked = True Then
                    If LBProgramAda.Items.Contains(p.ProcessName + ".exe") = False Then
                        LBProgramAda.Items.Add(p.ProcessName + ".exe")
                    End If
                End If
            End Try
            LBProgramAda.Sorted = True
        Next
        ToastNotification.Close(LBProgramAda)
    End Sub

    Private Sub LBProgramAda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBProgramAda.SelectedIndexChanged
        Dim a As String
        If LBProgramAda.SelectedItems.Count = 1 Then
            a = LBProgramAda.SelectedItem.ToString
            If a.Contains("(") Then
                a = a.Remove(0, a.LastIndexOf("(") + 1)
                Try
                    TBNamaP.Text = a.Remove(a.LastIndexOf(")"))
                Catch ex As Exception
                End Try
            Else
                TBNamaP.Text = a
            End If
        Else
            TBNamaP.Clear()
            For Each item As String In LBProgramAda.SelectedItems
                a = item
                If a.Contains("(") Then
                    a = a.Remove(0, a.LastIndexOf("(") + 1)
                    Try
                        TBNamaP.Text += a.Remove(a.LastIndexOf(")")) + "; "
                    Catch ex As Exception
                    End Try
                Else
                    TBNamaP.Text += a + "; "
                End If
            Next
            TBNamaP.Text = TBNamaP.Text.Remove(TBNamaP.Text.Length - 2)
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        TBNamaP.Clear()
        LBProgramAda.Items.Clear()
        Application.DoEvents()
        ToastNotification.Show(LBProgramAda, "Menyegarkan", 50000, eToastPosition.MiddleCenter)
        Dim des As String
        For Each p As Process In Process.GetProcesses()
            Application.DoEvents()
            Try
                Application.DoEvents()
                des = p.MainModule.FileVersionInfo.FileDescription
                If des.Length > 30 Then
                    des = des.Remove(27) + "..."
                End If
                Application.DoEvents()
                If LBProgramAda.Items.Contains(des + " (" + p.ProcessName + ".exe" + ")") = False And p.ProcessName <> "Quoologi" Then
                    LBProgramAda.Items.Add(des + " (" + p.ProcessName + ".exe" + ")")
                End If
                Application.DoEvents()
            Catch
                Application.DoEvents()
                If CheckBoxX1.Checked = True Then
                    If LBProgramAda.Items.Contains(p.ProcessName + ".exe") = False Then
                        LBProgramAda.Items.Add(p.ProcessName + ".exe")
                    End If
                End If
            End Try
            LBProgramAda.Sorted = True
        Next
        ToastNotification.Close(LBProgramAda)
    End Sub

    Private Sub CheckBoxX1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckBoxX1.CheckedChanged
        ButtonX1.PerformClick()
    End Sub

    Private Sub Tambah_Program_Load(sender As Object, e As EventArgs) Handles MyBase.Shown
        GeserKontrol(BBatal, 232, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(CheckBoxX1, 232, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BTambah, 232, Pengubah.Dikurangi, Orientasi.Vertikal)
        If FirstRun = True And Form_Handle.tambahprogramsudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berfungsi untuk menambah" + Environment.NewLine + "program terkecuali, baik dari dalam" + Environment.NewLine + "daftar ataupun dari inputan pengguna", 7000, eToastPosition.MiddleCenter)
            Form_Handle.tambahprogramsudah = True
        End If
    End Sub
End Class