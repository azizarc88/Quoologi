﻿Imports DevComponents.DotNetBar.Metro.MetroForm
Imports DevComponents.DotNetBar.Metro
Imports DevComponents.DotNetBar.Metro.Rendering.MetroRender
Imports DevComponents.DotNetBar

Public Class Form_PembuatQuote
    Inherits MetroForm

    Public status As Byte = 0
    Public temp As Byte = 0
    Public oldwindowproc As Long
    Public sem As Integer
    Public data As List(Of String) = New List(Of String)
    Dim salin As Boolean = False
    Public terbuang As List(Of String) = New List(Of String)
    Public alamatterbuang As List(Of String) = New List(Of String)
    Public panjangterbuang As List(Of String) = New List(Of String)
    Public alamatada As List(Of String) = New List(Of String)
    Public panjangada As List(Of String) = New List(Of String)
    Public ragu As Integer = 0
    Dim angka As Integer = 0

    Public Sub tambahterbuang(ByVal panjang As Integer, ByVal alamat As Integer, ByVal data As String)
        Dim banyak As Integer = 0
        panjangterbuang.Add(panjang)
        alamatterbuang.Add(alamat)
        terbuang.Add(data)
        For Each baris As String In TBPencegah.Lines
            If data.Contains(baris) Then
                banyak += 1
            End If
        Next
        If data.Length >= 70 And banyak = 1 Then
            ragu += 1
        End If
    End Sub

    Public Sub tambahada(ByVal panjang As Integer, ByVal alamat As Integer)
        panjangada.Add(panjang)
        alamatada.Add(alamat)
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles BKerjakan.Click
        If (TBData.Text.ToLower.Contains("retweet") And TBData.Text.ToLower.Contains("reply") And TBData.Text.ToLower.Contains("@")) Or (TBData.Text.ToLower.Contains("rxpand") Or TBData.Text.ToLower.Contains("times") Or TBData.Text.ToLower.Contains("delete")) Then
            If TBDari.Text = "" Then
                ToastNotification.Show(Me, "Kami mendeteksi bahwa data berasal dari Twitter, maka silakan masukan sumber quotes terlebih dahulu ""@NamaAkun""", Nothing, 3000, eToastGlowColor.Red, eToastPosition.BottomCenter)
                TBDari.Focus()
                Exit Sub
            End If
        ElseIf TBData.Text = "" Then
            ToastNotification.Show(Me, "Silakan isi dahulu data pada bagian ""Data:""", Nothing, 3000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            TBData.Focus()
            Exit Sub
        End If
        BSimpan.Text = "Simpan*"
        kerjakan()
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        If ragu > 5 Then
            Dim result As MsgBoxResult
            result = MessageBoxEx.Show("Sistem kami menemukan banyak teks yang mungkin termasuk quote akan tetapi" + Environment.NewLine + "terhapus oleh pengaturan di dalam program, ingin mengecek teks yang terhapus ?", "Pembuat Quote", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
            If result = MsgBoxResult.Yes Then
                Form_BukanQuote.Show()
                Exit Sub
            ElseIf result = MsgBoxResult.Cancel Then
                Exit Sub
            End If
        End If

        If TBFileName.Text = "" Then
            ToastNotification.Show(Me, "Masukan nama berkas terlebih dahulu", Nothing, 3000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            Exit Sub
        End If

        If Label2.Text = 0 Then
            ToastNotification.Show(Me, "Sesuai pengaturan yang ditetapkan, tidak ditemukan adanya quote" + Environment.NewLine + "tidak diizinkan menyimpan berkas.", Nothing, 7000, eToastGlowColor.Red, eToastPosition.MiddleCenter)
            Exit Sub
        End If

        If My.Computer.FileSystem.FileExists(AlamatDokumen + "\Quotes\" + TBFileName.Text + ".qot") = True Then
            If psan("QOT Maker", eTaskDialogIcon.Exclamation, "Berkas Sudah Ada", "Berkas """ + TBFileName.Text + ".qot"" sudah ada, ingin menimpa file tersebut, atau menambahkan data yang sudah ada ?", eTaskDialogButton.Close, eTaskDialogBackgroundColor.OliveGreen, True, "Timpa", "Hapus berkas lama dan buat berkas baru", "Tambahkan", "Tambahkan Quote terpilih ke berkas yang ada") = eTaskDialogResult.Close Then
                Exit Sub
            End If
        Else
            SimpanKeBerkas(TBFileName.Text, LBData.Items)
            ToastNotification.Show(Me, "file QOT telah dibuat dengan nama " + TBFileName.Text + ".qot", eToastPosition.MiddleCenter)
        End If
        BSimpan.Enabled = False

        For Each baris As String In TBPencegah.Lines
            If Not Pencegah.Contains(baris) Then
                Pencegah.Add(baris)
            End If
        Next
        For Each item As String In Pencegah
            If Not TBPencegah.Text.Contains(item) Then
                Pencegah.Remove(item)
            End If
        Next
        BSimpan.Text = "Simpan"
        SimpanPengaturan(BagianPenyimpanan.Pencegah)
    End Sub

    Private Sub TBDari_LostFocus(sender As Object, e As EventArgs) Handles TBDari.LostFocus
        If TBDari.Text.Length > 0 Then
            Dim a As String
            If Not TBDari.Text.Substring(0, 1) = "@" Then
                a = TBDari.Text.Replace("@", "")
                TBDari.Text = "@" + a
            End If
        End If
    End Sub

    Private Sub TBDari_TextChanged(sender As Object, e As EventArgs) Handles TBDari.TextChanged
        TBFileName.Text = Kapitalkan(TBDari.Text.Replace("@", ""), True)
    End Sub

    Private Sub ButtonX3_Click(sender As Object, e As EventArgs) Handles BBersihkan.Click
        If status = 1 Then
            status = 0
        End If
        QOT()
    End Sub

    Private Sub QOTMaker_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If status = 1 Then
            Form_PengaturanUtama.Show()
        End If
        Form_BukanQuote.Close()
        Form_GenerateNumbering.Close()
        Dispose()
    End Sub

    Private Sub ButtonX4_Click(sender As Object, e As EventArgs) Handles BPanduan.Click
        If BPanduan.Text = "Panduan" Then
            psan("Panduan", eTaskDialogIcon.BlueFlag, "Membuat Berkas Quoologi", "1. Tempel data dari twitter / sumber lain ke bagian ""Data:""" + Environment.NewLine + "2. Jika dari Twitter, isi nama akun Twitter di bagian ""Nama Akun""" + Environment.NewLine + "3. Masukan nama file di bagian ""Nama File""" + Environment.NewLine + "4. Atur panjang min dan maks, teks pencegah / teks sampah bila perlu" + Environment.NewLine + "5. Kemudian klik kerjakan" + Environment.NewLine + "6. Kemudian tekan tombol simpan" + Environment.NewLine + Environment.NewLine + "Maka secara otomatis file ini akan tersimpan di dalam folder dimana file² Quotes yang lain berada dan akan secara otomatis tersedia di menu pengaturan" + Environment.NewLine + Environment.NewLine, eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Blue)
        ElseIf BPanduan.Text = "Hentikan" Then
            If status = 1 Then
                status = 0
            End If
            QOT()
        End If
    End Sub

    Private Sub ButtonItem1_Click(sender As Object, e As EventArgs) Handles ButtonItem1.Click
        If Timer1.Enabled = True Then
            MessageBoxEx.Show("Tutorial sedang berjalan", "Pembuat Quote", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        If psan("QOT Maker", eTaskDialogIcon.Information2, "Tutorial Membuat Berkas Quoologi", "<font size=""+2"">" + "Ketika perintah ini dijalankan, program akan menunjukan cara kami dalam membuat file .qot. Sehingga semua kontrol akan kami matikan sementara." + Chr(13) + Chr(13) + "Lanjutkan...?" + "</font>", eTaskDialogButton.Yes + eTaskDialogButton.No, eTaskDialogBackgroundColor.OliveGreen) = eTaskDialogResult.Yes Then
            If status = 1 Then
                status = 0
            End If
            QOT2()
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim waktu As Random = New Random
        If temp = 0 Then
            TBData.Enabled = True
            TBData.Focus()
            Timer1.Interval = waktu.Next(1000, 4000)
            ToastNotification.Show(TBData, "Pada bagian ini....", eToastPosition.MiddleCenter)
            temp = 1
        ElseIf temp = 1 Then
            Dim filepath As String = AlamatInstall + "\Contoh Teks.txt"
            Try
                Dim objReader As New System.IO.StreamReader(filepath)
                TBData.Text = objReader.ReadToEnd
            Catch ex As System.IO.FileNotFoundException
                Timer1.Enabled = False
                If MessageBoxEx.Show("Kami tidak dapat menemukan berkas ""Contoh Teks.txt"". (kesalahan: 100201)" + Environment.NewLine + "Akantetapi kami bisa memperbaikinya, Apakah anda ingin memperbaiki ?", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                    PerbaikiQuoologi()
                    ToastNotification.Show(Me, "Jika sudah diperbaiki, silakan jalankan tutorial ini kembali")
                End If
                Exit Sub
            End Try
            Timer1.Interval = waktu.Next(1000, 4000)
            ToastNotification.Show(TBData, "Masukan data dari twitter", eToastPosition.MiddleCenter)
            temp = 2
        ElseIf temp = 2 Then
            TBDari.Enabled = True
            TBDari.Focus()
            Timer1.Interval = waktu.Next(1000, 4000)
            ToastNotification.Show(TBDari, "Pada bagian ini....", eToastPosition.MiddleCenter)
            temp = 3
        ElseIf temp = 3 Then
            TBDari.Text = "@NasehatHebat"
            Timer1.Interval = waktu.Next(1000, 4000)
            ToastNotification.Show(TBDari, "Masukan nama akun twitter ""@NamaAkun""", eToastPosition.MiddleCenter)
            temp = 4
        ElseIf temp = 4 Then
            TBFileName.Enabled = True
            TBFileName.Focus()
            Timer1.Interval = waktu.Next(1000, 4000)
            ToastNotification.Show(TBFileName, "Pada bagian ini....", eToastPosition.MiddleCenter)
            temp = 5
        ElseIf temp = 5 Then
            TBFileName.Text = "Nasehat Hebat"
            Timer1.Interval = waktu.Next(1000, 4000)
            ToastNotification.Show(TBFileName, "Masukan nama file .qot", eToastPosition.MiddleCenter)
            temp = 6
        ElseIf temp = 6 Then
            BKerjakan.Enabled = True
            BKerjakan.Focus()
            Timer1.Interval = waktu.Next(1000, 4000)
            ToastNotification.Show(Me, "Kemudian klik kerjakan", eToastPosition.MiddleCenter)
            temp = 7
        ElseIf temp = 7 Then
            BBersihkan.Enabled = True
            LBData.Enabled = True
            kerjakan()
            BBukan.Enabled = True
            Timer1.Interval = 17000
            ToastNotification.Show(Me, "Ketika perintah ini dijalankan, maka program ini akan secara otomatis memilih teks mana" + Chr(13) + "yang termasuk Quotes dengan kondisi jika suatu baris tidak memiliki salah satu" + Chr(13) + "kata di dalam ""Teks Pencegah"" maka baris tersebut akan dianggap sebagai Quotes" + Chr(13) + "Program ini juga akan secara otomatis mengapus kata yang terdapat di ""Teks Sampah"".", Nothing, 16000, eToastGlowColor.Green, eToastPosition.BottomCenter)
            BSimpan.Enabled = False
            temp = 8
        ElseIf temp = 8 Then
            Timer1.Interval = 17000
            ToastNotification.Show(Me, "Akan tetapi, jika hasil yang ditampilkan tidak meyakinkan Anda, Anda dapat merubah kondisi" + Chr(13) + "pemilihan quote dengan berdasarkan acuan yang terdapat pada jendela ""Bukan Quote""" + Chr(13) + "untuk membukanya, cukup klik tombol ""Terhapus"" di sebelah kiri tombol ""Kerjakan"".", Nothing, 15000, eToastGlowColor.Green, eToastPosition.BottomCenter)
            TTungguTombol.Enabled = True
            temp = 9
        ElseIf temp = 9 Then
            ToastNotification.Show(Me, "Data dari twitter yang di salin ialah teks yang terdapat pada wall / dinding akun twitter tersebut" + Chr(13) + "sehingga akan didapatkan twit² dan teks pencegah dari akun tersebut yang selanjutnya akan diproses oleh aplikasi", Nothing, 13000, eToastGlowColor.Green, eToastPosition.BottomCenter)
            Timer1.Enabled = False
            BPanduan.Text = "Panduan"
        End If
    End Sub

    Private Sub TBData_KeyDown(sender As Object, e As KeyEventArgs) Handles TBData.KeyDown
        If e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Control + C" And TBData.SelectionLength > 30 Then
            ToastNotification.Show(TBData, "Data tidak dapat disalin", Nothing, 5000, eToastGlowColor.Red, eToastPosition.MiddleCenter)
            salin = True
        ElseIf e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Control + Insert" And TBData.SelectionLength > 30 Then
            e.Handled = True
            ToastNotification.Show(TBData, "Data tidak dapat disalin", Nothing, 5000, eToastGlowColor.Red, eToastPosition.MiddleCenter)
            salin = True
        ElseIf e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Control + X" And TBData.SelectionLength > 30 Then
            e.Handled = True
            TBData.SelectionLength = 0
            ToastNotification.Show(TBData, "Data tidak dapat disalin", Nothing, 5000, eToastGlowColor.Red, eToastPosition.MiddleCenter)
        ElseIf e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Control + A" Then
            e.Handled = True
            TBData.SelectAll()
        ElseIf e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Shift, Control + S" Then
            My.Computer.Clipboard.SetText(TBData.SelectedText.ToString)
        End If
        Timer3.Enabled = True
    End Sub

    Private Sub TBData_TextChanged(sender As Object, e As EventArgs) Handles TBData.TextChanged
        If TBData.Text.Contains("@") Then
            Try
                Label1.Text = TBData.Lines.Length
                Dim DaftarSumber As List(Of String) = New List(Of String)
                Dim temp As List(Of String) = New List(Of String)
                Dim penghitung As List(Of Integer) = New List(Of Integer)
                Dim sumber As String

                For Each baris As String In TBData.Lines
                    If baris.Contains("@") And baris.Contains("·") Then
                        Dim a As Integer = baris.IndexOf("@") + 1
                        sumber = "@" + baris.Substring(a, baris.IndexOf(" · ") - (a + 1))
                        DaftarSumber.Add(sumber)
                        If Not temp.Contains(sumber) Then
                            temp.Add(sumber)
                        End If
                    ElseIf baris.Contains("@") And TBData.Text.Contains("Retweet") And baris.Contains("  ") And Not baris.Contains("·") Then
                        Dim a As Integer = baris.IndexOf("@") + 1
                        sumber = "@" + baris.Substring(a, baris.IndexOf("  ") - a)
                        DaftarSumber.Add(sumber)
                        If Not temp.Contains(sumber) Then
                            temp.Add(sumber)
                        End If
                    End If
                Next

                Dim b As Integer = 0
                For Each isi As String In temp
                    For Each data As String In DaftarSumber
                        If data = isi Then
                            b += 1
                        End If
                    Next
                    penghitung.Add(b)
                    b = 0
                Next

                Dim maks As Integer = 0
                Dim indeks As Byte = 0
                Dim c As Byte = 0
                For Each nilai As Integer In penghitung
                    If nilai > maks Then
                        maks = nilai
                        indeks = c
                    End If
                    c += 1
                Next

                If temp.Count > 0 Then
                    Try
                        TBDari.Text = temp.Item(indeks)
                    Catch ex As Exception
                    End Try
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub LBData_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBData.SelectedIndexChanged
        Label2.Text = LBData.Items.Count
        Dim indexchar As Integer
        Dim temp As String
        Dim panjang As Integer
        indexchar = TBData.GetFirstCharIndexFromLine(alamatada.Item(LBData.SelectedIndex))
        indexchar = indexchar + panjangada.Item(LBData.SelectedIndex)
        temp = "-" + panjangada.Item(LBData.SelectedIndex)
        panjang = temp
        TBData.Select(indexchar, panjang)
        TBData.ScrollToCaret()
    End Sub

    Private Sub QOTMaker_Load(sender As Object, e As EventArgs) Handles Me.Load
        TBData.ContextMenu = ContextMenu1
        Dim a As Integer = 0
        For Each item As String In Pencegah
            TBPencegah.Text += item
            a += 1
            If a < Pencegah.Count Then
                TBPencegah.Text += Environment.NewLine
            End If
        Next
        If FirstRun = True Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
        End If
    End Sub

    Private Sub PilihSemuaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIPilihSemua.Click
        TBData.SelectAll()
    End Sub

    Private Sub TempelToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MITempel.Click
        sem = TBData.SelectionStart.ToString
        TBData.Text = TBData.Text.Insert(TBData.SelectionStart.ToString, My.Computer.Clipboard.GetText)
        TBData.SelectionStart = sem
    End Sub

    Private Sub KerjakanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIKerjakan.Click
        BKerjakan.PerformClick()
    End Sub

    Private Sub BersihkanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIBersihkan.Click
        TBData.Clear()
    End Sub

    Private Sub HapusToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MIHapus.Click
        sem = TBData.SelectionStart.ToString
        TBData.Text = TBData.Text.Remove(TBData.SelectionStart.ToString, TBData.SelectionLength.ToString)
        TBData.SelectionStart = sem
    End Sub

    Private Sub ButtonItem2_Click(sender As Object, e As EventArgs) Handles ButtonItem2.Click
        System.Diagnostics.Process.Start("http://youtu.be/aIuDtV9DpdU")
    End Sub

    Private Sub TBKerjakan_KeyDown(sender As Object, e As KeyEventArgs) Handles BKerjakan.KeyDown
        If e.KeyCode.ToString = "Return" Then
            BKerjakan.PerformClick()
        End If
    End Sub

    Private Sub BBukan_Click(sender As Object, e As EventArgs) Handles BBukan.Click
        Form_BukanQuote.Show()
    End Sub

    Private Sub ButtonX1_Click_1(sender As Object, e As EventArgs) Handles BRPencegah.Click
        TBPencegah.Text = "Reply" + Environment.NewLine + "Expand" + Environment.NewLine + "Retweet" + Environment.NewLine + "retweets" + Environment.NewLine + "RETWEETED" + Environment.NewLine + "Tweps" + Environment.NewLine + "tweps" + Environment.NewLine + "More" + Environment.NewLine + "more" + Environment.NewLine + "Follow" + Environment.NewLine + "follow" + Environment.NewLine + "Play" + Environment.NewLine + "YouTube" + Environment.NewLine + "ow.ly" + Environment.NewLine + "http://" + Environment.NewLine + "Http://" + Environment.NewLine + "bit.ly" + Environment.NewLine + "tinyurl.com" + Environment.NewLine + "pic.twitter" + Environment.NewLine + "@" + Environment.NewLine + "?" + Environment.NewLine + "Hub " + Environment.NewLine + "hub " + Environment.NewLine + "bb " + Environment.NewLine + "hp" + Environment.NewLine + "BB " + Environment.NewLine + "gabung" + Environment.NewLine + "Lowongan" + Environment.NewLine + "lowongan" + Environment.NewLine + "berminat" + Environment.NewLine + "selamat" + Environment.NewLine + "Berminat" + Environment.NewLine + "anggota" + Environment.NewLine + "Anggota" + Environment.NewLine + "Selamat"
    End Sub

    Private Sub ButtonItem3_Click(sender As Object, e As EventArgs) Handles ButtonItem3.Click
        Form_GenerateNumbering.ShowDialog()
    End Sub

    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        If salin = True Then
            My.Computer.Clipboard.Clear()
            salin = False
        End If
        Timer3.Enabled = False
    End Sub

    Private Sub QOTMaker_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        GeserKontrol(BPanduan, 610, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(TBFileName, 610, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BBukan, 610, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BKerjakan, 610, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BBersihkan, 610, Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BSimpan, 610, Pengubah.Dikurangi, Orientasi.Vertikal)
        If FirstRun = True And Form_Handle.TutorialSudah = False Then
            If MessageBoxEx.Show("Selamat datang pada bagian Pembuatan Quote" + Environment.NewLine + "Sebelum memulai, apakah Anda ingin menjalankan tutorial pembuatan quote ?", "Pembuat Quote", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                If status = 1 Then
                    status = 0
                End If
                QOT2()
                Form_Handle.TutorialSudah = True
            Else
                BalloonTip1.AutoCloseTimeOut = 7
                BalloonTip1.ShowBalloon(BPanduan)
                BalloonTip1.Enabled = False
            End If
        End If
    End Sub

    Dim abc2, abc As Integer

    Private Sub TTombol_Tick(sender As Object, e As EventArgs) Handles TTombol.Tick
        If abc2 <> 10 Then
            If abc = 0 Then
                BBukan.ColorTable = eButtonColor.OrangeWithBackground
                abc = 1
            Else
                BBukan.ColorTable = eButtonColor.BlueWithBackground
                abc = 0
            End If
            abc2 += 1
            Exit Sub
        End If
        TTombol.Enabled = False
    End Sub

    Private Sub TTungguTombol_Tick(sender As Object, e As EventArgs) Handles TTungguTombol.Tick
        TTombol.Enabled = True
    End Sub
End Class