﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Kesukaan
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.BTutup = New DevComponents.DotNetBar.ButtonX()
        Me.TBDaftarQuote = New System.Windows.Forms.ListBox()
        Me.LAsal = New DevComponents.DotNetBar.LabelX()
        Me.BSimpan = New DevComponents.DotNetBar.ButtonX()
        Me.BHapus = New DevComponents.DotNetBar.ButtonX()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
        Me.MITampil = New System.Windows.Forms.MenuItem()
        Me.MIUbah = New System.Windows.Forms.MenuItem()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'LabelX1
        '
        Me.LabelX1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(598, 511)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LabelX1.Size = New System.Drawing.Size(24, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LabelX1, New DevComponents.DotNetBar.SuperTooltipInfo("Banyak Baris", "", "Berisi banyaknya quote yang terfavoritkan oleh Pengguna.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(176, 58)))
        Me.LabelX1.TabIndex = 22
        Me.LabelX1.Text = "baris"
        '
        'BTutup
        '
        Me.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTutup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BTutup.Location = New System.Drawing.Point(628, 512)
        Me.BTutup.Name = "BTutup"
        Me.BTutup.Size = New System.Drawing.Size(75, 23)
        Me.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BTutup, New DevComponents.DotNetBar.SuperTooltipInfo("Tutup", "", "Klik untuk menutup jendela Quote Favorit.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(185, 58)))
        Me.BTutup.TabIndex = 19
        Me.BTutup.Text = "Tutup"
        '
        'TBDaftarQuote
        '
        Me.TBDaftarQuote.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBDaftarQuote.BackColor = System.Drawing.Color.White
        Me.TBDaftarQuote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.TBDaftarQuote.ForeColor = System.Drawing.Color.Black
        Me.TBDaftarQuote.FormattingEnabled = True
        Me.TBDaftarQuote.HorizontalScrollbar = True
        Me.TBDaftarQuote.ItemHeight = 15
        Me.TBDaftarQuote.Location = New System.Drawing.Point(5, 5)
        Me.TBDaftarQuote.Name = "TBDaftarQuote"
        Me.TBDaftarQuote.Size = New System.Drawing.Size(698, 499)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBDaftarQuote, New DevComponents.DotNetBar.SuperTooltipInfo("Daftar Quote Terfavorit", "", "Berisi daftar quote yang difavoritkan oleh pengguna." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Klik kanan dan pilih ubah" & _
            " jika ingin menghapus quote yang terfavorit.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(211, 96)))
        Me.TBDaftarQuote.TabIndex = 20
        '
        'LAsal
        '
        Me.LAsal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LAsal.AutoSize = True
        Me.LAsal.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LAsal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LAsal.ForeColor = System.Drawing.Color.Black
        Me.LAsal.Location = New System.Drawing.Point(5, 516)
        Me.LAsal.Name = "LAsal"
        Me.LAsal.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.LAsal.Size = New System.Drawing.Size(54, 15)
        Me.SuperTooltip1.SetSuperTooltip(Me.LAsal, New DevComponents.DotNetBar.SuperTooltipInfo("Asal Quote", "", "Berisi asal quote dari quote yang terpilih di Daftar Quote.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(173, 61)))
        Me.LAsal.TabIndex = 23
        Me.LAsal.Text = "AsalQuote"
        '
        'BSimpan
        '
        Me.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BSimpan.Location = New System.Drawing.Point(547, 546)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Size = New System.Drawing.Size(75, 23)
        Me.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BSimpan, New DevComponents.DotNetBar.SuperTooltipInfo("Simpan", "", "Klik untuk menyimpan perubahan yang telah dibuat oleh Anda.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(190, 59)))
        Me.BSimpan.TabIndex = 24
        Me.BSimpan.Text = "Simpan"
        '
        'BHapus
        '
        Me.BHapus.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BHapus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BHapus.Location = New System.Drawing.Point(448, 546)
        Me.BHapus.Name = "BHapus"
        Me.BHapus.Size = New System.Drawing.Size(75, 23)
        Me.BHapus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BHapus, New DevComponents.DotNetBar.SuperTooltipInfo("Hapus", "", "Klik untuk menghapus quote terpilih dari Daftar Quote.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(179, 58)))
        Me.BHapus.TabIndex = 25
        Me.BHapus.Text = "Hapus"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MITampil, Me.MIUbah})
        '
        'MITampil
        '
        Me.MITampil.Index = 0
        Me.MITampil.Text = "Tampilkan ke Quoologi"
        '
        'MIUbah
        '
        Me.MIUbah.Index = 1
        Me.MIUbah.Text = "Ubah"
        '
        'Timer2
        '
        Me.Timer2.Interval = 1000
        '
        'Form_Kesukaan
        '
        Me.AcceptButton = Me.BSimpan
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BTutup
        Me.ClientSize = New System.Drawing.Size(708, 540)
        Me.Controls.Add(Me.BHapus)
        Me.Controls.Add(Me.BSimpan)
        Me.Controls.Add(Me.LAsal)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.BTutup)
        Me.Controls.Add(Me.TBDaftarQuote)
        Me.DoubleBuffered = True
        Me.MinimumSize = New System.Drawing.Size(500, 500)
        Me.Name = "Form_Kesukaan"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Kesukaan"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BTutup As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBDaftarQuote As System.Windows.Forms.ListBox
    Friend WithEvents LAsal As DevComponents.DotNetBar.LabelX
    Friend WithEvents BSimpan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BHapus As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MITampil As System.Windows.Forms.MenuItem
    Friend WithEvents MIUbah As System.Windows.Forms.MenuItem
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
End Class
