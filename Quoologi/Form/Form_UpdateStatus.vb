﻿Imports DevComponents.DotNetBar
Imports UIQuoologi.Gambar

Public Class Form_UpdateStatus
    Inherits Metro.MetroForm
    Dim posisiGP As Integer = 212
    Dim posisiLP As Integer = 186
    Dim posisiLK As Integer = 116
    Dim posisiTB As Integer = 142
    Dim posisiform As Integer
    Public nilaiawal As Integer
    Dim a As Integer = 1
    Dim b As Integer = 0
    Dim banyakbaristbquote As Integer
    Dim banyakbarisstatus As Integer
    Dim siap As Boolean = False
    Dim tinggiform As Integer
    Dim belumtampil As Boolean = True
    Dim posisixsem As Integer
    Dim posisiysem As Integer
    Dim indikatortampil As Boolean = False
    Dim nilaisemulanah As Integer
    Dim berhenti As Boolean = False
    Dim nilaisemulanam As Integer
    Dim abc As Integer = 0
    Dim abc2 As Integer = 0
    Dim batasubah As Integer = 400
    Public daripengaturan As Boolean

    Private Sub setStyleFB()
        LNamaAkun.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LNamaAkun.Text = NamaAkunFB
        Label2.ForeColor = Color.FromArgb(255, 145, 151, 163)
        Label1.ForeColor = Color.FromArgb(255, 145, 151, 163)
        LSuka.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LSuka.BackColor = Color.FromArgb(255, 250, 251, 251)

        LKomentar.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LKomentar.BackColor = Color.FromArgb(255, 250, 251, 251)

        LPromosikan.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LPromosikan.BackColor = Color.FromArgb(255, 250, 251, 251)

        LBagikan.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LBagikan.BackColor = Color.FromArgb(255, 250, 251, 251)

        LTitik.ForeColor = Color.FromArgb(255, 59, 89, 152)
        LTitik.BackColor = Color.FromArgb(255, 250, 251, 251)

        LQuoologi.ForeColor = Color.FromArgb(255, 145, 151, 163)
        LIndikator.ForeColor = Color.SteelBlue
    End Sub

    Public Sub Sesuaikan(Optional ByVal dariawal As Boolean = True)
        TBQuote.ReadOnly = True
        Dim L2 As Integer = LabelX2.Location.Y
        Dim TBT As Integer = TBTag.Location.Y
        Dim LK As Integer = LKomen.Location.Y
        Dim LP As Integer = LPratinjau.Location.Y
        Dim GP As Integer = GroupPanel4.Location.Y
        Dim TBK As Integer = TBKomentar.Location.Y

        If dariawal = True Then
            If daripengaturan = True Then
                TBQuote.Text = Form_Handle.quotesem
                TBKomentar.Text = Form_Handle.komentarsem
            ElseIf daripengaturan = False Then
                TBQuote.Text = Form_Handle.statusfb
            End If
            Try
                TBTag.Text = Form_PengaturanFacebook.setTag(Form_Quote.Label1.Text.Remove(Form_Quote.Label1.Text.Length - 11), True)
            Catch ex As Exception
            End Try
        End If

        banyakbaristbquote = TBQuote.GetLineFromCharIndex(TBQuote.Text.Length) + 1
        AturStatus()

        If a < banyakbaristbquote Then
            Do Until a = banyakbaristbquote
                TBQuote.Height = TBQuote.Height + 16
                tinggiform += 16
                L2 += 16
                TBT += 16
                LK += 16
                LP += 16
                GP += 16
                TBK += 16
                posisiGP += 16
                posisiLP += 16
                posisiLK += 16
                posisiTB += 16
                a += 1
                If siap = True Then
                    posisiform -= 16
                End If
            Loop

            If dariawal = False Then
                setTinggiForm()
            End If

            If siap = True Then
                If Me.Location.Y < posisiform Then
                    GeserKontrol(Me, posisiform, Pengubah.Ditambah, Orientasi.Vertikal)
                Else
                    GeserKontrol(Me, posisiform, Pengubah.Dikurangi, Orientasi.Vertikal)
                End If
            End If

            GeserKontrol(LabelX2, L2, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(TBTag, TBT, Pengubah.Ditambah, Orientasi.Vertikal)
            If IzinkanKomentar = True Then
                GeserKontrol(TBKomentar, TBK, Pengubah.Ditambah, Orientasi.Vertikal)
                GeserKontrol(LKomen, LK, Pengubah.Ditambah, Orientasi.Vertikal)
            End If
            GeserKontrol(LPratinjau, LP, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(GroupPanel4, GP, Pengubah.Ditambah, Orientasi.Vertikal)
        Else
            Do Until a = banyakbaristbquote
                TBQuote.Height = TBQuote.Height - 16
                tinggiform -= 16
                L2 -= 16
                TBT -= 16
                LK -= 16
                LP -= 16
                GP -= 16
                TBK -= 16
                posisiGP -= 16
                posisiLP -= 16
                posisiLK -= 16
                posisiTB -= 16
                a -= 1
                If siap = True Then
                    posisiform += 16
                End If
            Loop

            If siap = True Then
                If Me.Location.Y > posisiform Then
                    GeserKontrol(Me, posisiform, Pengubah.Dikurangi, Orientasi.Vertikal)
                Else
                    GeserKontrol(Me, posisiform, Pengubah.Ditambah, Orientasi.Vertikal)
                End If
            End If

            GeserKontrol(LabelX2, L2, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(TBTag, TBT, Pengubah.Dikurangi, Orientasi.Vertikal)
            If IzinkanKomentar = True Then
                GeserKontrol(LKomen, LK, Pengubah.Dikurangi, Orientasi.Vertikal)
                GeserKontrol(TBKomentar, TBK, Pengubah.Dikurangi, Orientasi.Vertikal)
            End If
            GeserKontrol(LPratinjau, LP, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(GroupPanel4, GP, Pengubah.Dikurangi, Orientasi.Vertikal)

            If dariawal = False Then
                setTinggiForm()
            End If
        End If
        TBQuote.ReadOnly = False
    End Sub

    Private Sub AturStatus(Optional ByVal darikomentar As Boolean = False)
        TBKomentar.ReadOnly = True
        'TBQuote.ReadOnly = True
        Dim komentar As String = TBKomentar.Text
        Dim quote As String = TBQuote.Text
        Dim tinggis As Integer
        Dim tinggiGP As Integer

        If TambahPetik = True Then
            quote = """" + TBQuote.Text + """"
        End If

        If komentar = Nothing Then
            komentar = "{komentar kosong}"
        End If

        If IzinkanKomentar = True And KomenSebelumQuote = True Then
            LStatus.Text = komentar + Environment.NewLine + Environment.NewLine + quote + " - " + TBTag.Text
        ElseIf IzinkanKomentar = True And KomenSebelumQuote = False Then
            LStatus.Text = quote + " - " + TBTag.Text + Environment.NewLine + Environment.NewLine + komentar
        ElseIf IzinkanKomentar = False Then
            LStatus.Text = quote + " - " + TBTag.Text
        End If

        tinggis = LStatus.Height
        tinggiGP = GroupPanel4.Height
        banyakbarisstatus = LStatus.GetLineFromCharIndex(LStatus.Text.Length)

        If b < banyakbarisstatus Then
            Do Until b = banyakbarisstatus
                If darikomentar = True And siap = True Then
                    posisiform -= 8
                End If
                tinggis += 16
                tinggiGP += 16
                tinggiform += 16
                b += 1
            Loop

            If darikomentar = True And siap = True Then
                If Me.Location.Y < posisiform Then
                    GeserKontrol(Me, posisiform, Pengubah.Ditambah, Orientasi.Vertikal)
                Else
                    GeserKontrol(Me, posisiform, Pengubah.Dikurangi, Orientasi.Vertikal)
                End If
            End If

            LStatus.Height = tinggis
            GroupPanel4.Height = tinggiGP
        Else
            Do Until b = banyakbarisstatus
                If darikomentar = True And siap = True Then
                    posisiform += 8
                End If
                tinggis -= 16
                tinggiGP -= 16
                tinggiform -= 16
                b -= 1
            Loop

            If darikomentar = True And siap = True Then
                If Me.Location.Y > posisiform Then
                    GeserKontrol(Me, posisiform, Pengubah.Dikurangi, Orientasi.Vertikal)
                Else
                    GeserKontrol(Me, posisiform, Pengubah.Ditambah, Orientasi.Vertikal)
                End If
            End If
            LStatus.Height = tinggis
            GroupPanel4.Height = tinggiGP
        End If
        TBKomentar.ReadOnly = False
        'TBQuote.ReadOnly = False
    End Sub

    Private Sub setTinggiForm()
        Me.Height = tinggiform
    End Sub

    Public Sub indikator(ByVal teks As String, ByVal warna As Color, Optional ByVal keluar As Boolean = False)
        Dim nam As Integer
        Dim nah As Integer
        LIndikator.ForeColor = warna
        nah = LIndikator.Location.Y + 35
        If indikatortampil = True Then
            GeserKontrol(LIndikator, nah, Pengubah.Ditambah, Orientasi.Vertikal)
            indikatortampil = False
        End If

        If keluar = True Then
            GeserKontrol(LIndikator, nah, Pengubah.Ditambah, Orientasi.Vertikal)
            indikatortampil = False
            Exit Sub
        End If

        nam = LIndikator.Location.Y - 35

        LIndikator.Text = teks
        GeserKontrol(LIndikator, nam, Pengubah.Dikurangi, Orientasi.Vertikal)
        indikatortampil = True
    End Sub

    Public hilang As Boolean = False
    Public na As Integer

    Public Function Hentikan() As Boolean
        If berhenti = True Then
            Return True
        End If
        Return False
    End Function

    Public Sub Kirim()
        Timer4.Enabled = True
        If TBKomentar.Text = "" And IzinkanKomentar = True Then
            If abc2 = 0 Then
                ToastNotification.Show(Me, "Jika Anda tidak menginginkan adanya komentar" + Environment.NewLine + "silakan menonaktifkan fitur komentar pada menu Pengaturan Facebook", 5000, eToastPosition.MiddleCenter)
            Else
                abc2 = 0
            End If
            Timer2.Enabled = True
            Exit Sub
        End If
        Dim datasiap As String
        na = LIndikator.Location.Y
        If hilang = False Then
            GeserKontrol(BPengaturan, na + 27, Pengubah.Ditambah, Orientasi.Vertikal)
            hilang = True
        End If
        Form_Handle.TSelama.Enabled = False
        datasiap = LStatus.Text
        BKirim.Text = " Batalkan"
        BKirim.Symbol = ""
        indikator("CEK KONEKSI...", Color.DodgerBlue)

        If berhenti = True Then
            indikator("DIBATALKAN OLEH PENGGUNA...", Color.Red)
            Timer3.Enabled = True
            hilang = False
            Form_Handle.TSelama.Enabled = True
            Form_LoginFacebook.Dispose()
            Exit Sub
        End If

        If CekKoneksi() = True Then
            Try
                indikator("MENGHUBUNGKAN KE FACEBOOK...", Color.DodgerBlue)
                Application.DoEvents()
                Form_LoginFacebook.WebBrowser1.Navigate("https://m.facebook.com/dialog/feed?app_id=638861432861662&redirect_uri=https%3A%2F%2Fm.facebook.com")
                Application.DoEvents()
                waitforpageload()
            Catch ex As Exception
                indikator("MENGHUBUNGKAN GAGAL, CEK KONEKSI", Color.Red)
                BKirim.Text = " Kirim | F2"
                BKirim.Symbol = ""
                Application.DoEvents()
                Form_Handle.TSelama.Enabled = True
                Form_LoginFacebook.Dispose()
                Exit Sub
            End Try
            If berhenti = True Then
                indikator("DIBATALKAN OLEH PENGGUNA...", Color.Red)
                Timer3.Enabled = True
                Form_Handle.TSelama.Enabled = True
                hilang = False
                Form_LoginFacebook.Dispose()
                Exit Sub
            End If
        Else
            indikator("CEK KONEKSI...", Color.DodgerBlue, True)
            BKirim.Text = " Kirim | F2"
            BKirim.Symbol = ""
            GeserKontrol(BPengaturan, na - 27, Pengubah.Dikurangi, Orientasi.Vertikal)
            hilang = False
            Form_LoginFacebook.Dispose()
            Exit Sub
            If berhenti = True Then
                indikator("DIBATALKAN OLEH PENGGUNA...", Color.Red)
                Timer3.Enabled = True
                Form_Handle.TSelama.Enabled = True
                hilang = False
                Form_LoginFacebook.Dispose()
                Exit Sub
            End If
        End If

        indikator("CEK AKUN...", Color.DodgerBlue)
        Application.DoEvents()
        If berhenti = True Then
            indikator("DIBATALKAN OLEH PENGGUNA...", Color.Red)
            Timer3.Enabled = True
            hilang = False
            Form_Handle.TSelama.Enabled = True
            Form_LoginFacebook.Dispose()
            Exit Sub
        End If

        If Form_LoginFacebook.WebBrowser1.DocumentTitle = "Selamat datang di Facebook" Then
            indikator("ANDA BELUM LOGIN", Color.YellowGreen)
            BKirim.Text = " Kirim | F2"
            BKirim.Symbol = ""
            Application.DoEvents()
            If MessageBoxEx.Show("Silakan login terlebih dahulu dengan akun Facebook Anda" + Environment.NewLine + "Ingin login ?", "Facebook", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Form_LoginFacebook.Show()
                Form_LoginFacebook.fromstat = 1
                Exit Sub
            Else
                indikator("ANDA BELUM LOGIN", Color.YellowGreen, True)
                GeserKontrol(BPengaturan, na - 27, Pengubah.Dikurangi, Orientasi.Vertikal)
                hilang = False
                BKirim.Text = " Kirim | F2"
                BKirim.Symbol = ""
                Form_Handle.TSelama.Enabled = True
                Form_LoginFacebook.Dispose()
                Exit Sub
            End If
        End If
        If berhenti = True Then
            indikator("DIBATALKAN OLEH PENGGUNA...", Color.Red)
            Timer3.Enabled = True
            hilang = False
            Form_Handle.TSelama.Enabled = True
            Form_LoginFacebook.Dispose()
            Exit Sub
        End If
        Try
            BKirim.Enabled = False
            indikator("MENGIRIM DATA KE FACEBOOK...", Color.DodgerBlue)
            Application.DoEvents()
            UpdateStatus(datasiap)
            waitforpageload()
        Catch ex As Exception
            MessageBoxEx.Show(ex.ToString)
            indikator("UPDATE STATUS GAGAL", Color.Red)
            Application.DoEvents()
            BKirim.Text = " Kirim | F2"
            BKirim.Symbol = ""
            Form_Handle.TSelama.Enabled = True
            Form_LoginFacebook.Dispose()
            Exit Sub
        End Try

        Form_LoginFacebook.Dispose()
        BKirim.Enabled = True
        indikator("STATUS TERUPDATE...", Color.Green)
        Application.DoEvents()
        BKirim.Text = " Kirim | F2"
        BKirim.Symbol = ""
        Timer1.Enabled = True
        Form_Handle.TSelama.Enabled = True
        Timer4.Enabled = False
    End Sub

    Private Sub Update_Status_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        OtoTutupJendela = CBOtoTutup.Checked
        SimpanPengaturan()
        Form_Handle.quotesem = TBQuote.Text
        Form_Handle.komentarsem = TBKomentar.Text
        Form_Handle.updatestatustampil = False
        Form_LoginFacebook.Dispose()
        Form_LoginFacebook.WebBrowser1.Stop()
    End Sub

    Private Sub Update_Status_Load(sender As Object, e As EventArgs) Handles Me.Load
        PictureBox1.Image = AmbilGambar("fp_fb")
        PictureBox2.Image = AmbilGambar("privasi")
        If OtoTutupJendela = True Then
            CBOtoTutup.Checked = True
        Else
            CBOtoTutup.Checked = False
        End If

        If IzinkanKomentar = False Then
            tinggiform = 335
        Else
            tinggiform = 405
        End If
        Sesuaikan()
        If IzinkanEdit = False Then
            TBQuote.ReadOnly = True
        ElseIf IzinkanEdit = True Then
            TBQuote.ReadOnly = False
        End If
        setStyleFB()
        setTinggiForm()
        Me.CenterToScreen()
    End Sub

    Private Sub Update_Status_Move(sender As Object, e As EventArgs) Handles Me.Move
        If Me.Location.Y < posisiysem Then

        End If
    End Sub

    Private Sub Update_Status_Resize(sender As Object, e As EventArgs) Handles Me.ResizeEnd
        posisixsem = Me.Location.X
        posisiysem = Me.Location.Y
    End Sub

    Private Sub Update_Status_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Form_Handle.updatestatustampil = True
        posisiform = Me.Location.Y
        If IzinkanKomentar = True Then
            TBKomentar.Focus()
            TBKomentar.Visible = True
            LKomen.Visible = True
            GeserKontrol(GroupPanel4, posisiGP, Pengubah.Ditambah, Orientasi.Vertikal, 1, 0)
            GeserKontrol(LPratinjau, posisiLP, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(LKomen, posisiLK, Pengubah.Ditambah, Orientasi.Vertikal, 1, 5)
            GeserKontrol(TBKomentar, posisiTB, Pengubah.Dikurangi, Orientasi.Vertikal, 1, 5)
        End If
        siap = True
        nilaiawal = NilaiString(TBQuote.Text)

        If FirstRun = True And Form_Handle.updatestatussudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berfungsi untuk mengedit teks yang diperlukan" + Environment.NewLine + "dalam mengupdate status ke akun Facebook pengguna", 8000, eToastPosition.MiddleCenter)
            Form_Handle.updatestatussudah = True
        End If
    End Sub

    Private Sub TBQuote_LostFocus(sender As Object, e As EventArgs) Handles TBQuote.LostFocus
        If nilaiawal - NilaiString(TBQuote.Text) >= batasubah Or nilaiawal - NilaiString(TBQuote.Text) <= -batasubah Then
            ToastNotification.Show(Me, "Sepertinya Anda terlalu banyak merubah Quote, ini tidak diperkenankan.", Nothing, 5000, eToastGlowColor.Red, eToastPosition.TopCenter)
            TBQuote.Text = Form_Handle.statusfb
        End If
    End Sub

    Private Sub TBQuote_TextChanged(sender As Object, e As EventArgs) Handles TBQuote.TextChanged
        If TBQuote.Text <> Form_Handle.statusfb And belumtampil = True And siap = True Then
            ToastNotification.Show(Me, "Pengeditan quote tidak diperkenankan untuk terlalu banyak merubah quote.", Nothing, 5000, eToastGlowColor.Green, eToastPosition.TopCenter)
            belumtampil = False
        End If
        If banyakbaristbquote <> TBQuote.GetLineFromCharIndex(TBQuote.Text.Length) + 1 And siap = True Then
            Sesuaikan(False)
            AturStatus()
            Exit Sub
        End If
        TBQuote.ReadOnly = True
        AturStatus(True)
        TBQuote.ReadOnly = False
        setTinggiForm()
    End Sub

    Private Sub TBKomentar_TextChanged(sender As Object, e As EventArgs) Handles TBKomentar.TextChanged
        AturStatus(True)
        setTinggiForm()
    End Sub

    Private Sub LSuka_Click(sender As Object, e As EventArgs) Handles LSuka.MouseEnter
        LSuka.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LSuka_Leave(sender As Object, e As EventArgs) Handles LSuka.MouseLeave
        LSuka.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub LKomentar_Click(sender As Object, e As EventArgs) Handles LKomentar.MouseEnter
        LKomentar.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LKomentar_Leave(sender As Object, e As EventArgs) Handles LKomentar.MouseLeave
        LKomentar.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub LPromosikan_Click(sender As Object, e As EventArgs) Handles LPromosikan.MouseEnter
        LPromosikan.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LPromosikan_Leave(sender As Object, e As EventArgs) Handles LPromosikan.MouseLeave
        LPromosikan.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub LBagikan_Click(sender As Object, e As EventArgs) Handles LBagikan.MouseEnter
        LBagikan.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub LBagikan_Leave(sender As Object, e As EventArgs) Handles LBagikan.MouseLeave
        LBagikan.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles LQuoologi.MouseEnter
        LQuoologi.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub Label1_Leave(sender As Object, e As EventArgs) Handles LQuoologi.MouseLeave
        LQuoologi.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub Label2_Click(sender As Object, e As EventArgs) Handles Label2.MouseEnter
        Label2.Font = New Font("Arial", 9, FontStyle.Underline)
    End Sub

    Private Sub Label2_Leave(sender As Object, e As EventArgs) Handles Label2.MouseLeave
        Label2.Font = New Font("Arial", 9, FontStyle.Regular)
    End Sub

    Private Sub LNamaAkun_Click(sender As Object, e As EventArgs) Handles LNamaAkun.MouseEnter
        LNamaAkun.Font = New Font("Arial", 10.5, FontStyle.Bold Or FontStyle.Underline)
    End Sub

    Private Sub LNamaAkun_Leave(sender As Object, e As EventArgs) Handles LNamaAkun.MouseLeave
        LNamaAkun.Font = New Font("Arial", 10.5, FontStyle.Bold)
    End Sub

    Private Sub LQuoologi_Click(sender As Object, e As EventArgs) Handles LQuoologi.Click
        System.Diagnostics.Process.Start("https://www.facebook.com/games/quoologi/?fbs=-1&preview=1&locale=id_ID")
    End Sub

    Private Sub LNamaAkun_Click_1(sender As Object, e As EventArgs) Handles LNamaAkun.Click
        System.Diagnostics.Process.Start("https://www.facebook.com/")
    End Sub

    Private Sub LSuka_Click_1(sender As Object, e As EventArgs) Handles LSuka.Click
        System.Diagnostics.Process.Start("https://www.facebook.com/Quoologi")
    End Sub

    Private Sub BKirim_Click(sender As Object, e As EventArgs) Handles BKirim.Click
        If BKirim.Text = " Kirim | F2" Then
            berhenti = False
            Kirim()
        ElseIf BKirim.Text = " Batalkan" Then
            berhenti = True
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles BPengaturan.Click
        Form_Handle.dariupdatestatus = True
        Form_PengaturanFacebook.Show()
        Me.Close()
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If CBOtoTutup.Enabled = True Then
            Me.Close()
        End If
        Timer1.Enabled = False
    End Sub

    Private Sub TBTag_KeyPress(sender As Object, e As KeyEventArgs) Handles TBTag.KeyUp
        If e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Control + U" Then
            TBTag.ReadOnly = False
            batasubah = 10000
            ToastNotification.Show(Me, " ")
        End If
    End Sub

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        If abc2 <> 10 Then
            If abc = 0 Then
                BPengaturan.ColorTable = eButtonColor.OrangeWithBackground
                abc = 1
            Else
                BPengaturan.ColorTable = eButtonColor.BlueWithBackground
                abc = 0
            End If
            abc2 += 1
            Exit Sub
        End If
        Timer2.Enabled = False
    End Sub

    Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
        BKirim.Text = " Kirim | F2"
        BKirim.Symbol = ""
        indikator("DIBATALKAN OLEH PENGGUNA...", Color.Red, True)
        GeserKontrol(BPengaturan, na - 27, Pengubah.Dikurangi, Orientasi.Vertikal)
        Timer3.Enabled = False
        Timer4.Enabled = False
    End Sub

    Private Sub Timer4_Tick(sender As Object, e As EventArgs) Handles Timer4.Tick
        Application.DoEvents()
    End Sub
End Class