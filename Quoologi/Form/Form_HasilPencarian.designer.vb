﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_HasilPencarian
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LBaris = New DevComponents.DotNetBar.LabelX()
        Me.BTutup = New DevComponents.DotNetBar.ButtonX()
        Me.TBDaftarQuote = New System.Windows.Forms.ListBox()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.LAlasan = New DevComponents.DotNetBar.LabelX()
        Me.SuspendLayout()
        '
        'LabelX1
        '
        Me.LabelX1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(598, 511)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LabelX1.Size = New System.Drawing.Size(24, 23)
        Me.LabelX1.TabIndex = 17
        Me.LabelX1.Text = "baris"
        '
        'LBaris
        '
        Me.LBaris.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBaris.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LBaris.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LBaris.ForeColor = System.Drawing.Color.Black
        Me.LBaris.Location = New System.Drawing.Point(517, 511)
        Me.LBaris.Name = "LBaris"
        Me.LBaris.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LBaris.Size = New System.Drawing.Size(75, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBaris, New DevComponents.DotNetBar.SuperTooltipInfo("Banyak Baris", "", "Menunjukan banyaknya baris di dalam daftar hasil pencarian.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.LBaris.TabIndex = 16
        '
        'BTutup
        '
        Me.BTutup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTutup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BTutup.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BTutup.Location = New System.Drawing.Point(628, 512)
        Me.BTutup.Name = "BTutup"
        Me.BTutup.Size = New System.Drawing.Size(75, 23)
        Me.BTutup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BTutup, New DevComponents.DotNetBar.SuperTooltipInfo("Tutup", "", "Klik untuk menutup jendela ini.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BTutup.TabIndex = 13
        Me.BTutup.Text = "Tutup"
        '
        'TBDaftarQuote
        '
        Me.TBDaftarQuote.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBDaftarQuote.BackColor = System.Drawing.Color.White
        Me.TBDaftarQuote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.TBDaftarQuote.ForeColor = System.Drawing.Color.Black
        Me.TBDaftarQuote.FormattingEnabled = True
        Me.TBDaftarQuote.HorizontalScrollbar = True
        Me.TBDaftarQuote.ItemHeight = 15
        Me.TBDaftarQuote.Location = New System.Drawing.Point(5, 5)
        Me.TBDaftarQuote.Name = "TBDaftarQuote"
        Me.TBDaftarQuote.Size = New System.Drawing.Size(698, 499)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBDaftarQuote, New DevComponents.DotNetBar.SuperTooltipInfo("Daftar Hasil Pencarian", "", "Berisi teks yang terpilih sesuai dengan ada tidaknya teks yang telah ditentukan." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Dobel klik untuk menampilkanya di Quoologi.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBDaftarQuote.TabIndex = 14
        '
        'LAlasan
        '
        Me.LAlasan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LAlasan.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LAlasan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LAlasan.Font = New System.Drawing.Font("Consolas", 8.25!)
        Me.LAlasan.ForeColor = System.Drawing.Color.Black
        Me.LAlasan.Location = New System.Drawing.Point(86, 512)
        Me.LAlasan.Name = "LAlasan"
        Me.LAlasan.Size = New System.Drawing.Size(425, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LAlasan, New DevComponents.DotNetBar.SuperTooltipInfo("Alasan", "", "Teks yang ditampilkan disini menunjukan alasan kenapa teks yang terpilih dimasuka" & _
            "n ke daftar hasil pencarian.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(187, 84)))
        Me.LAlasan.TabIndex = 18
        Me.LAlasan.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'Form_HasilPencarian
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BTutup
        Me.ClientSize = New System.Drawing.Size(708, 540)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.LBaris)
        Me.Controls.Add(Me.BTutup)
        Me.Controls.Add(Me.TBDaftarQuote)
        Me.Controls.Add(Me.LAlasan)
        Me.MinimumSize = New System.Drawing.Size(500, 500)
        Me.Name = "Form_HasilPencarian"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Hasil Pencarian"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LBaris As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents BTutup As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBDaftarQuote As System.Windows.Forms.ListBox
    Friend WithEvents LAlasan As DevComponents.DotNetBar.LabelX
End Class
