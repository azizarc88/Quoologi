﻿Imports DevComponents.DotNetBar.Metro.MetroForm
Imports DevComponents.DotNetBar.Metro
Imports DevComponents.DotNetBar.Metro.Rendering.MetroRender
Imports DevComponents.DotNetBar
Imports System.Threading

Public Class Form_PengaturanUtama
    Inherits MetroForm
    Public Client As New Net.WebClient
    Public berkas As IO.FileInfo
    Public pathquote As New IO.DirectoryInfo(AlamatDokumen + "\Quotes")
    Public pathsuara As New IO.DirectoryInfo(AlamatDokumen + "\Sounds")
    Dim trans As Byte = 0
    Dim tem As Byte
    Dim tinggi_a As Integer
    Dim lebar_a As Integer
    Public total As Integer
    Dim listusesementara As List(Of String) = New List(Of String)
    Dim done As System.Collections.Specialized.StringCollection
    Dim trda As Thread

    Public Sub simpan()
        TampilSelama = SSelama.Value
        TampilSetiap = SSetiap.Value
        StartupQuoologi = CBStartup.Checked
        SatuanSetiap = CBWaktuSetiap.SelectedIndex
        QuoteTerpakai.Clear()
        For Each item As Object In LBQuotesUse.Items
            QuoteTerpakai.Add(item)
        Next
        BerkasSuara = CBFileSuara.Text
        OffsetVer = IIOffVer.Value
        OffsetHor = IIOffHor.Value
        ModeUpdate = CBModeUpd.SelectedIndex
        If Form_Handle.fixjendela = 1 Then
            defoffsethor = IIOffHor.Value
        End If
        MainkanSuara = CBSuara.Checked
        OtoSembunyi = CBAutoHide.Checked
        AnimasiTutup = CBAnimasiClose.Text
        AnimasiBuka = CBAnimasiOpen.Text
        KecAnimasi = SKecAni.Value
        JanganUlangi = CBJanganUlangiQuote.Checked
        AlamatTampungan = TBPathTampungan.Text
        OtomatisHentikan = CBHentikanQ.Checked
        Pengaturan.SimpanPengaturan()
        Pengaturan.SimpanPengaturan(BagianPenyimpanan.QuoteTerpakai)

        set_startup()
        set_timer()
    End Sub

    Public Sub set_startup()
        Dim applicationName As String = Application.ProductName
        Dim applicationPath As String = Application.ExecutablePath

        If CBStartup.Checked Then
            Dim RegKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            RegKey.SetValue(applicationName, "" & applicationPath & "")
            RegKey.Close()
        Else
            Dim RegKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
            RegKey.DeleteValue(applicationName, False)
            RegKey.Close()
        End If
    End Sub

    Public Sub baca_data()
        SSelama.Value = TampilSelama
        SSetiap.Value = Pengaturan.TampilSetiap
        CBWaktuSetiap.SelectedIndex = SatuanSetiap
        CBStartup.Checked = StartupQuoologi

        For Each item As Object In QuoteTerpakai
            listusesementara.Add(item)
            LBQuotesUse.Items.Add(item)
        Next

        CBFileSuara.Text = BerkasSuara
        CBSuara.Checked = MainkanSuara
        IIOffHor.Value = OffsetHor
        CBModeUpd.SelectedIndex = ModeUpdate
        IIOffVer.Value = OffsetVer
        CBAutoHide.Checked = OtoSembunyi
        SKecAni.Value = KecAnimasi
        CBAnimasiOpen.Text = AnimasiBuka
        CBAnimasiClose.Text = AnimasiTutup
        LTotalQot.Text = Form_Handle.totalqot
        CBJanganUlangiQuote.Checked = JanganUlangi
        TBPathTampungan.Text = AlamatTampungan
        CBHentikanQ.Checked = OtomatisHentikan
    End Sub

    Public Sub segarkan()
        Dim folder As IO.FileInfo() = pathquote.GetFiles("*.qot")
        LBQuotesAda.Items.Clear()
        For Each Me.berkas In folder
            LBQuotesAda.Items.Add(berkas.Name)
        Next
    End Sub

    Public Sub segarkan_hapus()
        LBQuotesUse.Items.Clear()
        For Each item As String In listusesementara
            LBQuotesUse.Items.Add(item)
        Next
        If LBQuotesUse.Items.Count > 0 Then
            BHapusSemua.Enabled = True
        Else
            BHapus.Enabled = False
            BHapusSemua.Enabled = False
        End If
        Form_Handle.berubah = True
    End Sub

    Public Sub geser_jendela()
        Dim temp As Int16
        If (Me.Location.X + Me.Size.Width >= Form_Quote.Location.X And Me.Location.X <= 1366 And Me.Location.Y + Me.Size.Height >= Form_Quote.Location.Y And Me.Location.Y <= 768) Then
            Do Until Me.Location.X = 670
                temp = Me.Location.X
                If temp > 780 Then
                    temp -= 10
                ElseIf temp > 750 And temp <= 780 Then
                    temp -= 3
                ElseIf temp > 730 And temp <= 750 Then
                    temp -= 2
                Else
                    temp -= 1
                End If
                Me.Location = New Point(temp, Form_Handle.y)
            Loop
        End If
    End Sub

    Private Sub Main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Form_Handle.MIHentikan.Checked = False Or Form_Handle.MIHentikan.Text.Contains(".exe") Then
            Form_Handle.TSetiap.Enabled = True
        ElseIf Form_Handle.MIHentikan.Checked = True Then
            Form_Handle.TSetiap.Enabled = False
        End If
        Form_Handle.spengaturan = False
        Form_Handle.TCekUpdate.Enabled = True
        Dispose()
    End Sub

    Private Sub Main_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim foldersuara As IO.FileInfo() = pathsuara.GetFiles("*.wav")
        Form_Handle.spengaturan = True
        segarkan()
        If Form_Handle.stat = 0 Then
            Form_Handle.y = Screen.PrimaryScreen.Bounds.Height - 630
            Form_Handle.x = Screen.PrimaryScreen.WorkingArea.Width - 450
            Form_Handle.stat = 1
        End If
        Me.Location = New Point(Form_Handle.x, Form_Handle.y)

        For Each item As IO.FileInfo In foldersuara
            CBFileSuara.Items.Add(item.Name)
        Next

        baca_data()
        Form_Handle.TCekUpdate.Dispose()
        Form_Handle.TCekUpdate.Interval = 10000
        Form_Handle.TCekUpdate.Enabled = False
        setstatus()

        If DaftarQuote.Count = 0 Then
            ButtonX1.Enabled = False
        ElseIf DaftarQuote.Count <> 0 Then
            ButtonX1.Enabled = True
        End If

        If LBQuotesUse.SelectedItem = Nothing Then
            BHapus.Enabled = False
        Else
            BHapus.Enabled = True
        End If

        If LBQuotesUse.Items.Count = 0 Then
            BHapusSemua.Enabled = False
        End If

        LBanyakQuoteAda.Text = LBQuotesAda.Items.Count
        LBanyakQuoteUse.Text = LBQuotesUse.Items.Count
    End Sub

    Private Sub pengaturan_Move(sender As Object, e As EventArgs) Handles Me.Move
        Form_Handle.x = Me.Location.X
        Form_Handle.y = Me.Location.Y
    End Sub

    Private Sub Main_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If Form_Handle.ceka = 1 Then
            BCPembaruanQuoologi.Enabled = False
        End If
        If Form_Handle.cekq = 1 Then
            BCekQuotesBaru.Enabled = False
        End If
        If Form_Handle.bug = True Then
            Form_Handle.segarkan()
            Form_Handle.bug = False
        End If
        Form_Handle.TSetiap.Enabled = False
        LSelama.Text = SSelama.Value.ToString
        LSetiap.Text = SSetiap.Value.ToString

        trans = 1

        If FirstRun = True And Form_Handle.pengaturanutamasudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berisi properti² pengaturan secara umum" + Environment.NewLine + "Anda bisa merubah pengaturan Quoologi disini", 8000, eToastPosition.MiddleCenter)
            Form_Handle.pengaturanutamasudah = True
        End If
    End Sub

    Private Sub SSelama_ValueChanged(sender As Object, e As EventArgs) Handles SSelama.ValueChanged
        LSelama.Text = SSelama.Value.ToString
    End Sub

    Private Sub SSetiap_ValueChanged(sender As Object, e As EventArgs) Handles SSetiap.ValueChanged
        LSetiap.Text = SSetiap.Value.ToString
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        Dim n As String
        n = TampilSelama
        Form_Handle.TSelama.Interval = n + "000"
        Form_Handle.TSelama.Enabled = True
        Quoologi.HideQuoologi()
        read_q()
        Quoologi.ShowQuoologi()
        Form_Quote.Label2.Visible = False
        geser_jendela()
        Me.Focus()
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        Dim b As Byte = 0

        If LBQuotesUse.Items.Count = 0 Then
            ButtonX1.Enabled = False
            MessageBoxEx.Show("Quote terpilih kosong?, silakan pilih minimal satu berkas Quote terlebih dahulu.", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If Form_Handle.berubah = True Then
            DaftarQuote.Clear()
            DaftarAlamatQuote.Clear()
            QuoteTerpakai.Clear()
            Try
                QuoteSudahTampil.Clear()
                My.Computer.FileSystem.DeleteFile(AlamatTampungan + "\quote.dft", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
            Catch ex As Exception
            End Try

            For Each item As Object In LBQuotesUse.Items
                QuoteTerpakai.Add(item)
            Next

            Try
                MuatDataQuotePengaturan()
            Catch ex As NullReferenceException
                MessageBoxEx.Show("Kesalahan pemuatan data, Terdapat masalah saat memasukan teks Quote ke memori." + Environment.NewLine + ex.Message + " (kesalahan: 080601)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If

        If LBQuotesUse.Items.Count > 0 Then
            ButtonX1.Enabled = True
        End If
        simpan()
        ToastNotification.Show(Me, "Pengaturan telah tersimpan", eToastPosition.BottomCenter)

        If DaftarQuote.Count <> 0 Then
            ButtonX1.Enabled = True
        End If
        Form_Handle.totalqot = DaftarQuote.Count
        LTotalQot.Text = DaftarQuote.Count
        Form_Handle.berubah = False
    End Sub

    Private Sub BKeluar_Click(sender As Object, e As EventArgs) Handles BKeluar.Click
        If MessageBoxEx.Show("Apakah Anda yakin ingin keluar ?", "Quoologi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Form_Handle.Close()
            Application.ExitThread()
        End If
    End Sub

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        Dim banyak As Byte = LBQuotesAda.SelectedItems.Count
        If listusesementara.Contains(LBQuotesAda.SelectedItem) And banyak = 1 Then
            ToastNotification.Show(LBQuotesUse, "Data sudah ada", Nothing, 2000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            Exit Sub
        ElseIf LBQuotesAda.SelectedItem = Nothing Then
            ToastNotification.Show(LBQuotesUse, "Pilih Quotes dahulu", Nothing, 2000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            Exit Sub
        End If

        For Each terpilih As Object In LBQuotesAda.SelectedItems
            If listusesementara.Contains(terpilih) = False Then
                listusesementara.Add(terpilih)
            End If
        Next
        segarkan_hapus()
        LBanyakQuoteUse.Text = LBQuotesUse.Items.Count
    End Sub

    Private Sub BHapus_Click(sender As Object, e As EventArgs) Handles BHapus.Click
        Form_Handle.TSetiap.Enabled = False
        For Each terpilih As Object In LBQuotesUse.SelectedItems
            listusesementara.Remove(terpilih)
        Next
        segarkan_hapus()
        BHapus.Enabled = False
        LBanyakQuoteUse.Text = LBQuotesUse.Items.Count
    End Sub

    Private Sub BTambahSemua_Click(sender As Object, e As EventArgs) Handles BTambahSemua.Click
        For Each value As String In LBQuotesAda.Items
            If LBQuotesUse.Items.Contains(value) = False Then
                listusesementara.Add(value)
            End If
        Next
        segarkan_hapus()
        LBanyakQuoteUse.Text = LBQuotesUse.Items.Count
    End Sub

    Private Sub BHapusSemua_Click(sender As Object, e As EventArgs) Handles BHapusSemua.Click
        For Each value As String In LBQuotesUse.Items
            listusesementara.Remove(value)
        Next
        segarkan_hapus()
        BHapusSemua.Enabled = False
        LBanyakQuoteUse.Text = LBQuotesUse.Items.Count
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
        Form_Handle.berubah = False
    End Sub

    Private Sub CBFileSuara_TextChanged(sender As Object, e As EventArgs) Handles CBFileSuara.TextChanged
        If CBFileSuara.Text = "" Then
            CBSuara.Checked = False
            CBSuara.Enabled = False
        Else
            CBSuara.Enabled = True
        End If
    End Sub

    Private Sub LBQuotesAda_KeyUp(sender As Object, e As KeyEventArgs) Handles LBQuotesAda.KeyUp
        If e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Control + O" Then
            Diagnostics.Process.Start("Explorer.exe", "/select," + AlamatDokumen + "\Quotes\" + LBQuotesAda.SelectedItem.ToString)
        End If
    End Sub

    Private Sub LBQuotesAda_MouseClick(sender As Object, e As MouseEventArgs) Handles LBQuotesAda.MouseClick
        If LBQuotesAda.SelectedItems.Count > 0 Then
            BTambah.Enabled = True
            BHapusH.Enabled = True
            BEdit.Enabled = True
        Else
            BTambah.Enabled = False
            BHapusH.Enabled = False
            BEdit.Enabled = False
        End If
    End Sub

    Private Sub LBQuotesUse_MouseClick(sender As Object, e As MouseEventArgs) Handles LBQuotesUse.MouseClick
        If LBQuotesUse.SelectedItem = Nothing Then
            BHapus.Enabled = False
        Else
            BHapus.Enabled = True
        End If
    End Sub

    Private Sub RMIQOTMaker_Click(sender As Object, e As EventArgs) Handles RMIQOTMaker.Click
        Form_PembuatQuote.Show()
        Form_PembuatQuote.status = 1
        statusqot = 1
        Me.Close()
    End Sub

    Private Sub RMITentang_Click(sender As Object, e As EventArgs) Handles RMITentang.Click
        Form_Tentang.Show()
        Form_Handle.status_2 = 1
        Me.Close()
    End Sub

    Private Sub SKecAni_ValueChanged(sender As Object, e As EventArgs) Handles SKecAni.ValueChanged
        LKecAnimasi.Text = SKecAni.Value.ToString + "0"
    End Sub

    Private Sub CBAnimasiClose_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBAnimasiClose.SelectedIndexChanged
        Dim waktu As String
        If trans = 0 Then
            Exit Sub
        End If
        If CBAnimasiOpen.Text = "Menyatu" And CBAnimasiClose.Text = "Menyatu" = False Then
            ToastNotification.Show(Me, "Gaya ""Menyatu"" saat mulai tidak bisa dikombinasikan" + Chr(13) + "dengan gaya selain ""Menyatu"" saat hilang", Nothing, 5000, eToastGlowColor.Orange, eToastPosition.BottomCenter)
            CBAnimasiOpen.Text = "Geser Horizontal Kanan"
        End If
        geser_jendela()
        AnimateWindow(Form_Quote.Handle, 100, AnimateWindowFlags.AKTIF)
        waktu = KecAnimasi.ToString + "0"
        If CBAnimasiClose.Text = "Tengah" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.TENGAH Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Menyatu" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.MENYATU Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Geser Horizontal Kiri" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KIRI Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Geser Horizontal Kanan" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KANAN Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Geser Vertikal Atas" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_ATAS Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Geser Vertikal Bawah" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_BAWAH Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Persempit Horizontal Kiri" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.HORIZONTAL_KIRI Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Persempit Horizontal Kanan" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.HORIZONTAL_KANAN Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Persempit Vertikal Atas" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.VERTIKAL_ATAS Or AnimateWindowFlags.HILANG)
        ElseIf CBAnimasiClose.Text = "Persempit Vertikal Bawah" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.VERTIKAL_BAWAH Or AnimateWindowFlags.HILANG)
        End If
        AnimateWindow(Form_Quote.Handle, 100, AnimateWindowFlags.AKTIF)
        Form_Quote.Show()
        Form_Quote.Label2.Visible = False
        resetTPreview()
        Form_Handle.TSelama.Enabled = False
    End Sub

    Private Sub BHapusH_Click(sender As Object, e As EventArgs) Handles BHapusH.Click
        If MessageBoxEx.Show("Apa Anda yakin ingin menghapus berkas terpilih secara permanen ?", "Quoologi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Exit Sub
        End If
        Try
            For Each terpilih As Object In LBQuotesAda.SelectedItems
                My.Computer.FileSystem.DeleteFile(AlamatDokumen + "\Quotes\" + terpilih.ToString(), FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                If QuoteTerpakai.Contains(terpilih) Then
                    listusesementara.Remove(terpilih)
                    segarkan_hapus()
                    simpan()
                End If
                LBQuotesAda.Items.Remove(terpilih)
            Next
        Catch ex2 As System.InvalidOperationException
        Catch ex As Exception
            MessageBoxEx.Show("Timbul galat saat menghapus berkas, pesan eror: " + ex.GetBaseException.ToString + Environment.NewLine + "Pastikan berkas tidak dipakai oleh aplikasi lain. (kesalahan: 080701)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        If LBQuotesAda.SelectedItems.Count > 0 Then
            BTambah.Enabled = True
            BHapusH.Enabled = True
            BEdit.Enabled = True
        Else
            BTambah.Enabled = False
            BHapusH.Enabled = False
            BEdit.Enabled = False
        End If

        LBQuotesAda.Focus()
        LBanyakQuoteAda.Text = LBQuotesAda.Items.Count
    End Sub

    Private Sub BEdit_Click(sender As Object, e As EventArgs) Handles BEdit.Click
        Dim b As Integer = 1
        Dim f, l As Integer
        Dim temp, fix As String
        Dim dataedit As List(Of String) = New List(Of String)
        Dim teks As String = vbNullString
        Form_PembuatQuote.Show()
        ToastNotification.Show(Form_PembuatQuote, "Memuat....", 100000, eToastPosition.MiddleCenter)
        Application.DoEvents()
        Me.Close()
        Dim filepath As String = AlamatDokumen + "\Quotes\" + LBQuotesAda.SelectedItem.ToString
        Try
            Using Berkas As New IO.StreamReader(filepath)
                Application.DoEvents()
                Do While Not Berkas.EndOfStream
                    temp = Berkas.ReadLine
                    If temp.Contains("  Q[") Then
                        f = temp.IndexOf("]") + 1
                        l = temp.LastIndexOf("[") - f
                        fix = temp.Substring(f, l)
                        dataedit.Add(fix)
                    End If
                    Application.DoEvents()
                Loop
            End Using
        Catch ex As Exception
            MessageBoxEx.Show("Galat saat memuat data, galat ini terjadi mungkin karena berkas quote mengalami kerusakan atau berkas tidak berisi quote.", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        total = dataedit.Count
        For Each baris As String In dataedit
            teks += baris + Environment.NewLine
            Application.DoEvents()
        Next
        Form_PembuatQuote.TBData.Text = teks
        Dim a As String = LBQuotesAda.SelectedItem.ToString
        a = a.Remove(a.Length - 4)
        If a.Chars(0) = "@" Then
            Form_PembuatQuote.TBDari.Text = a.Replace(" ", "")
        Else
            Form_PembuatQuote.TBDari.Text = "@" + a.Replace(" ", "")
        End If
        Form_PembuatQuote.TBFileName.Text = a
        Form_PembuatQuote.status = 1
        statusqot = 1
        ToastNotification.Close(Form_PembuatQuote)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles TPreview.Tick
        AnimateWindow(Form_Quote.Handle, 200, AnimateWindowFlags.MENYATU Or AnimateWindowFlags.HILANG)
        TPreview.Enabled = False
        QuoteMuncul = False
    End Sub

    Private Sub CBAnimasiOpen_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBAnimasiOpen.SelectedIndexChanged
        Dim waktu As String
        If trans = 0 Then
            Exit Sub
        End If
        If CBAnimasiOpen.Text = "Menyatu" And CBAnimasiClose.Text = "Menyatu" = False Then
            ToastNotification.Show(Me, "Gaya ""Menyatu"" saat mulai tidak bisa dikombinasikan" + Chr(13) + "dengan gaya selain ""Menyatu"" saat hilang", Nothing, 5000, eToastGlowColor.Orange, eToastPosition.BottomCenter)
            CBAnimasiClose.Text = "Menyatu"
        End If
        geser_jendela()
        AnimateWindow(Form_Quote.Handle, 80, AnimateWindowFlags.MENYATU Or AnimateWindowFlags.HILANG)
        waktu = KecAnimasi.ToString + "0"
        If CBAnimasiOpen.Text = "Tengah" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.TENGAH)
        ElseIf CBAnimasiOpen.Text = "Menyatu" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.MENYATU)
        ElseIf CBAnimasiOpen.Text = "Geser Horizontal Kiri" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KIRI)
        ElseIf CBAnimasiOpen.Text = "Geser Horizontal Kanan" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KANAN)
        ElseIf CBAnimasiOpen.Text = "Geser Vertikal Atas" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_ATAS)
        ElseIf CBAnimasiOpen.Text = "Geser Vertikal Bawah" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_BAWAH)
        ElseIf CBAnimasiOpen.Text = "Perluas Horizontal Kiri" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.HORIZONTAL_KIRI)
        ElseIf CBAnimasiOpen.Text = "Perluas Horizontal Kanan" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.HORIZONTAL_KANAN)
        ElseIf CBAnimasiOpen.Text = "Perluas Vertikal Atas" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.VERTIKAL_ATAS)
        ElseIf CBAnimasiOpen.Text = "Perluas Vertikal Bawah" Then
            AnimateWindow(Form_Quote.Handle, waktu, AnimateWindowFlags.VERTIKAL_BAWAH)
        End If
        Form_Quote.Show()
        Form_Quote.Label2.Visible = False
        resetTPreview()
        Form_Handle.TSelama.Enabled = False
    End Sub

    Private Sub RMIFacebook_Click(sender As Object, e As EventArgs) Handles RMIFacebook.Click
        System.Diagnostics.Process.Start("https://www.facebook.com/Quoologi")
        Me.Close()
    End Sub

    Private Sub RMITwitter_Click(sender As Object, e As EventArgs) Handles RMITwitter.Click
        System.Diagnostics.Process.Start("https://twitter.com/Agateophobea")
        Me.Close()
    End Sub

    Private Sub like_click(sender As Object, e As EventArgs) Handles RMILike.Click
        System.Diagnostics.Process.Start("https://www.facebook.com/Quoologi")
        Me.Close()
    End Sub

    Private Sub dislike_click(sender As Object, e As EventArgs) Handles RMIDislike.Click
        System.Diagnostics.Process.Start("https://www.facebook.com/Quoologi")
        Me.Close()
    End Sub

    Private Sub RMICari_Click(sender As Object, e As EventArgs) Handles RMIKQuoologi.Click
        Form_PengaturanGaya.Show()
        Form_PengaturanGaya.status = 1
        Me.Close()
    End Sub

    Private Sub BCPembaruanQuoologi_Click(sender As Object, e As EventArgs) Handles BCPembaruanQuoologi.Click
        BCPembaruanQuoologi.Enabled = False
        Form_Handle.ceka = 1
        updating.cek_pengaturan_a()
        TThreadA.Enabled = True
    End Sub

    Private Sub BCekQuotesBaru_Click(sender As Object, e As EventArgs) Handles BCekQuotesBaru.Click
        BCekQuotesBaru.Enabled = False
        updating.cek_pengaturan_q()
    End Sub

    Private Sub RMIUpdateApp_Click(sender As Object, e As EventArgs) Handles RMIUpdateApp.Click
        TEvent.Enabled = True
        BCPembaruanQuoologi.PerformClick()
        TEvent.Enabled = False
    End Sub

    Private Sub RMIUpdateQuotes_Click(sender As Object, e As EventArgs) Handles RMIUpdateQot.Click
        BCekQuotesBaru.PerformClick()
    End Sub

    Private Sub RMIUpdate_Click(sender As Object, e As EventArgs) Handles RMIUpdate.Click
        BCPembaruanQuoologi.PerformClick()
    End Sub

    Private Sub RadialMenu1_ItemClick(sender As Object, e As EventArgs) Handles RadialMenu1.MouseEnter
        RadialMenu1.Symbol = ""
    End Sub

    Private Sub RadialMenu1_MouseLeave(sender As Object, e As EventArgs) Handles RadialMenu1.MouseLeave
        RadialMenu1.Symbol = ""
    End Sub

    Private Sub TEvent_Tick(sender As Object, e As EventArgs) Handles TEvent.Tick
        Application.DoEvents()
    End Sub

    Private Sub LBQuotesAda_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBQuotesAda.DoubleClick
        BTambah.PerformClick()
    End Sub

    Private Sub LBQuotesUse_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBQuotesUse.DoubleClick
        BHapus.PerformClick()
    End Sub

    Private Sub IIOffVer_ValueChanged(sender As Object, e As EventArgs) Handles IIOffVer.ValueChanged
        Dim x As Integer
        Dim y As Integer
        Dim tinggi As Integer = Quoologi.postinggi
        If trans = 1 Then
            If QuoteMuncul = False Then
                geser_jendela()
                Dim tinggi_s As Integer = tinggi + IIOffVer.Value
                y = Screen.PrimaryScreen.WorkingArea.Height - tinggi_s
                x = Screen.PrimaryScreen.WorkingArea.Width - lebar_a
                Form_Quote.Location = New Point(x, y)
                AnimasiOpen(Form_Quote.Handle)
                Form_Quote.Show()
                Form_Quote.Label2.Visible = False
                TPreview.Enabled = True
            End If
        End If
        tinggi = tinggi + IIOffVer.Value
        y = Screen.PrimaryScreen.WorkingArea.Height - tinggi
        tinggi_a = tinggi
        x = Screen.PrimaryScreen.WorkingArea.Width - lebar_a
        Form_Quote.Location = New Point(x, y)
        resetTPreview()
        Form_Handle.TSelama.Enabled = False
    End Sub

    Private Sub IIOffHor_ValueChanged(sender As Object, e As EventArgs) Handles IIOffHor.ValueChanged
        Dim x As Integer
        Dim y As Integer
        Dim lebar As Integer = Quoologi.lebar
        If trans = 1 Then
            If QuoteMuncul = False Then
                geser_jendela()
                Dim lebar_s As Integer = lebar + IIOffHor.Value
                y = Screen.PrimaryScreen.WorkingArea.Height - tinggi_a
                x = Screen.PrimaryScreen.WorkingArea.Width - lebar_s
                Form_Quote.Location = New Point(x, y)
                AnimasiOpen(Form_Quote.Handle)
                Form_Quote.Show()
                Form_Quote.Label2.Visible = False
                TPreview.Enabled = True
            End If
        Else
            lebar = Quoologi.lebar
        End If
        lebar = lebar + IIOffHor.Value
        y = Screen.PrimaryScreen.WorkingArea.Height - tinggi_a
        x = Screen.PrimaryScreen.WorkingArea.Width - lebar
        lebar_a = lebar
        Form_Quote.Location = New Point(x, y)
        resetTPreview()
        Form_Handle.TSelama.Enabled = False
    End Sub

    Public Sub resetTPreview()
        Form_Handle.TSelama.Enabled = False
        Form_Quote.TFirst.Enabled = False
        TPreview.Dispose()
        TPreview.Enabled = True
        TPreview.Interval = 3000
    End Sub

    Private Sub BTesSuara_Click(sender As Object, e As EventArgs) Handles BTesSuara.Click
        My.Computer.Audio.Play(AlamatDokumen + "\Sounds\" + CBFileSuara.Text)
    End Sub

    Private Sub CBJanganUlangiQuote_CheckedChanged(sender As Object, e As EventArgs) Handles CBJanganUlangiQuote.CheckedChanged
        If CBJanganUlangiQuote.Checked = True Then
            Me.Height = 603
            If Me.Location.Y > 112 Then
                GeserKontrol(Me, 112, Pengubah.Dikurangi, Orientasi.Vertikal)
            End If
            GeserKontrol(GroupPanel2, 540, Pengubah.Ditambah, Orientasi.Vertikal)
            GeserKontrol(GroupPanel1, 489, Pengubah.Ditambah, Orientasi.Vertikal)
        Else
            GeserKontrol(GroupPanel1, 437, Pengubah.Dikurangi, Orientasi.Vertikal)
            GeserKontrol(GroupPanel2, 488, Pengubah.Dikurangi, Orientasi.Vertikal)
            Me.Height = 551
            If Me.Location.Y >= 112 And Me.Location.Y <= 165 Then
                GeserKontrol(Me, 165, Pengubah.Ditambah, Orientasi.Vertikal)
            End If
        End If
    End Sub

    Private Sub BTampunganJelajah_Click(sender As Object, e As EventArgs) Handles BTampunganJelajah.Click
        FolderBrowserDialog1.ShowDialog()
        If FolderBrowserDialog1.SelectedPath = "" Then
            Exit Sub
        End If
        TBPathTampungan.Text = FolderBrowserDialog1.SelectedPath
    End Sub

    Private Sub BReset_Click(sender As Object, e As EventArgs) Handles BReset.Click
        TBPathTampungan.Text = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Quoologi"
    End Sub

    Private Sub CBAutoHide_CheckedChanged1(sender As Object, e As EventArgs) Handles CBAutoHide.MouseEnter
        If CBAutoHide.Text = "Sembunyikan Quoologi ketika..." Then
            CBAutoHide.Text = "... kursor mendekati Quoologi"
        Else
            CBAutoHide.Text = "Sembunyikan Quoologi ketika..."
        End If
    End Sub

    Private Sub CBHentikanQ_CheckedChanged(sender As Object, e As EventArgs) Handles CBHentikanQ.CheckedChanged
        If CBHentikanQ.Checked = True Then
            GeserKontrol(BProgramH, 323, Pengubah.Dikurangi, Orientasi.Horizontal)
            CBHentikanQ.Text = "Hentikan otomatis ketika"
        Else
            CBHentikanQ.Text = "Hentikan quoologi otomatis ketika"
            GeserKontrol(BProgramH, 372, Pengubah.Ditambah, Orientasi.Horizontal)
        End If
    End Sub

    Private Sub BProgramH_MouseClick(sender As Object, e As MouseEventArgs) Handles BProgramH.MouseClick
        Form_ProgramTerkecuali.ShowDialog()
    End Sub

    Private Sub TThread_Tick(sender As Object, e As EventArgs) Handles TThreadA.Tick
        Try
            If Form_Handle.gagala = True Then
                trda.Abort()
            End If
        Catch ex As Exception
        End Try
        Form_Handle.ceka = 0
        BCPembaruanQuoologi.Enabled = True
        TThreadA.Enabled = False
    End Sub
End Class
