﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Quote
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Quote))
        Me.TBQuotes = New System.Windows.Forms.TextBox()
        Me.BHide = New DevComponents.DotNetBar.ButtonX()
        Me.BSalin = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.BCari = New DevComponents.DotNetBar.ButtonX()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FB = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.BFav = New DevComponents.DotNetBar.ButtonX()
        Me.BInfo = New DevComponents.DotNetBar.ButtonX()
        Me.TFirst = New System.Windows.Forms.Timer(Me.components)
        Me.TAutoHide = New System.Windows.Forms.Timer(Me.components)
        Me.TTombol = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.TDelayT = New System.Windows.Forms.Timer(Me.components)
        Me.TFormHilang = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'TBQuotes
        '
        Me.TBQuotes.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TBQuotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBQuotes.CausesValidation = False
        Me.TBQuotes.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.TBQuotes.Font = New System.Drawing.Font("Bailey Sans ITC TT", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBQuotes.ForeColor = System.Drawing.Color.Black
        Me.TBQuotes.ImeMode = System.Windows.Forms.ImeMode.Disable
        Me.TBQuotes.Location = New System.Drawing.Point(18, 15)
        Me.TBQuotes.Multiline = True
        Me.TBQuotes.Name = "TBQuotes"
        Me.TBQuotes.ReadOnly = True
        Me.TBQuotes.ShortcutsEnabled = False
        Me.TBQuotes.Size = New System.Drawing.Size(266, 96)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBQuotes, New DevComponents.DotNetBar.SuperTooltipInfo("Quote", "", "Menampilkan salah satu quote dari berkas yang dipakai.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(183, 61)))
        Me.TBQuotes.TabIndex = 9
        Me.TBQuotes.TabStop = False
        Me.TBQuotes.Text = "Aziz Nur Ariffianto" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "           Cinta" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " Nurani Rindiasih"
        '
        'BHide
        '
        Me.BHide.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BHide.AntiAlias = True
        Me.BHide.BackColor = System.Drawing.SystemColors.Control
        Me.BHide.CausesValidation = False
        Me.BHide.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat
        Me.BHide.Dock = System.Windows.Forms.DockStyle.Left
        Me.BHide.EnableMarkup = False
        Me.BHide.FocusCuesEnabled = False
        Me.BHide.Location = New System.Drawing.Point(0, 0)
        Me.BHide.Name = "BHide"
        Me.BHide.Size = New System.Drawing.Size(18, 128)
        Me.BHide.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.BHide, New DevComponents.DotNetBar.SuperTooltipInfo("Sembunyikan", "", "Tombol ini berfungsi untuk menyembunyikan jendela Quoologi.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(164, 70)))
        Me.BHide.Symbol = ""
        Me.BHide.SymbolSize = 18.0!
        Me.BHide.TabIndex = 8
        Me.BHide.TextColor = System.Drawing.Color.DarkGray
        '
        'BSalin
        '
        Me.BSalin.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSalin.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BSalin.AntiAlias = True
        Me.BSalin.BackColor = System.Drawing.SystemColors.Control
        Me.BSalin.CausesValidation = False
        Me.BSalin.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat
        Me.BSalin.EnableMarkup = False
        Me.BSalin.FocusCuesEnabled = False
        Me.BSalin.Location = New System.Drawing.Point(19, 130)
        Me.BSalin.Name = "BSalin"
        Me.BSalin.Size = New System.Drawing.Size(15, 14)
        Me.BSalin.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.BSalin, New DevComponents.DotNetBar.SuperTooltipInfo("Salin", "", "Tombol ini berfungsi untuk menyalin teks quote ke dalam clipboard.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(152, 70)))
        Me.BSalin.Symbol = ""
        Me.BSalin.SymbolSize = 11.0!
        Me.BSalin.TabIndex = 10
        Me.BSalin.TextColor = System.Drawing.Color.DarkGray
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.CausesValidation = False
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label1.ForeColor = System.Drawing.Color.DarkGray
        Me.Label1.Location = New System.Drawing.Point(120, 114)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label1.Size = New System.Drawing.Size(163, 13)
        Me.SuperTooltip1.SetSuperTooltip(Me.Label1, New DevComponents.DotNetBar.SuperTooltipInfo("Nama Berkas", "", "Merupakan indikator nama berkas dari quote yang ditampilkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(191, 58)))
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Quoologi"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'BCari
        '
        Me.BCari.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BCari.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BCari.AntiAlias = True
        Me.BCari.BackColor = System.Drawing.SystemColors.Control
        Me.BCari.CausesValidation = False
        Me.BCari.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat
        Me.BCari.EnableMarkup = False
        Me.BCari.FocusCuesEnabled = False
        Me.BCari.Location = New System.Drawing.Point(35, 130)
        Me.BCari.Name = "BCari"
        Me.BCari.Size = New System.Drawing.Size(15, 14)
        Me.BCari.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.BCari, New DevComponents.DotNetBar.SuperTooltipInfo("Cari", "", "Tombol ini berfungsi untuk menampilkan jendela ""Cari"" yang berfungsi untuk mencar" & _
            "i dan menampilkan quote² yang mengandung teks yang dicari.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(188, 99)))
        Me.BCari.Symbol = ""
        Me.BCari.SymbolSize = 10.0!
        Me.BCari.TabIndex = 12
        Me.BCari.TextColor = System.Drawing.Color.DarkGray
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.CausesValidation = False
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label2.ForeColor = System.Drawing.Color.DarkGray
        Me.Label2.Location = New System.Drawing.Point(21, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label2.Size = New System.Drawing.Size(0, 13)
        Me.SuperTooltip1.SetSuperTooltip(Me.Label2, New DevComponents.DotNetBar.SuperTooltipInfo("Hasil Pencarian", "", "Berisi indikator hasil pencarian yang ditampilkan." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """3 - 20""" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Menampilkan hasil" & _
            " ke 3 dari 20.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(183, 99)))
        Me.Label2.TabIndex = 13
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FB
        '
        Me.FB.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.FB.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FB.AntiAlias = True
        Me.FB.BackColor = System.Drawing.SystemColors.Control
        Me.FB.CausesValidation = False
        Me.FB.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat
        Me.FB.EnableMarkup = False
        Me.FB.FocusCuesEnabled = False
        Me.FB.Location = New System.Drawing.Point(50, 130)
        Me.FB.Name = "FB"
        Me.FB.Size = New System.Drawing.Size(15, 14)
        Me.FB.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.FB, New DevComponents.DotNetBar.SuperTooltipInfo("Update Status Langsung", "", resources.GetString("FB.SuperTooltip"), Nothing, CType(resources.GetObject("FB.SuperTooltip1"), System.Drawing.Image), DevComponents.DotNetBar.eTooltipColor.Gray, True, False, New System.Drawing.Size(262, 134)))
        Me.FB.Symbol = ""
        Me.FB.SymbolSize = 11.0!
        Me.FB.TabIndex = 14
        Me.FB.TextColor = System.Drawing.Color.DarkGray
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.HoverDelayMultiplier = 4
        Me.SuperTooltip1.IgnoreFormActiveState = True
        '
        'BFav
        '
        Me.BFav.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BFav.AntiAlias = True
        Me.BFav.BackColor = System.Drawing.SystemColors.Control
        Me.BFav.CausesValidation = False
        Me.BFav.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat
        Me.BFav.EnableMarkup = False
        Me.BFav.FocusCuesEnabled = False
        Me.BFav.Location = New System.Drawing.Point(17, -16)
        Me.BFav.Name = "BFav"
        Me.BFav.Size = New System.Drawing.Size(15, 14)
        Me.BFav.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.BFav, New DevComponents.DotNetBar.SuperTooltipInfo("Quote Favorit", "", "Tombol ini berfungsi untuk menambahkan quote yang ditampilkan ke dalam daftar quo" & _
            "te terfavorit." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Untuk melihat quote favorit, silakan klik kanan icon Quoologi " & _
            "di tray, dan pilih Quotes Favorit.", Nothing, CType(resources.GetObject("BFav.SuperTooltip"), System.Drawing.Image), DevComponents.DotNetBar.eTooltipColor.Gray, True, False, New System.Drawing.Size(265, 98)))
        Me.BFav.Symbol = ""
        Me.BFav.SymbolSize = 8.0!
        Me.BFav.TabIndex = 15
        Me.BFav.TextColor = System.Drawing.Color.DarkGray
        '
        'BInfo
        '
        Me.BInfo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BInfo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BInfo.AntiAlias = True
        Me.BInfo.BackColor = System.Drawing.SystemColors.Control
        Me.BInfo.CausesValidation = False
        Me.BInfo.ColorTable = DevComponents.DotNetBar.eButtonColor.Flat
        Me.BInfo.EnableMarkup = False
        Me.BInfo.FocusCuesEnabled = False
        Me.BInfo.Location = New System.Drawing.Point(268, 0)
        Me.BInfo.Name = "BInfo"
        Me.BInfo.Size = New System.Drawing.Size(15, 14)
        Me.BInfo.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.BInfo, New DevComponents.DotNetBar.SuperTooltipInfo("Info Penentu Tema", "", "Tombol ini berfungsi untuk menampilkan informasi mengenai Program dari Penentu Te" & _
            "ma yang sedang berjalan dan memberikan informasi tentang tema yang ada.", Nothing, CType(resources.GetObject("BInfo.SuperTooltip"), System.Drawing.Image), DevComponents.DotNetBar.eTooltipColor.Gray, True, False, New System.Drawing.Size(268, 83)))
        Me.BInfo.Symbol = ""
        Me.BInfo.SymbolSize = 8.0!
        Me.BInfo.TabIndex = 16
        Me.BInfo.TextColor = System.Drawing.Color.DarkGray
        Me.BInfo.Visible = False
        '
        'TFirst
        '
        Me.TFirst.Interval = 8000
        '
        'TAutoHide
        '
        Me.TAutoHide.Interval = 1000
        '
        'TTombol
        '
        Me.TTombol.Interval = 2000
        '
        'Timer2
        '
        Me.Timer2.Interval = 7500
        '
        'Timer3
        '
        Me.Timer3.Interval = 1000
        '
        'TDelayT
        '
        Me.TDelayT.Interval = 500
        '
        'TFormHilang
        '
        '
        'Form_Quote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(284, 128)
        Me.ControlBox = False
        Me.Controls.Add(Me.BInfo)
        Me.Controls.Add(Me.BFav)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.FB)
        Me.Controls.Add(Me.BCari)
        Me.Controls.Add(Me.TBQuotes)
        Me.Controls.Add(Me.BHide)
        Me.Controls.Add(Me.BSalin)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form_Quote"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TBQuotes As System.Windows.Forms.TextBox
    Friend WithEvents BHide As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BSalin As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BCari As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents FB As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents TFirst As System.Windows.Forms.Timer
    Friend WithEvents BFav As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TAutoHide As System.Windows.Forms.Timer
    Friend WithEvents TTombol As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents TDelayT As System.Windows.Forms.Timer
    Friend WithEvents TFormHilang As System.Windows.Forms.Timer
    Friend WithEvents BInfo As DevComponents.DotNetBar.ButtonX

End Class
