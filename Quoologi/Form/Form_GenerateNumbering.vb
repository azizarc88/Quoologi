﻿Imports DevComponents.DotNetBar

Public Class Form_GenerateNumbering
    Inherits Metro.MetroForm
    Private Sub BOk_Click(sender As Object, e As EventArgs) Handles BOk.Click
        If CBFormat.Text.Contains("{angka}") = False Then
            ToastNotification.Show(Me, "Format salah!, Tidak ditemukan ""{angka}""", Nothing, 3000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            Exit Sub
        End If
        Dim a As Integer = IIMaks.Text
        Do Until a = 0
            Dim format As String
            format = CBFormat.Text.Replace("{angka}", a)
            Form_PembuatQuote.TBSampah.Text += format
            Form_PembuatQuote.TBSampah.Text += Environment.NewLine
            a -= 1
        Loop
        Me.Close()
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub Form_GenerateNumbering_Load(sender As Object, e As EventArgs) Handles Me.Load
        If FirstRun = True Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
        End If
    End Sub
End Class