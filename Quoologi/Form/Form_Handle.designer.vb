﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Handle
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Handle))
        Me.TSelama = New System.Windows.Forms.Timer(Me.components)
        Me.TSetiap = New System.Windows.Forms.Timer(Me.components)
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.LBQuotesUse = New System.Windows.Forms.ListBox()
        Me.TCekUpdate = New System.Windows.Forms.Timer(Me.components)
        Me.TBKataJudul = New System.Windows.Forms.TextBox()
        Me.CBNo = New DevComponents.DotNetBar.Command(Me.components)
        Me.CBYes = New DevComponents.DotNetBar.Command(Me.components)
        Me.TPosisiMouse = New System.Windows.Forms.Timer(Me.components)
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
        Me.MIPengaturanUtama = New System.Windows.Forms.MenuItem()
        Me.MIKustomisasiQuoologi = New System.Windows.Forms.MenuItem()
        Me.MenuItem18 = New System.Windows.Forms.MenuItem()
        Me.MITampilQuote = New System.Windows.Forms.MenuItem()
        Me.MIQotMaker = New System.Windows.Forms.MenuItem()
        Me.MICari = New System.Windows.Forms.MenuItem()
        Me.MIFavorit = New System.Windows.Forms.MenuItem()
        Me.MIQuoteTerakhir = New System.Windows.Forms.MenuItem()
        Me.MenuItem19 = New System.Windows.Forms.MenuItem()
        Me.MIFacebook = New System.Windows.Forms.MenuItem()
        Me.MILoginFB = New System.Windows.Forms.MenuItem()
        Me.MILogoutFB = New System.Windows.Forms.MenuItem()
        Me.MIPengaturanFB = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem20 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MIUpdateP = New System.Windows.Forms.MenuItem()
        Me.MIUpdateQ = New System.Windows.Forms.MenuItem()
        Me.MenuItem21 = New System.Windows.Forms.MenuItem()
        Me.MIPartner = New System.Windows.Forms.MenuItem()
        Me.MITentang = New System.Windows.Forms.MenuItem()
        Me.MenuItem22 = New System.Windows.Forms.MenuItem()
        Me.MIHentikan = New System.Windows.Forms.MenuItem()
        Me.MIKeluar = New System.Windows.Forms.MenuItem()
        Me.TAnimasiAktif = New System.Windows.Forms.Timer(Me.components)
        Me.TTampil = New System.Windows.Forms.Timer(Me.components)
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TNotif = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'TSelama
        '
        Me.TSelama.Interval = 10000
        '
        'TSetiap
        '
        Me.TSetiap.Interval = 300000
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.NotifyIcon1.BalloonTipText = "Selamat Datang di Quoologi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Silakan jelajahi kami, kami akan memberikan informasi" & _
    " untuk masing² fungsi." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Untuk membuka pengaturan, klik icon ini."
        Me.NotifyIcon1.BalloonTipTitle = "Info"
        Me.NotifyIcon1.Text = "Klik dua kali untuk menampilkan Quote."
        Me.NotifyIcon1.Visible = True
        '
        'LBQuotesUse
        '
        Me.LBQuotesUse.BackColor = System.Drawing.Color.White
        Me.LBQuotesUse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBQuotesUse.ForeColor = System.Drawing.Color.Black
        Me.LBQuotesUse.FormattingEnabled = True
        Me.LBQuotesUse.Location = New System.Drawing.Point(66, 74)
        Me.LBQuotesUse.Name = "LBQuotesUse"
        Me.LBQuotesUse.Size = New System.Drawing.Size(156, 119)
        Me.LBQuotesUse.TabIndex = 36
        '
        'TCekUpdate
        '
        Me.TCekUpdate.Interval = 8000
        '
        'TBKataJudul
        '
        Me.TBKataJudul.BackColor = System.Drawing.Color.White
        Me.TBKataJudul.ForeColor = System.Drawing.Color.Black
        Me.TBKataJudul.Location = New System.Drawing.Point(12, 3)
        Me.TBKataJudul.Multiline = True
        Me.TBKataJudul.Name = "TBKataJudul"
        Me.TBKataJudul.Size = New System.Drawing.Size(100, 52)
        Me.TBKataJudul.TabIndex = 37
        '
        'CBNo
        '
        Me.CBNo.Image = Global.Quoologi.My.Resources.Resources.Tag_add__Custom_
        Me.CBNo.Name = "CBNo"
        '
        'CBYes
        '
        Me.CBYes.Image = Global.Quoologi.My.Resources.Resources.Tag__Custom_
        Me.CBYes.Name = "CBYes"
        '
        'TPosisiMouse
        '
        Me.TPosisiMouse.Interval = 10
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MIPengaturanUtama, Me.MIKustomisasiQuoologi, Me.MenuItem18, Me.MITampilQuote, Me.MIQotMaker, Me.MICari, Me.MIFavorit, Me.MIQuoteTerakhir, Me.MenuItem19, Me.MIFacebook, Me.MenuItem20, Me.MenuItem11, Me.MenuItem21, Me.MIPartner, Me.MITentang, Me.MenuItem22, Me.MIHentikan, Me.MIKeluar})
        '
        'MIPengaturanUtama
        '
        Me.MIPengaturanUtama.Index = 0
        Me.MIPengaturanUtama.Text = "Pengaturan Utama"
        '
        'MIKustomisasiQuoologi
        '
        Me.MIKustomisasiQuoologi.Index = 1
        Me.MIKustomisasiQuoologi.Text = "Kustomisasi Quoologi"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "-"
        '
        'MITampilQuote
        '
        Me.MITampilQuote.Index = 3
        Me.MITampilQuote.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftQ
        Me.MITampilQuote.Text = "Tampilkan Quote"
        Me.MITampilQuote.Visible = False
        '
        'MIQotMaker
        '
        Me.MIQotMaker.Index = 4
        Me.MIQotMaker.Text = "Buat Berkas Quote [.qot]"
        '
        'MICari
        '
        Me.MICari.Index = 5
        Me.MICari.Text = "Cari Quote"
        '
        'MIFavorit
        '
        Me.MIFavorit.Index = 6
        Me.MIFavorit.Text = "Quote Favorit"
        '
        'MIQuoteTerakhir
        '
        Me.MIQuoteTerakhir.Index = 7
        Me.MIQuoteTerakhir.Text = "Quote Terakhir"
        '
        'MenuItem19
        '
        Me.MenuItem19.Index = 8
        Me.MenuItem19.Text = "-"
        '
        'MIFacebook
        '
        Me.MIFacebook.Index = 9
        Me.MIFacebook.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MILoginFB, Me.MILogoutFB, Me.MIPengaturanFB, Me.MenuItem1})
        Me.MIFacebook.Text = "Facebook"
        '
        'MILoginFB
        '
        Me.MILoginFB.Index = 0
        Me.MILoginFB.Text = "Login"
        '
        'MILogoutFB
        '
        Me.MILogoutFB.Index = 1
        Me.MILogoutFB.Text = "Logout"
        '
        'MIPengaturanFB
        '
        Me.MIPengaturanFB.Index = 2
        Me.MIPengaturanFB.Text = "Pengaturan Facebook"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 3
        Me.MenuItem1.Text = "Kebijakan Privasi"
        '
        'MenuItem20
        '
        Me.MenuItem20.Index = 10
        Me.MenuItem20.Text = "-"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 11
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MIUpdateP, Me.MIUpdateQ})
        Me.MenuItem11.Text = "Pembaruan"
        '
        'MIUpdateP
        '
        Me.MIUpdateP.Index = 0
        Me.MIUpdateP.Text = "Program Utama"
        '
        'MIUpdateQ
        '
        Me.MIUpdateQ.Index = 1
        Me.MIUpdateQ.Text = "Quote"
        '
        'MenuItem21
        '
        Me.MenuItem21.Index = 12
        Me.MenuItem21.Text = "-"
        '
        'MIPartner
        '
        Me.MIPartner.Index = 13
        Me.MIPartner.Text = "Didukung Oleh"
        '
        'MITentang
        '
        Me.MITentang.Index = 14
        Me.MITentang.Text = "Tentang"
        '
        'MenuItem22
        '
        Me.MenuItem22.Index = 15
        Me.MenuItem22.Text = "-"
        '
        'MIHentikan
        '
        Me.MIHentikan.Index = 16
        Me.MIHentikan.Text = "Hentikan"
        '
        'MIKeluar
        '
        Me.MIKeluar.Index = 17
        Me.MIKeluar.Text = "Keluar"
        '
        'TAnimasiAktif
        '
        Me.TAnimasiAktif.Interval = 10
        '
        'TTampil
        '
        Me.TTampil.Interval = 10
        '
        'Timer1
        '
        Me.Timer1.Interval = 500
        '
        'TNotif
        '
        Me.TNotif.Enabled = True
        Me.TNotif.Interval = 30000
        '
        'Form_Handle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.Disable
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(169, 177)
        Me.ControlBox = False
        Me.Controls.Add(Me.TBKataJudul)
        Me.Controls.Add(Me.LBQuotesUse)
        Me.DoubleBuffered = True
        Me.Enabled = False
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Form_Handle"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "handle"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TSelama As System.Windows.Forms.Timer
    Friend WithEvents TSetiap As System.Windows.Forms.Timer
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents LBQuotesUse As System.Windows.Forms.ListBox
    Friend WithEvents CBYes As DevComponents.DotNetBar.Command
    Friend WithEvents CBNo As DevComponents.DotNetBar.Command
    Friend WithEvents TCekUpdate As System.Windows.Forms.Timer
    Friend WithEvents TBKataJudul As System.Windows.Forms.TextBox
    Friend WithEvents TPosisiMouse As System.Windows.Forms.Timer
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MIPengaturanUtama As System.Windows.Forms.MenuItem
    Friend WithEvents MIKustomisasiQuoologi As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MIQotMaker As System.Windows.Forms.MenuItem
    Friend WithEvents MICari As System.Windows.Forms.MenuItem
    Friend WithEvents MIFavorit As System.Windows.Forms.MenuItem
    Friend WithEvents MIQuoteTerakhir As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem19 As System.Windows.Forms.MenuItem
    Friend WithEvents MIFacebook As System.Windows.Forms.MenuItem
    Friend WithEvents MILoginFB As System.Windows.Forms.MenuItem
    Friend WithEvents MILogoutFB As System.Windows.Forms.MenuItem
    Friend WithEvents MIPengaturanFB As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem20 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MIUpdateP As System.Windows.Forms.MenuItem
    Friend WithEvents MIUpdateQ As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem21 As System.Windows.Forms.MenuItem
    Friend WithEvents MIPartner As System.Windows.Forms.MenuItem
    Friend WithEvents MITentang As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem22 As System.Windows.Forms.MenuItem
    Friend WithEvents MIHentikan As System.Windows.Forms.MenuItem
    Friend WithEvents MIKeluar As System.Windows.Forms.MenuItem
    Friend WithEvents MITampilQuote As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents TAnimasiAktif As System.Windows.Forms.Timer
    Friend WithEvents TTampil As System.Windows.Forms.Timer
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TNotif As System.Windows.Forms.Timer
End Class
