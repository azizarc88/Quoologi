﻿Imports DevComponents.DotNetBar
Imports System.Net

Public Class Form_LoginFacebook
    Inherits Metro.MetroForm
    Public fromstat As Byte

    Public Sub logining()
        WebBrowser1.ScriptErrorsSuppressed = False
        UserName = TBUser.Text
        My.Settings.Save()
        If TBPass.Text = "" Or TBUser.Text = "" Then
            ToastNotification.Show(Me, "Isi semua data terlebih dahulu", Nothing, 3000, eToastGlowColor.Red, eToastPosition.BottomCenter)
            Exit Sub
        End If
        WebBrowser1.Navigate("https://m.facebook.com/login.php?next=https%3A%2F%2Fm.facebook.com%2Fmessages&refsrc=https%3A%2F%2Fm.facebook.com%2Fmessages&_rdr")
        waitforpageload()
        Login(TBUser.Text, TBPass.Text)
        waitforpageload()
        If WebBrowser1.DocumentTitle = "Selamat datang di Facebook" Then
            ButtonX1.Enabled = True
            MessageBoxEx.Show("Login gagal. Pastikan e-mail / username / no. telp. dan password yang Anda masukan benar.", "Facebook", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            CariUsername()
            MessageBoxEx.Show("Login " + TBUser.Text + ", telah berhasil", "Facebook", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

        If fromstat = 1 Then
            fromstat = 0
            Me.Close()
            Me.Dispose()
            Form_UpdateStatus.BKirim.PerformClick()
        Else
            Dispose()
        End If
    End Sub

    Public Sub Login(username As String, password As String)
        Dim email As HtmlElement = WebBrowser1.Document.GetElementById("email")
        email.InnerText = username
        Dim pass As HtmlElement = WebBrowser1.Document.GetElementById("pass")
        pass.InnerText = password
        Dim login As HtmlElement = WebBrowser1.Document.GetElementById("login")
        login.InvokeMember("click")
    End Sub

    Public Sub CariUsername()
        Dim eror As Boolean = True
        Dim ele As HtmlElementCollection = WebBrowser1.Document.GetElementsByTagName("a")
        Dim temp As String
        Dim user As String
        Dim indeks As Integer
        For Each elem As HtmlElement In ele
            temp = elem.InnerText
            If temp <> Nothing Then
                If temp.Contains("Keluar") Or temp.Contains("Logout") Then
                    indeks = elem.InnerText.LastIndexOf("(") + 1
                    user = elem.InnerText.Substring(indeks)
                    user = user.Replace(")", "")
                    Application.DoEvents()
                    NamaAkunFB = user
                    SimpanPengaturan(BagianPenyimpanan.Umum)
                    eror = False
                End If
            End If
        Next

        If eror = True Then
            NamaAkunFB = "{Terjadi Kesalahan}"
            SimpanPengaturan()
        End If
    End Sub

    Public Sub Logout()
        WebBrowser1.ScriptErrorsSuppressed = False
        Application.DoEvents()
        Form_Pesan.LabelX1.Text = "cek status..."
        WebBrowser1.Navigate("https://m.facebook.com/messages")
        Application.DoEvents()
        waitforpageload()
        Application.DoEvents()
        If WebBrowser1.DocumentTitle <> "Pesan" Then
            If Form_Handle.psanmuncul = True Then
                Form_Pesan.Close()
            End If
            Application.DoEvents()
            MessageBoxEx.Show("Akun Facebook Anda belum terhubung, sehingga tidak bisa melakukan perintah logout", "Facebook", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        Form_Pesan.LabelX1.Text = "selesai"
        Dim ele As HtmlElementCollection = WebBrowser1.Document.GetElementsByTagName("a")
        Dim sukses As Boolean = False
        Try
            For Each elem As HtmlElement In ele
                Application.DoEvents()
                If elem.InnerText <> Nothing Then
                    Application.DoEvents()
                    Form_Pesan.LabelX1.Text = elem.InnerText.ToString
                    System.Threading.Thread.Sleep(100)
                    Application.DoEvents()
                    If elem.InnerText.ToString.Contains("Keluar") Or elem.InnerText.ToString.Contains("Logout") Then
                        elem.InvokeMember("click")
                        sukses = True
                    End If
                End If
            Next
            If sukses = False Then
                If Form_Handle.psanmuncul = True Then
                    Form_Pesan.Close()
                End If
                MessageBoxEx.Show("Kami tidak dapat menemukan elemen untuk logout Facebook, silakan hubungi Pembuat Quoologi. (kesalahan: 040501)" + Environment.NewLine + "Akan tetapi, Anda tetap bisa logout dari Facebook dengan menghapus cookies & chace atau logout Facebook lewat Internet Explorer.", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.Dispose()
                Exit Sub
            End If
            Application.DoEvents()
        Catch ex As Exception
            If Form_Handle.psanmuncul = True Then
                Form_Pesan.Close()
            End If
            MessageBoxEx.Show("Kami tidak dapat menemukan elemen untuk logout Facebook, silakan hubungi Pembuat Quoologi. (kesalahan: 040502)" + Environment.NewLine + "Akan tetapi, Anda tetap bisa logout dari Facebook dengan menghapus cookies & chace atau logout Facebook lewat Internet Explorer." + Environment.NewLine + "Pesan error: " + ex.Message, "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.Dispose()
            Exit Sub
        End Try
        NamaAkunFB = "{Nama Pengguna Facebook}"
        SimpanPengaturan()
        waitforpageload()
        If Form_Handle.psanmuncul = True Then
            Form_Pesan.Close()
        End If
        Application.DoEvents()
        MessageBoxEx.Show("Program berhasil Logout dari akun Facebook Anda", "Quoologi", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        logining()
    End Sub

    Private Sub ButtonX3_Click(sender As Object, e As EventArgs) Handles ButtonX3.Click
        psan("Facebook", eTaskDialogIcon.Information2, "Kebijakan dan Privasi", "Kami tidak akan menyimpan data pribadi Anda mulai dari data username (seperti surel, no. telp., ID akun, nama akun), dan password Anda pada server kami." + Environment.NewLine + Environment.NewLine + "Kami hanya akan menyimpan data username pada media penyimpanan lokal Anda guna memberikan layanan yang lebih baik bagi Anda", eTaskDialogButton.Ok, eTaskDialogBackgroundColor.Blue)
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles ButtonX2.Click
        If QuoteMuncul = True Then
            Form_Handle.TSelama.Enabled = False
        End If
        Me.Close()
        Dispose()
    End Sub

    Private Sub TBPass_GotFocus2(sender As Object, e As EventArgs) Handles TBPass.Click
        TBPass.SelectAll()
    End Sub

    Private Sub TBUser_GotFocus2(sender As Object, e As EventArgs) Handles TBUser.Click
        TBUser.SelectAll()
    End Sub

    Private Sub customclick(sender As Object, e As EventArgs) Handles TBPass.ButtonCustomClick
        If TBPass.PasswordChar = Nothing Then
            TBPass.ButtonCustom.Text = "A"
            TBPass.PasswordChar = "●"
        ElseIf TBPass.PasswordChar = "●" Then
            TBPass.ButtonCustom.Text = "●"
            TBPass.PasswordChar = Nothing
        End If
    End Sub

    Private Sub TBPass_KeyDown(sender As Object, e As KeyEventArgs) Handles TBPass.KeyDown
        If e.KeyCode.ToString = "Return" Then
            ButtonX1.Enabled = False
            logining()
        End If
    End Sub

    Private Sub Facebook_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If fromstat = 1 Then
            Form_UpdateStatus.indikator("ANDA BELUM LOGIN", Color.YellowGreen, True)
            GeserKontrol(Form_UpdateStatus.BPengaturan, Form_UpdateStatus.na - 27, Pengubah.Dikurangi, Orientasi.Vertikal)
            Form_UpdateStatus.hilang = False
            fromstat = 0
        End If
    End Sub

    Private Sub Form_LoginFacebook_Load(sender As Object, e As EventArgs) Handles Me.Load
        If FirstRun = True Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
        End If
    End Sub

    Private Sub Facebook_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        TBUser.Text = username
        If TBUser.Text <> "" Then
            TBPass.Focus()
        Else
            TBUser.Focus()
        End If
    End Sub
End Class