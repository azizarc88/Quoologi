﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PengaturanFacebook
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim SuperTooltipInfo1 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo2 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo3 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo8 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo4 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo5 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo6 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo7 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo9 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo10 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_PengaturanFacebook))
        Dim SuperTooltipInfo11 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo13 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo12 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo14 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo15 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo16 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo17 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo18 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Dim SuperTooltipInfo19 As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel3 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.CBHapusKNon = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CBHapusSpasi = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CBUbahSpasi = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel2 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.TBCustom = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.RBCustom = New System.Windows.Forms.RadioButton()
        Me.RBAt = New System.Windows.Forms.RadioButton()
        Me.RBCross = New System.Windows.Forms.RadioButton()
        Me.BSimpan = New DevComponents.DotNetBar.ButtonX()
        Me.TBPratinjau = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LNamaAkun = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupPanel4 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.GroupPanel5 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LPromosikan = New System.Windows.Forms.Label()
        Me.LKomentar = New System.Windows.Forms.Label()
        Me.LBagikan = New System.Windows.Forms.Label()
        Me.LSuka = New System.Windows.Forms.Label()
        Me.LTitik = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.TextBoxX1 = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupPanel6 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.CBTambahPetik = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CBIzinEdit = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.RBSebelumKomen = New System.Windows.Forms.RadioButton()
        Me.RBSetelahKomen = New System.Windows.Forms.RadioButton()
        Me.CBIzinKomen = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.GroupPanel1.SuspendLayout()
        Me.GroupPanel3.SuspendLayout()
        Me.GroupPanel2.SuspendLayout()
        Me.GroupPanel4.SuspendLayout()
        Me.GroupPanel5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPanel6.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupPanel1
        '
        Me.GroupPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel1.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.LabelX2)
        Me.GroupPanel1.Controls.Add(Me.GroupPanel3)
        Me.GroupPanel1.Controls.Add(Me.LabelX1)
        Me.GroupPanel1.Controls.Add(Me.GroupPanel2)
        Me.GroupPanel1.Location = New System.Drawing.Point(12, 183)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(309, 132)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 0
        Me.GroupPanel1.Text = "Tag Asal Quote"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(125, 4)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(131, 16)
        Me.LabelX2.TabIndex = 3
        Me.LabelX2.Text = "Pilihan:"
        '
        'GroupPanel3
        '
        Me.GroupPanel3.BackColor = System.Drawing.Color.White
        Me.GroupPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel3.Controls.Add(Me.CBHapusKNon)
        Me.GroupPanel3.Controls.Add(Me.CBHapusSpasi)
        Me.GroupPanel3.Controls.Add(Me.CBUbahSpasi)
        Me.GroupPanel3.DrawTitleBox = False
        Me.GroupPanel3.Location = New System.Drawing.Point(125, 26)
        Me.GroupPanel3.Name = "GroupPanel3"
        Me.GroupPanel3.Size = New System.Drawing.Size(174, 81)
        '
        '
        '
        Me.GroupPanel3.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel3.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel3.Style.BackColorGradientAngle = 90
        Me.GroupPanel3.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderBottomWidth = 1
        Me.GroupPanel3.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel3.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderLeftWidth = 1
        Me.GroupPanel3.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderRightWidth = 1
        Me.GroupPanel3.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel3.Style.BorderTopWidth = 1
        Me.GroupPanel3.Style.CornerDiameter = 4
        Me.GroupPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel3.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel3.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel3.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel3.TabIndex = 2
        '
        'CBHapusKNon
        '
        Me.CBHapusKNon.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBHapusKNon.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBHapusKNon.ForeColor = System.Drawing.Color.Black
        Me.CBHapusKNon.Location = New System.Drawing.Point(3, 27)
        Me.CBHapusKNon.Name = "CBHapusKNon"
        Me.CBHapusKNon.Size = New System.Drawing.Size(162, 23)
        Me.CBHapusKNon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo1.BodyText = "Aktifkan opsi ini jika ingin menghapus karakter yang bukan termasuk huruf." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """Ka" & _
    "ta - Kata Mutiara"" menjadi ""Kata Kata Mutiara""."
        SuperTooltipInfo1.CustomSize = New System.Drawing.Size(226, 95)
        SuperTooltipInfo1.HeaderText = "Hapus Karakter Non-Huruf"
        Me.SuperTooltip1.SetSuperTooltip(Me.CBHapusKNon, SuperTooltipInfo1)
        Me.CBHapusKNon.TabIndex = 1
        Me.CBHapusKNon.Text = "Hapus Karakter Non-Huruf"
        '
        'CBHapusSpasi
        '
        Me.CBHapusSpasi.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBHapusSpasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBHapusSpasi.ForeColor = System.Drawing.Color.Black
        Me.CBHapusSpasi.Location = New System.Drawing.Point(3, 3)
        Me.CBHapusSpasi.Name = "CBHapusSpasi"
        Me.CBHapusSpasi.Size = New System.Drawing.Size(100, 23)
        Me.CBHapusSpasi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo2.BodyText = "Aktifkan opsi ini jika ingin menghapus spasi pada asal quote." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """WOW Fakta"" menj" & _
    "adi ""WOWFakta"""
        SuperTooltipInfo2.CustomSize = New System.Drawing.Size(202, 84)
        SuperTooltipInfo2.HeaderText = "Hilangkan Spasi"
        Me.SuperTooltip1.SetSuperTooltip(Me.CBHapusSpasi, SuperTooltipInfo2)
        Me.CBHapusSpasi.TabIndex = 0
        Me.CBHapusSpasi.Text = "Hilangkan Spasi"
        '
        'CBUbahSpasi
        '
        Me.CBUbahSpasi.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBUbahSpasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBUbahSpasi.ForeColor = System.Drawing.Color.Black
        Me.CBUbahSpasi.Location = New System.Drawing.Point(20, 27)
        Me.CBUbahSpasi.Name = "CBUbahSpasi"
        Me.CBUbahSpasi.Size = New System.Drawing.Size(145, 23)
        Me.CBUbahSpasi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo3.BodyText = "Aktifkan opsi ini jika ingin mengubah spasi pada asal quote menjadi karakter ""_""." & _
    "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & """WOW Fakta"" menjadi ""WOW_Fakta""."
        SuperTooltipInfo3.CustomSize = New System.Drawing.Size(211, 100)
        SuperTooltipInfo3.HeaderText = "Ubah Spasi"
        Me.SuperTooltip1.SetSuperTooltip(Me.CBUbahSpasi, SuperTooltipInfo3)
        Me.CBUbahSpasi.TabIndex = 2
        Me.CBUbahSpasi.Text = "Ubah Spasi Menjadi -> _"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(3, 4)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(131, 16)
        Me.LabelX1.TabIndex = 1
        Me.LabelX1.Text = "Karakter Awal Tag:"
        '
        'GroupPanel2
        '
        Me.GroupPanel2.BackColor = System.Drawing.Color.White
        Me.GroupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel2.Controls.Add(Me.TBCustom)
        Me.GroupPanel2.Controls.Add(Me.RBCustom)
        Me.GroupPanel2.Controls.Add(Me.RBAt)
        Me.GroupPanel2.Controls.Add(Me.RBCross)
        Me.GroupPanel2.DrawTitleBox = False
        Me.GroupPanel2.Location = New System.Drawing.Point(3, 26)
        Me.GroupPanel2.Name = "GroupPanel2"
        Me.GroupPanel2.Size = New System.Drawing.Size(116, 81)
        '
        '
        '
        Me.GroupPanel2.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel2.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel2.Style.BackColorGradientAngle = 90
        Me.GroupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderBottomWidth = 1
        Me.GroupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderLeftWidth = 1
        Me.GroupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderRightWidth = 1
        Me.GroupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderTopWidth = 1
        Me.GroupPanel2.Style.CornerDiameter = 4
        Me.GroupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        SuperTooltipInfo8.BodyText = "Berisi pilihan karakter tag."
        SuperTooltipInfo8.HeaderText = "Pilihan Karakter Tag"
        Me.SuperTooltip1.SetSuperTooltip(Me.GroupPanel2, SuperTooltipInfo8)
        Me.GroupPanel2.TabIndex = 0
        '
        'TBCustom
        '
        Me.TBCustom.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBCustom.Border.Class = "TextBoxBorder"
        Me.TBCustom.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBCustom.Enabled = False
        Me.TBCustom.ForeColor = System.Drawing.Color.Black
        Me.TBCustom.Location = New System.Drawing.Point(65, 52)
        Me.TBCustom.MaxLength = 1
        Me.TBCustom.Name = "TBCustom"
        Me.TBCustom.Size = New System.Drawing.Size(43, 20)
        SuperTooltipInfo4.BodyText = "Tempat untuk mengisi karakter yang tidak terdapat di pilihan."
        SuperTooltipInfo4.HeaderText = "Pilihan Karakter"
        Me.SuperTooltip1.SetSuperTooltip(Me.TBCustom, SuperTooltipInfo4)
        Me.TBCustom.TabIndex = 8
        Me.TBCustom.WatermarkText = "Char..."
        '
        'RBCustom
        '
        Me.RBCustom.AutoSize = True
        Me.RBCustom.BackColor = System.Drawing.Color.White
        Me.RBCustom.ForeColor = System.Drawing.Color.Black
        Me.RBCustom.Location = New System.Drawing.Point(9, 54)
        Me.RBCustom.Name = "RBCustom"
        Me.RBCustom.Size = New System.Drawing.Size(60, 17)
        SuperTooltipInfo5.BodyText = "Klik jika ingin memilih karakter yang tidak ada di pilihan."
        SuperTooltipInfo5.HeaderText = "Pilihan Pengguna"
        Me.SuperTooltip1.SetSuperTooltip(Me.RBCustom, SuperTooltipInfo5)
        Me.RBCustom.TabIndex = 7
        Me.RBCustom.TabStop = True
        Me.RBCustom.Text = "Pilihan:"
        Me.RBCustom.UseVisualStyleBackColor = False
        '
        'RBAt
        '
        Me.RBAt.AutoSize = True
        Me.RBAt.BackColor = System.Drawing.Color.White
        Me.RBAt.ForeColor = System.Drawing.Color.Black
        Me.RBAt.Location = New System.Drawing.Point(9, 8)
        Me.RBAt.Name = "RBAt"
        Me.RBAt.Size = New System.Drawing.Size(37, 17)
        SuperTooltipInfo6.BodyText = "Klik untuk memilih simbol ""@"" sebagai karakter awal tag asal quote."
        SuperTooltipInfo6.HeaderText = "At"
        Me.SuperTooltip1.SetSuperTooltip(Me.RBAt, SuperTooltipInfo6)
        Me.RBAt.TabIndex = 6
        Me.RBAt.TabStop = True
        Me.RBAt.Text = "@"
        Me.RBAt.UseVisualStyleBackColor = False
        '
        'RBCross
        '
        Me.RBCross.AutoSize = True
        Me.RBCross.BackColor = System.Drawing.Color.White
        Me.RBCross.ForeColor = System.Drawing.Color.Black
        Me.RBCross.Location = New System.Drawing.Point(9, 31)
        Me.RBCross.Name = "RBCross"
        Me.RBCross.Size = New System.Drawing.Size(33, 17)
        SuperTooltipInfo7.BodyText = "Klik untuk memilih simbol ""#"" sebagai karakter awal tag asal quote."
        SuperTooltipInfo7.HeaderText = "Cross"
        Me.SuperTooltip1.SetSuperTooltip(Me.RBCross, SuperTooltipInfo7)
        Me.RBCross.TabIndex = 5
        Me.RBCross.TabStop = True
        Me.RBCross.Text = "#"
        Me.RBCross.UseVisualStyleBackColor = False
        '
        'BSimpan
        '
        Me.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSimpan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BSimpan.Location = New System.Drawing.Point(420, 325)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F5)
        Me.BSimpan.Size = New System.Drawing.Size(93, 23)
        Me.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo9.BodyText = "Kllik untuk menyimpan pengaturan."
        SuperTooltipInfo9.FooterText = "Jalan Pintas: F5"
        SuperTooltipInfo9.HeaderText = "Simpan"
        Me.SuperTooltip1.SetSuperTooltip(Me.BSimpan, SuperTooltipInfo9)
        Me.BSimpan.Symbol = ""
        Me.BSimpan.SymbolSize = 13.0!
        Me.BSimpan.TabIndex = 2
        Me.BSimpan.Text = " Simpan | F5"
        '
        'TBPratinjau
        '
        Me.TBPratinjau.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBPratinjau.Border.BackColor = System.Drawing.Color.White
        Me.TBPratinjau.Border.BackColor2 = System.Drawing.Color.White
        Me.TBPratinjau.Border.BorderColor = System.Drawing.Color.White
        Me.TBPratinjau.Border.Class = "TextBoxBorder"
        Me.TBPratinjau.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBPratinjau.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.TBPratinjau.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.TBPratinjau.ForeColor = System.Drawing.Color.Black
        Me.TBPratinjau.Location = New System.Drawing.Point(1, 55)
        Me.TBPratinjau.Multiline = True
        Me.TBPratinjau.Name = "TBPratinjau"
        Me.TBPratinjau.ReadOnly = True
        Me.TBPratinjau.Size = New System.Drawing.Size(488, 74)
        SuperTooltipInfo10.BodyText = "Quote status."
        SuperTooltipInfo10.HeaderText = "Quote"
        Me.SuperTooltip1.SetSuperTooltip(Me.TBPratinjau, SuperTooltipInfo10)
        Me.TBPratinjau.TabIndex = 1
        Me.TBPratinjau.Text = resources.GetString("TBPratinjau.Text")
        '
        'LNamaAkun
        '
        Me.LNamaAkun.AutoSize = True
        Me.LNamaAkun.BackColor = System.Drawing.Color.White
        Me.LNamaAkun.Font = New System.Drawing.Font("Arial", 10.5!, System.Drawing.FontStyle.Bold)
        Me.LNamaAkun.ForeColor = System.Drawing.Color.Black
        Me.LNamaAkun.Location = New System.Drawing.Point(53, 10)
        Me.LNamaAkun.Name = "LNamaAkun"
        Me.LNamaAkun.Size = New System.Drawing.Size(194, 16)
        SuperTooltipInfo11.BodyText = "Berisi nama pengguna yang sudah login." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Jika tulisan {Nama Pengguna Facebook} i" & _
    "tu artinya tidak ada pengguna yang login pada Facebook di aplikasi ini."
        SuperTooltipInfo11.CustomSize = New System.Drawing.Size(224, 99)
        SuperTooltipInfo11.HeaderText = "Nama Pengguna Facebook"
        Me.SuperTooltip1.SetSuperTooltip(Me.LNamaAkun, SuperTooltipInfo11)
        Me.LNamaAkun.TabIndex = 7
        Me.LNamaAkun.Text = "Nama Pengguna Facebook"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(53, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(241, 15)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "beberapa detik  yang lalu melalui Quoologi"
        '
        'GroupPanel4
        '
        Me.GroupPanel4.BackColor = System.Drawing.Color.White
        Me.GroupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel4.Controls.Add(Me.GroupPanel5)
        Me.GroupPanel4.Controls.Add(Me.Label2)
        Me.GroupPanel4.Controls.Add(Me.PictureBox1)
        Me.GroupPanel4.Controls.Add(Me.LNamaAkun)
        Me.GroupPanel4.Controls.Add(Me.PictureBox2)
        Me.GroupPanel4.Controls.Add(Me.TextBoxX1)
        Me.GroupPanel4.Controls.Add(Me.TBPratinjau)
        Me.GroupPanel4.DrawTitleBox = False
        Me.GroupPanel4.Location = New System.Drawing.Point(12, 5)
        Me.GroupPanel4.Name = "GroupPanel4"
        Me.GroupPanel4.Size = New System.Drawing.Size(501, 169)
        '
        '
        '
        Me.GroupPanel4.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel4.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel4.Style.BackColorGradientAngle = 90
        Me.GroupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderBottomWidth = 1
        Me.GroupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderLeftWidth = 1
        Me.GroupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderRightWidth = 1
        Me.GroupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderTopWidth = 1
        Me.GroupPanel4.Style.CornerDiameter = 4
        Me.GroupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        SuperTooltipInfo13.BodyText = "Menampilkan beberapa teks dan foto yang disamakan dengan tampilan status yang ter" & _
    "dapat di FB sesuai dengan isi komentar (jika diizinkan), quote, dan tag yang tel" & _
    "ah ditentukan."
        SuperTooltipInfo13.CustomSize = New System.Drawing.Size(199, 109)
        SuperTooltipInfo13.HeaderText = "Pratinjau"
        Me.SuperTooltip1.SetSuperTooltip(Me.GroupPanel4, SuperTooltipInfo13)
        Me.GroupPanel4.TabIndex = 9
        '
        'GroupPanel5
        '
        Me.GroupPanel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel5.BackColor = System.Drawing.Color.White
        Me.GroupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel5.Controls.Add(Me.LPromosikan)
        Me.GroupPanel5.Controls.Add(Me.LKomentar)
        Me.GroupPanel5.Controls.Add(Me.LBagikan)
        Me.GroupPanel5.Controls.Add(Me.LSuka)
        Me.GroupPanel5.Controls.Add(Me.LTitik)
        Me.GroupPanel5.DrawTitleBox = False
        Me.GroupPanel5.Location = New System.Drawing.Point(-1, 136)
        Me.GroupPanel5.Name = "GroupPanel5"
        Me.GroupPanel5.Size = New System.Drawing.Size(499, 28)
        '
        '
        '
        Me.GroupPanel5.Style.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.GroupPanel5.Style.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.GroupPanel5.Style.BackColorGradientAngle = 90
        Me.GroupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderBottomWidth = 1
        Me.GroupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderLeftWidth = 1
        Me.GroupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderRightWidth = 1
        Me.GroupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderTopWidth = 1
        Me.GroupPanel5.Style.CornerDiameter = 4
        Me.GroupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel5.Style.CornerTypeBottomLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel5.Style.CornerTypeBottomRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel5.Style.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.Style.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.TabIndex = 9
        '
        'LPromosikan
        '
        Me.LPromosikan.AutoSize = True
        Me.LPromosikan.BackColor = System.Drawing.Color.White
        Me.LPromosikan.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LPromosikan.ForeColor = System.Drawing.Color.Black
        Me.LPromosikan.Location = New System.Drawing.Point(110, 6)
        Me.LPromosikan.Name = "LPromosikan"
        Me.LPromosikan.Size = New System.Drawing.Size(74, 15)
        Me.LPromosikan.TabIndex = 3
        Me.LPromosikan.Text = "Promosikan"
        '
        'LKomentar
        '
        Me.LKomentar.AutoSize = True
        Me.LKomentar.BackColor = System.Drawing.Color.White
        Me.LKomentar.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LKomentar.ForeColor = System.Drawing.Color.Black
        Me.LKomentar.Location = New System.Drawing.Point(41, 6)
        Me.LKomentar.Name = "LKomentar"
        Me.LKomentar.Size = New System.Drawing.Size(64, 15)
        Me.LKomentar.TabIndex = 2
        Me.LKomentar.Text = "Komentari"
        '
        'LBagikan
        '
        Me.LBagikan.AutoSize = True
        Me.LBagikan.BackColor = System.Drawing.Color.White
        Me.LBagikan.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LBagikan.ForeColor = System.Drawing.Color.Black
        Me.LBagikan.Location = New System.Drawing.Point(189, 6)
        Me.LBagikan.Name = "LBagikan"
        Me.LBagikan.Size = New System.Drawing.Size(52, 15)
        Me.LBagikan.TabIndex = 1
        Me.LBagikan.Text = "Bagikan"
        '
        'LSuka
        '
        Me.LSuka.AutoSize = True
        Me.LSuka.BackColor = System.Drawing.Color.White
        Me.LSuka.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LSuka.ForeColor = System.Drawing.Color.Black
        Me.LSuka.Location = New System.Drawing.Point(1, 6)
        Me.LSuka.Name = "LSuka"
        Me.LSuka.Size = New System.Drawing.Size(35, 15)
        Me.LSuka.TabIndex = 0
        Me.LSuka.Text = "Suka"
        '
        'LTitik
        '
        Me.LTitik.AutoSize = True
        Me.LTitik.BackColor = System.Drawing.Color.White
        Me.LTitik.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LTitik.ForeColor = System.Drawing.Color.Black
        Me.LTitik.Location = New System.Drawing.Point(2, 6)
        Me.LTitik.Name = "LTitik"
        Me.LTitik.Size = New System.Drawing.Size(193, 15)
        Me.LTitik.TabIndex = 3
        Me.LTitik.Text = "          ·                      ·                         · "
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.ForeColor = System.Drawing.Color.Black
        Me.PictureBox1.Location = New System.Drawing.Point(7, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(42, 42)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.ForeColor = System.Drawing.Color.Black
        Me.PictureBox2.Location = New System.Drawing.Point(298, 32)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox2.TabIndex = 6
        Me.PictureBox2.TabStop = False
        '
        'TextBoxX1
        '
        Me.TextBoxX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TextBoxX1.Border.BackColor = System.Drawing.Color.White
        Me.TextBoxX1.Border.BackColor2 = System.Drawing.Color.White
        Me.TextBoxX1.Border.BorderColor = System.Drawing.Color.White
        Me.TextBoxX1.Border.Class = "TextBoxBorder"
        Me.TextBoxX1.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TextBoxX1.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.TextBoxX1.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.TextBoxX1.ForeColor = System.Drawing.Color.Black
        Me.TextBoxX1.Location = New System.Drawing.Point(1, 57)
        Me.TextBoxX1.Multiline = True
        Me.TextBoxX1.Name = "TextBoxX1"
        Me.TextBoxX1.ReadOnly = True
        Me.TextBoxX1.Size = New System.Drawing.Size(235, 23)
        SuperTooltipInfo12.BodyText = "Komentar status."
        SuperTooltipInfo12.HeaderText = "Komentar"
        Me.SuperTooltip1.SetSuperTooltip(Me.TextBoxX1, SuperTooltipInfo12)
        Me.TextBoxX1.TabIndex = 10
        Me.TextBoxX1.Text = "Terus dikembangkan yah kawan :)"
        Me.TextBoxX1.Visible = False
        '
        'GroupPanel6
        '
        Me.GroupPanel6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel6.BackColor = System.Drawing.Color.White
        Me.GroupPanel6.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel6.Controls.Add(Me.CBTambahPetik)
        Me.GroupPanel6.Controls.Add(Me.CBIzinEdit)
        Me.GroupPanel6.Controls.Add(Me.RBSebelumKomen)
        Me.GroupPanel6.Controls.Add(Me.RBSetelahKomen)
        Me.GroupPanel6.Controls.Add(Me.CBIzinKomen)
        Me.GroupPanel6.Location = New System.Drawing.Point(331, 183)
        Me.GroupPanel6.Name = "GroupPanel6"
        Me.GroupPanel6.Size = New System.Drawing.Size(181, 132)
        '
        '
        '
        Me.GroupPanel6.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel6.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel6.Style.BackColorGradientAngle = 90
        Me.GroupPanel6.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel6.Style.BorderBottomWidth = 1
        Me.GroupPanel6.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel6.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel6.Style.BorderLeftWidth = 1
        Me.GroupPanel6.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel6.Style.BorderRightWidth = 1
        Me.GroupPanel6.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel6.Style.BorderTopWidth = 1
        Me.GroupPanel6.Style.CornerDiameter = 4
        Me.GroupPanel6.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel6.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel6.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel6.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel6.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel6.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel6.TabIndex = 10
        Me.GroupPanel6.Text = "Status"
        '
        'CBTambahPetik
        '
        Me.CBTambahPetik.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBTambahPetik.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBTambahPetik.ForeColor = System.Drawing.Color.Black
        Me.CBTambahPetik.Location = New System.Drawing.Point(5, 47)
        Me.CBTambahPetik.Name = "CBTambahPetik"
        Me.CBTambahPetik.Size = New System.Drawing.Size(164, 23)
        Me.CBTambahPetik.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo14.BodyText = "Aktifkan opsi ini jika ingin menambahkan petik pada quote yang nantinya akan dija" & _
    "dikan sebagai status di Facebook."
        SuperTooltipInfo14.CustomSize = New System.Drawing.Size(224, 71)
        SuperTooltipInfo14.HeaderText = "Tambahkan Petik"
        Me.SuperTooltip1.SetSuperTooltip(Me.CBTambahPetik, SuperTooltipInfo14)
        Me.CBTambahPetik.TabIndex = 5
        Me.CBTambahPetik.Text = "Tambahkan Petik Pada Quote"
        '
        'CBIzinEdit
        '
        Me.CBIzinEdit.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBIzinEdit.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBIzinEdit.ForeColor = System.Drawing.Color.Black
        Me.CBIzinEdit.Location = New System.Drawing.Point(5, 24)
        Me.CBIzinEdit.Name = "CBIzinEdit"
        Me.CBIzinEdit.Size = New System.Drawing.Size(164, 23)
        Me.CBIzinEdit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo15.BodyText = "Aktifkan opsi ini jika ingin mengedit quote yang ingin dijadikan sebagai status d" & _
    "i Facebook." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Pengeditan quote yang berlebihan tidak diperkenankan."
        SuperTooltipInfo15.CustomSize = New System.Drawing.Size(196, 109)
        SuperTooltipInfo15.HeaderText = "Izinkan Edit Quote"
        Me.SuperTooltip1.SetSuperTooltip(Me.CBIzinEdit, SuperTooltipInfo15)
        Me.CBIzinEdit.TabIndex = 2
        Me.CBIzinEdit.Text = "Izinkan Pengeditan Quote"
        '
        'RBSebelumKomen
        '
        Me.RBSebelumKomen.AutoSize = True
        Me.RBSebelumKomen.BackColor = System.Drawing.Color.White
        Me.RBSebelumKomen.ForeColor = System.Drawing.Color.Black
        Me.RBSebelumKomen.Location = New System.Drawing.Point(22, 46)
        Me.RBSebelumKomen.Name = "RBSebelumKomen"
        Me.RBSebelumKomen.Size = New System.Drawing.Size(147, 17)
        SuperTooltipInfo16.BodyText = "Aktifkan pilihan ini jika ingin komentar pada status berada sebelum quote."
        SuperTooltipInfo16.HeaderText = "Komentar Sebelum Quote"
        Me.SuperTooltip1.SetSuperTooltip(Me.RBSebelumKomen, SuperTooltipInfo16)
        Me.RBSebelumKomen.TabIndex = 4
        Me.RBSebelumKomen.TabStop = True
        Me.RBSebelumKomen.Text = "Komentar Sebelum Quote"
        Me.RBSebelumKomen.UseVisualStyleBackColor = False
        '
        'RBSetelahKomen
        '
        Me.RBSetelahKomen.AutoSize = True
        Me.RBSetelahKomen.BackColor = System.Drawing.Color.White
        Me.RBSetelahKomen.ForeColor = System.Drawing.Color.Black
        Me.RBSetelahKomen.Location = New System.Drawing.Point(22, 25)
        Me.RBSetelahKomen.Name = "RBSetelahKomen"
        Me.RBSetelahKomen.Size = New System.Drawing.Size(142, 17)
        SuperTooltipInfo17.BodyText = "Aktifkan pilihan ini jika ingin komentar pada status berada setelah quote."
        SuperTooltipInfo17.HeaderText = "Komentar Setelah Quote"
        Me.SuperTooltip1.SetSuperTooltip(Me.RBSetelahKomen, SuperTooltipInfo17)
        Me.RBSetelahKomen.TabIndex = 3
        Me.RBSetelahKomen.TabStop = True
        Me.RBSetelahKomen.Text = "Komentar Setelah Quote"
        Me.RBSetelahKomen.UseVisualStyleBackColor = False
        '
        'CBIzinKomen
        '
        Me.CBIzinKomen.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.CBIzinKomen.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBIzinKomen.ForeColor = System.Drawing.Color.Black
        Me.CBIzinKomen.Location = New System.Drawing.Point(5, 1)
        Me.CBIzinKomen.Name = "CBIzinKomen"
        Me.CBIzinKomen.Size = New System.Drawing.Size(164, 23)
        Me.CBIzinKomen.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo18.BodyText = "Aktifkan opsi ini jika ingin memberi komentar pada status FB."
        SuperTooltipInfo18.HeaderText = "Izinkan Komentar"
        Me.SuperTooltip1.SetSuperTooltip(Me.CBIzinKomen, SuperTooltipInfo18)
        Me.CBIzinKomen.TabIndex = 1
        Me.CBIzinKomen.Text = "Izinkan Komentar User"
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BBatal.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(12, 325)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(84, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        SuperTooltipInfo19.BodyText = "Klik untuk keluar jendela dan mengabaikan pengaturan yang diubah (tidak disimpan)" & _
    "."
        SuperTooltipInfo19.FooterText = "Jalan Pintas: Esc"
        SuperTooltipInfo19.HeaderText = "Batal"
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, SuperTooltipInfo19)
        Me.BBatal.Symbol = ""
        Me.BBatal.SymbolSize = 13.0!
        Me.BBatal.TabIndex = 11
        Me.BBatal.Text = " Batal | Esc"
        '
        'Form_PengaturanFacebook
        '
        Me.AcceptButton = Me.BSimpan
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(525, 354)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.GroupPanel6)
        Me.Controls.Add(Me.BSimpan)
        Me.Controls.Add(Me.GroupPanel4)
        Me.Controls.Add(Me.GroupPanel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "Form_PengaturanFacebook"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pengaturan Status (FB)"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel3.ResumeLayout(False)
        Me.GroupPanel2.ResumeLayout(False)
        Me.GroupPanel2.PerformLayout()
        Me.GroupPanel4.ResumeLayout(False)
        Me.GroupPanel4.PerformLayout()
        Me.GroupPanel5.ResumeLayout(False)
        Me.GroupPanel5.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPanel6.ResumeLayout(False)
        Me.GroupPanel6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents GroupPanel2 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents TBCustom As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents RBCustom As System.Windows.Forms.RadioButton
    Friend WithEvents RBAt As System.Windows.Forms.RadioButton
    Friend WithEvents RBCross As System.Windows.Forms.RadioButton
    Friend WithEvents GroupPanel3 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents CBHapusKNon As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CBHapusSpasi As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents BSimpan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBPratinjau As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Public WithEvents LNamaAkun As System.Windows.Forms.Label
    Public WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupPanel4 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents GroupPanel5 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents LSuka As System.Windows.Forms.Label
    Friend WithEvents LPromosikan As System.Windows.Forms.Label
    Friend WithEvents LKomentar As System.Windows.Forms.Label
    Friend WithEvents LBagikan As System.Windows.Forms.Label
    Friend WithEvents LTitik As System.Windows.Forms.Label
    Friend WithEvents CBUbahSpasi As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CBIzinKomen As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupPanel6 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents CBIzinEdit As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents RBSebelumKomen As System.Windows.Forms.RadioButton
    Friend WithEvents RBSetelahKomen As System.Windows.Forms.RadioButton
    Friend WithEvents TextBoxX1 As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents CBTambahPetik As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
End Class
