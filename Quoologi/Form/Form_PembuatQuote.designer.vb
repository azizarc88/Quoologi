﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PembuatQuote
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_PembuatQuote))
        Me.TBFileName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.BSimpan = New DevComponents.DotNetBar.ButtonX()
        Me.TBDari = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LBData = New System.Windows.Forms.ListBox()
        Me.BKerjakan = New DevComponents.DotNetBar.ButtonX()
        Me.TBData = New System.Windows.Forms.TextBox()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.BBersihkan = New DevComponents.DotNetBar.ButtonX()
        Me.BPanduan = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.TBMaks = New System.Windows.Forms.TextBox()
        Me.TBMin = New System.Windows.Forms.TextBox()
        Me.TBPencegah = New System.Windows.Forms.TextBox()
        Me.TBSampah = New System.Windows.Forms.TextBox()
        Me.BBukan = New DevComponents.DotNetBar.ButtonX()
        Me.BRPencegah = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.CheckBoxX4 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBoxX3 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CheckBoxX2 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBoxX1 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
        Me.MIPilihSemua = New System.Windows.Forms.MenuItem()
        Me.MITempel = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MIHapus = New System.Windows.Forms.MenuItem()
        Me.MIBersihkan = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MIKerjakan = New System.Windows.Forms.MenuItem()
        Me.TTombol = New System.Windows.Forms.Timer(Me.components)
        Me.TTungguTombol = New System.Windows.Forms.Timer(Me.components)
        Me.BalloonTip1 = New DevComponents.DotNetBar.BalloonTip()
        Me.GroupPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TBFileName
        '
        Me.TBFileName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBFileName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBFileName.Border.Class = "TextBoxBorder"
        Me.TBFileName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBFileName.ForeColor = System.Drawing.Color.Black
        Me.TBFileName.Location = New System.Drawing.Point(102, 646)
        Me.TBFileName.Name = "TBFileName"
        Me.TBFileName.PreventEnterBeep = True
        Me.TBFileName.Size = New System.Drawing.Size(467, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBFileName, New DevComponents.DotNetBar.SuperTooltipInfo("Nama File", "", "Bagian ini berfungsi untuk memberikan nama berkas dengan tipe "".qot"" saat menyimp" & _
            "an ke dalam hardisk.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBFileName.TabIndex = 2
        Me.TBFileName.WatermarkText = "Nama File"
        '
        'BSimpan
        '
        Me.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BSimpan.Enabled = False
        Me.BSimpan.Location = New System.Drawing.Point(836, 644)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BSimpan.Size = New System.Drawing.Size(75, 23)
        Me.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BSimpan, New DevComponents.DotNetBar.SuperTooltipInfo("Simpan", "", "Perintah ini berfungsi untuk menyimpan Quotes ke dalam berkas "".qot"" dengan nama " & _
            "yang ada pada bagian ""Nama File"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BSimpan.Symbol = ""
        Me.BSimpan.SymbolSize = 12.0!
        Me.BSimpan.TabIndex = 5
        Me.BSimpan.Text = "Simpan"
        '
        'TBDari
        '
        Me.TBDari.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TBDari.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBDari.Border.Class = "TextBoxBorder"
        Me.TBDari.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBDari.ForeColor = System.Drawing.Color.Black
        Me.TBDari.Location = New System.Drawing.Point(6, 4)
        Me.TBDari.Name = "TBDari"
        Me.TBDari.Size = New System.Drawing.Size(904, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBDari, New DevComponents.DotNetBar.SuperTooltipInfo("Nama Akun", "@Nama_Akun", "Bagian ini merupakan tempat untuk memasukan nama akun dari sumber data yang didap" & _
            "atkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, False, New System.Drawing.Size(0, 0)))
        Me.TBDari.TabIndex = 0
        Me.TBDari.WatermarkText = "Nama Akun (Opsional)"
        '
        'LBData
        '
        Me.LBData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBData.BackColor = System.Drawing.Color.White
        Me.LBData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.LBData.ForeColor = System.Drawing.Color.Black
        Me.LBData.FormattingEnabled = True
        Me.LBData.HorizontalScrollbar = True
        Me.LBData.ItemHeight = 16
        Me.LBData.Location = New System.Drawing.Point(462, 54)
        Me.LBData.Name = "LBData"
        Me.LBData.ScrollAlwaysVisible = True
        Me.LBData.Size = New System.Drawing.Size(448, 546)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBData, New DevComponents.DotNetBar.SuperTooltipInfo("Daftar Quotes", "", "Pada bagian ini berisi Quotes² hasil pemilihan dari bagian ""Data"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.LBData.TabIndex = 3
        '
        'BKerjakan
        '
        Me.BKerjakan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BKerjakan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BKerjakan.Location = New System.Drawing.Point(676, 644)
        Me.BKerjakan.Name = "BKerjakan"
        Me.BKerjakan.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BKerjakan.Size = New System.Drawing.Size(75, 23)
        Me.BKerjakan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BKerjakan, New DevComponents.DotNetBar.SuperTooltipInfo("Kerjakan", "", resources.GetString("BKerjakan.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(200, 154)))
        Me.BKerjakan.Symbol = ""
        Me.BKerjakan.SymbolSize = 12.0!
        Me.BKerjakan.TabIndex = 3
        Me.BKerjakan.Text = "Kerjakan"
        '
        'TBData
        '
        Me.TBData.AcceptsReturn = True
        Me.TBData.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TBData.BackColor = System.Drawing.Color.White
        Me.TBData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBData.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBData.ForeColor = System.Drawing.Color.Black
        Me.TBData.HideSelection = False
        Me.TBData.Location = New System.Drawing.Point(6, 54)
        Me.TBData.MaxLength = 99999999
        Me.TBData.Multiline = True
        Me.TBData.Name = "TBData"
        Me.TBData.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TBData.Size = New System.Drawing.Size(448, 362)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBData, New DevComponents.DotNetBar.SuperTooltipInfo("Data", "", "Bagian ini merupakan bagian dimana data yang ingin diproses menjadi Quotes dimasu" & _
            "kan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBData.TabIndex = 1
        Me.TBData.WordWrap = False
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(462, 31)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(116, 16)
        Me.LabelX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.LabelX2.TabIndex = 9
        Me.LabelX2.Text = "Daftar Quotes:"
        '
        'BBersihkan
        '
        Me.BBersihkan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBersihkan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BBersihkan.Enabled = False
        Me.BBersihkan.Location = New System.Drawing.Point(756, 644)
        Me.BBersihkan.Name = "BBersihkan"
        Me.BBersihkan.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BBersihkan.Size = New System.Drawing.Size(75, 23)
        Me.BBersihkan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBersihkan, New DevComponents.DotNetBar.SuperTooltipInfo("Bersihkan", "", "Perintah ini berfungsi untuk membuat lembar kerja baru dalam pembuatan berkas "".q" & _
            "ot""", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BBersihkan.Symbol = ""
        Me.BBersihkan.SymbolSize = 12.0!
        Me.BBersihkan.TabIndex = 4
        Me.BBersihkan.Text = "Bersihkan"
        '
        'BPanduan
        '
        Me.BPanduan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BPanduan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BalloonTip1.SetBalloonCaption(Me.BPanduan, "Panduan")
        Me.BalloonTip1.SetBalloonText(Me.BPanduan, "Anda dapat melihat tutorial pembuatan quote disini kapan saja, dengan mengeklik t" & _
        "ombol Panduan -> Bantu Saya Mengerti")
        Me.BPanduan.Location = New System.Drawing.Point(6, 644)
        Me.BPanduan.Name = "BPanduan"
        Me.BPanduan.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(4)
        Me.BPanduan.Size = New System.Drawing.Size(90, 23)
        Me.BPanduan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BPanduan.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem1, Me.ButtonItem2})
        Me.SuperTooltip1.SetSuperTooltip(Me.BPanduan, New DevComponents.DotNetBar.SuperTooltipInfo("Panduan", "", "Tombol ini berisi panduan dan perintah untuk menampilkan cara kami dalam membuat " & _
            "berkas "".qot""", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BPanduan.Symbol = ""
        Me.BPanduan.SymbolSize = 12.0!
        Me.BPanduan.TabIndex = 6
        Me.BPanduan.Text = "Panduan"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem1.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueWithBackground
        Me.ButtonItem1.EnableImageAnimation = True
        Me.ButtonItem1.GlobalItem = False
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.Symbol = ""
        Me.ButtonItem1.Text = "Bantu Saya Mengerti"
        '
        'ButtonItem2
        '
        Me.ButtonItem2.ColorTable = DevComponents.DotNetBar.eButtonColor.BlueWithBackground
        Me.ButtonItem2.GlobalItem = False
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.Symbol = ""
        Me.ButtonItem2.Text = "Tonton Video"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.HoverDelayMultiplier = 5
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.EnableMarkup = False
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(380, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label1.Size = New System.Drawing.Size(66, 23)
        Me.Label1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.Label1, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Total baris ""Data"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(100, 18)))
        Me.Label1.Symbol = ""
        Me.Label1.SymbolColor = System.Drawing.Color.SteelBlue
        Me.Label1.SymbolSize = 15.0!
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "0"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.EnableMarkup = False
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(836, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Label2.Size = New System.Drawing.Size(66, 23)
        Me.Label2.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.SuperTooltip1.SetSuperTooltip(Me.Label2, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Total baris ""Daftar Quotes""", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.Label2.Symbol = ""
        Me.Label2.SymbolColor = System.Drawing.Color.SteelBlue
        Me.Label2.SymbolSize = 15.0!
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "0"
        '
        'TBMaks
        '
        Me.TBMaks.BackColor = System.Drawing.Color.White
        Me.TBMaks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBMaks.ForeColor = System.Drawing.Color.Black
        Me.TBMaks.Location = New System.Drawing.Point(105, 37)
        Me.TBMaks.Name = "TBMaks"
        Me.TBMaks.Size = New System.Drawing.Size(67, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBMaks, New DevComponents.DotNetBar.SuperTooltipInfo("Panjang Maksimum", "", "Berisi total jumlah maksimum karakter yang dianggap sebagai Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBMaks.TabIndex = 2
        Me.TBMaks.Text = "900"
        '
        'TBMin
        '
        Me.TBMin.BackColor = System.Drawing.Color.White
        Me.TBMin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBMin.ForeColor = System.Drawing.Color.Black
        Me.TBMin.Location = New System.Drawing.Point(105, 10)
        Me.TBMin.Name = "TBMin"
        Me.TBMin.Size = New System.Drawing.Size(67, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBMin, New DevComponents.DotNetBar.SuperTooltipInfo("Panjang Minimum", "", "Berisi total jumlah minimum karakter yang dianggap sebagai Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBMin.TabIndex = 0
        Me.TBMin.Text = "30"
        '
        'TBPencegah
        '
        Me.TBPencegah.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TBPencegah.BackColor = System.Drawing.Color.White
        Me.TBPencegah.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPencegah.ForeColor = System.Drawing.Color.Black
        Me.TBPencegah.Location = New System.Drawing.Point(221, 454)
        Me.TBPencegah.Multiline = True
        Me.TBPencegah.Name = "TBPencegah"
        Me.TBPencegah.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBPencegah.Size = New System.Drawing.Size(110, 144)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBPencegah, New DevComponents.DotNetBar.SuperTooltipInfo("Teks Pencegah", "", "Jika salah satu baris yang terdapat di bagian ""Data"" berisi salah satu kata di da" & _
            "lam ""Teks Pencegah"" maka baris tersebut tidak akan ditambahkan ke ""Daftar Quotes" & _
            """.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.TBPencegah.TabIndex = 16
        '
        'TBSampah
        '
        Me.TBSampah.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TBSampah.BackColor = System.Drawing.Color.White
        Me.TBSampah.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBSampah.ForeColor = System.Drawing.Color.Black
        Me.TBSampah.Location = New System.Drawing.Point(342, 454)
        Me.TBSampah.Multiline = True
        Me.TBSampah.Name = "TBSampah"
        Me.TBSampah.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBSampah.Size = New System.Drawing.Size(110, 144)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBSampah, New DevComponents.DotNetBar.SuperTooltipInfo("Teks Sampah", "", resources.GetString("TBSampah.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(273, 191)))
        Me.TBSampah.TabIndex = 18
        '
        'BBukan
        '
        Me.BBukan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBukan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BBukan.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BBukan.Enabled = False
        Me.BBukan.Location = New System.Drawing.Point(578, 644)
        Me.BBukan.Name = "BBukan"
        Me.BBukan.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BBukan.Size = New System.Drawing.Size(92, 23)
        Me.BBukan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBukan, New DevComponents.DotNetBar.SuperTooltipInfo("Terhapus", "", "Tombol ini berfungsi untuk menampilkan teks dari bagian data, yang tidak terpilih" & _
            " sebagai salah satu quote.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(212, 82)))
        Me.BBukan.Symbol = ""
        Me.BBukan.SymbolSize = 12.0!
        Me.BBukan.TabIndex = 20
        Me.BBukan.Text = "Terhapus"
        '
        'BRPencegah
        '
        Me.BRPencegah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BRPencegah.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BRPencegah.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BRPencegah.Location = New System.Drawing.Point(311, 429)
        Me.BRPencegah.Name = "BRPencegah"
        Me.BRPencegah.Size = New System.Drawing.Size(20, 19)
        Me.BRPencegah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BRPencegah, New DevComponents.DotNetBar.SuperTooltipInfo("Reset Teks Pencegah", "", "Klik tombol ini untuk mengatur ulang teks pencegah ke pengaturan asali.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(174, 70)))
        Me.BRPencegah.Symbol = ""
        Me.BRPencegah.SymbolSize = 10.0!
        Me.BRPencegah.TabIndex = 21
        '
        'ButtonItem3
        '
        Me.ButtonItem3.GlobalItem = False
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonItem3, New DevComponents.DotNetBar.SuperTooltipInfo("Numbering", "", "Tombol ini berfungsi untuk menampilkan jendela yang berfungsi untuk menambahkan t" & _
            "eks numbering secara otomatis ke dalam teks sampah yang berfungsi untuk menghapu" & _
            "s nomor² yang sering ada di awal quote.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(215, 110)))
        Me.ButtonItem3.Symbol = ""
        Me.ButtonItem3.SymbolSize = 15.0!
        Me.ButtonItem3.Text = "Numbering"
        '
        'GroupPanel1
        '
        Me.GroupPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel1.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.CheckBoxX4)
        Me.GroupPanel1.Controls.Add(Me.CheckBoxX3)
        Me.GroupPanel1.Controls.Add(Me.Label6)
        Me.GroupPanel1.Controls.Add(Me.Label5)
        Me.GroupPanel1.Controls.Add(Me.CheckBoxX2)
        Me.GroupPanel1.Controls.Add(Me.CheckBoxX1)
        Me.GroupPanel1.Controls.Add(Me.Label4)
        Me.GroupPanel1.Controls.Add(Me.TBMaks)
        Me.GroupPanel1.Controls.Add(Me.Label3)
        Me.GroupPanel1.Controls.Add(Me.TBMin)
        Me.GroupPanel1.Location = New System.Drawing.Point(6, 425)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(204, 173)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 15
        Me.GroupPanel1.Text = "Lebih Lanjut"
        '
        'CheckBoxX4
        '
        Me.CheckBoxX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.CheckBoxX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBoxX4.Checked = True
        Me.CheckBoxX4.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxX4.CheckValue = "Y"
        Me.CheckBoxX4.Enabled = False
        Me.CheckBoxX4.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxX4.Location = New System.Drawing.Point(6, 126)
        Me.CheckBoxX4.Name = "CheckBoxX4"
        Me.CheckBoxX4.Size = New System.Drawing.Size(158, 23)
        Me.CheckBoxX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.CheckBoxX4.TabIndex = 9
        Me.CheckBoxX4.Text = "Hapus Baris Kosong"
        '
        'CheckBoxX3
        '
        Me.CheckBoxX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.CheckBoxX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBoxX3.Checked = True
        Me.CheckBoxX3.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxX3.CheckValue = "Y"
        Me.CheckBoxX3.Enabled = False
        Me.CheckBoxX3.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxX3.Location = New System.Drawing.Point(6, 104)
        Me.CheckBoxX3.Name = "CheckBoxX3"
        Me.CheckBoxX3.Size = New System.Drawing.Size(158, 23)
        Me.CheckBoxX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.CheckBoxX3.TabIndex = 8
        Me.CheckBoxX3.Text = "Cek 1 Baris"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(172, 41)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(26, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Kar."
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(172, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Kar."
        '
        'CheckBoxX2
        '
        Me.CheckBoxX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.CheckBoxX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBoxX2.Checked = True
        Me.CheckBoxX2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxX2.CheckValue = "Y"
        Me.CheckBoxX2.Enabled = False
        Me.CheckBoxX2.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxX2.Location = New System.Drawing.Point(6, 82)
        Me.CheckBoxX2.Name = "CheckBoxX2"
        Me.CheckBoxX2.Size = New System.Drawing.Size(158, 23)
        Me.CheckBoxX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.CheckBoxX2.TabIndex = 5
        Me.CheckBoxX2.Text = "Cegah Quotes Ganda"
        '
        'CheckBoxX1
        '
        Me.CheckBoxX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.CheckBoxX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBoxX1.Checked = True
        Me.CheckBoxX1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBoxX1.CheckValue = "Y"
        Me.CheckBoxX1.Enabled = False
        Me.CheckBoxX1.ForeColor = System.Drawing.Color.Black
        Me.CheckBoxX1.Location = New System.Drawing.Point(6, 60)
        Me.CheckBoxX1.Name = "CheckBoxX1"
        Me.CheckBoxX1.Size = New System.Drawing.Size(100, 23)
        Me.CheckBoxX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.CheckBoxX1.TabIndex = 4
        Me.CheckBoxX1.Text = "Hapus Iklan"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(3, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(99, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Panjang Maksimum"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(2, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Panjang Minimum"
        '
        'LabelX3
        '
        Me.LabelX3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelX3.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(220, 429)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(112, 20)
        Me.LabelX3.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.LabelX3.TabIndex = 17
        Me.LabelX3.Text = "Teks Pencegah:"
        '
        'LabelX4
        '
        Me.LabelX4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LabelX4.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(342, 429)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(110, 20)
        Me.LabelX4.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.LabelX4.TabIndex = 19
        Me.LabelX4.Text = "Teks Sampah:"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(6, 31)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(116, 16)
        Me.LabelX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.LabelX1.TabIndex = 8
        Me.LabelX1.Text = "Data:"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Location = New System.Drawing.Point(421, 429)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(31, 19)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem3})
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 10.0!
        Me.ButtonX1.TabIndex = 22
        '
        'Timer2
        '
        Me.Timer2.Interval = 3000
        '
        'Timer3
        '
        Me.Timer3.Interval = 500
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MIPilihSemua, Me.MITempel, Me.MenuItem7, Me.MIHapus, Me.MIBersihkan, Me.MenuItem6, Me.MIKerjakan})
        '
        'MIPilihSemua
        '
        Me.MIPilihSemua.Index = 0
        Me.MIPilihSemua.Text = "Pilih semua"
        '
        'MITempel
        '
        Me.MITempel.Index = 1
        Me.MITempel.Text = "Tempel"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "-"
        '
        'MIHapus
        '
        Me.MIHapus.Index = 3
        Me.MIHapus.Text = "Hapus"
        '
        'MIBersihkan
        '
        Me.MIBersihkan.Index = 4
        Me.MIBersihkan.Text = "Bersihkan"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 5
        Me.MenuItem6.Text = "-"
        '
        'MIKerjakan
        '
        Me.MIKerjakan.Index = 6
        Me.MIKerjakan.Text = "Kerjakan"
        '
        'TTombol
        '
        Me.TTombol.Interval = 500
        '
        'TTungguTombol
        '
        Me.TTungguTombol.Interval = 10000
        '
        'BalloonTip1
        '
        Me.BalloonTip1.AlertAnimationDuration = 1000
        '
        'Form_PembuatQuote
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(918, 640)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.BRPencegah)
        Me.Controls.Add(Me.BBukan)
        Me.Controls.Add(Me.LabelX4)
        Me.Controls.Add(Me.TBSampah)
        Me.Controls.Add(Me.LabelX3)
        Me.Controls.Add(Me.TBPencegah)
        Me.Controls.Add(Me.GroupPanel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.BPanduan)
        Me.Controls.Add(Me.BBersihkan)
        Me.Controls.Add(Me.LabelX2)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.TBData)
        Me.Controls.Add(Me.TBDari)
        Me.Controls.Add(Me.BSimpan)
        Me.Controls.Add(Me.LBData)
        Me.Controls.Add(Me.TBFileName)
        Me.Controls.Add(Me.BKerjakan)
        Me.DoubleBuffered = True
        Me.MinimumSize = New System.Drawing.Size(650, 500)
        Me.Name = "Form_PembuatQuote"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pembuat Quote"
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TBFileName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents BSimpan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBDari As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents BKerjakan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBData As System.Windows.Forms.TextBox
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BBersihkan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BPanduan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TBPencegah As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TBMin As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxX3 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents CheckBoxX2 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBoxX1 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TBMaks As System.Windows.Forms.TextBox
    Friend WithEvents CheckBoxX4 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TBSampah As System.Windows.Forms.TextBox
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BBukan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BRPencegah As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Public WithEvents LBData As System.Windows.Forms.ListBox
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents MIPilihSemua As System.Windows.Forms.MenuItem
    Friend WithEvents MITempel As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MIHapus As System.Windows.Forms.MenuItem
    Friend WithEvents MIBersihkan As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MIKerjakan As System.Windows.Forms.MenuItem
    Friend WithEvents TTombol As System.Windows.Forms.Timer
    Friend WithEvents TTungguTombol As System.Windows.Forms.Timer
    Friend WithEvents BalloonTip1 As DevComponents.DotNetBar.BalloonTip
End Class
