﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PengaturanUtama
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_PengaturanUtama))
        Me.SSelama = New DevComponents.DotNetBar.Controls.Slider()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LSelama = New DevComponents.DotNetBar.LabelX()
        Me.LSetiap = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.SSetiap = New DevComponents.DotNetBar.Controls.Slider()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.BTambahSemua = New DevComponents.DotNetBar.ButtonX()
        Me.BTambah = New DevComponents.DotNetBar.ButtonX()
        Me.BHapus = New DevComponents.DotNetBar.ButtonX()
        Me.BHapusSemua = New DevComponents.DotNetBar.ButtonX()
        Me.BCekQuotesBaru = New DevComponents.DotNetBar.ButtonX()
        Me.BCPembaruanQuoologi = New DevComponents.DotNetBar.ButtonX()
        Me.CBWaktuSetiap = New System.Windows.Forms.ComboBox()
        Me.BSimpan = New DevComponents.DotNetBar.ButtonX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.BKeluar = New DevComponents.DotNetBar.ButtonX()
        Me.CBStartup = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.LBQuotesAda = New System.Windows.Forms.ListBox()
        Me.LBQuotesUse = New System.Windows.Forms.ListBox()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.CBSuara = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CBAutoHide = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.RadialMenu1 = New DevComponents.DotNetBar.RadialMenu()
        Me.RMIQOTMaker = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMITentang = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMILike = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMIFacebook = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMIDislike = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMITwitter = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMIKQuoologi = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMIUpdate = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RadialMenuItem3 = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMIUpdateApp = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RadialMenuItem5 = New DevComponents.DotNetBar.RadialMenuItem()
        Me.RMIUpdateQot = New DevComponents.DotNetBar.RadialMenuItem()
        Me.LKecAnimasi = New DevComponents.DotNetBar.LabelX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.SKecAni = New DevComponents.DotNetBar.Controls.Slider()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.CBAnimasiClose = New System.Windows.Forms.ComboBox()
        Me.CBAnimasiOpen = New System.Windows.Forms.ComboBox()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.BHapusH = New DevComponents.DotNetBar.ButtonX()
        Me.BEdit = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.CBFileSuara = New System.Windows.Forms.ComboBox()
        Me.LTotalQot = New DevComponents.DotNetBar.LabelX()
        Me.CBJanganUlangiQuote = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.IIOffHor = New DevComponents.Editors.IntegerInput()
        Me.IIOffVer = New DevComponents.Editors.IntegerInput()
        Me.LabelX18 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.CBModeUpd = New System.Windows.Forms.ComboBox()
        Me.BTesSuara = New DevComponents.DotNetBar.ButtonX()
        Me.TBPathTampungan = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.BTampunganJelajah = New DevComponents.DotNetBar.ButtonX()
        Me.BReset = New DevComponents.DotNetBar.ButtonItem()
        Me.CBHentikanQ = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.BProgramH = New DevComponents.DotNetBar.ButtonX()
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.Highlighter1 = New DevComponents.DotNetBar.Validator.Highlighter()
        Me.TEvent = New System.Windows.Forms.Timer(Me.components)
        Me.LBanyakQuoteAda = New DevComponents.DotNetBar.LabelX()
        Me.LBanyakQuoteUse = New DevComponents.DotNetBar.LabelX()
        Me.TPreview = New System.Windows.Forms.Timer(Me.components)
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX17 = New DevComponents.DotNetBar.LabelX()
        Me.LTampungan = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.GroupPanel2 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.TThreadA = New System.Windows.Forms.Timer(Me.components)
        CType(Me.IIOffHor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.IIOffVer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPanel1.SuspendLayout()
        Me.GroupPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'SSelama
        '
        Me.SSelama.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.SSelama.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.SSelama.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.SSelama.DecreaseTooltip = "Memperpendek waktu Quotes ditampilkan"
        Me.SSelama.ForeColor = System.Drawing.Color.Black
        Me.SSelama.IncreaseTooltip = "Memperlama waktu Quotes ditampilkan"
        Me.SSelama.LabelVisible = False
        Me.SSelama.Location = New System.Drawing.Point(11, 26)
        Me.SSelama.Maximum = 60
        Me.SSelama.Minimum = 5
        Me.SSelama.Name = "SSelama"
        Me.SSelama.Size = New System.Drawing.Size(364, 20)
        Me.SSelama.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SSelama.TabIndex = 1
        Me.SSelama.Value = 20
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(341, 2)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(30, 23)
        Me.LabelX2.TabIndex = 2
        Me.LabelX2.Text = "detik"
        '
        'LSelama
        '
        Me.LSelama.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LSelama.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LSelama.ForeColor = System.Drawing.Color.Black
        Me.LSelama.Location = New System.Drawing.Point(325, 2)
        Me.LSelama.Name = "LSelama"
        Me.LSelama.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LSelama.Size = New System.Drawing.Size(14, 23)
        Me.LSelama.TabIndex = 3
        Me.LSelama.Text = "##"
        '
        'LSetiap
        '
        Me.LSetiap.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LSetiap.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LSetiap.ForeColor = System.Drawing.Color.Black
        Me.LSetiap.Location = New System.Drawing.Point(305, 49)
        Me.LSetiap.Name = "LSetiap"
        Me.LSetiap.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LSetiap.Size = New System.Drawing.Size(14, 23)
        Me.LSetiap.TabIndex = 7
        Me.LSetiap.Text = "##"
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.ForeColor = System.Drawing.Color.Black
        Me.LabelX6.Location = New System.Drawing.Point(13, 48)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(182, 23)
        Me.LabelX6.Symbol = ""
        Me.LabelX6.SymbolSize = 14.0!
        Me.LabelX6.TabIndex = 4
        Me.LabelX6.Text = " Tampilkan Quote setiap:"
        '
        'SSetiap
        '
        Me.SSetiap.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.SSetiap.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.SSetiap.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.SSetiap.DecreaseTooltip = "Mempercepat jeda antar penampilan Quotes"
        Me.SSetiap.ForeColor = System.Drawing.Color.Black
        Me.SSetiap.IncreaseTooltip = "Memperlambat jeda antar penampilan Quotes"
        Me.SSetiap.LabelVisible = False
        Me.SSetiap.Location = New System.Drawing.Point(9, 72)
        Me.SSetiap.Maximum = 60
        Me.SSetiap.Minimum = 1
        Me.SSetiap.Name = "SSetiap"
        Me.SSetiap.Size = New System.Drawing.Size(364, 20)
        Me.SSetiap.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SSetiap.TabIndex = 5
        Me.SSetiap.Value = 10
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.ForeColor = System.Drawing.Color.Black
        Me.LabelX7.Location = New System.Drawing.Point(13, 194)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(132, 23)
        Me.LabelX7.TabIndex = 8
        Me.LabelX7.Text = "Berkas Quotes tersedia:"
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.ForeColor = System.Drawing.Color.Black
        Me.LabelX8.Location = New System.Drawing.Point(213, 194)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(132, 23)
        Me.LabelX8.TabIndex = 9
        Me.LabelX8.Text = "Berkas Quotes terpakai:"
        '
        'BTambahSemua
        '
        Me.BTambahSemua.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTambahSemua.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BTambahSemua.Location = New System.Drawing.Point(176, 222)
        Me.BTambahSemua.Name = "BTambahSemua"
        Me.BTambahSemua.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BTambahSemua.Size = New System.Drawing.Size(32, 23)
        Me.BTambahSemua.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BTambahSemua, New DevComponents.DotNetBar.SuperTooltipInfo("Tambah semua data tersedia", "", "Ketika perintah ini dijalankan, semua data yang terdapat di bagian ""Quotes tersed" & _
            "ia"" akan ditambahkan ke bagian ""Quotes terpakai"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BTambahSemua.Symbol = ""
        Me.BTambahSemua.SymbolSize = 16.0!
        Me.BTambahSemua.TabIndex = 15
        '
        'BTambah
        '
        Me.BTambah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTambah.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BTambah.Enabled = False
        Me.BTambah.Location = New System.Drawing.Point(176, 254)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BTambah.Size = New System.Drawing.Size(32, 23)
        Me.BTambah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BTambah, New DevComponents.DotNetBar.SuperTooltipInfo("Tambah data terpilih", "", "Tombol ini berfungsi untuk menambah data terpilih dari bagian ""Quotes tersedia"" k" & _
            "e bagian ""Quotes terpakai"" sehingga kata² yang terdapat di berkas tersebut akan " & _
            "ditampilkan saat Quotes muncul.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(160, 130)))
        Me.BTambah.Symbol = ""
        Me.BTambah.SymbolSize = 16.0!
        Me.BTambah.TabIndex = 16
        '
        'BHapus
        '
        Me.BHapus.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BHapus.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BHapus.Location = New System.Drawing.Point(176, 286)
        Me.BHapus.Name = "BHapus"
        Me.BHapus.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BHapus.Size = New System.Drawing.Size(32, 23)
        Me.BHapus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BHapus, New DevComponents.DotNetBar.SuperTooltipInfo("Buang data terpilih", "", "Tombol ini berfungsi untuk menghapus data terpilih dari bagian ""Quotes terpakai"" " & _
            "sehingga kata² yang terdapat di berkas tersebut tidak akan ditampilkan saat Quot" & _
            "es muncul.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BHapus.Symbol = ""
        Me.BHapus.SymbolSize = 16.0!
        Me.BHapus.TabIndex = 17
        '
        'BHapusSemua
        '
        Me.BHapusSemua.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BHapusSemua.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BHapusSemua.Location = New System.Drawing.Point(176, 318)
        Me.BHapusSemua.Name = "BHapusSemua"
        Me.BHapusSemua.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BHapusSemua.Size = New System.Drawing.Size(32, 23)
        Me.BHapusSemua.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BHapusSemua, New DevComponents.DotNetBar.SuperTooltipInfo("Buang semua data terpakai", "", "Ketika perintah ini dijalankan, semua data yang terdapat di bagian ""Quotes terpak" & _
            "ai"" akan dihilangkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BHapusSemua.Symbol = ""
        Me.BHapusSemua.SymbolSize = 16.0!
        Me.BHapusSemua.TabIndex = 18
        '
        'BCekQuotesBaru
        '
        Me.BCekQuotesBaru.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BCekQuotesBaru.AntiAlias = True
        Me.BCekQuotesBaru.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BCekQuotesBaru.Location = New System.Drawing.Point(13, 349)
        Me.BCekQuotesBaru.Name = "BCekQuotesBaru"
        Me.BCekQuotesBaru.Size = New System.Drawing.Size(116, 23)
        Me.BCekQuotesBaru.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BCekQuotesBaru, New DevComponents.DotNetBar.SuperTooltipInfo("Pembaruan Quotes", "Glow orange menandakan update quote tersedia.", "Program ini akan mengecek ketersediaan berkas Quotes terbaru yang ada pada server" & _
            ".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BCekQuotesBaru.TabIndex = 20
        Me.BCekQuotesBaru.Text = "Cek Quotes terbaru"
        '
        'BCPembaruanQuoologi
        '
        Me.BCPembaruanQuoologi.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BCPembaruanQuoologi.AntiAlias = True
        Me.BCPembaruanQuoologi.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BCPembaruanQuoologi.Location = New System.Drawing.Point(254, 349)
        Me.BCPembaruanQuoologi.Name = "BCPembaruanQuoologi"
        Me.BCPembaruanQuoologi.Size = New System.Drawing.Size(116, 23)
        Me.BCPembaruanQuoologi.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BCPembaruanQuoologi, New DevComponents.DotNetBar.SuperTooltipInfo("Pembaruan Quoologi", "Glow orange menandakan update Quoologi tersedia.", "Program ini akan mengecek ketersediaan Quoologi terbaru yang ada pada server kami" & _
            ".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BCPembaruanQuoologi.SymbolSize = 10.0!
        Me.BCPembaruanQuoologi.TabIndex = 21
        Me.BCPembaruanQuoologi.Text = "Cek pembaruan [Q]"
        '
        'CBWaktuSetiap
        '
        Me.CBWaktuSetiap.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CBWaktuSetiap.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBWaktuSetiap.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CBWaktuSetiap.ForeColor = System.Drawing.Color.Black
        Me.CBWaktuSetiap.FormattingEnabled = True
        Me.CBWaktuSetiap.Items.AddRange(New Object() {"detik", "menit", "jam"})
        Me.CBWaktuSetiap.Location = New System.Drawing.Point(319, 50)
        Me.CBWaktuSetiap.Name = "CBWaktuSetiap"
        Me.CBWaktuSetiap.Size = New System.Drawing.Size(51, 21)
        Me.CBWaktuSetiap.TabIndex = 22
        '
        'BSimpan
        '
        Me.BSimpan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BSimpan.AntiAlias = True
        Me.BSimpan.Location = New System.Drawing.Point(301, 7)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.BSimpan.Size = New System.Drawing.Size(75, 23)
        Me.BSimpan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BSimpan, New DevComponents.DotNetBar.SuperTooltipInfo("", "", " Simpan pengaturan program ", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(150, 19)))
        Me.BSimpan.TabIndex = 23
        Me.BSimpan.Text = "Simpan | F1"
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.AntiAlias = True
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(219, 7)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(75, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, New DevComponents.DotNetBar.SuperTooltipInfo("", "", " Tutup pengaturan ", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, False, False, New System.Drawing.Size(100, 18)))
        Me.BBatal.TabIndex = 24
        Me.BBatal.Text = "Tutup | Esc"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(13, 2)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(174, 23)
        Me.LabelX1.Symbol = ""
        Me.LabelX1.SymbolSize = 14.0!
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = " Tampilkan Quote selama:"
        '
        'BKeluar
        '
        Me.BKeluar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BKeluar.AntiAlias = True
        Me.BKeluar.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground
        Me.BKeluar.Location = New System.Drawing.Point(1, 7)
        Me.BKeluar.Name = "BKeluar"
        Me.BKeluar.Size = New System.Drawing.Size(75, 23)
        Me.BKeluar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BKeluar, New DevComponents.DotNetBar.SuperTooltipInfo("", "", " Keluar dari program ", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(107, 18)))
        Me.BKeluar.Symbol = ""
        Me.BKeluar.SymbolSize = 13.0!
        Me.BKeluar.TabIndex = 25
        Me.BKeluar.Text = " Keluar"
        '
        'CBStartup
        '
        Me.CBStartup.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.CBStartup.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBStartup.EnableMarkup = False
        Me.CBStartup.ForeColor = System.Drawing.Color.Black
        Me.CBStartup.Location = New System.Drawing.Point(6, 1)
        Me.CBStartup.Name = "CBStartup"
        Me.CBStartup.Size = New System.Drawing.Size(174, 23)
        Me.CBStartup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBStartup, New DevComponents.DotNetBar.SuperTooltipInfo("Jalankan Quoologi saat startup", "", "Perintah ini memungkinkan program untuk otomatis dijalankan saat memulai Windows." & _
            "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.CBStartup.TabIndex = 26
        Me.CBStartup.Text = "Jalankan Quoologi saat startup"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonX1.Enabled = False
        Me.ButtonX1.Location = New System.Drawing.Point(176, 349)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.ButtonX1.Size = New System.Drawing.Size(32, 23)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.ButtonX1, New DevComponents.DotNetBar.SuperTooltipInfo("Tampilkan Quotes", "", "Ketika perintah ini dijalankan, program akan menampilkan Quotes.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 15.0!
        Me.ButtonX1.TabIndex = 28
        '
        'LBQuotesAda
        '
        Me.LBQuotesAda.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LBQuotesAda.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBQuotesAda.ForeColor = System.Drawing.Color.Black
        Me.LBQuotesAda.FormattingEnabled = True
        Me.LBQuotesAda.Location = New System.Drawing.Point(13, 222)
        Me.LBQuotesAda.Name = "LBQuotesAda"
        Me.LBQuotesAda.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBQuotesAda.Size = New System.Drawing.Size(156, 119)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBQuotesAda, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Berisi data .qot yang tersedia di dalam hardisk.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(150, 32)))
        Me.LBQuotesAda.TabIndex = 34
        '
        'LBQuotesUse
        '
        Me.LBQuotesUse.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LBQuotesUse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBQuotesUse.ForeColor = System.Drawing.Color.Black
        Me.LBQuotesUse.FormattingEnabled = True
        Me.LBQuotesUse.Location = New System.Drawing.Point(214, 222)
        Me.LBQuotesUse.Name = "LBQuotesUse"
        Me.LBQuotesUse.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBQuotesUse.Size = New System.Drawing.Size(156, 119)
        Me.SuperTooltip1.SetSuperTooltip(Me.LBQuotesUse, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Berisi data .qot yang terpakai di dalam program.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(150, 32)))
        Me.LBQuotesUse.TabIndex = 35
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.ForeColor = System.Drawing.Color.Black
        Me.LabelX3.Location = New System.Drawing.Point(13, 381)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(132, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LabelX3, New DevComponents.DotNetBar.SuperTooltipInfo("Mainkan suara", "", "Quoologi memungkinkan pengguna untuk memainkan berkas suara tiap kali Quotes dita" & _
            "mpilkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.LabelX3.Symbol = ""
        Me.LabelX3.SymbolSize = 15.0!
        Me.LabelX3.TabIndex = 36
        Me.LabelX3.Text = " Mainkan Suara:"
        '
        'CBSuara
        '
        Me.CBSuara.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.CBSuara.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBSuara.FocusCuesEnabled = False
        Me.CBSuara.ForeColor = System.Drawing.Color.Black
        Me.CBSuara.Location = New System.Drawing.Point(169, 382)
        Me.CBSuara.Name = "CBSuara"
        Me.CBSuara.Size = New System.Drawing.Size(14, 23)
        Me.CBSuara.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBSuara, New DevComponents.DotNetBar.SuperTooltipInfo("Mainkan suara ?", "", "Quoologi memungkinkan pengguna untuk memainkan berkas suara tiap kali Quotes dita" & _
            "mpilkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.CBSuara.TabIndex = 39
        '
        'CBAutoHide
        '
        Me.CBAutoHide.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.CBAutoHide.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBAutoHide.EnableMarkup = False
        Me.CBAutoHide.ForeColor = System.Drawing.Color.Black
        Me.CBAutoHide.Location = New System.Drawing.Point(6, 27)
        Me.CBAutoHide.Name = "CBAutoHide"
        Me.CBAutoHide.Size = New System.Drawing.Size(181, 23)
        Me.CBAutoHide.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBAutoHide, New DevComponents.DotNetBar.SuperTooltipInfo("Sembunyikan Quotes saat mouse mendekat", "", "Perintah ini akan membuat program langsung menghilangkan Quotes saat mouse mendek" & _
            "at.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, False, New System.Drawing.Size(0, 0)))
        Me.CBAutoHide.TabIndex = 40
        Me.CBAutoHide.Text = "Sembunyikan Quote ketika..."
        '
        'RadialMenu1
        '
        Me.RadialMenu1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.RadialMenu1.Colors.CircularBackColor = System.Drawing.Color.CornflowerBlue
        Me.RadialMenu1.Colors.CircularBorderColor = System.Drawing.Color.CornflowerBlue
        Me.RadialMenu1.Colors.CircularForeColor = System.Drawing.Color.Gray
        Me.RadialMenu1.Colors.RadialMenuBackground = System.Drawing.Color.White
        Me.RadialMenu1.Colors.RadialMenuBorder = System.Drawing.Color.SteelBlue
        Me.RadialMenu1.ForeColor = System.Drawing.Color.Black
        Me.RadialMenu1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.RMIQOTMaker, Me.RMITentang, Me.RMIKQuoologi, Me.RMIUpdate})
        Me.RadialMenu1.Location = New System.Drawing.Point(177, 5)
        Me.RadialMenu1.Name = "RadialMenu1"
        Me.RadialMenu1.Size = New System.Drawing.Size(28, 28)
        Me.SuperTooltip1.SetSuperTooltip(Me.RadialMenu1, New DevComponents.DotNetBar.SuperTooltipInfo("Menu", "", "Tekan tombol untuk menampilkan menu.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(126, 54)))
        Me.RadialMenu1.Symbol = ""
        Me.RadialMenu1.TabIndex = 41
        Me.RadialMenu1.Text = "RadialMenu1"
        '
        'RMIQOTMaker
        '
        Me.RMIQOTMaker.CircularBackColor = System.Drawing.Color.DimGray
        Me.RMIQOTMaker.CircularBorderColor = System.Drawing.Color.Gray
        Me.RMIQOTMaker.CircularForeColor = System.Drawing.Color.Black
        Me.RMIQOTMaker.Name = "RMIQOTMaker"
        Me.SuperTooltip1.SetSuperTooltip(Me.RMIQOTMaker, New DevComponents.DotNetBar.SuperTooltipInfo("QOT Maker", "[W]", resources.GetString("RMIQOTMaker.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, False, New System.Drawing.Size(188, 154)))
        Me.RMIQOTMaker.Symbol = ""
        Me.RMIQOTMaker.Text = "QOT Maker"
        '
        'RMITentang
        '
        Me.RMITentang.Name = "RMITentang"
        Me.RMITentang.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.RMILike, Me.RMIFacebook, Me.RMIDislike, Me.RMITwitter})
        Me.SuperTooltip1.SetSuperTooltip(Me.RMITentang, New DevComponents.DotNetBar.SuperTooltipInfo("Tentang", "", "Tombol ini berfungsi untuk menampilkan jendela Tentang." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Di dalam tentang, beri" & _
            "si tanggal dan versi rilis apllikasi ini dan akun Facebook dan Twitter pembuat.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(209, 109)))
        Me.RMITentang.Symbol = ""
        Me.RMITentang.Text = "Tentang"
        '
        'RMILike
        '
        Me.RMILike.Name = "RMILike"
        Me.SuperTooltip1.SetSuperTooltip(Me.RMILike, New DevComponents.DotNetBar.SuperTooltipInfo("Suka ?", "", "Klik untuk menyukai Quoologi di Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.RMILike.Symbol = ""
        Me.RMILike.SymbolSize = 30.0!
        Me.RMILike.Text = " "
        '
        'RMIFacebook
        '
        Me.RMIFacebook.Name = "RMIFacebook"
        Me.RMIFacebook.Symbol = ""
        Me.RMIFacebook.SymbolSize = 38.0!
        '
        'RMIDislike
        '
        Me.RMIDislike.Name = "RMIDislike"
        Me.SuperTooltip1.SetSuperTooltip(Me.RMIDislike, New DevComponents.DotNetBar.SuperTooltipInfo("Tidak Suka ?", "", "Klik untuk me-unlike Quoologi di Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.RMIDislike.Symbol = ""
        Me.RMIDislike.SymbolSize = 30.0!
        Me.RMIDislike.Text = " "
        Me.RMIDislike.TextOffset = New System.Drawing.Point(10, 0)
        '
        'RMITwitter
        '
        Me.RMITwitter.Name = "RMITwitter"
        Me.RMITwitter.Symbol = ""
        Me.RMITwitter.SymbolSize = 38.0!
        '
        'RMIKQuoologi
        '
        Me.RMIKQuoologi.Name = "RMIKQuoologi"
        Me.SuperTooltip1.SetSuperTooltip(Me.RMIKQuoologi, New DevComponents.DotNetBar.SuperTooltipInfo("Kustomisasi Quoologi", "", "Tombol ini berfungsi untuk menampilkan jendela Kustomisasi Quoologi." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Alat ini " & _
            "berfungsi untuk mengubah tampilan Quoologi.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(218, 106)))
        Me.RMIKQuoologi.Symbol = ""
        Me.RMIKQuoologi.Text = "K. Quoologi"
        '
        'RMIUpdate
        '
        Me.RMIUpdate.Name = "RMIUpdate"
        Me.RMIUpdate.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.RadialMenuItem3, Me.RMIUpdateApp, Me.RadialMenuItem5, Me.RMIUpdateQot})
        Me.SuperTooltip1.SetSuperTooltip(Me.RMIUpdate, New DevComponents.DotNetBar.SuperTooltipInfo("Update", "", "Tombol ini berfungsi untuk menampilkan jendela Update." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Update berfungsi untuk " & _
            "memutakhirkan apllikasi sehingga apllikasi fitur² baru akan bisa segera dinikmat" & _
            "i oleh pengguna.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.RMIUpdate.Symbol = ""
        Me.RMIUpdate.Text = "Update"
        '
        'RadialMenuItem3
        '
        Me.RadialMenuItem3.Name = "RadialMenuItem3"
        '
        'RMIUpdateApp
        '
        Me.RMIUpdateApp.Name = "RMIUpdateApp"
        Me.SuperTooltip1.SetSuperTooltip(Me.RMIUpdateApp, New DevComponents.DotNetBar.SuperTooltipInfo("Update Quoologi", "", "Tombol ini berfungsi untuk memutakhirkan aplikasi Quoologi  menjadi versi terbaru" & _
            ".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.RMIUpdateApp.Symbol = ""
        Me.RMIUpdateApp.SymbolSize = 20.0!
        Me.RMIUpdateApp.Text = "App"
        '
        'RadialMenuItem5
        '
        Me.RadialMenuItem5.Name = "RadialMenuItem5"
        '
        'RMIUpdateQot
        '
        Me.RMIUpdateQot.Name = "RMIUpdateQot"
        Me.SuperTooltip1.SetSuperTooltip(Me.RMIUpdateQot, New DevComponents.DotNetBar.SuperTooltipInfo("Update Quotes", "", "Tombol ini berfungsi untuk mengecek ketersediaan berkas Quoologi (.qot) terbaru.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.RMIUpdateQot.Symbol = ""
        Me.RMIUpdateQot.SymbolSize = 20.0!
        Me.RMIUpdateQot.Text = "Quote"
        '
        'LKecAnimasi
        '
        Me.LKecAnimasi.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LKecAnimasi.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LKecAnimasi.ForeColor = System.Drawing.Color.Black
        Me.LKecAnimasi.Location = New System.Drawing.Point(313, 93)
        Me.LKecAnimasi.Name = "LKecAnimasi"
        Me.LKecAnimasi.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LKecAnimasi.Size = New System.Drawing.Size(38, 23)
        Me.LKecAnimasi.TabIndex = 45
        Me.LKecAnimasi.Text = "######"
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.ForeColor = System.Drawing.Color.Black
        Me.LabelX10.Location = New System.Drawing.Point(353, 93)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(16, 23)
        Me.LabelX10.TabIndex = 44
        Me.LabelX10.Text = "ms"
        '
        'LabelX11
        '
        Me.LabelX11.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.ForeColor = System.Drawing.Color.Black
        Me.LabelX11.Location = New System.Drawing.Point(199, 93)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(123, 23)
        Me.LabelX11.Symbol = ""
        Me.LabelX11.SymbolSize = 14.0!
        Me.LabelX11.TabIndex = 42
        Me.LabelX11.Text = " Waktu Animasi:"
        '
        'SKecAni
        '
        Me.SKecAni.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.SKecAni.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.SKecAni.Cursor = System.Windows.Forms.Cursors.SizeWE
        Me.SKecAni.ForeColor = System.Drawing.Color.Black
        Me.SKecAni.LabelVisible = False
        Me.SKecAni.Location = New System.Drawing.Point(195, 122)
        Me.SKecAni.Maximum = 150
        Me.SKecAni.Minimum = 1
        Me.SKecAni.Name = "SKecAni"
        Me.SKecAni.Size = New System.Drawing.Size(178, 20)
        Me.SKecAni.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.SKecAni, New DevComponents.DotNetBar.SuperTooltipInfo("Lama animasi ditampilkan", "", "Slider ini berfungsi untuk menetapkan lama waktu animasi berjalan dalam satuan mi" & _
            "lisecond (ms)." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Semakin sedikit waktu, semakin cepat animasi itu berjalan, dan s" & _
            "ebaliknya.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.SKecAni.TabIndex = 43
        Me.SKecAni.Value = 2
        '
        'LabelX12
        '
        Me.LabelX12.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.ForeColor = System.Drawing.Color.Black
        Me.LabelX12.Location = New System.Drawing.Point(199, 146)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(163, 23)
        Me.LabelX12.Symbol = ""
        Me.LabelX12.SymbolSize = 14.0!
        Me.LabelX12.TabIndex = 46
        Me.LabelX12.Text = " Gaya Animasi Saat Hilang:"
        '
        'CBAnimasiClose
        '
        Me.CBAnimasiClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CBAnimasiClose.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBAnimasiClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CBAnimasiClose.ForeColor = System.Drawing.Color.Black
        Me.CBAnimasiClose.FormattingEnabled = True
        Me.CBAnimasiClose.Items.AddRange(New Object() {"Tengah", "Menyatu", "Geser Horizontal Kiri", "Geser Horizontal Kanan", "Geser Vertikal Atas", "Geser Vertikal Bawah", "Persempit Horizontal Kiri", "Persempit Horizontal Kanan", "Persempit Vertikal Atas", "Persempit Vertikal Bawah"})
        Me.CBAnimasiClose.Location = New System.Drawing.Point(199, 170)
        Me.CBAnimasiClose.Name = "CBAnimasiClose"
        Me.CBAnimasiClose.Size = New System.Drawing.Size(170, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.CBAnimasiClose, New DevComponents.DotNetBar.SuperTooltipInfo("Gaya animasi saat hilang", "", "Pada bagian ini berisi animasi yang bisa diterapkan ketika Quotes dihilangkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.CBAnimasiClose.TabIndex = 47
        '
        'CBAnimasiOpen
        '
        Me.CBAnimasiOpen.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CBAnimasiOpen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBAnimasiOpen.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CBAnimasiOpen.ForeColor = System.Drawing.Color.Black
        Me.CBAnimasiOpen.FormattingEnabled = True
        Me.CBAnimasiOpen.Items.AddRange(New Object() {"Tengah", "Menyatu", "Geser Horizontal Kiri", "Geser Horizontal Kanan", "Geser Vertikal Atas", "Geser Vertikal Bawah", "Perluas Horizontal Kiri", "Perluas Horizontal Kanan", "Perluas Vertikal Atas", "Perluas Vertikal Bawah"})
        Me.CBAnimasiOpen.Location = New System.Drawing.Point(13, 170)
        Me.CBAnimasiOpen.Name = "CBAnimasiOpen"
        Me.CBAnimasiOpen.Size = New System.Drawing.Size(172, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.CBAnimasiOpen, New DevComponents.DotNetBar.SuperTooltipInfo("Gaya animasi saat muncul", "", "Pada bagian ini berisi animasi yang bisa diterapkan ketika Quotes ditampilkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.CBAnimasiOpen.TabIndex = 49
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.ForeColor = System.Drawing.Color.Black
        Me.LabelX13.Location = New System.Drawing.Point(11, 146)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(171, 23)
        Me.LabelX13.Symbol = ""
        Me.LabelX13.SymbolSize = 14.0!
        Me.LabelX13.TabIndex = 48
        Me.LabelX13.Text = " Gaya Animasi Saat Muncul:"
        '
        'BHapusH
        '
        Me.BHapusH.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BHapusH.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BHapusH.Enabled = False
        Me.BHapusH.Location = New System.Drawing.Point(137, 349)
        Me.BHapusH.Name = "BHapusH"
        Me.BHapusH.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BHapusH.Size = New System.Drawing.Size(32, 23)
        Me.BHapusH.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BHapusH, New DevComponents.DotNetBar.SuperTooltipInfo("Hapus berkas Quotes dari hardisk", "", "Ketika perintah ini dijalankan, program akan menghapus berkas "".qot"" dari hardisk" & _
            " yang terpilih di bagian ""Quotes tersedia"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BHapusH.Symbol = ""
        Me.BHapusH.SymbolSize = 15.0!
        Me.BHapusH.TabIndex = 50
        '
        'BEdit
        '
        Me.BEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BEdit.Enabled = False
        Me.BEdit.Location = New System.Drawing.Point(215, 349)
        Me.BEdit.Name = "BEdit"
        Me.BEdit.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BEdit.Size = New System.Drawing.Size(32, 23)
        Me.BEdit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BEdit, New DevComponents.DotNetBar.SuperTooltipInfo("Edit berkas Quotes", "", resources.GetString("BEdit.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(200, 120)))
        Me.BEdit.Symbol = ""
        Me.BEdit.SymbolSize = 15.0!
        Me.BEdit.TabIndex = 51
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.HoverDelayMultiplier = 5
        '
        'CBFileSuara
        '
        Me.CBFileSuara.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CBFileSuara.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBFileSuara.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CBFileSuara.ForeColor = System.Drawing.Color.Black
        Me.CBFileSuara.FormattingEnabled = True
        Me.CBFileSuara.Location = New System.Drawing.Point(13, 407)
        Me.CBFileSuara.Name = "CBFileSuara"
        Me.CBFileSuara.Size = New System.Drawing.Size(147, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.CBFileSuara, New DevComponents.DotNetBar.SuperTooltipInfo("Suara", "", "Pada bagian ini berisi pilihan suara yang akan dimainkan ketika Quotes muncul.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.CBFileSuara.TabIndex = 52
        '
        'LTotalQot
        '
        Me.LTotalQot.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LTotalQot.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LTotalQot.ForeColor = System.Drawing.Color.Black
        Me.LTotalQot.Location = New System.Drawing.Point(91, 7)
        Me.LTotalQot.Name = "LTotalQot"
        Me.LTotalQot.Size = New System.Drawing.Size(74, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LTotalQot, New DevComponents.DotNetBar.SuperTooltipInfo("Total Kata² Quotes", "", "Menunjukan banyaknya kata² Quotes yang digunakan", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.LTotalQot.Symbol = ""
        Me.LTotalQot.SymbolColor = System.Drawing.Color.DarkSlateBlue
        Me.LTotalQot.SymbolSize = 15.0!
        Me.LTotalQot.TabIndex = 59
        Me.LTotalQot.Text = "#####"
        Me.LTotalQot.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'CBJanganUlangiQuote
        '
        Me.CBJanganUlangiQuote.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.CBJanganUlangiQuote.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBJanganUlangiQuote.EnableMarkup = False
        Me.CBJanganUlangiQuote.ForeColor = System.Drawing.Color.Black
        Me.CBJanganUlangiQuote.Location = New System.Drawing.Point(183, 1)
        Me.CBJanganUlangiQuote.Name = "CBJanganUlangiQuote"
        Me.CBJanganUlangiQuote.Size = New System.Drawing.Size(174, 23)
        Me.CBJanganUlangiQuote.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBJanganUlangiQuote, New DevComponents.DotNetBar.SuperTooltipInfo("Jangan Ulangi Quote", "", resources.GetString("CBJanganUlangiQuote.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(261, 134)))
        Me.CBJanganUlangiQuote.TabIndex = 60
        Me.CBJanganUlangiQuote.Text = "Jangan ulangi quote yang sama"
        '
        'IIOffHor
        '
        Me.IIOffHor.AntiAlias = True
        Me.IIOffHor.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IIOffHor.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IIOffHor.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IIOffHor.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IIOffHor.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.IIOffHor.ForeColor = System.Drawing.Color.Black
        Me.IIOffHor.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center
        Me.IIOffHor.Location = New System.Drawing.Point(306, 410)
        Me.IIOffHor.Name = "IIOffHor"
        Me.IIOffHor.ShowUpDown = True
        Me.IIOffHor.Size = New System.Drawing.Size(66, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IIOffHor, New DevComponents.DotNetBar.SuperTooltipInfo("Offset Horizontal", "", "Pada bagian ini berisi offset posisi horizontal untuk jendela Quotes, semakin tin" & _
            "ggi nilainya maka semakin dekat jendela dengan sisi bagian kiri", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.IIOffHor.TabIndex = 82
        '
        'IIOffVer
        '
        Me.IIOffVer.AntiAlias = True
        Me.IIOffVer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.IIOffVer.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.IIOffVer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.IIOffVer.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.IIOffVer.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.IIOffVer.ForeColor = System.Drawing.Color.Black
        Me.IIOffVer.InputHorizontalAlignment = DevComponents.Editors.eHorizontalAlignment.Center
        Me.IIOffVer.Location = New System.Drawing.Point(213, 410)
        Me.IIOffVer.Name = "IIOffVer"
        Me.IIOffVer.ShowUpDown = True
        Me.IIOffVer.Size = New System.Drawing.Size(64, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.IIOffVer, New DevComponents.DotNetBar.SuperTooltipInfo("Offset Vertikal", "", "Pada bagian ini berisi offset posisi vertikal untuk jendela Quotes, semakin tingg" & _
            "i nilainya maka semakin dekat jendela dengan sisi atas", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.IIOffVer.TabIndex = 80
        '
        'LabelX18
        '
        Me.LabelX18.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX18.ForeColor = System.Drawing.Color.Black
        Me.LabelX18.Location = New System.Drawing.Point(199, 382)
        Me.LabelX18.Name = "LabelX18"
        Me.LabelX18.Size = New System.Drawing.Size(175, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LabelX18, New DevComponents.DotNetBar.SuperTooltipInfo("Mainkan suara", "", "Quoologi memungkinkan pengguna untuk memainkan berkas suara tiap kali Quotes dita" & _
            "mpilkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.LabelX18.Symbol = ""
        Me.LabelX18.SymbolSize = 15.0!
        Me.LabelX18.TabIndex = 79
        Me.LabelX18.Text = " Offset Jendela Quote: "
        '
        'LabelX5
        '
        Me.LabelX5.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.ForeColor = System.Drawing.Color.Black
        Me.LabelX5.Location = New System.Drawing.Point(13, 96)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(175, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.LabelX5, New DevComponents.DotNetBar.SuperTooltipInfo("Mainkan suara", "", "Quoologi memungkinkan pengguna untuk memainkan berkas suara tiap kali Quotes dita" & _
            "mpilkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.LabelX5.Symbol = ""
        Me.LabelX5.SymbolSize = 15.0!
        Me.LabelX5.TabIndex = 84
        Me.LabelX5.Text = "Mode Pembaruan: "
        '
        'CBModeUpd
        '
        Me.CBModeUpd.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CBModeUpd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBModeUpd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.CBModeUpd.ForeColor = System.Drawing.Color.Black
        Me.CBModeUpd.FormattingEnabled = True
        Me.CBModeUpd.Items.AddRange(New Object() {"Beritahu Ketika Update Tersedia", "Update Manual"})
        Me.CBModeUpd.Location = New System.Drawing.Point(13, 120)
        Me.CBModeUpd.Name = "CBModeUpd"
        Me.CBModeUpd.Size = New System.Drawing.Size(172, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.CBModeUpd, New DevComponents.DotNetBar.SuperTooltipInfo("Mode Pembaruan", "", "Berisi pilihan pembaruan program utama.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.CBModeUpd.TabIndex = 85
        '
        'BTesSuara
        '
        Me.BTesSuara.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTesSuara.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BTesSuara.Location = New System.Drawing.Point(164, 408)
        Me.BTesSuara.Name = "BTesSuara"
        Me.BTesSuara.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BTesSuara.Size = New System.Drawing.Size(19, 19)
        Me.BTesSuara.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BTesSuara, New DevComponents.DotNetBar.SuperTooltipInfo("Dengarkan", "", "Klik tombol ini untuk mendengarkan suara dari berkas suara yang dipilih.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BTesSuara.Symbol = ""
        Me.BTesSuara.SymbolSize = 13.0!
        Me.BTesSuara.TabIndex = 86
        '
        'TBPathTampungan
        '
        Me.TBPathTampungan.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBPathTampungan.Border.Class = "TextBoxBorder"
        Me.TBPathTampungan.Border.CornerDiameter = 4
        Me.TBPathTampungan.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBPathTampungan.ForeColor = System.Drawing.Color.Black
        Me.TBPathTampungan.Location = New System.Drawing.Point(13, 463)
        Me.TBPathTampungan.Name = "TBPathTampungan"
        Me.TBPathTampungan.Size = New System.Drawing.Size(304, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBPathTampungan, New DevComponents.DotNetBar.SuperTooltipInfo("Alamat Penyimpanan", "", "Berisi alamat drive dan folder tampungan sementara untuk quote yang sudah ditampi" & _
            "lkan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.TBPathTampungan.TabIndex = 88
        Me.TBPathTampungan.Text = "asdasd"
        '
        'BTampunganJelajah
        '
        Me.BTampunganJelajah.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BTampunganJelajah.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BTampunganJelajah.Location = New System.Drawing.Point(316, 463)
        Me.BTampunganJelajah.Name = "BTampunganJelajah"
        Me.BTampunganJelajah.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(0, 4, 0, 4)
        Me.BTampunganJelajah.Size = New System.Drawing.Size(56, 20)
        Me.BTampunganJelajah.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BTampunganJelajah.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.BReset})
        Me.SuperTooltip1.SetSuperTooltip(Me.BTampunganJelajah, New DevComponents.DotNetBar.SuperTooltipInfo("Jelajah", "", "Klik untuk membuka mini explorer untuk menentukan alamat tampungan sementara.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BTampunganJelajah.TabIndex = 89
        Me.BTampunganJelajah.Text = "Jelajah"
        '
        'BReset
        '
        Me.BReset.GlobalItem = False
        Me.BReset.Name = "BReset"
        Me.SuperTooltip1.SetSuperTooltip(Me.BReset, New DevComponents.DotNetBar.SuperTooltipInfo("Atur Ulang", "", "Klik tombol ini untuk mengatur ulang alamat tampungan sementara yang direkomendas" & _
            "ikan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(207, 71)))
        Me.BReset.Text = "Atur Ulang"
        '
        'CBHentikanQ
        '
        Me.CBHentikanQ.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.CBHentikanQ.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CBHentikanQ.EnableMarkup = False
        Me.CBHentikanQ.ForeColor = System.Drawing.Color.Black
        Me.CBHentikanQ.Location = New System.Drawing.Point(183, 27)
        Me.CBHentikanQ.Name = "CBHentikanQ"
        Me.CBHentikanQ.Size = New System.Drawing.Size(182, 23)
        Me.CBHentikanQ.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.CBHentikanQ, New DevComponents.DotNetBar.SuperTooltipInfo("Hentikan Otomatis", "", "Ketika pilihan ini terpilih, maka jika Anda menjalankan aplikasi yang terdaftar s" & _
            "ebagai aplikasi terkecuali, maka Quoologi akan menghentikan penampilan quote sec" & _
            "ara otomatis.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue, True, True, New System.Drawing.Size(237, 95)))
        Me.CBHentikanQ.TabIndex = 61
        Me.CBHentikanQ.Text = "Hentikan Quoologi otomatis ketika"
        '
        'BProgramH
        '
        Me.BProgramH.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BProgramH.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.BProgramH.Location = New System.Drawing.Point(372, 28)
        Me.BProgramH.Name = "BProgramH"
        Me.BProgramH.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(2)
        Me.BProgramH.Size = New System.Drawing.Size(40, 21)
        Me.BProgramH.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BProgramH, New DevComponents.DotNetBar.SuperTooltipInfo("Program", "", "Klik tombol ini untuk melihat / mengatur daftar program terkecuali untuk hentikan" & _
            " Quoologi otomatis.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Blue))
        Me.BProgramH.Symbol = ""
        Me.BProgramH.SymbolSize = 13.0!
        Me.BProgramH.TabIndex = 62
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerColorTint = System.Drawing.Color.White
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(87, Byte), Integer), CType(CType(154, Byte), Integer)))
        '
        'Highlighter1
        '
        Me.Highlighter1.ContainerControl = Me
        Me.Highlighter1.FocusHighlightColor = DevComponents.DotNetBar.Validator.eHighlightColor.Green
        '
        'TEvent
        '
        Me.TEvent.Interval = 10
        '
        'LBanyakQuoteAda
        '
        '
        '
        '
        Me.LBanyakQuoteAda.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LBanyakQuoteAda.Location = New System.Drawing.Point(137, 195)
        Me.LBanyakQuoteAda.Name = "LBanyakQuoteAda"
        Me.LBanyakQuoteAda.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LBanyakQuoteAda.Size = New System.Drawing.Size(32, 23)
        Me.LBanyakQuoteAda.TabIndex = 61
        Me.LBanyakQuoteAda.Text = "10"
        '
        'LBanyakQuoteUse
        '
        '
        '
        '
        Me.LBanyakQuoteUse.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LBanyakQuoteUse.Location = New System.Drawing.Point(338, 194)
        Me.LBanyakQuoteUse.Name = "LBanyakQuoteUse"
        Me.LBanyakQuoteUse.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.LBanyakQuoteUse.Size = New System.Drawing.Size(32, 23)
        Me.LBanyakQuoteUse.TabIndex = 62
        Me.LBanyakQuoteUse.Text = "10"
        '
        'TPreview
        '
        Me.TPreview.Interval = 3000
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.ForeColor = System.Drawing.Color.Black
        Me.LabelX4.Location = New System.Drawing.Point(286, 410)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(20, 23)
        Me.LabelX4.Symbol = ""
        Me.LabelX4.SymbolSize = 15.0!
        Me.LabelX4.TabIndex = 83
        '
        'LabelX17
        '
        Me.LabelX17.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX17.ForeColor = System.Drawing.Color.Black
        Me.LabelX17.Location = New System.Drawing.Point(199, 410)
        Me.LabelX17.Name = "LabelX17"
        Me.LabelX17.Size = New System.Drawing.Size(14, 23)
        Me.LabelX17.Symbol = ""
        Me.LabelX17.SymbolSize = 15.0!
        Me.LabelX17.TabIndex = 81
        '
        'LTampungan
        '
        Me.LTampungan.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LTampungan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LTampungan.ForeColor = System.Drawing.Color.Black
        Me.LTampungan.Location = New System.Drawing.Point(13, 433)
        Me.LTampungan.Name = "LTampungan"
        Me.LTampungan.Size = New System.Drawing.Size(235, 23)
        Me.LTampungan.Symbol = ""
        Me.LTampungan.SymbolSize = 14.0!
        Me.LTampungan.TabIndex = 87
        Me.LTampungan.Text = " Simpan Tampungan Quote Terpakai Di:"
        '
        'GroupPanel1
        '
        Me.GroupPanel1.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.BProgramH)
        Me.GroupPanel1.Controls.Add(Me.CBHentikanQ)
        Me.GroupPanel1.Controls.Add(Me.CBAutoHide)
        Me.GroupPanel1.Controls.Add(Me.CBStartup)
        Me.GroupPanel1.Controls.Add(Me.CBJanganUlangiQuote)
        Me.GroupPanel1.DrawTitleBox = False
        Me.GroupPanel1.Location = New System.Drawing.Point(8, 436)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(372, 53)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderColor = System.Drawing.Color.White
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 90
        '
        'GroupPanel2
        '
        Me.GroupPanel2.BackColor = System.Drawing.Color.White
        Me.GroupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel2.Controls.Add(Me.BSimpan)
        Me.GroupPanel2.Controls.Add(Me.BBatal)
        Me.GroupPanel2.Controls.Add(Me.BKeluar)
        Me.GroupPanel2.Controls.Add(Me.RadialMenu1)
        Me.GroupPanel2.Controls.Add(Me.LTotalQot)
        Me.GroupPanel2.DrawTitleBox = False
        Me.GroupPanel2.Location = New System.Drawing.Point(4, 488)
        Me.GroupPanel2.Name = "GroupPanel2"
        Me.GroupPanel2.Size = New System.Drawing.Size(391, 38)
        '
        '
        '
        Me.GroupPanel2.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel2.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel2.Style.BackColorGradientAngle = 90
        Me.GroupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel2.Style.BorderColor = System.Drawing.Color.White
        Me.GroupPanel2.Style.CornerDiameter = 4
        Me.GroupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel2.TabIndex = 91
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyComputer
        '
        'TThreadA
        '
        Me.TThreadA.Interval = 8000
        '
        'Form_PengaturanUtama
        '
        Me.AcceptButton = Me.BSimpan
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(385, 524)
        Me.Controls.Add(Me.GroupPanel1)
        Me.Controls.Add(Me.GroupPanel2)
        Me.Controls.Add(Me.BTampunganJelajah)
        Me.Controls.Add(Me.TBPathTampungan)
        Me.Controls.Add(Me.LTampungan)
        Me.Controls.Add(Me.BTesSuara)
        Me.Controls.Add(Me.CBModeUpd)
        Me.Controls.Add(Me.LabelX5)
        Me.Controls.Add(Me.LabelX4)
        Me.Controls.Add(Me.IIOffHor)
        Me.Controls.Add(Me.LabelX17)
        Me.Controls.Add(Me.IIOffVer)
        Me.Controls.Add(Me.LabelX18)
        Me.Controls.Add(Me.LBanyakQuoteUse)
        Me.Controls.Add(Me.LBanyakQuoteAda)
        Me.Controls.Add(Me.CBFileSuara)
        Me.Controls.Add(Me.BEdit)
        Me.Controls.Add(Me.BHapusH)
        Me.Controls.Add(Me.CBAnimasiOpen)
        Me.Controls.Add(Me.LabelX13)
        Me.Controls.Add(Me.CBAnimasiClose)
        Me.Controls.Add(Me.LabelX12)
        Me.Controls.Add(Me.LKecAnimasi)
        Me.Controls.Add(Me.LabelX10)
        Me.Controls.Add(Me.LabelX11)
        Me.Controls.Add(Me.SKecAni)
        Me.Controls.Add(Me.CBSuara)
        Me.Controls.Add(Me.LabelX3)
        Me.Controls.Add(Me.LBQuotesUse)
        Me.Controls.Add(Me.LBQuotesAda)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.CBWaktuSetiap)
        Me.Controls.Add(Me.BCPembaruanQuoologi)
        Me.Controls.Add(Me.BCekQuotesBaru)
        Me.Controls.Add(Me.BHapusSemua)
        Me.Controls.Add(Me.BHapus)
        Me.Controls.Add(Me.BTambah)
        Me.Controls.Add(Me.BTambahSemua)
        Me.Controls.Add(Me.LabelX8)
        Me.Controls.Add(Me.LabelX7)
        Me.Controls.Add(Me.LSetiap)
        Me.Controls.Add(Me.LabelX6)
        Me.Controls.Add(Me.SSetiap)
        Me.Controls.Add(Me.LSelama)
        Me.Controls.Add(Me.LabelX2)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.SSelama)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButtonText = "BANTUAN"
        Me.MaximizeBox = False
        Me.Name = "Form_PengaturanUtama"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Pengaturan"
        CType(Me.IIOffHor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.IIOffVer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPanel1.ResumeLayout(False)
        Me.GroupPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSelama As DevComponents.DotNetBar.Controls.Slider
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LSelama As DevComponents.DotNetBar.LabelX
    Friend WithEvents LSetiap As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SSetiap As DevComponents.DotNetBar.Controls.Slider
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BTambahSemua As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BTambah As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BHapus As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BHapusSemua As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BCekQuotesBaru As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BCPembaruanQuoologi As DevComponents.DotNetBar.ButtonX
    Friend WithEvents CBWaktuSetiap As System.Windows.Forms.ComboBox
    Friend WithEvents BSimpan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BKeluar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents CBStartup As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LBQuotesAda As System.Windows.Forms.ListBox
    Friend WithEvents LBQuotesUse As System.Windows.Forms.ListBox
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents CBSuara As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CBAutoHide As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents RadialMenu1 As DevComponents.DotNetBar.RadialMenu
    Friend WithEvents RMIQOTMaker As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMITentang As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMIUpdate As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents LKecAnimasi As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SKecAni As DevComponents.DotNetBar.Controls.Slider
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents CBAnimasiClose As System.Windows.Forms.ComboBox
    Friend WithEvents CBAnimasiOpen As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents BHapusH As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents CBFileSuara As System.Windows.Forms.ComboBox
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents LTotalQot As DevComponents.DotNetBar.LabelX
    Friend WithEvents RMIKQuoologi As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMILike As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMIFacebook As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMIDislike As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMITwitter As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RadialMenuItem3 As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RadialMenuItem5 As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMIUpdateApp As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents RMIUpdateQot As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents Highlighter1 As DevComponents.DotNetBar.Validator.Highlighter
    Friend WithEvents TEvent As System.Windows.Forms.Timer
    Friend WithEvents CBJanganUlangiQuote As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents LBanyakQuoteUse As DevComponents.DotNetBar.LabelX
    Friend WithEvents LBanyakQuoteAda As DevComponents.DotNetBar.LabelX
    Friend WithEvents TPreview As System.Windows.Forms.Timer
    Friend WithEvents CBModeUpd As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents IIOffHor As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents IIOffVer As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents BTesSuara As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BTampunganJelajah As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TBPathTampungan As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LTampungan As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents GroupPanel2 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents BReset As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents BProgramH As DevComponents.DotNetBar.ButtonX
    Friend WithEvents CBHentikanQ As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TThreadA As System.Windows.Forms.Timer

End Class
