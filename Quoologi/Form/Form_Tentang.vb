﻿Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.Metro
Imports DevComponents.DotNetBar.Metro.Rendering.MetroRender
Imports UIQuoologi.Gambar

Public NotInheritable Class Form_Tentang
    Inherits MetroForm
    Public statuspartner As Byte
    Dim Client As New Net.WebClient

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles ButtonX2.Click
        Dim temp As Int16
        System.Diagnostics.Process.Start("https://www.facebook.com/Quoologi")
        temp = Me.Location.Y
        If Me.Location.Y > 0 And Me.Location.Y < 523 Then
            Do Until Me.Location.Y = 523
                If temp > 0 And temp < 390 Then
                    temp += 8
                ElseIf temp >= 390 And temp <= 450 Then
                    temp += 3
                ElseIf temp > 450 And temp <= 480 Then
                    temp += 2
                Else
                    temp += 1
                End If
                Me.Location = New Point(Me.Location.X, temp)
            Loop
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        Dim temp As Int16
        System.Diagnostics.Process.Start("https://twitter.com/Agateophobea")
        temp = Me.Location.Y
        If Me.Location.Y > 0 And Me.Location.Y < 523 Then
            Do Until Me.Location.Y = 523
                If temp > 0 And temp < 390 Then
                    temp += 8
                ElseIf temp >= 390 And temp <= 450 Then
                    temp += 3
                ElseIf temp > 450 And temp <= 480 Then
                    temp += 2
                Else
                    temp += 1
                End If
                Me.Location = New Point(Me.Location.X, temp)
            Loop
        End If
    End Sub

    Private Sub ButtonX3_Click(sender As Object, e As EventArgs) Handles ButtonX3.Click
        Me.Close()
    End Sub

    Private Sub Tentang_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Form_Handle.status_2 = 1 And Form_Partner.status = 0 Then
            Form_PengaturanUtama.Show()
        End If
        Dispose()
    End Sub

    Private Sub ButtonX4_Click(sender As Object, e As EventArgs) Handles ButtonX4.Click
        Form_Partner.Show()
        Form_Partner.status = 1
        statuspartner = 1
        Me.Close()
    End Sub

    Private Function AmbilQuote() As String
        Try
            Dim last As Int16
            Dim d As Int16
            d = DaftarQuote.Count
            last = acak(d)
            Return DaftarQuote(last) + " - " + DaftarAlamatQuote(last)
        Catch ex As Exception
            Return "Jadilah yang lebih baik. :)"
        End Try
    End Function

    Private Sub Tentang_Load(sender As Object, e As EventArgs) Handles Me.Load
        PictureBox1.Image = AmbilGambar("logo_quoologi")
        Dim sejak As String
        sejak = TerinstallPada.Replace("-", " - ")
        Label1.Text = "Versi: " + My.Application.Info.Version.ToString
        LabelX3.Text = " Anda sudah memakai Quoologi sejak:  " + sejak
        LabelX1.Text = My.Application.Info.Copyright.ToString + ". Semua hak dilindungi undang undang."
        LabelX2.TextAlignment = StringAlignment.Near
        LabelX2.TextLineAlignment = StringAlignment.Near
        LabelX2.Text = AmbilQuote()
        Timer1.Enabled = True
    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click
        Try
            Process.Start(AlamatInstall + "\info_update.txt")
        Catch ex As System.IO.FileNotFoundException
            MessageBoxEx.Show("Tidak dapat menemukan berkas ""info_update.txt"", berkas mungkin tidak sengaja terhapus. (kesalahan: 120201)", "Galat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Form_Handle.status_2 = 0
        Me.Close()
    End Sub

    Private Sub ReflectionLabel1_Click(sender As Object, e As EventArgs) Handles ReflectionLabel1.Click
        Dim temp As Int16
        System.Diagnostics.Process.Start("https://aziz-nur.id")
        temp = Me.Location.Y
        If Me.Location.Y > 0 And Me.Location.Y < 523 Then
            Do Until Me.Location.Y = 523
                If temp > 0 And temp < 390 Then
                    temp += 8
                ElseIf temp >= 390 And temp <= 450 Then
                    temp += 3
                ElseIf temp > 450 And temp <= 480 Then
                    temp += 2
                Else
                    temp += 1
                End If
                Me.Location = New Point(Me.Location.X, temp)
            Loop
        End If
    End Sub

    Private Sub ButtonX5_Click(sender As Object, e As EventArgs) Handles ButtonX5.Click
        Dim temp As Int16
        System.Diagnostics.Process.Start("http://quoologi.blogspot.com")
        temp = Me.Location.Y
        If Me.Location.Y > 0 And Me.Location.Y < 523 Then
            Do Until Me.Location.Y = 523
                If temp > 0 And temp < 390 Then
                    temp += 8
                ElseIf temp >= 390 And temp <= 450 Then
                    temp += 3
                ElseIf temp > 450 And temp <= 480 Then
                    temp += 2
                Else
                    temp += 1
                End If
                Me.Location = New Point(Me.Location.X, temp)
            Loop
        End If
    End Sub

    'Private Sub LabelX4_Click(sender As Object, e As EventArgs)
    '    LabelX4.ForeColor = Color.Blue
    '    LabelX4.Font = New Font("Candara", 10.25, FontStyle.Underline)
    'End Sub

    'Private Sub LabelX4_Leave(sender As Object, e As EventArgs)
    '    LabelX4.ForeColor = Color.Black
    '    LabelX4.Font = New Font("Candara", 10.25, FontStyle.Regular)
    'End Sub

    Private Sub LabelX4_Click_1(sender As Object, e As EventArgs)
        System.Diagnostics.Process.Start("https://www.facebook.com/dilittlesnoopy")
    End Sub

    Private Sub ReflectionLabel1_MouseEnter(sender As Object, e As EventArgs) Handles ReflectionLabel1.MouseEnter
        ReflectionLabel1.Text = "<b>Quoologi</b>"
        LabelX5.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold)
    End Sub

    Private Sub ReflectionLabel1_MouseLeave(sender As Object, e As EventArgs) Handles ReflectionLabel1.MouseLeave
        ReflectionLabel1.Text = "Quoologi"
        LabelX5.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Regular)
    End Sub

End Class
