﻿Imports DevComponents.DotNetBar
Imports DevComponents.DotNetBar.OfficeForm

Public Class Form_PengaturanGaya
    Inherits Metro.MetroForm
    Public status As Byte = 0
    Public tinggiquotes As Integer
    Public tinggijendela As Integer
    Public lebarquotes As Integer
    Public lebarjendela As Integer
    Public tinggiquotes2 As Integer
    Public tinggijendela2 As Integer
    Dim lebarquotes2 As Integer
    Dim lebarjendela2 As Integer
    Public semtj As Byte
    Dim semtq As Byte
    Dim semlq As Byte
    Dim semlj As Byte
    Public seml As Byte = 0
    Dim lj As Integer
    Dim lq As Integer
    Dim trans As Byte = 0
    Dim pertama As Byte = 0
    Dim ukuran As Boolean = False

    Dim gh As String
    Dim uh As Integer
    Dim utj As Integer
    Dim ulj As Integer
    Dim whq As System.Drawing.Color
    Dim wbq As System.Drawing.Color
    Dim whj As System.Drawing.Color
    Dim wbj As System.Drawing.Color
    Dim utq As Integer
    Dim ulq As Integer
    Dim k As Boolean
    Dim t As Integer
    Dim b As Boolean
    Dim dariteks As Boolean
    Dim indeks As Byte = 0

    Dim first As Byte = 1

    Dim statussimpan As Byte = 1

    Sub last()
        gh = CBHQuotes.Text
        uh = IIUkuranHQuotes.Value
        utj = IITinggiJendela.Value
        ulj = IILebarJendela.Value
        whq = CPBHQuotes.SelectedColor
        wbq = CPBBQuotes.SelectedColor
        whj = CPBHJendela.SelectedColor
        wbj = CPBBJendela.SelectedColor
        utq = IITinggiQuotes.Value
        ulq = IILebarQuotes.Value
        t = STransparasi.Value
        k = BKunci.Checked
        b = CBTTombol.Checked
    End Sub

    Sub setlast()
        CBHQuotes.Text = gh
        Application.DoEvents()
        IIUkuranHQuotes.Value = uh
        Application.DoEvents()
        IITinggiJendela.Value = utj
        Application.DoEvents()
        IILebarJendela.Value = ulj
        Application.DoEvents()
        CPBHQuotes.SelectedColor = whq
        Application.DoEvents()
        CPBBQuotes.SelectedColor = wbq
        Application.DoEvents()
        IITinggiJendela.Value = utj
        Application.DoEvents()
        IILebarJendela.Value = ulj
        Application.DoEvents()
        CPBBJendela.SelectedColor = wbj
        Application.DoEvents()
        CPBHJendela.SelectedColor = whj
        Application.DoEvents()
        STransparasi.Value = t
        Application.DoEvents()
        BKunci.Checked = k
        Application.DoEvents()
        CBTTombol.Checked = b
        Application.DoEvents()
        simpan()
    End Sub

    Sub baca_pengaturan()
        CBHQuotes.Text = GayaHuruf
        IIUkuranHQuotes.Value = UkuranHuruf
        STransparasi.Value = Transparasi * 100
        IITinggiJendela.Value = OffTinggiJendela
        IILebarJendela.Value = OffLebarJendela
        CPBHQuotes.SelectedColor = WarnaHurufQuote
        CPBBQuotes.SelectedColor = WarnaBackQuote
        IITinggiQuotes.Value = OffTinggiJendela
        IILebarQuotes.Value = OffLebarJendela
        CPBBJendela.SelectedColor = WarnaBackJendela
        CPBHJendela.SelectedColor = WarnaHurufJendela
        BKunci.Checked = Kunci
        TBSelamatDatang.Text = TeksMulai
        CBTTombol.Checked = SelaluTampilTombol
    End Sub

    Private Sub setwarna(ByVal wbackj As System.Drawing.Color, ByVal wbackq As System.Drawing.Color, ByVal whurufj As System.Drawing.Color, ByVal whurufq As System.Drawing.Color)
        CPBHQuotes.SelectedColor = whurufq
        CPBBQuotes.SelectedColor = wbackq
        CPBHJendela.SelectedColor = whurufj
        CPBBJendela.SelectedColor = wbackj
    End Sub

    Public Sub simpan()
        GayaHuruf = CBHQuotes.Text
        Transparasi = STransparasi.Value
        UkuranHuruf = IIUkuranHQuotes.Value
        OffTinggiJendela = IITinggiJendela.Value
        OffsetVer = IITinggiJendela.Value
        OffLebarJendela = IILebarJendela.Value
        OffsetHor = IILebarJendela.Value
        OffTinggiQuote = IITinggiQuotes.Value
        OffLebarQuote = IILebarQuotes.Value
        WarnaBackJendela = CPBBJendela.SelectedColor
        WarnaHurufJendela = CPBHJendela.SelectedColor
        WarnaBackQuote = CPBBQuotes.SelectedColor
        WarnaHurufQuote = CPBHQuotes.SelectedColor
        Kunci = BKunci.Checked
        TeksMulai = TBSelamatDatang.Text
        SelaluTampilTombol = CBTTombol.Checked

        SimpanPengaturan()

        If dariteks = True Then
            SimpanPengaturan(BagianPenyimpanan.TeksWelcome)
            dariteks = False
        End If
        
        If ukuran = True Then
            Quoologi.hidequoologi()
            Form_Quote.Label2.Visible = False
            read_q()
            Quoologi.showquoologi()
            ukuran = False
        End If
    End Sub

    Private Sub PengaturanQuoologi_Load(sender As Object, e As EventArgs) Handles Me.Load
        IILebarJendela.MaxValue = My.Computer.Screen.WorkingArea.Width - 285
        IILebarQuotes.MaxValue = My.Computer.Screen.WorkingArea.Width - 285
        baca_pengaturan()
        Dim Fonts As New System.Drawing.Text.InstalledFontCollection()
        For Each Family As FontFamily In Fonts.Families
            CBHQuotes.Items.Add(Family.Name)
            CBHQuotes.AutoCompleteCustomSource.Add(Family.Name)
        Next
        last()
    End Sub

    Private Sub ButtonX1_Click_2(sender As Object, e As EventArgs) Handles BKunci.Click
        If BKunci.Checked = True Then
            BKunci.Checked = False
            BKunci.Symbol = ""
        Else
            BKunci.Checked = True
            BKunci.Symbol = ""
        End If
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        last()
        simpan()
        ToastNotification.Show(Me, "Pengaturan telah tersimpan", eToastPosition.BottomCenter)
        statussimpan = 1
    End Sub

    Private Sub IITinggiJendela_Click(sender As Object, e As EventArgs) Handles IITinggiJendela.Click
        first = 0
    End Sub

    Private Sub IITinggi_ValueChanged(sender As Object, e As EventArgs) Handles IITinggiJendela.ValueChanged
        If BKunci.Checked = True Then
            IITinggiQuotes.Value = IITinggiJendela.Value
        End If

        tinggijendela = IITinggiJendela.Value
        If first = 0 Then
            statussimpan = 0
            ukuran = True
        End If
    End Sub

    Private Sub IILebarJendela_Click(sender As Object, e As EventArgs) Handles IILebarJendela.Click
        first = 0
    End Sub

    Private Sub IILebarJendela_ValueChanged(sender As Object, e As EventArgs) Handles IILebarJendela.ValueChanged
        If BKunci.Checked = True Then
            IILebarQuotes.Value = IILebarJendela.Value
        End If

        lebarjendela = IILebarJendela.Value
        If first = 0 Then
            statussimpan = 0
            ukuran = True
        End If
    End Sub

    Private Sub IITinggiQuotes_Click(sender As Object, e As EventArgs) Handles IITinggiQuotes.Click
        first = 0
    End Sub

    Private Sub IITinggiQuotes_ValueChanged(sender As Object, e As EventArgs) Handles IITinggiQuotes.ValueChanged
        If BKunci.Checked = True Then
            IITinggiJendela.Value = IITinggiQuotes.Value
        End If

        tinggiquotes = IITinggiQuotes.Value
        If first = 0 Then
            statussimpan = 0
            ukuran = True
        End If
    End Sub

    Private Sub IILebarQuotes_Click(sender As Object, e As EventArgs) Handles IILebarQuotes.Click
        first = 0
    End Sub

    Private Sub IILebarQuotes_ValueChanged(sender As Object, e As EventArgs) Handles IILebarQuotes.ValueChanged
        If BKunci.Checked = True Then
            IILebarJendela.Value = IILebarQuotes.Value
        End If

        lebarquotes = IILebarQuotes.Value
        If first = 0 Then
            statussimpan = 0
            ukuran = True
        End If
    End Sub

    Private Sub CBHQuotes_Click(sender As Object, e As EventArgs) Handles CBHQuotes.Click
        first = 0
    End Sub

    Private Sub CBHQuotes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBHQuotes.SelectedIndexChanged
        If QuoteMuncul = False Then
            AnimasiOpen(Form_Quote.Handle)
            Form_Quote.Show()
            Form_Quote.Label2.Visible = False
            QuoteMuncul = True
        End If

        Try
            Form_Quote.TBQuotes.Font = New Font(CBHQuotes.Text, IIUkuranHQuotes.Value)
        Catch ex As Exception
            IIUkuranHQuotes.Value = 12
        End Try
        If first = 0 Then
            statussimpan = 0
        End If

        ToastNotification.Show(Me, "Mungkin Anda harus menyesuaikan ukuran jendela" + Environment.NewLine + "dan ukuran huruf setelah merubah gaya huruf Quote", 5000, eToastPosition.BottomCenter)
    End Sub

    Private Sub IIUkuranHQuotes_Click(sender As Object, e As EventArgs) Handles IIUkuranHQuotes.Click
        first = 0
    End Sub

    Private Sub IIUkuranHQuotes_ValueChanged(sender As Object, e As EventArgs) Handles IIUkuranHQuotes.ValueChanged
        If QuoteMuncul = False Then
            AnimasiOpen(Form_Quote.Handle)
            Form_Quote.Show()
            Form_Quote.Label2.Visible = False
            QuoteMuncul = True
        End If

        Try
            Form_Quote.TBQuotes.Font = New Font(CBHQuotes.Text, IIUkuranHQuotes.Value)
        Catch ex As Exception
            IIUkuranHQuotes.Value = 12
        End Try
        If first = 0 Then
            statussimpan = 0
        End If
    End Sub

    Private Sub CPBHQuotes_Click(sender As Object, e As EventArgs) Handles CPBHQuotes.GotFocus
        first = 0
    End Sub

    Private Sub CPBHQuotes_SelectedColorChanged(sender As Object, e As EventArgs) Handles CPBHQuotes.SelectedColorChanged
        If QuoteMuncul = False Then
            AnimasiOpen(Form_Quote.Handle)
            Form_Quote.Show()
            Form_Quote.Label2.Visible = False
            QuoteMuncul = True
        End If

        Form_Quote.TBQuotes.ForeColor = CPBHQuotes.SelectedColor
        If first = 0 Then
            statussimpan = 0
        End If
    End Sub

    Private Sub CPBBQuotes_Click(sender As Object, e As EventArgs) Handles CPBBQuotes.GotFocus
        first = 0
    End Sub

    Private Sub CPBBQuotes_SelectedColorChanged(sender As Object, e As EventArgs) Handles CPBBQuotes.SelectedColorChanged
        If QuoteMuncul = False Then
            AnimasiOpen(Form_Quote.Handle)
            Form_Quote.Show()
            Form_Quote.Label2.Visible = False
            QuoteMuncul = True
        End If

        Form_Quote.TBQuotes.BackColor = CPBBQuotes.SelectedColor
        If first = 0 Then
            statussimpan = 0
        End If
    End Sub

    Private Sub CBTTombol_CheckedChanged1(sender As Object, e As EventArgs) Handles CBTTombol.Click
        first = 0
    End Sub

    Private Sub CBTTombol_CheckedChanged(sender As Object, e As EventArgs) Handles CBTTombol.CheckedChanged
        If QuoteMuncul = False Then
            AnimasiOpen(Form_Quote.Handle)
            Form_Quote.Show()
            Form_Quote.Label2.Visible = False
            QuoteMuncul = True
        End If

        If CBTTombol.Checked = True Then
            Form_Quote.TampilTombol()
        Else
            Form_Quote.HilangTombol()
        End If

        If first = 0 Then
            statussimpan = 0
        End If
    End Sub

    Private Sub CPBHJendela_Click(sender As Object, e As EventArgs) Handles CPBHJendela.GotFocus
        first = 0
    End Sub

    Private Sub CPBHJendela_SelectedColorChanged(sender As Object, e As EventArgs) Handles CPBHJendela.SelectedColorChanged
        If QuoteMuncul = False Then
            AnimasiOpen(Form_Quote.Handle)
            Form_Quote.Show()
            Form_Quote.Label2.Visible = False
            QuoteMuncul = True
        End If

        Form_Quote.Label1.ForeColor = CPBHJendela.SelectedColor
        Form_Quote.Label2.ForeColor = CPBHJendela.SelectedColor
        Form_Quote.BHide.TextColor = CPBHJendela.SelectedColor
        Form_Quote.BCari.TextColor = CPBHJendela.SelectedColor
        Form_Quote.BSalin.TextColor = CPBHJendela.SelectedColor
        Form_Quote.FB.TextColor = CPBHJendela.SelectedColor
        Form_Quote.BFav.TextColor = CPBHJendela.SelectedColor
        If first = 0 Then
            statussimpan = 0
        End If
    End Sub

    Private Sub CPBBJendela_Click(sender As Object, e As EventArgs) Handles CPBBJendela.GotFocus
        first = 0
    End Sub

    Private Sub CPBBJendela_SelectedColorChanged(sender As Object, e As EventArgs) Handles CPBBJendela.SelectedColorChanged
        If QuoteMuncul = False Then
            AnimasiOpen(Form_Quote.Handle)
            Form_Quote.Show()
            Form_Quote.Label2.Visible = False
            QuoteMuncul = True
        End If

        Form_Quote.BackColor = CPBBJendela.SelectedColor
        Form_Quote.BHide.BackColor = CPBBJendela.SelectedColor
        Form_Quote.BCari.BackColor = CPBBJendela.SelectedColor
        Form_Quote.BSalin.BackColor = CPBBJendela.SelectedColor
        Form_Quote.FB.BackColor = CPBBJendela.SelectedColor
        Form_Quote.BFav.BackColor = CPBBJendela.SelectedColor
        If first = 0 Then
            statussimpan = 0
        End If
    End Sub

    Private Sub QOTMaker_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        trans = 0
        If status = 1 Then
            Form_PengaturanUtama.Show()
        End If
        Dispose()
        Form_Handle.TSetiap.Enabled = True
        Form_Handle.TSelama.Enabled = True
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        If statussimpan = 0 Then
            setlast()
        End If
        Me.Close()
    End Sub

    Private Sub index(ByVal index As Byte)
        If index = 1 Then
            ButtonX1.Enabled = False
            setwarna(Color.FromArgb(255, 36, 64, 97), Color.FromArgb(255, 23, 54, 93), Color.FromArgb(255, 84, 141, 212), Color.FromArgb(255, 149, 179, 215))
        ElseIf index = 2 Then
            ButtonX1.Enabled = True
            setwarna(Color.FromArgb(255, 71, 132, 255), Color.FromArgb(255, 102, 153, 255), Color.FromArgb(255, 54, 96, 146), Color.FromArgb(255, 23, 54, 93))
        ElseIf index = 3 Then
            setwarna(Color.FromArgb(255, 51, 153, 102), Color.FromArgb(255, 153, 204, 153), Color.FromArgb(255, 0, 102, 51), Color.FromArgb(255, 0, 102, 51))
        ElseIf index = 4 Then
            setwarna(Color.FromArgb(255, 255, 102, 0), Color.FromArgb(255, 204, 51, 0), Color.FromArgb(255, 255, 153, 102), Color.FromArgb(255, 255, 153, 102))
        ElseIf index = 5 Then
            setwarna(Color.FromArgb(255, 102, 0, 102), Color.FromArgb(255, 153, 0, 153), Color.FromArgb(255, 102, 51, 153), Color.FromArgb(255, 204, 153, 255))
        ElseIf index = 6 Then
            setwarna(Color.FromArgb(255, 51, 204, 51), Color.FromArgb(255, 0, 220, 0), Color.FromArgb(255, 0, 153, 51), Color.FromArgb(255, 0, 102, 51))
        ElseIf index = 7 Then
            setwarna(Color.FromArgb(255, 204, 204, 205), Color.FromArgb(255, 230, 230, 230), Color.FromArgb(255, 153, 153, 153), Color.FromArgb(255, 26, 26, 26))
        ElseIf index = 8 Then
            setwarna(Color.FromArgb(255, 153, 0, 0), Color.FromArgb(255, 124, 0, 0), Color.FromArgb(255, 204, 51, 0), Color.FromArgb(255, 255, 102, 0))
        ElseIf index = 9 Then
            setwarna(Color.FromArgb(255, 63, 49, 81), Color.FromArgb(255, 95, 73, 122), Color.FromArgb(255, 57, 90, 134), Color.FromArgb(255, 204, 193, 217))
        ElseIf index = 10 Then
            setwarna(Color.FromArgb(255, 151, 72, 6), Color.FromArgb(255, 227, 108, 9), Color.FromArgb(255, 227, 108, 9), Color.FromArgb(255, 251, 213, 181))
        ElseIf index = 11 Then
            setwarna(Color.FromArgb(255, 32, 88, 103), Color.FromArgb(255, 49, 133, 155), Color.FromArgb(255, 49, 133, 155), Color.FromArgb(255, 183, 221, 232))
        ElseIf index = 12 Then
            setwarna(Color.FromArgb(255, 79, 97, 40), Color.FromArgb(255, 118, 146, 60), Color.FromArgb(255, 118, 146, 60), Color.FromArgb(255, 215, 227, 188))
        ElseIf index = 13 Then
            setwarna(Color.FromArgb(255, 99, 36, 35), Color.FromArgb(255, 149, 55, 52), Color.FromArgb(255, 181, 76, 36), Color.FromArgb(255, 229, 185, 183))
        ElseIf index = 14 Then
            setwarna(Color.FromArgb(255, 36, 64, 97), Color.FromArgb(255, 54, 96, 146), Color.FromArgb(255, 54, 96, 146), Color.FromArgb(255, 184, 204, 228))
        ElseIf index = 15 Then
            setwarna(Color.FromArgb(255, 15, 36, 62), Color.FromArgb(255, 23, 54, 93), Color.FromArgb(255, 84, 141, 212), Color.FromArgb(255, 141, 179, 226))
        ElseIf index = 16 Then
            setwarna(Color.FromArgb(255, 29, 27, 16), Color.FromArgb(255, 73, 68, 41), Color.FromArgb(255, 73, 68, 41), Color.FromArgb(255, 221, 217, 195))
        ElseIf index = 17 Then
            setwarna(Color.FromArgb(255, 12, 12, 12), Color.FromArgb(255, 38, 38, 38), Color.FromArgb(255, 63, 63, 63), Color.FromArgb(255, 238, 236, 225))
        ElseIf index = 18 Then
            setwarna(Color.FromArgb(255, 127, 127, 127), Color.FromArgb(255, 165, 165, 165), Color.FromArgb(255, 165, 165, 165), Color.FromArgb(255, 38, 38, 38))
        ElseIf index = 19 Then
            setwarna(Color.FromArgb(255, 0, 112, 192), Color.FromArgb(255, 0, 176, 80), Color.FromArgb(255, 216, 216, 216), Color.FromArgb(255, 255, 255, 255))
        ElseIf index = 20 Then
            setwarna(Color.FromArgb(255, 112, 48, 160), Color.FromArgb(255, 0, 32, 96), Color.FromArgb(255, 216, 216, 216), Color.FromArgb(255, 255, 255, 255))
        ElseIf index = 21 Then
            setwarna(Color.FromArgb(255, 0, 176, 80), Color.FromArgb(255, 146, 208, 80), Color.FromArgb(255, 79, 97, 40), Color.FromArgb(255, 79, 97, 40))
        ElseIf index = 22 Then
            setwarna(Color.FromArgb(255, 255, 192, 0), Color.FromArgb(255, 255, 0, 0), Color.FromArgb(255, 227, 108, 9), Color.FromArgb(255, 255, 255, 0))
        ElseIf index = 23 Then
            setwarna(Color.FromArgb(255, 255, 0, 0), Color.FromArgb(255, 192, 0, 0), Color.FromArgb(255, 227, 108, 9), Color.FromArgb(255, 242, 220, 219))
        ElseIf index = 24 Then
            setwarna(Color.FromArgb(255, 63, 49, 81), Color.FromArgb(255, 36, 64, 97), Color.FromArgb(255, 54, 96, 146), Color.FromArgb(255, 198, 217, 240))
        ElseIf index = 25 Then
            setwarna(Color.FromArgb(255, 255, 192, 0), Color.FromArgb(255, 112, 48, 160), Color.FromArgb(255, 227, 108, 9), Color.FromArgb(255, 229, 224, 236))
        ElseIf index = 26 Then
            setwarna(Color.FromArgb(255, 192, 0, 0), Color.FromArgb(255, 0, 112, 192), Color.FromArgb(255, 227, 108, 9), Color.FromArgb(255, 229, 224, 236))
        ElseIf index = 27 Then
            setwarna(Color.FromArgb(255, 73, 68, 41), Color.FromArgb(255, 79, 97, 40), Color.FromArgb(255, 147, 137, 83), Color.FromArgb(255, 235, 241, 221))
        ElseIf index = 28 Then
            setwarna(Color.FromArgb(255, 54, 96, 146), Color.FromArgb(255, 0, 112, 192), Color.FromArgb(255, 84, 141, 212), Color.FromArgb(255, 198, 217, 240))
        ElseIf index = 29 Then
            ButtonX2.Enabled = True
            setwarna(Color.FromArgb(255, 63, 49, 81), Color.FromArgb(255, 99, 36, 35), Color.FromArgb(255, 84, 141, 212), Color.FromArgb(255, 198, 217, 240))
        ElseIf index = 30 Then
            ButtonX2.Enabled = False
            setwarna(Color.FromArgb(255, 36, 64, 97), Color.FromArgb(255, 79, 97, 40), Color.FromArgb(255, 54, 96, 146), Color.FromArgb(255, 198, 217, 240))
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        first = 0
        indeks -= 1
        index(indeks)
    End Sub

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles ButtonX2.Click
        first = 0
        indeks += 1
        index(indeks)
    End Sub

    Private Sub ButtonX3_Click(sender As Object, e As EventArgs) Handles ButtonX3.Click
        TBSelamatDatang.Text = "Quoologi, siap memberikan semua informasi, motivasi, & hiburan sesuai keinginan Anda." + Environment.NewLine + Environment.NewLine + "Jadilah yang lebih baik. :)"
    End Sub

    Private Sub TBSelamatDatang_KeyDown(sender As Object, e As KeyEventArgs) Handles TBSelamatDatang.KeyDown
        If e.Modifiers.ToString + " + " + e.KeyCode.ToString = "Control + A" Then
            e.Handled = True
            TBSelamatDatang.SelectAll()
        End If
    End Sub

    Private Sub STransparasi_Focus(sender As Object, e As EventArgs) Handles STransparasi.GotFocus
        first = 0
    End Sub

    Private Sub STransparasi_ValueChanged(sender As Object, e As EventArgs) Handles STransparasi.ValueChanged
        LTransparasi.Text = STransparasi.Value.ToString
        If trans = 1 Then
            If QuoteMuncul = False Then
                Quoologi.ShowQuoologi()
                Form_Quote.Label2.Visible = False
            End If
            Form_Quote.Opacity = LTransparasi.Text / 100
            If pertama = 0 Then
                If AnimasiBuka = "Menyatu" Or AnimasiTutup = "Menyatu" Then
                    ToastNotification.Show(Me, "Transparasi dibawah 100% tidak berfungsi" + Environment.NewLine + "jika memakai animasi menyatu", Nothing, 5000, eToastGlowColor.Orange, eToastPosition.BottomCenter)
                    pertama = 1
                End If
            End If
        End If

        If first = 0 Then
            statussimpan = 0
        End If
    End Sub

    Private Sub PengaturanQuoologi_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        LTransparasi.Text = STransparasi.Value.ToString
        Form_Handle.TSetiap.Enabled = False
        Form_Handle.TSelama.Enabled = False
        trans = 1

        If FirstRun = True And Form_Handle.pengaturangayasudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berisi properti² pengaturan gaya jendela Quote" + Environment.NewLine + "Anda bisa merubah pengaturan penampilan jendela quote disini", 7000, eToastPosition.MiddleCenter)
            Form_Handle.pengaturangayasudah = True
        End If
    End Sub

    Private Sub TBSelamatDatang_TextChanged(sender As Object, e As EventArgs) Handles TBSelamatDatang.TextChanged
        dariteks = True
    End Sub
End Class