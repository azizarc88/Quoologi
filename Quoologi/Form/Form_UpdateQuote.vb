﻿Imports DevComponents.DotNetBar
Imports System.IO

Public Class Form_UpdateQuote
    Inherits Metro.MetroForm
    Public path As New IO.DirectoryInfo(AlamatDokumen + "\Quotes")
    Dim berkas As IO.FileInfo
    Dim folder As IO.FileInfo() = Path.GetFiles("*.qot")
    Public status As Byte = 0

    Sub Write()
        sem_upd_q.Clear()
        Dim max As Byte = LBQuotesBaru.Items.Count
        If max - LBQuotesBaru.SelectedItems.Count = 0 Then
            AdaUpQuote = 2
            SimpanPengaturan()
            setstatus()
        Else
            AdaUpQuote = 1
            SimpanPengaturan()
            setstatus()
        End If

        Dim a As Integer = 0
        If My.Computer.FileSystem.FileExists(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\daftar_file.txt") = True Then
            My.Computer.FileSystem.DeleteFile(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\daftar_file.txt", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        End If

        Using SW As New IO.StreamWriter(My.Computer.FileSystem.SpecialDirectories.Temp.ToString & "\daftar_file.txt", True)
            SW.WriteLine("[daftar_file]")
            For Each baris As String In Form_Handle.qotbaru
                If LBQuotesBaru.SelectedItems.Contains(baris.Replace("quotes: #", "")) Then

                    SW.WriteLine(baris)
                    SW.WriteLine(Form_Handle.linkqotbaru.Item(a))
                End If
                a += 1
            Next
            SW.Close()
        End Using
    End Sub

    Private Sub update_quotes_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If status = 1 Then
            Form_PengaturanUtama.Show()
        End If
        Me.Dispose()
    End Sub

    Private Sub update_quotes_Load(sender As Object, e As EventArgs) Handles Me.Load
        For Each Me.berkas In folder
            LBQuotesLama.Items.Add(berkas.Name)
        Next

        For Each baris As String In Form_Handle.qotbaru
            If Form_Handle.qotlama.Contains(baris.Replace("quotes: #", "")) = False Then
                LBQuotesBaru.Items.Add(baris.Replace("quotes: #", ""))
            End If
        Next

        If LBQuotesBaru.Items.Count = 0 Then
            AdaUpQuote = 2
            SimpanPengaturan()
        Else
            AdaUpQuote = 1
            SimpanPengaturan()
        End If
        setstatus()
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BPerbarui.Click
        Form_Handle.bug = True
        Dim a As Integer
        If LBQuotesBaru.SelectedItems.Count = 0 Then
            For a = 0 To LBQuotesBaru.Items.Count - 1
                LBQuotesBaru.SetSelected(a, True)
            Next
        End If
        Write()
        status = 0
        Try
            sem_upd_q.Clear()
            Me.Close()
            Process.Start(AlamatInstall + "\update_quoologi_q.exe")
            Exit Sub
        Catch ex As System.ComponentModel.Win32Exception
            If ex.Message = "The system cannot find the file specified" Then
                If MsgBox("Updater tidak ditemukan, perbaiki ?", MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, "Galat") = MsgBoxResult.Yes Then
                    PerbaikiQuoologi()
                Else
                    Form_PengaturanUtama.Show()
                End If
            End If
        End Try
        Me.Close()
    End Sub

    Private Sub Form_UpdateQuote_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If FirstRun = True And Form_Handle.updatequotesudah = False Then
            SuperTooltip1.HoverDelayMultiplier = 1 / 5
            ToastNotification.Show(Me, "Selamat datang pada bagian " + Me.Text.ToString + Environment.NewLine + Environment.NewLine + "Jendela ini berfungsi untuk memilih quote baru" + Environment.NewLine + "yang ingin diunduh oleh Anda", 7000, eToastPosition.MiddleCenter)
            Form_Handle.updatequotesudah = True
        End If
    End Sub
End Class