﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_UpdateStatus
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.TBQuote = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TBTag = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupPanel4 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LQuoologi = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupPanel5 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.LPromosikan = New System.Windows.Forms.Label()
        Me.LKomentar = New System.Windows.Forms.Label()
        Me.LBagikan = New System.Windows.Forms.Label()
        Me.LSuka = New System.Windows.Forms.Label()
        Me.LTitik = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.LNamaAkun = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.LStatus = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TBKomentar = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LPratinjau = New DevComponents.DotNetBar.LabelX()
        Me.LKomen = New DevComponents.DotNetBar.LabelX()
        Me.BKirim = New DevComponents.DotNetBar.ButtonX()
        Me.CBOtoTutup = New DevComponents.DotNetBar.CheckBoxItem()
        Me.BPengaturan = New DevComponents.DotNetBar.ButtonX()
        Me.BBatal = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.LIndikator = New DevComponents.DotNetBar.Controls.ReflectionLabel()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer4 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupPanel4.SuspendLayout()
        Me.GroupPanel5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.ForeColor = System.Drawing.Color.Black
        Me.LabelX1.Location = New System.Drawing.Point(9, 8)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Quote:"
        '
        'TBQuote
        '
        Me.TBQuote.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBQuote.Border.Class = "TextBoxBorder"
        Me.TBQuote.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBQuote.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.TBQuote.ForeColor = System.Drawing.Color.Black
        Me.TBQuote.Location = New System.Drawing.Point(6, 34)
        Me.TBQuote.Multiline = True
        Me.TBQuote.Name = "TBQuote"
        Me.TBQuote.Size = New System.Drawing.Size(512, 22)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBQuote, New DevComponents.DotNetBar.SuperTooltipInfo("Quote", "", "Berisi quote yang akan dijadikan sebagai status.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(143, 61)))
        Me.TBQuote.TabIndex = 99
        Me.TBQuote.Text = "1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'TBTag
        '
        Me.TBTag.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBTag.Border.Class = "TextBoxBorder"
        Me.TBTag.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBTag.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.TBTag.ForeColor = System.Drawing.Color.Black
        Me.TBTag.Location = New System.Drawing.Point(6, 88)
        Me.TBTag.Multiline = True
        Me.TBTag.Name = "TBTag"
        Me.TBTag.ReadOnly = True
        Me.TBTag.Size = New System.Drawing.Size(512, 22)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBTag, New DevComponents.DotNetBar.SuperTooltipInfo("Tag Asal Quote", "", "Berisi asal quote yang sudah disempurnakan sesuai dengan pengaturan ""Tag Asal Quo" & _
            "te"" pada ""Pengaturan Status (FB)"".", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(198, 84)))
        Me.TBTag.TabIndex = 3
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ForeColor = System.Drawing.Color.Black
        Me.LabelX2.Location = New System.Drawing.Point(9, 62)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(80, 23)
        Me.LabelX2.TabIndex = 2
        Me.LabelX2.Text = "Tag Asal Quote:"
        '
        'GroupPanel4
        '
        Me.GroupPanel4.BackColor = System.Drawing.Color.White
        Me.GroupPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel4.Controls.Add(Me.LQuoologi)
        Me.GroupPanel4.Controls.Add(Me.Label1)
        Me.GroupPanel4.Controls.Add(Me.GroupPanel5)
        Me.GroupPanel4.Controls.Add(Me.Label2)
        Me.GroupPanel4.Controls.Add(Me.PictureBox1)
        Me.GroupPanel4.Controls.Add(Me.LNamaAkun)
        Me.GroupPanel4.Controls.Add(Me.PictureBox2)
        Me.GroupPanel4.Controls.Add(Me.LStatus)
        Me.GroupPanel4.DrawTitleBox = False
        Me.GroupPanel4.Location = New System.Drawing.Point(6, 142)
        Me.GroupPanel4.Name = "GroupPanel4"
        Me.GroupPanel4.Size = New System.Drawing.Size(512, 117)
        '
        '
        '
        Me.GroupPanel4.Style.BackColor = System.Drawing.Color.White
        Me.GroupPanel4.Style.BackColor2 = System.Drawing.Color.White
        Me.GroupPanel4.Style.BackColorGradientAngle = 90
        Me.GroupPanel4.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderBottomWidth = 1
        Me.GroupPanel4.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel4.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderLeftWidth = 1
        Me.GroupPanel4.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderRightWidth = 1
        Me.GroupPanel4.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel4.Style.BorderTopWidth = 1
        Me.GroupPanel4.Style.CornerDiameter = 4
        Me.GroupPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel4.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel4.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel4.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.SuperTooltip1.SetSuperTooltip(Me.GroupPanel4, New DevComponents.DotNetBar.SuperTooltipInfo("Pratinjau", "", "Menampilkan beberapa teks dan foto yang disamakan dengan tampilan status yang ter" & _
            "dapat di FB sesuai dengan isi komentar (jika diizinkan), quote, dan tag yang tel" & _
            "ah ditentukan.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(217, 97)))
        Me.GroupPanel4.TabIndex = 10
        '
        'LQuoologi
        '
        Me.LQuoologi.AutoSize = True
        Me.LQuoologi.BackColor = System.Drawing.Color.White
        Me.LQuoologi.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LQuoologi.ForeColor = System.Drawing.Color.Black
        Me.LQuoologi.Location = New System.Drawing.Point(237, 31)
        Me.LQuoologi.Name = "LQuoologi"
        Me.LQuoologi.Size = New System.Drawing.Size(57, 15)
        Me.LQuoologi.TabIndex = 10
        Me.LQuoologi.Text = "Quoologi"
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(193, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 15)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "melalui"
        '
        'GroupPanel5
        '
        Me.GroupPanel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupPanel5.BackColor = System.Drawing.Color.White
        Me.GroupPanel5.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel5.Controls.Add(Me.LPromosikan)
        Me.GroupPanel5.Controls.Add(Me.LKomentar)
        Me.GroupPanel5.Controls.Add(Me.LBagikan)
        Me.GroupPanel5.Controls.Add(Me.LSuka)
        Me.GroupPanel5.Controls.Add(Me.LTitik)
        Me.GroupPanel5.DrawTitleBox = False
        Me.GroupPanel5.Location = New System.Drawing.Point(-2, 84)
        Me.GroupPanel5.Name = "GroupPanel5"
        Me.GroupPanel5.Size = New System.Drawing.Size(510, 28)
        '
        '
        '
        Me.GroupPanel5.Style.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.GroupPanel5.Style.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(251, Byte), Integer), CType(CType(251, Byte), Integer))
        Me.GroupPanel5.Style.BackColorGradientAngle = 90
        Me.GroupPanel5.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderBottomWidth = 1
        Me.GroupPanel5.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel5.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderLeftWidth = 1
        Me.GroupPanel5.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderRightWidth = 1
        Me.GroupPanel5.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel5.Style.BorderTopWidth = 1
        Me.GroupPanel5.Style.CornerDiameter = 4
        Me.GroupPanel5.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel5.Style.CornerTypeBottomLeft = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel5.Style.CornerTypeBottomRight = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel5.Style.CornerTypeTopLeft = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.Style.CornerTypeTopRight = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel5.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel5.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel5.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel5.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel5.TabIndex = 9
        '
        'LPromosikan
        '
        Me.LPromosikan.AutoSize = True
        Me.LPromosikan.BackColor = System.Drawing.Color.White
        Me.LPromosikan.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LPromosikan.ForeColor = System.Drawing.Color.Black
        Me.LPromosikan.Location = New System.Drawing.Point(110, 6)
        Me.LPromosikan.Name = "LPromosikan"
        Me.LPromosikan.Size = New System.Drawing.Size(74, 15)
        Me.LPromosikan.TabIndex = 3
        Me.LPromosikan.Text = "Promosikan"
        '
        'LKomentar
        '
        Me.LKomentar.AutoSize = True
        Me.LKomentar.BackColor = System.Drawing.Color.White
        Me.LKomentar.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LKomentar.ForeColor = System.Drawing.Color.Black
        Me.LKomentar.Location = New System.Drawing.Point(41, 6)
        Me.LKomentar.Name = "LKomentar"
        Me.LKomentar.Size = New System.Drawing.Size(64, 15)
        Me.LKomentar.TabIndex = 2
        Me.LKomentar.Text = "Komentari"
        '
        'LBagikan
        '
        Me.LBagikan.AutoSize = True
        Me.LBagikan.BackColor = System.Drawing.Color.White
        Me.LBagikan.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LBagikan.ForeColor = System.Drawing.Color.Black
        Me.LBagikan.Location = New System.Drawing.Point(189, 6)
        Me.LBagikan.Name = "LBagikan"
        Me.LBagikan.Size = New System.Drawing.Size(52, 15)
        Me.LBagikan.TabIndex = 1
        Me.LBagikan.Text = "Bagikan"
        '
        'LSuka
        '
        Me.LSuka.AutoSize = True
        Me.LSuka.BackColor = System.Drawing.Color.White
        Me.LSuka.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LSuka.ForeColor = System.Drawing.Color.Black
        Me.LSuka.Location = New System.Drawing.Point(1, 6)
        Me.LSuka.Name = "LSuka"
        Me.LSuka.Size = New System.Drawing.Size(35, 15)
        Me.LSuka.TabIndex = 0
        Me.LSuka.Text = "Suka"
        '
        'LTitik
        '
        Me.LTitik.AutoSize = True
        Me.LTitik.BackColor = System.Drawing.Color.White
        Me.LTitik.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.LTitik.ForeColor = System.Drawing.Color.Black
        Me.LTitik.Location = New System.Drawing.Point(2, 6)
        Me.LTitik.Name = "LTitik"
        Me.LTitik.Size = New System.Drawing.Size(193, 15)
        Me.LTitik.TabIndex = 3
        Me.LTitik.Text = "          ·                      ·                         · "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.0!)
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(53, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(144, 15)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "beberapa detik  yang lalu"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.White
        Me.PictureBox1.ForeColor = System.Drawing.Color.Black
        Me.PictureBox1.Location = New System.Drawing.Point(7, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(42, 42)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'LNamaAkun
        '
        Me.LNamaAkun.AutoSize = True
        Me.LNamaAkun.BackColor = System.Drawing.Color.White
        Me.LNamaAkun.Font = New System.Drawing.Font("Arial", 10.5!, System.Drawing.FontStyle.Bold)
        Me.LNamaAkun.ForeColor = System.Drawing.Color.Black
        Me.LNamaAkun.Location = New System.Drawing.Point(53, 10)
        Me.LNamaAkun.Name = "LNamaAkun"
        Me.LNamaAkun.Size = New System.Drawing.Size(204, 16)
        Me.SuperTooltip1.SetSuperTooltip(Me.LNamaAkun, New DevComponents.DotNetBar.SuperTooltipInfo("Nama Pengguna Facebook", "", "Berisi nama pengguna yang sudah login." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Jika tulisan {Nama Pengguna Facebook} i" & _
            "tu artinya tidak ada pengguna yang login pada Facebook di aplikasi ini.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(227, 99)))
        Me.LNamaAkun.TabIndex = 7
        Me.LNamaAkun.Text = "{Nama Pengguna Facebook}"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.White
        Me.PictureBox2.ForeColor = System.Drawing.Color.Black
        Me.PictureBox2.Location = New System.Drawing.Point(298, 32)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(12, 12)
        Me.PictureBox2.TabIndex = 6
        Me.PictureBox2.TabStop = False
        '
        'LStatus
        '
        Me.LStatus.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LStatus.Border.BackColor = System.Drawing.Color.White
        Me.LStatus.Border.BackColor2 = System.Drawing.Color.White
        Me.LStatus.Border.BorderColor = System.Drawing.Color.White
        Me.LStatus.Border.Class = "TextBoxBorder"
        Me.LStatus.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LStatus.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.LStatus.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.LStatus.ForeColor = System.Drawing.Color.Black
        Me.LStatus.Location = New System.Drawing.Point(1, 55)
        Me.LStatus.Multiline = True
        Me.LStatus.Name = "LStatus"
        Me.LStatus.ReadOnly = True
        Me.LStatus.Size = New System.Drawing.Size(504, 22)
        Me.SuperTooltip1.SetSuperTooltip(Me.LStatus, New DevComponents.DotNetBar.SuperTooltipInfo("Status", "", "Berisi status yang sudah ditentukan dari komentar (jika diizinkan), quote, dan as" & _
            "al quote, yang mirip pada tampilan status di facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(203, 84)))
        Me.LStatus.TabIndex = 1
        Me.LStatus.Text = "Status"
        '
        'TBKomentar
        '
        Me.TBKomentar.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.TBKomentar.Border.Class = "TextBoxBorder"
        Me.TBKomentar.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TBKomentar.Font = New System.Drawing.Font("Arial", 10.5!)
        Me.TBKomentar.ForeColor = System.Drawing.Color.Black
        Me.TBKomentar.Location = New System.Drawing.Point(6, 212)
        Me.TBKomentar.Multiline = True
        Me.TBKomentar.Name = "TBKomentar"
        Me.TBKomentar.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBKomentar.Size = New System.Drawing.Size(512, 38)
        Me.SuperTooltip1.SetSuperTooltip(Me.TBKomentar, New DevComponents.DotNetBar.SuperTooltipInfo("Komentar", "", "Tempat mengisi komentar untuk status yang nantinya akan di kirim ke Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(184, 71)))
        Me.TBKomentar.TabIndex = 100
        Me.TBKomentar.Visible = False
        '
        'LPratinjau
        '
        Me.LPratinjau.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LPratinjau.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LPratinjau.ForeColor = System.Drawing.Color.Black
        Me.LPratinjau.Location = New System.Drawing.Point(8, 116)
        Me.LPratinjau.Name = "LPratinjau"
        Me.LPratinjau.Size = New System.Drawing.Size(47, 23)
        Me.LPratinjau.TabIndex = 12
        Me.LPratinjau.Text = "Pratinjau:"
        '
        'LKomen
        '
        Me.LKomen.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LKomen.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LKomen.ForeColor = System.Drawing.Color.Black
        Me.LKomen.Location = New System.Drawing.Point(9, 87)
        Me.LKomen.Name = "LKomen"
        Me.LKomen.Size = New System.Drawing.Size(63, 23)
        Me.LKomen.TabIndex = 13
        Me.LKomen.Text = "Komentar:"
        Me.LKomen.Visible = False
        '
        'BKirim
        '
        Me.BKirim.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BKirim.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BKirim.Location = New System.Drawing.Point(425, 273)
        Me.BKirim.Name = "BKirim"
        Me.BKirim.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F2)
        Me.BKirim.Size = New System.Drawing.Size(93, 23)
        Me.BKirim.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.BKirim.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.CBOtoTutup})
        Me.SuperTooltip1.SetSuperTooltip(Me.BKirim, New DevComponents.DotNetBar.SuperTooltipInfo("Kirim", "Jalan Pintas: F2", "Untuk mengirim status ke Facebook dari beberapa opsi yang telah ditentukan (quote" & _
            ", komentar, tag).", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BKirim.Symbol = ""
        Me.BKirim.SymbolSize = 13.0!
        Me.BKirim.TabIndex = 1
        Me.BKirim.Text = " Kirim | F2"
        '
        'CBOtoTutup
        '
        Me.CBOtoTutup.EnableMarkup = False
        Me.CBOtoTutup.GlobalItem = False
        Me.CBOtoTutup.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.CBOtoTutup.KeyTips = "SAAT OPSI INI AKTIF, KETIKA PENGIRIMAN SELESAI, MAKA JENDELA AKAN TERTUTUP OTOMAT" & _
    "IS."
        Me.CBOtoTutup.Name = "CBOtoTutup"
        Me.SuperTooltip1.SetSuperTooltip(Me.CBOtoTutup, New DevComponents.DotNetBar.SuperTooltipInfo("Otomatis Tutup Jendela", "", "Saat opsi ini aktif, ketika pengiriman selesai, maka jendela akan tertutup otomat" & _
            "is.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.CBOtoTutup.Text = "Otomatis Tutup Jendela"
        '
        'BPengaturan
        '
        Me.BPengaturan.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BPengaturan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BPengaturan.AntiAlias = True
        Me.BPengaturan.Location = New System.Drawing.Point(192, 273)
        Me.BPengaturan.Name = "BPengaturan"
        Me.BPengaturan.Shape = New DevComponents.DotNetBar.RoundRectangleShapeDescriptor(4, 1, 1, 4)
        Me.BPengaturan.Shortcuts.Add(DevComponents.DotNetBar.eShortcut.F3)
        Me.BPengaturan.Size = New System.Drawing.Size(140, 23)
        Me.BPengaturan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BPengaturan, New DevComponents.DotNetBar.SuperTooltipInfo("Ubah Pengaturan", "Jalan Pintas: F3", "Untuk membuka jendela pengaturan jika ingin mengubah pengaturan yang sudah ditent" & _
            "ukan sebelumnya.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(213, 95)))
        Me.BPengaturan.Symbol = ""
        Me.BPengaturan.SymbolSize = 13.0!
        Me.BPengaturan.TabIndex = 101
        Me.BPengaturan.Text = " Ubah Pengaturan | F3"
        '
        'BBatal
        '
        Me.BBatal.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.BBatal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BBatal.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground
        Me.BBatal.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatal.Location = New System.Drawing.Point(6, 273)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(89, 23)
        Me.BBatal.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.SuperTooltip1.SetSuperTooltip(Me.BBatal, New DevComponents.DotNetBar.SuperTooltipInfo("Batal", "Jalan Pintas: Esc", "Untuk membatalkan proses pengiriman status ke Facebook.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.BBatal.Symbol = ""
        Me.BBatal.SymbolSize = 13.0!
        Me.BBatal.TabIndex = 102
        Me.BBatal.Text = " Batal | Esc"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'LIndikator
        '
        Me.LIndikator.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LIndikator.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.LIndikator.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LIndikator.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.LIndikator.Font = New System.Drawing.Font("Candara", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LIndikator.ForeColor = System.Drawing.Color.Black
        Me.LIndikator.Location = New System.Drawing.Point(103, 300)
        Me.LIndikator.Name = "LIndikator"
        Me.LIndikator.Size = New System.Drawing.Size(318, 38)
        Me.LIndikator.TabIndex = 103
        Me.LIndikator.Text = "MENGHUBUNGKAN KE FACEBOOK..."
        '
        'Timer2
        '
        Me.Timer2.Interval = 500
        '
        'Timer3
        '
        Me.Timer3.Interval = 2000
        '
        'Timer4
        '
        '
        'Form_UpdateStatus
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.BBatal
        Me.ClientSize = New System.Drawing.Size(525, 305)
        Me.Controls.Add(Me.LIndikator)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BPengaturan)
        Me.Controls.Add(Me.GroupPanel4)
        Me.Controls.Add(Me.LPratinjau)
        Me.Controls.Add(Me.TBTag)
        Me.Controls.Add(Me.LabelX2)
        Me.Controls.Add(Me.TBQuote)
        Me.Controls.Add(Me.BKirim)
        Me.Controls.Add(Me.LabelX1)
        Me.Controls.Add(Me.TBKomentar)
        Me.Controls.Add(Me.LKomen)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "Form_UpdateStatus"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update Status"
        Me.GroupPanel4.ResumeLayout(False)
        Me.GroupPanel4.PerformLayout()
        Me.GroupPanel5.ResumeLayout(False)
        Me.GroupPanel5.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TBQuote As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TBTag As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupPanel4 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents GroupPanel5 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents LPromosikan As System.Windows.Forms.Label
    Friend WithEvents LKomentar As System.Windows.Forms.Label
    Friend WithEvents LBagikan As System.Windows.Forms.Label
    Friend WithEvents LSuka As System.Windows.Forms.Label
    Friend WithEvents LTitik As System.Windows.Forms.Label
    Public WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Public WithEvents LNamaAkun As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents LStatus As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TBKomentar As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LPratinjau As DevComponents.DotNetBar.LabelX
    Friend WithEvents LKomen As DevComponents.DotNetBar.LabelX
    Public WithEvents LQuoologi As System.Windows.Forms.Label
    Friend WithEvents BKirim As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BPengaturan As DevComponents.DotNetBar.ButtonX
    Friend WithEvents BBatal As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents CBOtoTutup As DevComponents.DotNetBar.CheckBoxItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Public WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LIndikator As DevComponents.DotNetBar.Controls.ReflectionLabel
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents Timer3 As System.Windows.Forms.Timer
    Friend WithEvents Timer4 As System.Windows.Forms.Timer
End Class
