﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updater_main
    Inherits DevComponents.DotNetBar.Metro.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(updater_main))
        Me.LNamaFile = New DevComponents.DotNetBar.LabelX()
        Me.ButtonX2 = New DevComponents.DotNetBar.ButtonX()
        Me.ButtonX1 = New DevComponents.DotNetBar.ButtonX()
        Me.LProses = New DevComponents.DotNetBar.LabelX()
        Me.PB = New DevComponents.DotNetBar.Controls.ProgressBarX()
        Me.SuspendLayout()
        '
        'LNamaFile
        '
        Me.LNamaFile.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LNamaFile.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LNamaFile.ForeColor = System.Drawing.Color.Black
        Me.LNamaFile.Location = New System.Drawing.Point(6, 12)
        Me.LNamaFile.Name = "LNamaFile"
        Me.LNamaFile.Size = New System.Drawing.Size(219, 23)
        Me.LNamaFile.TabIndex = 10
        '
        'ButtonX2
        '
        Me.ButtonX2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX2.AntiAlias = True
        Me.ButtonX2.Location = New System.Drawing.Point(438, 68)
        Me.ButtonX2.Name = "ButtonX2"
        Me.ButtonX2.Size = New System.Drawing.Size(75, 23)
        Me.ButtonX2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX2.Symbol = ""
        Me.ButtonX2.SymbolSize = 13.0!
        Me.ButtonX2.TabIndex = 9
        Me.ButtonX2.Text = "Update"
        '
        'ButtonX1
        '
        Me.ButtonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.ButtonX1.AntiAlias = True
        Me.ButtonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.MagentaWithBackground
        Me.ButtonX1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.ButtonX1.Location = New System.Drawing.Point(356, 68)
        Me.ButtonX1.Name = "ButtonX1"
        Me.ButtonX1.Size = New System.Drawing.Size(75, 23)
        Me.ButtonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ButtonX1.Symbol = ""
        Me.ButtonX1.SymbolSize = 15.0!
        Me.ButtonX1.TabIndex = 8
        Me.ButtonX1.Text = "Tutup"
        '
        'LProses
        '
        Me.LProses.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.LProses.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LProses.ForeColor = System.Drawing.Color.Black
        Me.LProses.Location = New System.Drawing.Point(7, 68)
        Me.LProses.Name = "LProses"
        Me.LProses.Size = New System.Drawing.Size(219, 23)
        Me.LProses.TabIndex = 7
        '
        'PB
        '
        Me.PB.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        '
        '
        '
        Me.PB.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.PB.ForeColor = System.Drawing.Color.Black
        Me.PB.Location = New System.Drawing.Point(4, 40)
        Me.PB.Name = "PB"
        Me.PB.Size = New System.Drawing.Size(508, 23)
        Me.PB.Style = DevComponents.DotNetBar.eDotNetBarStyle.Metro
        Me.PB.TabIndex = 6
        '
        'updater_main
        '
        Me.AcceptButton = Me.ButtonX2
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.ButtonX1
        Me.ClientSize = New System.Drawing.Size(516, 95)
        Me.Controls.Add(Me.LNamaFile)
        Me.Controls.Add(Me.ButtonX2)
        Me.Controls.Add(Me.ButtonX1)
        Me.Controls.Add(Me.LProses)
        Me.Controls.Add(Me.PB)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "updater_main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Quoologi Updater (Quotes)"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LNamaFile As DevComponents.DotNetBar.LabelX
    Friend WithEvents ButtonX2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ButtonX1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LProses As DevComponents.DotNetBar.LabelX
    Friend WithEvents PB As DevComponents.DotNetBar.Controls.ProgressBarX

End Class
