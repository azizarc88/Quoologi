﻿Imports DevComponents.DotNetBar
Imports System.Net
Imports System.IO
Imports Microsoft.Win32

Public Class updater_main
    Inherits Metro.MetroForm
    Private WithEvents Downloader As WebFileDownloader
    Dim sem_upd_a As List(Of String) = New List(Of String)
    Dim link As List(Of String) = New List(Of String)
    Dim nama As List(Of String) = New List(Of String)
    Dim temppath As String = My.Computer.FileSystem.SpecialDirectories.Temp.ToString
    Dim banyak As Byte
    Dim terunduh As List(Of String) = New List(Of String)
    Dim verbaru As String
    Public docdir As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Quoologi\Quotes\"

    Public Shared Sub Main()
        Application.DoEvents()
    End Sub

    Sub done()
        Dim a As Byte = 0
        For Each Data As String In terunduh
            If FileIO.FileSystem.FileExists(docdir + nama.Item(a)) Then
                Try
                    FileIO.FileSystem.DeleteFile(docdir + nama.Item(a), FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                Catch ex As Exception
                    MsgBox("Silakan tutup Quoologi terlebih dahulu, jika sudah, tekan OK", MsgBoxStyle.Exclamation + MsgBoxStyle.OkOnly, "Pembaruan Quoologi")
                    FileIO.FileSystem.DeleteFile(docdir + nama.Item(a), FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
                End Try
            End If
            FileIO.FileSystem.MoveFile(temppath + "\" + Data, docdir + nama.Item(a))
            a += 1
        Next
        MsgBox("Update selesai, silakan buka pengaturan untuk melihat Quotes terbaru", MsgBoxStyle.Information, "Update Selesai")
        FileIO.FileSystem.DeleteFile(temppath & "\daftar_file.txt", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        FileIO.FileSystem.DeleteFile(temppath & "\quotes_v.txt", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        Me.Close()
    End Sub

    Sub baca_file()
        Try
            Dim objReader As New System.IO.StreamReader(temppath & "\daftar_file.txt")
            Do While Not objReader.EndOfStream
                sem_upd_a.Add(objReader.ReadLine())
            Loop
            objReader.Close()


            For Each baris As String In sem_upd_a
                If baris.Contains("link: #") Then
                    link.Add(baris.Replace("link: #", ""))
                ElseIf baris.Contains("quotes: #") Then
                    nama.Add(baris.Replace("quotes: #", ""))
                End If
            Next

            banyak = link.Count
        Catch ex As Exception
            MsgBox("Supaya tidak terjadi kesalahan, Silakan update program melalui pengaturan di Quoologi", MsgBoxStyle.Exclamation, "Pembaruan Quoologi")
            Me.Close()
        End Try

    End Sub

    Private Sub updater_main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dispose()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        baca_file()
        Show()
    End Sub

    Private Sub _Downloader_FileDownloadSizeObtained(ByVal iFileSize As Long) Handles Downloader.FileDownloadSizeObtained
        PB.Value = 0
        PB.Maximum = Convert.ToInt32(iFileSize)
    End Sub

    Private Sub _Downloader_FileDownloadComplete() Handles Downloader.FileDownloadComplete
        PB.Value = PB.Maximum
    End Sub

    Private Sub _Downloader_FileDownloadFailed(ByVal ex As System.Exception) Handles Downloader.FileDownloadFailed
        MessageBox.Show("Galat ketika sedang mendownload update, " & ex.Message, "Pembaruan Quoologi")
        Me.Close()
    End Sub

    Private Sub _Downloader_AmountDownloadedChanged(ByVal iNewProgress As Long) Handles Downloader.AmountDownloadedChanged
        PB.Value = Convert.ToInt32(iNewProgress)
        LProses.Text = WebFileDownloader.FormatFileSize(iNewProgress) & " dari " & WebFileDownloader.FormatFileSize(PB.Maximum) & " terunduh"
        Application.DoEvents()
    End Sub

    Private Sub ButtonX1_Click(sender As Object, e As EventArgs) Handles ButtonX1.Click
        Me.Close()
    End Sub

    Private Function GetFileNameFromURL(ByVal URL As String) As String
        If URL.IndexOf("/"c) = -1 Then Return String.Empty

        Return "\" & URL.Substring(URL.LastIndexOf("/"c) + 1)
    End Function

    Private Sub ButtonX2_Click(sender As Object, e As EventArgs) Handles ButtonX2.Click
        ButtonX2.Enabled = False
        Dim h As String

        For a As Byte = 0 To banyak - 1
            h = a + 1
            LNamaFile.Text = "Berkas: " + nama.Item(a).ToString + " - (" + h + "/" + banyak.ToString + ")"
            Try
                Downloader = New WebFileDownloader
                Downloader.DownloadFileWithProgress(link.Item(a).ToString, temppath + GetFileNameFromURL(link.Item(a).ToString))
                terunduh.Add(link.Item(a).ToString.Substring(link.Item(a).ToString.LastIndexOf("/"c) + 1))
            Catch ex As Exception
                MsgBox("Update gagal!" + Chr(13) + "Silakan cek terlebih dahulu apakah Anda terhubung dengan internet dan Quoologi_updater sudah diijinkan di firewall atau tidak", MsgBoxStyle.Critical, "Galat")
                Exit Sub
                Exit For
            End Try
        Next
        done()

    End Sub
End Class
